<?php
/**
 * Theme, template, and stylesheet functions.
 *
 * @package TTM
 * @subpackage Theme
 */

/**
 * Returns an array of TTM_Theme objects based on the arguments.
 *
 * Despite advances over get_themes(), this function is quite expensive, and grows
 * linearly with additional themes. Stick to ttm_get_theme() if possible.
 *
 * @since 3.4.0
 *
 * @param array $args The search arguments. Optional.
 * - errors      mixed  True to return themes with errors, false to return themes without errors, null
 *                      to return all themes. Defaults to false.
 * - allowed     mixed  (Multisite) True to return only allowed themes for a site. False to return only
 *                      disallowed themes for a site. 'site' to return only site-allowed themes. 'network'
 *                      to return only network-allowed themes. Null to return all themes. Defaults to null.
 * - blog_id     int    (Multisite) The blog ID used to calculate which themes are allowed. Defaults to 0,
 *                      synonymous for the current blog.
 * @return Array of TTM_Theme objects.
 */
 
 function get_template_directory_uri() {
	return get_template_directory();
 } 
 
 function get_template_directory() {
	$theme_root_dir = get_site_url() . '/ttm-content/themes/'. strtolower(THEME);
	return $theme_root_dir;
 }
 
 function get_site_url() {
	return get_option('siteurl');
 }
 
 ?>