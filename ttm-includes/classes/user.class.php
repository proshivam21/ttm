<?php

class user {
	
	function __construct() {
		require_ttm_db();
	}
	/**
	 * Count number of users who have each of the user roles.
	 *
	 * Assumes there are neither duplicated nor orphaned capabilities meta_values.
	 * Assumes role names are unique phrases. Same assumption made by WP_User_Query::prepare_query()
	 * Using $strategy = 'time' this is CPU-intensive and should handle around 10^7 users.
	 * Using $strategy = 'memory' this is memory-intensive and should handle around 10^5 users, but see WP Bug #12257.
	 *
	 * @since 3.0.0
	 * @param string $strategy 'time' or 'memory'
	 * @return array Includes a grand total and an array of counts indexed by role strings.
	 */
}