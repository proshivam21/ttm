<?php
/**
* This is the file that includes the class information.
* 
*/

function __autoload($classname) {
	if(file_exists($class = ABSPATH . TTMINC .'/classes/' . $classname . '.class.php')) {
		include_once $class;
	}
}