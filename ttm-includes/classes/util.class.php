<?php

class util {
	
	function print_error($mesg, $status = false) {
		if(false==$status)
			echo $mesg;
	}
	
	function get_mail_template($template_code, $data) {
		$mail_template  = get_option("$template_code");
		if(is_array($data)) {
			foreach ($data as $key => $value) {
				$mail_template = str_replace($key, $value, $mail_template);
			}
		} else{
			
		} 
		return $mail_template;			
	}
	
	/*
	 * Extract the post value in the variable
	 */
	public function extract_post($post,$debug=0) {
		foreach($post as $key => $value) {
			global $$key;
			$$key = $value;
			if($debug==1) {
				echo '$'.$key.'='.$value.',<br />';
			}
				
		}
	}
	
	/**
	 * debugArray function use the print_r() function
	 * and show html tag <pre></pre> to show correctly in your screen
	 *
	 * @param Array $array
	 */
	public function debug_array($array){
		echo '<style>.debug-array{font-size:12px;color:#01FF00;position:fixed;bottom:10px;left:10px;background:rgba(0,0,0,0.8);padding:0 20px 0 20px;overflow:scroll;max-height:300px;max-width:600px;text-shadow:0 0 1px #333;}
					 .debugArray div{font-size:15px;text-decoration:blink;float:left;position:absolute;bottom:10px;;left:30px;}</style> ';
		echo ' <div class="debug-array" id="draggable" onclick="this.style.display=\'none\';"><pre><code>';
		var_dump($array);
		echo '<div>|</div>';
		echo "</code></pre></div>";
	}
	
	/**
	 * get_current_page_url function is used to fatch the current page URL
	 * @param string URL
	 */
	function get_current_page_url() {

		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}

		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}

		return $pageURL;
	}
}