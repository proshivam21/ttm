<?php
/**
 * General template tags that can go anywhere in a template.
 *
 * @package TTM
 * @subpackage Template
 */

/**
 * Load header template.
 *
 * Includes the header template for a theme or if a name is specified then a
 * specialised header will be included.
 *
 * For the parameter, if the file is called "header-special.php" then specify
 * "special".
 *
 * @since 1.5.0
 *
 * @uses locate_template()
 *
 * @param string $name The name of the specialised header.
 */
function get_header( $name = null ) {
	include TTM_CONTENT_DIR.'/themes/'.strtolower(THEME).'/header.php';
}


/**
 * Load footer template.
 *
 * Includes the footer template for a theme or if a name is specified then a
 * specialised footer will be included.
 *
 * For the parameter, if the file is called "footer-special.php" then specify
 * "special".
 *
 * @since 1.5.0
 *
 * @uses locate_template()
 *
 * @param string $name The name of the specialised footer.
 */
function get_footer( $name = null ) {
	include TTM_CONTENT_DIR.'/themes/'.strtolower(THEME).'/footer.php';
}

/**
 * Load sidebar template.
 *
 * Includes the sidebar template for a theme or if a name is specified then a
 * specialised sidebar will be included.
 *
 * For the parameter, if the file is called "sidebar-special.php" then specify
 * "special".
 *
 * @since 1.5.0
 *
 * @uses locate_template()
 *
 * @param string $name The name of the specialised sidebar.
 */
function get_sidebar( $name = null ) {
	/**
	 * Fires before the sidebar template file is loaded.
	 *
	 * The hook allows a specific sidebar template file to be used in place of the
	 * default sidebar template file. If your file is called sidebar-new.php,
	 * you would specify the filename in the hook as get_sidebar( 'new' ).
	 *
	 * @since 2.2.0
	 * @since 2.8.0 $name parameter added.
	 *
	 * @param string $name Name of the specific sidebar file to use.
	 */
	do_action( 'get_sidebar', $name );

	$templates = array();
	$name = (string) $name;
	if ( '' !== $name )
		$templates[] = "sidebar-{$name}.php";

	$templates[] = 'sidebar.php';

	// Backward compat code will be removed in a future release
	include TTM_CONTENT_DIR.'/themes/'.strtolower(THEME).'/sidebar.php';
	
	/* if ('' == locate_template($templates, true))
		load_template( ABSPATH . TTMINC . '/theme-compat/sidebar.php'); */
}

/**
 * Returns the Lost Password URL.
 *
 * Returns the URL that allows the user to retrieve the lost password
 *
 * @since 2.8.0
 *
 * @uses site_url() To generate the lost password URL
 *
 * @param string $redirect Path to redirect to on login.
 * @return string Lost password URL.
 */
 
function ttm_forgotpass_url($redirect = '') {
	$args = array('action' => 'forgotpassword');
	
	if(!empty($redirect)) {
		$args['redirect_to'] = $redirect;
	}
	
	$url = get_site_url();
	$url .= '/login';
	$url .= '?';
	foreach($args as $key => $value) {
		$url .= "$key=$value&";
	}
	
	$url = substr($url, 0, -1);
	return $url;
}


function ttm_forgotpassword_url($redirect = '') {
	$args = array('action' => 'forgotpassword');
	
	if(!empty($redirect)) {
		$args['redirect_to'] = $redirect;
	}
	
	$url = get_site_url();
	$url .= '/ttm-login.php';
	$url .= '?';
	foreach($args as $key => $value) {
		$url .= "$key=$value&";
	}
	
	$url = substr($url, 0, -1);
	return $url;
}
 
/**
 * Returns the Log Out URL.
 *
 * Returns the URL that allows the user to log out of the site.
 *
 * @since 2.7.0
 *
 * @param string $redirect Path to redirect to on logout.
 * @return string A log out URL.
 */
function ttm_logout_url($redirect = '') {
	$args = array( 'action' => 'logout' );
	if ( !empty($redirect) ) {
		$args['redirect_to'] = urlencode( $redirect );
	}

	$logout_url = add_query_arg($args, get_site_url().'/login'/* site_url('login', 'login') */);
	$logout_url = ttm_nonce_url( $logout_url, 'log-out' );

	/**
	 * Filter the logout URL.
	 *
	 * @since 2.8.0
	 *
	 * @param string $logout_url The Log Out URL.
	 * @param string $redirect   Path to redirect to on logout.
	 */
	return apply_filters( 'logout_url', $logout_url, $redirect );
}
function paginate_links( $args = '' ) {
    global $ttm_query, $ttm_rewrite;
 
    // Setting up default values based on the current URL.
    $pagenum_link = html_entity_decode( 'http://www.iqs21.in/cm/search-results' );
    $url_parts    = explode( '?', $pagenum_link );
 
    // Get max pages and current page out of the current query, if available.
    $total   = isset( $ttm_query->max_num_pages ) ? $ttm_query->max_num_pages : 1;
    $current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
 
    // Append the format placeholder to the base URL.
    $pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';
 
    // URL base depends on permalink settings.
    $format  = $ttm_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $ttm_rewrite->using_permalinks() ? user_trailingslashit( $ttm_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
 
    $defaults = array(
        'base' => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
        'format' => $format, // ?page=%#% : %#% is replaced by the page number
        'total' => $total,
        'current' => $current,
        'show_all' => false,
        'prev_next' => true,
        'prev_text' => __('&laquo; Previous'),
        'next_text' => __('Next &raquo;'),
        'end_size' => 1,
        'mid_size' => 2,
        'type' => 'plain',
        'add_args' => array(), // array of query args to add
        'add_fragment' => '',
        'before_page_number' => '',
        'after_page_number' => ''
    );
 
    $args = ttm_parse_args( $args, $defaults );
 
    if ( ! is_array( $args['add_args'] ) ) {
        $args['add_args'] = array();
    }
 
    // Merge additional query vars found in the original URL into 'add_args' array.
    if ( isset( $url_parts[1] ) ) {
        // Find the format argument.
        $format = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
        $format_query = isset( $format[1] ) ? $format[1] : '';
        ttm_parse_str( $format_query, $format_args );
 
        // Find the query args of the requested URL.
        ttm_parse_str( $url_parts[1], $url_query_args );
 
        // Remove the format argument from the array of query arguments, to avoid overwriting custom format.
        foreach ( $format_args as $format_arg => $format_arg_value ) {
            unset( $url_query_args[ $format_arg ] );
        }
 
        $args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
    }
 
    // Who knows what else people pass in $args
    $total = (int) $args['total'];
    if ( $total < 2 ) {
        return;
    }
    $current  = (int) $args['current'];
    $end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
    if ( $end_size < 1 ) {
        $end_size = 1;
    }
    $mid_size = (int) $args['mid_size'];
    if ( $mid_size < 0 ) {
        $mid_size = 2;
    }
    $add_args = $args['add_args'];
    $r = '';
    $page_links = array();
    $dots = false;
 
    if ( $args['prev_next'] && $current && 1 < $current ) :
        $link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current - 1, $link );
        if ( $add_args )
            $link = add_query_arg( $add_args, $link );
        $link .= $args['add_fragment'];
 
        /**
         * Filters the paginated links for the given archive pages.
         *
         * @since 3.0.0
         *
         * @param string $link The paginated link URL.
         */
        $page_links[] = '<a class="prev page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a>';
    endif;
    for ( $n = 1; $n <= $total; $n++ ) :
        if ( $n == $current ) :
            $page_links[] = "<span class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
            $dots = true;
        else :
            if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
                $link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
                $link = str_replace( '%#%', $n, $link );
                if ( $add_args )
                    $link = add_query_arg( $add_args, $link );
                $link .= $args['add_fragment'];
 
                /** This filter is documented in ttm-includes/general-template.php */
                $page_links[] = "<a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
                $dots = true;
            elseif ( $dots && ! $args['show_all'] ) :
                $page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
                $dots = false;
            endif;
        endif;
    endfor;
    if ( $args['prev_next'] && $current && ( $current < $total || -1 == $total ) ) :
        $link = str_replace( '%_%', $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current + 1, $link );
        if ( $add_args )
            $link = add_query_arg( $add_args, $link );
        $link .= $args['add_fragment'];
 
        /** This filter is documented in ttm-includes/general-template.php */
        $page_links[] = '<a class="next page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['next_text'] . '</a>';
    endif;
    switch ( $args['type'] ) {
        case 'array' :
            return $page_links;
 
        case 'list' :
            $r .= "<ul class='page-numbers'>\n\t<li>";
            $r .= join("</li>\n\t<li>", $page_links);
            $r .= "</li>\n</ul>\n";
            break;
 
        default :
            $r = join("\n", $page_links);
            break;
    }
    return $r;
}
function get_pagenum_link($pagenum = 1, $escape = true ) {
    global $ttm_rewrite;
 
    $pagenum = (int) $pagenum;
 
    $request = remove_query_arg( 'paged' );
 
    $home_root = parse_url(get_site_url());
    $home_root = ( isset($home_root['path']) ) ? $home_root['path'] : '';
    $home_root = preg_quote( $home_root, '|' );
 
    $request = preg_replace('|^'. $home_root . '|i', '', $request);
    $request = preg_replace('|^/+|', '', $request);
 
    if ( !$ttm_rewrite->using_permalinks() || is_admin() ) {
        $base = trailingslashit( get_bloginfo( 'url' ) );
 
        if ( $pagenum > 1 ) {
            $result = add_query_arg( 'paged', $pagenum, $base . $request );
        } else {
            $result = $base . $request;
        }
    } else {
        $qs_regex = '|\?.*?$|';
        preg_match( $qs_regex, $request, $qs_match );
 
        if ( !empty( $qs_match[0] ) ) {
            $query_string = $qs_match[0];
            $request = preg_replace( $qs_regex, '', $request );
        } else {
            $query_string = '';
        }
 
        $request = preg_replace( "|$ttm_rewrite->pagination_base/\d+/?$|", '', $request);
        $request = preg_replace( '|^' . preg_quote( $ttm_rewrite->index, '|' ) . '|i', '', $request);
        $request = ltrim($request, '/');
 
        $base = trailingslashit( get_bloginfo( 'url' ) );
 
        if ( $ttm_rewrite->using_index_permalinks() && ( $pagenum > 1 || '' != $request ) )
            $base .= $ttm_rewrite->index . '/';
 
        if ( $pagenum > 1 ) {
            $request = ( ( !empty( $request ) ) ? trailingslashit( $request ) : $request ) . user_trailingslashit( $ttm_rewrite->pagination_base . "/" . $pagenum, 'paged' );
        }
 
        $result = $base . $request . $query_string;
    }
 
    /**
     * Filters the page number link for the current request.
     *
     * @since 2.5.0
     *
     * @param string $result The page number link.
     */
    $result = apply_filters( 'get_pagenum_link', $result );
 
    if ( $escape )
        return esc_url( $result );
    else
        return esc_url_raw( $result );
}
include('query.php');