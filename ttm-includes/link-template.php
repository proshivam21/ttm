<?php 
function get_pagenum_link($pagenum = 1, $escape = true ) {
    global $ttm_rewrite;
 
    $pagenum = (int) $pagenum;
 
    $request = remove_query_arg( 'paged' );
 
    $home_root = parse_url(home_url());
    $home_root = ( isset($home_root['path']) ) ? $home_root['path'] : '';
    $home_root = preg_quote( $home_root, '|' );
 
    $request = preg_replace('|^'. $home_root . '|i', '', $request);
    $request = preg_replace('|^/+|', '', $request);
 
    if ( !$ttm_rewrite->using_permalinks() || is_admin() ) {
        $base = trailingslashit( get_bloginfo( 'url' ) );
 
        if ( $pagenum > 1 ) {
            $result = add_query_arg( 'paged', $pagenum, $base . $request );
        } else {
            $result = $base . $request;
        }
    } else {
        $qs_regex = '|\?.*?$|';
        preg_match( $qs_regex, $request, $qs_match );
 
        if ( !empty( $qs_match[0] ) ) {
            $query_string = $qs_match[0];
            $request = preg_replace( $qs_regex, '', $request );
        } else {
            $query_string = '';
        }
 
        $request = preg_replace( "|$ttm_rewrite->pagination_base/\d+/?$|", '', $request);
        $request = preg_replace( '|^' . preg_quote( $ttm_rewrite->index, '|' ) . '|i', '', $request);
        $request = ltrim($request, '/');
 
        $base = trailingslashit( get_bloginfo( 'url' ) );
 
        if ( $ttm_rewrite->using_index_permalinks() && ( $pagenum > 1 || '' != $request ) )
            $base .= $ttm_rewrite->index . '/';
 
        if ( $pagenum > 1 ) {
            $request = ( ( !empty( $request ) ) ? trailingslashit( $request ) : $request ) . user_trailingslashit( $ttm_rewrite->pagination_base . "/" . $pagenum, 'paged' );
        }
 
        $result = $base . $request . $query_string;
    }
 
    /**
     * Filters the page number link for the current request.
     *
     * @since 2.5.0
     *
     * @param string $result The page number link.
     */
    $result = apply_filters( 'get_pagenum_link', $result );
 
    if ( $escape )
        return esc_url( $result );
    else
        return esc_url_raw( $result );
}
?>