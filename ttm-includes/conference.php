<?php

function add_conference_meta($conference_id, $meta_key, $meta_value, $unique = false) {
	return add_metadata('conference', $conference_id, $meta_key, $meta_value, $unique);
}
?>