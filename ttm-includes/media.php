<?php
/**
 * FP API for media display.
 *
 * @package FP
 * @subpackage Media
 */

/**
 * Scale down the default size of an image.
 *
 * This is so that the image is a better fit for the editor and theme.
 *
 * The $size parameter accepts either an array or a string. The supported string
 * values are 'thumb' or 'thumbnail' for the given thumbnail size or defaults at
 * 128 width and 96 height in pixels. Also supported for the string value is
 * 'medium' and 'full'. The 'full' isn't actually supported, but any value other
 * than the supported will result in the content_width size or 500 if that is
 * not set.
 *
 * Finally, there is a filter named 'editor_max_image_size', that will be called
 * on the calculated array for width and height, respectively. The second
 * parameter will be the value that was in the $size parameter. The returned
 * type for the hook is an array with the width as the first element and the
 * height as the second element.
 *
 * @since 2.5.0
 * @uses ttm_constrain_dimensions() This function passes the widths and the heights.
 *
 * @param int $width Width of the image
 * @param int $height Height of the image
 * @param string|array $size Size of what the result image should be.
 * @param context Could be 'display' (like in a theme) or 'edit' (like inserting into an editor)
 * @return array Width and height of what the result image should resize to.
 */
 
 /**
 * Add meta data field to a user.
 *
 * Post meta data is called "Custom Fields" on the Administration Screens.
 *
 * @since 1.0.0
 * @uses add_metadata()
 * @link http://codex.iquincesoft.com/Function_Reference/add_user_meta
 *
 * @param int $media_id Media ID.
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Metadata value.
 * @param bool $unique Optional, default is false. Whether the same key should not be added.
 * @return int|bool Meta ID on success, false on failure.
 */
 
function add_media_meta($media_id, $meta_key, $meta_value, $unique = false) {
	return add_metadata('media', $media_id, $meta_key, $meta_value, $unique);
}


/**
 * Retrieve media meta field for a user.
 *
 * @since 3.0.0
 * @uses get_metadata()
 * @link http://codex.iquincesoft.com/Function_Reference/get_user_meta
 *
 * @param int $media_id Media ID.
 * @param string $key Optional. The meta key to retrieve. By default, returns data for all keys.
 * @param bool $single Whether to return a single value.
 * @return mixed Will be an array if $single is false. Will be value of meta data field if $single
 *  is true.
 */
function get_media_meta($media_id, $key = '', $single = false) {
	return get_metadata('media', $media_id, $key, $single);
}

function get_media_metas($media_id) {
	global $db;
	$data = $db->get_row("select * from {$db->mediameta} where media_id = {$media_id} and meta_key = 'ttm_mediameta'");
	
	return $data->meta_value;
	//return get_metadata('media', $media_id, $key, $single);
}

/**
 * Image resize
 * @param int $width
 * @param int $height
 */
function resize($width, $height, $dirPath, $file_name, $fileTmpName, $fileType ){
	 ini_set("display_errors", "1");
	error_reporting(E_ALL);
		/* Get original image x y*/
		list($w, $h) = getimagesize($fileTmpName);
		/* calculate new image size with ratio */
		$ratio = max($width/$w, $height/$h);
		$h = ceil($height / $ratio);
		$x = ($w - $width / $ratio) / 2;
		$w = ceil($width / $ratio);
			
		$file_path = $dirPath.$file_name;  
		
		$without_extension = pathinfo($file_name, PATHINFO_FILENAME);
		$ext = pathinfo($file_path, PATHINFO_EXTENSION);

		$path = $dirPath.$without_extension .'-'.$width.'x'.$height.'.'.$ext ;
		
		/* Code for check file is exist in upload folder if Yes, it will rename the file name **/
		$increment = 0;
		$file_name = $without_extension.'.'.$ext;
			
		while(is_file($path)) {
				$increment++;
				$path = $dirPath.$without_extension .$increment.'-'.$width.'x'.$height.'.'.$ext ;
				//return $uploadPath;
		}
		
		/* read binary data from image file */
		$imgString = file_get_contents($fileTmpName);
		/* create image from string */
		$image = imagecreatefromstring($imgString);
		$tmp = imagecreatetruecolor($width, $height);
		imagecopyresampled($tmp, $image,
		0, 0,
		$x, 0,
		$width, $height,
		$w, $h);
		/* Save image */
		switch ($fileType) {
				case 'image/jpeg':
				case 'image/jpg':
					imagejpeg($tmp, $path, 100);
					break;
				case 'image/png':
					imagepng($tmp, $path, 0);
					break;
				case 'image/gif':
					imagegif($tmp, $path);
					break;
				default:
					exit;
					break;
		}
		return $path;
		/* cleanup memory */
		imagedestroy($image);
		imagedestroy($tmp);
}


function upload_media($image_field_name, $status = 1, $uploaded_by = 0) {

$max_file_size = 600; // 600kb
$valid_exts = array('jpeg', 'jpg', 'png', 'gif', 'mp4', 'mp3', 'avi', '3gp', 'mov');
// thumbnail sizes
$sizes = array('small' => array(50, 50), 'thumbnail' => array(150, 150), 'medium' => array(300, 300), 'large' => array(640, 640));

global $db;
	if(is_array($image_field_name)) {
	 $file_ary = reArrayFiles($image_field_name);
	 $media_id1 = array();
	  $media_id2 = array();
		foreach ($file_ary as $file) {
	 
			$ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
			
			if( $ext == 'pdf' || $ext == 'doc' || $ext == 'docx' || $ext == 'zip' || $ext == 'xls' || $ext == 'mp4' || $ext == 'mp3' || $ext == 'avi' || $ext == '3gp'|| $ext == 'mov') {
				$media_id1[]  = is_file_doc($ext, $file['name'], $file['type'], $file['size'], $file['tmp_name'], $status, $uploaded_by);
				
			}
			
			else if (in_array($ext, $valid_exts)) {
				$media_id2[] = is_fileImage($ext, $valid_exts, $sizes, str_replace(' ', '', $file['name']), $file['tmp_name'], $file['size'], $file['type'], $status, $uploaded_by);
			} 
		} 
			return array_merge($media_id1, $media_id2) ;
	}

	else if (isset($_FILES[$image_field_name])) {

		$fileSize = $_FILES[$image_field_name]["size"]/1024;
		
	//	if($fileSize < $max_file_size ){
		
			$file_name = str_replace(' ', '', $_FILES[$image_field_name]['name']);
			
			$fileType = $_FILES[$image_field_name]["type"]; 
			$fileTmpName = $_FILES[$image_field_name]["tmp_name"]; 
			
			$ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
			
			if( $ext == 'pdf' || $ext == 'doc' || $ext == 'docx' || $ext == 'zip' || $ext == 'xls' || $ext == 'mp4' || $ext == 'mp3' || $ext == 'avi' || $ext == '3gp'|| $ext == 'mov') {
				return $media_id  = is_file_doc($ext, $file_name, $fileType, $fileSize, $fileTmpName, $status, $uploaded_by);
			}
			
			else if (in_array($ext, $valid_exts)) {
			
				return $media_id = is_fileImage($ext, $valid_exts, $sizes, $file_name, $fileTmpName, $fileSize, $fileType, $status, $uploaded_by);
			}
	}
} 

function ttm_get_attachment_image($media_id, $size, $attr = array()){
	global $db;
	if(empty($media_id)) {
		return '<i class="fa fa-picture-o fa-1x"></i>';
	}
	$url = get_site_url();
	//$mediameta_info = get_media_meta($media_id, 'ttm_mediameta', true);
	$mediameta_info =  get_media_metas($media_id);
	
	$data = unserialize($mediameta_info);
	
	$img_sizes = array('small', 'thumbnail', 'medium', 'large');

	$i=0;
	$attrs = '';
	foreach($attr as $key => $value) {
		$attrs .= "$key=\"$value\" ";
	}
	
	if($size == 'full') {
		return '<img '.$attrs.' src = "'.$url.$data[file].'" >' ; 
	} elseif('custom' == $size) {
		$media = $db->get_row($db->prepare("SELECT `guid` FROM {$db->media} WHERE `ID`=%d", $media_id));
		return '<img '.$attrs.' src = "'.$url.$media->guid.'" >' ; 
	} else {
		foreach($img_sizes as $img_size ) {
		if ($img_size == $size)
		return '<img '.$attrs.' src = "'.$url.$data['sizes'][$size]['guid'].'" >' ; 
		}
	}	
} 
/*
** get image url by image id
** 0 => unapproved, 2 => Rejected
*/
function ttm_get_attachment_image_url($media_id, $size, $attr = array()){
	global $db;
	
	$url = get_site_url();
	$mediameta_info = get_media_metas($media_id);

	$data = @unserialize($mediameta_info);

	$img_sizes = array('small', 'thumbnail', 'medium', 'large');

	$i=0;
	
	foreach($attr as $key => $value) {
		$attrs .= "$key=\"$value\" ";
	}
	
	if($size == 'full') {
		return $url.$data[file];
	} elseif('custom' == $size) {
		$media = $db->get_row($db->prepare("SELECT `guid` FROM {$db->media} WHERE `ID`=%d", $media_id));
		return $url.$media->guid; 
	} else {
		foreach($img_sizes as $img_size ) {
		if ($img_sizes[$i] == $size)
		return $url.$data[sizes][$size][guid];
		else $i++; 
		}
	}	
}

/*
** get media status(approved or not)
*/

function get_media_status($media_id){
	global $db;
	$media = $db->get_row("select approval_status from {$db->media} where ID = {$media_id}");

	return $media->approval_status;
}

/*
** get media status text
*/

function get_media_status_text($media_id){
	$media_status = get_media_status($media_id);
	if($media_status == 0){
		$status = 'Unapproved';
	}
	elseif($media_status == 1){
		$status = 'Approved';
	}
	elseif($media_status == 2){
		$status = 'Rejected';
	}
	return $status;
}

/*
** Update media status
*/

function update_media_status($media_id, $status){
	global $db;
	if($db->update($db->media, array('approval_status' => $status), array('ID' => $media_id))){
		return true;
	}
	else{
		return false;
	}
	
}

/*
** get image in provided size
*/

function get_image_in_size($media_id, $size){
	global $db;
	$i=0;
	$url = get_site_url();
	$mediameta_info = get_media_meta($media_id, 'ttm_mediameta', true);
	$data = unserialize($mediameta_info);
	$img_sizes = array('small', 'thumbnail', 'medium', 'large');
	foreach($img_sizes as $img_size ) {
		if ($img_sizes[$i] == $size)
		return $url.$data[sizes][$size][guid];
		else $i++; 
	}
}
/*
** get unapproved users with images
*/

function get_unapproved_images(){
	global $db;
	$medias = array();
	$response = $db->get_results("select uploaded_by, ID from {$db->media} where approval_status = 0");
	if(count($response)){
		foreach($response as $media){
			$medias[$media->uploaded_by][] =  $media->ID;
		}
	}
	return $medias;
}

/*
** get_unapproved_images_by_uid
*/

function get_images_by_uid($user_id){
	$img_ids = array();
	$data = array();
	$profile_pic = get_user_meta($user_id, 'profile_pic', true);
	$album = get_user_meta($user_id, 'album', true);
	$data[] = $profile_pic;
	if(is_array($album)){
		foreach($album as $img_id){
			$data[] = $img_id;
		}
	}
	return $data;
}

/*
** get unapproved images by user id
*/

function get_unapproved_images_by_uid($user_id){
	$response = get_unapproved_images();
	return $response[$user_id];
}

function ttm_get_attachment_url($media_id) {
		global $db;
		$media_info = $db->get_row($db->prepare("SELECT * FROM {$db->media} WHERE `ID`=%d ", $media_id));
		$site_url = get_site_url();
		$file_path = $site_url.$media_info->guid;
		//echo $file_path;
return $file_path;
		//echo '<a href="'.$file_path.'">'.$file_path.'</a>'; 
}	
	
function ttm_get_attachment_files($media_ids){
	foreach($media_ids as $media_id) {
		global $db;
		echo '<div class="form-group pro-upload field mediafile'.$media_id.'">';
		$url = $db->get_row($db->prepare("SELECT * FROM {$db->media} WHERE `ID`=%d ", $media_id));
		$site_url = get_site_url();
		
		$info_mediameta = $db->get_row($db->prepare("SELECT * FROM {$db->mediameta} WHERE `media_id`=%d ", $media_id));
		
		$media_metavalue = $info_mediameta->meta_value;
		$media_metavalue = unserialize($media_metavalue);
		
		//echo $url->mime_type;
		switch ($url->mime_type) {
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/vnd.openxmlformats-officedocument.word':
			case 'application/msword':
				$class = '<i class="fa fa-file-word-o fa-4x"></i>' ;
				break;
				
			case 'application/pdf':
				$class = '<i class="fa fa-file-pdf-o fa-4x"></i>';
				break;
				
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				$class = '<i class="fa fa-file-excel-o fa-4x"></i>';
				break;
				
			case 'application/zip':
			case 'application/rar':
			case 'application/download':
				$class = '<i class="fa fa-file-zip-o fa-4x"></i>';
				break;
				
			case  'image/png':
			case  'image/jpg':
			case  'image/jpeg':
			case  'image/gif':
				
				echo '<a href ="'.$site_url.$media_metavalue[file].'"><img src = "'.$site_url.$media_metavalue['sizes']['small']['guid'].'"  class = "img-rounded"></a>';
				$class = 'image';
				echo '<a href="'.$site_url.$url->guid.'"><span class="file-title">'.$url->file_name.'</span></a>'; 
				break;
		}
		
		if ($class!= 'image') {
			echo '<a href="'.$site_url.$url->guid.'">'.$class.'</a>'; 
			echo '<a href="'.$site_url.$url->guid.'"><span class="file-title">'.$url->file_name.'</span></a>'; 
			} 
		if(!isset($_GET['page'])) {
			echo '<a onClick="deletefile('.$media_id.')" class="btn btn-danger dan btn-xs" title="Delete Media File"><span id="deletefile('.$media_id.')" class="glyphicon glyphicon-trash"></span></a>';
		}
		echo '<input type="hidden" value="'.$media_id.'">';
		echo '</div>';
		echo '<br>';
		
	}
}
	
	function ttm_get_attachment_file($media_id){

		global $db;
		/* echo '<div class="form-group pro-upload field mediafile'.$media_id.'">'; */
		$url = $db->get_row($db->prepare("SELECT * FROM {$db->media} WHERE `ID`=%d ", $media_id));
		$site_url = get_site_url();
		
		$info_mediameta = $db->get_row($db->prepare("SELECT * FROM {$db->mediameta} WHERE `media_id`=%d ", $media_id));
		
		$media_metavalue = $info_mediameta->meta_value;
		$media_metavalue = unserialize($media_metavalue);
		switch ($url->mime_type) {
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/vnd.openxmlformats-officedocument.word':
			case 'application/msword':
				return $guid =  '<a href="'.$site_url.$url->guid.'"><img class="doc_imgs" src = "'.get_template_directory_uri().'/assets/images/word.png"></a>'; 
				break;
				
			case 'application/pdf':
			return $guid =  '<a href="'.$site_url.$url->guid.'"><img class="doc_imgs" src = "'.get_template_directory_uri().'/assets/images/pdf.png"></a>'; 
				break;
				
			case 'application/vnd.ms-excel':
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
				return $guid =  '<a href="'.$site_url.$url->guid.'"><img class="doc_imgs" src = "'.get_template_directory_uri().'/assets/images/excel.png"></a>'; 
				break;
				
			case 'application/zip':
			case 'application/rar':
			case 'application/download':
				return $guid =  '<a href="'.$site_url.$url->guid.'"><img class="doc_imgs" src = "'.get_template_directory_uri().'/assets/images/rar.png"></a>'; 
				break;
				
			case  'image/png':
			case  'image/jpg':
			case  'image/jpeg':
			case  'image/gif':
				
				//return $guid = '<a href ="'.$site_url.$media_metavalue[file].'"><img src = "'.$site_url.$media_metavalue['sizes']['thumbnail']['guid'].'"></a>';
				
				//return $guid = '<a href ="'.$site_url.$media_metavalue[file].'"><img src = "'.$site_url.$media_metavalue['sizes']['thumbnail']['guid'].'"></a>';
				
				return $guid = '<img src = "'.$site_url.$media_metavalue['sizes']['thumbnail']['guid'].'">';
				
				$class = 'image'; 
				break;
		}
		
		/* echo '</div>';
		echo '<br>'; */
	
}
	
	
	
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }
    return $file_ary;
}

function delete_media_file($media_id) {
	global $db; 
	$media_info = $db->get_row($db->prepare("SELECT * FROM $db->media WHERE ID='%s'", $media_id));
	$media_info_mime = $media_info->mime_type;
	
	if($media_info_mime == 'image/jpg' || $media_info_mime == 'image/jpeg' || $media_info_mime == 'image/png' ||$media_info_mime == 'image/gif') {
	
	
	/*
	** unlink image
	*/
										
	$res = get_media_metas($media_id);
	$image = @unserialize($res);
	
	/*
	** unlink original image
	*/
	
	@unlink(IMAGEPATH.$image['file']);
	
	/*
	** unlink various sizes images
	*/
	
	if(@is_array($image['sizes'])){
		foreach(@$image['sizes'] as $key => $value){
		
			@unlink(IMAGEPATH.$value['guid']);
		}
	}
	/*
	** unlink end
	*/
	$db->query("DELETE FROM {$db-> mediameta} WHERE media_id={$media_id}");
	}
	else{
		
		/***** if file type is not image then **********/
		
		@unlink(IMAGEPATH.$media_info->guid);
		
	}
	if(!($db->query("DELETE FROM {$db-> media} WHERE ID=$media_id"))) {
		return false;
	}
	return true;
}

function is_file_doc($ext, $file_name, $fileType, $fileSize, $fileTmpName, $status = 1, $uploaded_by = 0) {

				$array = folder_exists($file_name);
				
				$uploadPath = $array[0];
				$dir_url = $array[1];
				$dir_url_til_month = $array[2];  
				$dirPath = $array[3];
				
				$file_name_without_extension = pathinfo($file_name, PATHINFO_FILENAME);
				$increment = 0;
				
				$file_name = $file_name_without_extension.'.'.$ext;
				
				while(is_file($uploadPath)) {
					$increment++;
					$file_name = $file_name_without_extension .'('.$increment . ').' . $ext;
					$uploadPath = $dirPath.$file_name;
					$dir_url =  $dir_url_til_month.$file_name;
				} 
				if(move_uploaded_file($fileTmpName,$uploadPath)){
					global $db;
					$db -> insert($db->media , array('ID'=>'', 'file_name'=>$file_name, 'mime_type'=>$fileType, 'guid'=>$dir_url , 'file_size'=>$fileSize, 'approval_status' => $status, 'uploaded_by' => $uploaded_by));	
					return $media_id = $db -> insert_id;
				} 
			}

function folder_exists($file_name){
				$year = date('Y');
				$month = date('m');

				$dirPath = TTM_CONTENT_DIR."/uploads/".$year;
				if (!file_exists($dirPath)) {
					mkdir($dirPath);
					chmod($dirPath, 0755); 
				}
							
				$dirPath =TTM_CONTENT_DIR."/uploads/".$year."/".$month."/";
				$dir_url_til_month = "/ttm-content/uploads/".$year."/".$month."/";
				$dir_url = $dir_url_til_month.$file_name;
				
				if (!file_exists($dirPath)) {
					mkdir($dirPath);
					chmod($dirPath, 0755); 
				}			
				$uploadPath = $dirPath.$file_name;
				return array($uploadPath,$dir_url,$dir_url_til_month,$dirPath);
}




function is_fileImage($ext, $valid_exts, $sizes, $file_name, $fileTmpName, $fileSize, $fileType, $status = 1, $uploaded_by = 0){
				global $db;
				
				$array = folder_exists($file_name);
				
				$uploadPath = $array[0];
				$dir_url = $array[1];
				$dir_url_til_month = $array[2];  
				$dirPath = $array[3];
			
				foreach ($sizes as $size) {
						$files[] = resize($size[0], $size[1], $dirPath, $file_name, $fileTmpName, $fileType );
				}			
				/* Code for check file is exist in upload folder if Yes, it will rename the file name **/	
					
				$file_name_without_extension = pathinfo($file_name, PATHINFO_FILENAME);
				$increment = 0;
				
				$file_name = $file_name_without_extension.'.'.$ext;
				
				while(is_file($uploadPath)) {
					$increment++;
					$file_name = $file_name_without_extension .'('.$increment .').' . $ext;
					$uploadPath = $dirPath.$file_name;
					$dir_url =  $dir_url_til_month.$file_name;
					//return $uploadPath;
				}
					
		
				if(move_uploaded_file($fileTmpName,$uploadPath)){
				global $db;
				$db->insert('ttm_media' , array('file_name'=>$file_name, 'mime_type'=>$fileType, 'guid'=>$dir_url , 'file_size'=>$fileSize));	
				$media_id = $db -> insert_id;
				}
					
				$i=0;
				$diff_medias = array();
				foreach($sizes as $key => $value) {
								
				$file_info = pathinfo("./".$files[$i]);
				$filename = $file_info['filename'];    // get image name 
			
				$imgsize=filesize($files[$i]);
				$imgsize_inkb = $imgsize/1024;
				
				$path = $dir_url_til_month.$filename.'.'.$ext; 
			
				$diff_medias1 =  array($key => array ( 
														'file' => $filename,
														'width' => $value[0],
														'height' => $value[1],
														'mime-type' => $fileType,
														'size' => $imgsize_inkb,
														'guid' => $path
													),
									  ); 
				$diff_medias = array_merge($diff_medias, $diff_medias1);
				$i++;
				}
			
				list($width, $height) = getimagesize($uploadPath); 
				$media_metadata = array (
										  'file' 	=> 	$dir_url,
										  'width' 	=> 	$width,
										  'height' 	=> 	$height,
										  'sizes' 	=> 	$diff_medias
										);			
	
				
				//$media_metadata = serialize($media_metadata); 
			
				add_media_meta($media_id, $db->mediameta, $media_metadata);	
				
				return $media_id;
		}
/*
** Upload media from url
*/		

function upload_media_from_url($url){ 

	$response = array();
	if ( $url ) {
			$imageurl = $url;
			$imageurl = stripslashes($imageurl);
			$uploads = ttm_upload_dir();
			
			$post_id = isset($_GET['post_id'])? (int) $_GET['post_id'] : 0;
			$ext = pathinfo( basename($imageurl) , PATHINFO_EXTENSION);
			$nwflnm = uniqid();
			$newfilename = $nwflnm ? $nwflnm . "." . $ext : basename($imageurl);



			$filename = ttm_unique_filename( $uploads['path'], $newfilename, $unique_filename_callback = null );
			//$ttm_filetype = ttm_check_filetype($filename, null );
			$ttm_filetype = get_mime_type($url);
			$fullpathfilename = $uploads['path'] . "/" . $filename;
			
			try {
				/* if ( !substr_count($ttm_filetype['type'], "image") ) {
					throw new Exception( basename($imageurl) . ' is not a valid image. ' . $ttm_filetype['type']  . '' );
				} */
			
				$image_string = fetch_image($imageurl);
				
				/* $fileSaved = file_put_contents($uploads['path'] . "/" . $filename, $image_string);
				if ( !$fileSaved ) {
					throw new Exception("The file cannot be saved.");
				}
				
				$attachment = array(
					 'post_mime_type' => $ttm_filetype,
					 'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					 'post_content' => '',
					 'post_status' => 'inherit',
					 'guid' => $uploads['url'] . "/" . $filename
				);
				$attach_id = ttm_insert_attachment( $attachment, $fullpathfilename, $post_id );
				if ( !$attach_id ) {
					throw new Exception("Failed to save record into database.");
				}
				require_once(ABSPATH . "fp-admin" . '/includes/image.php');
				$attach_data = ttm_generate_attachment_metadata( $attach_id, $fullpathfilename );
				ttm_update_attachment_metadata( $attach_id,  $attach_data ); */
			
			} catch (Exception $e) {
				$error = '<div id="message" class="error"><p>' . $e->getMessage() . '</p></div>';
			}
			
			/* if ( !function_exists("curl_init") && !ini_get("allow_url_fopen") ) {
				$message = '<div id="message" class="error"><p><b>cURL</b> or <b>allow_url_fopen</b> needs to be enabled. Please consult your server Administrator.</p></div>';
				$response = array('type' => 'error', 'message' => $message);

			} elseif ( $error ) {
				$response = array('type' => 'error', 'message' => $error);
			} else {
				if ( $fileSaved && $attach_id ) {
					$message = '<div id="message" class="updated"><p>File saved.</p></div>';
					$response = array('type' => 'success', 'message' => $message, 'media_id' => $attach_id);
				}
			} */

		}
		return $response;
}
/*
** fetch Image
*/
function fetch_image($url) {
		if ( function_exists("curl_init") ) {
			return curl_fetch_image($url);
		} elseif ( ini_get("allow_url_fopen") ) {
			return fopen_fetch_image($url);
		}
	}
	
function curl_fetch_image($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$image = curl_exec($ch);
	curl_close($ch);
	return $image;
}
function fopen_fetch_image($url) {
	$image = file_get_contents($url, false, $context);
	return $image;
}

function get_mime_type($url){
	$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_NOBODY, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$results = explode("\n", trim(curl_exec($ch)));
//print_r($results);
foreach($results as $line) {
        if (strtok($line, ':') == 'Content-Type') {
                $parts = explode(":", $line);
                return trim($parts[1]);
        }
}
}
function upload_base64_media_old($image, $ext = 'jpg') {

    global $db;
    $decode_img = base64_decode($image);

    if(@imagecreatefromstring($decode_img)) {

        $filename = md5(time());

        $result = folder_exists($filename);

        $upload_path =  $result[0].'.'.$ext;
        $upload_dir =  $result[1].'.'.$ext;
        $fileType = "image/$ext";

        if(file_put_contents($upload_path, $decode_img)) {
            $media_id = $db -> insert('ttm_media' , array('ID'=>'', 'file_name'=>$filename.'.'.$ext, 'mime_type'=>$fileType, 'guid'=>$upload_dir, 'file_size'=>@$fileSize));
            return $db->insert_id ;
        } else {
            return false;
        }
    }
}
/*
** New function for media upload from base64 code
*/
function upload_base64_media($image, $ext = 'jpg') {

    global $db;
    $decode_img = base64_decode($image);

    if(@imagecreatefromstring($decode_img)) {

        $file_name = uniqid().'.'.$ext;

        $result = folder_exists($file_name);

        $upload_path =  $result[0];
        $upload_dir =  $result[1];
		$dir_url_til_month = $result[2];  
		$dirPath = $result[3];
		
        $fileType = "image/$ext";
		
        if(file_put_contents($upload_path, $decode_img)) {
            
			if($db -> insert('ttm_media' , array('ID'=>'', 'file_name'=>$file_name, 'mime_type'=>$fileType, 'guid'=>$upload_dir, 'file_size'=>@$fileSize))){
				
				$media_id = $db->insert_id;
				
			/*
			** small sizes creation for images
			*/
				$fileTmpName = get_site_url().$upload_dir;
				$sizes = get_image_sizes();
				
				foreach ($sizes as $size) {
					$files[] = resize($size[0], $size[1], $dirPath, $file_name, $fileTmpName, $fileType );
				}

				$i=0;
				$diff_medias = array();
				foreach($sizes as $key => $value) {
								
				$file_info = pathinfo("./".$files[$i]);
				$filename = $file_info['filename'];    // get image name 
			
				$imgsize=filesize($files[$i]);
				$imgsize_inkb = $imgsize/1024;
				
				$path = $dir_url_til_month.$filename.'.'.$ext; 
			
				$diff_medias1 =  array($key => array ( 
														'file' => $filename,
														'width' => $value[0],
														'height' => $value[1],
														'mime-type' => $fileType,
														'size' => $imgsize_inkb,
														'guid' => $path
													),
									  ); 
				$diff_medias = array_merge($diff_medias, $diff_medias1);
				$i++;
				}
			
				list($width, $height) = getimagesize($upload_path); 
				$media_metadata = array (
										  'file' 	=> 	$upload_dir,
										  'width' 	=> 	$width,
										  'height' 	=> 	$height,
										  'sizes' 	=> 	$diff_medias
										);			
	
				
				//$media_metadata = serialize($media_metadata); 
			
				add_media_meta($media_id, 'ttm_mediameta', $media_metadata);
				return $media_id;
			}
			else{
				return false;
			}
			
			//return $db->insert_id ;
        } else {
            return false;
        }
    }
}

function get_image_sizes(){
	$sizes = array('small' => array(50, 50), 'thumbnail' => array(150, 150), 'medium' => array(300, 300), 'large' => array(640, 640));
	return $sizes;
}
?>