<?php
/**
 * The TTM version string
 *
 * @global string $ttm_version
 */
$ttm_version = '3.9.5';

/**
 * Holds the TTM DB revision, increments when changes are made to the TTM DB schema.
 *
 * @global int $ttm_db_version
 */
$ttm_db_version = 27916;

/**
 * Holds the TinyMCE version
 *
 * @global string $tinymce_version
 */
$tinymce_version = '4021-20140423';

/**
 * Holds the required PHP version
 *
 * @global string $required_php_version
 */
$required_php_version = '5.2.4';

/**
 * Holds the required MySQL version
 *
 * @global string $required_mysql_version
 */
$required_mysql_version = '5.0';
