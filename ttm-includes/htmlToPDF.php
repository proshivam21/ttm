<?php 

function htmltopdf($pdf_options) {
    
	/* $post_data = http_build_query($pdf_options);
	$post_array = array( 
		'http' => array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );
	
	$context = stream_context_create($post_array);
	
    $result = file_get_contents("http://freehtmltopdf.com", false, $context); */
	
	$apikey = '8e989bc8-6342-4007-93ff-2f243b9bd16a';
	$html =  $pdf_options['html'];
	$file_name = explode('.', $pdf_options['file_name']);
	$destination = ABSPATH . TTMINC.'/invoices-html/'.$file_name[0].'.html';
	file_put_contents($destination, $html);
	$value = get_site_url().'/ttm-includes/invoices-html/'.$file_name[0].'.html';
	$margin = 10;
	$result = file_get_contents("http://api.html2pdfrocket.com/pdf?apikey=" . urlencode($apikey) . "&value=" . urlencode($value)."&MarginLeft=$margin&MarginRight=$margin&MarginTop=$margin");
	
    $action = preg_replace('!\s+!', '', $pdf_options['action']);
    if(isset($action) && !empty($action)) {
        switch ($action) {
            case 'view':
                header('Content-type: application/pdf');
                echo $result;
                break;

            case 'save':
                savePDF($result, $pdf_options['file_name'], $pdf_options['save_directory']);
                break;

            case 'download':
                downloadPDF($result, $pdf_options['file_name']);
                break;
			
			case 'save_and_download':
				savePDF($result, $pdf_options['file_name'], $pdf_options['save_directory']);
				downloadPDF($result, $pdf_options['file_name']);
                break;
            default:
                header('Content-type: application/pdf');
                echo $result;
                break;
        }
    } else {
        header('Content-type: application/pdf');
        echo $result;
    }
}

function savePDF($result, $file_name, $save_directory) {
	file_put_contents($save_directory . '/' . $file_name, $result);
}

function downloadPDF($result, $file_name) {
	// set the pdf data as download content:
	ob_clean();
	header('Content-type: application/pdf');
	header('Content-Disposition: attachment; filename="'.$file_name.'"');
	echo $result;
	//var_dump($result);
	exit();
}

?>