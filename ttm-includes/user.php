<?php
/**
 * TTM User API
 *
 * @package TTM
 * @subpackage Users
 */

/**
 * Authenticate user with remember capability.
 *
 * The credentials is an array that has 'user_login', 'user_password', and
 * 'remember' indices. If the credentials is not given, then the log in form
 * will be assumed and used if set.
 *
 * The various authentication cookies will be set by this function and will be
 * set for a longer period depending on if the 'remember' credential is set to
 * true.
 *
 * @since 2.5.0
 *
 * @param array       $credentials   Optional. User info in order to sign on.
 * @param string|bool $secure_cookie Optional. Whether to use secure cookie.
 * @return TTM_User|TTM_Error TTM_User on success, TTM_Error on failure.
 */
 
function ttm_signon( $credentials = array(), $secure_cookie = '' ) {
	if ( empty($credentials) ) {
		if ( ! empty($_POST['log']) )
			$credentials['user_login'] = $_POST['log'];
		if ( ! empty($_POST['pwd']) )
			$credentials['user_password'] = $_POST['pwd'];
		if ( ! empty($_POST['rememberme']) )
			$credentials['remember'] = $_POST['rememberme'];
	}

	if ( !empty($credentials['remember']) )
		$credentials['remember'] = true;
	else
		$credentials['remember'] = false;

	/**
	 * Fires before the user is authenticated.
	 *
	 * The variables passed to the callbacks are passed by reference,
	 * and can be modified by callback functions.
	 *
	 * @since 1.5.1
	 *
	 * @todo Decide whether to deprecate the ttm_authenticate action.
	 *
	 * @param string $user_login    Username, passed by reference.
	 * @param string $user_password User password, passed by reference.
	 */
	do_action_ref_array( 'ttm_authenticate', array( &$credentials['user_login'], &$credentials['user_password'] ) );

	if ( '' === $secure_cookie )
		$secure_cookie = is_ssl();

	/**
	 * Filter whether to use a secure sign-on cookie.
	 *
	 * @since 3.1.0
	 *
	 * @param bool  $secure_cookie Whether to use a secure sign-on cookie.
	 * @param array $credentials {
 	 *     Array of entered sign-on data.
 	 *
 	 *     @type string $user_login    Username.
 	 *     @type string $user_password Password entered.
	 *     @type bool   $remember      Whether to 'remember' the user. Increases the time
	 *                                 that the cookie will be kept. Default false.
 	 * }
	 */
	$secure_cookie = apply_filters( 'secure_signon_cookie', $secure_cookie, $credentials );

	global $auth_secure_cookie; // XXX ugly hack to pass this to ttm_authenticate_cookie
	$auth_secure_cookie = $secure_cookie;

	add_filter('authenticate', 'ttm_authenticate_cookie', 30, 3);
	
	
	$user = ttm_authenticate($credentials['user_login'], $credentials['user_password']);
	
	if ( is_ttm_error($user) ) {
		if ( $user->get_error_codes() == array('empty_username', 'empty_password') ) {
			$user = new TTM_Error('', '');
		}

		return $user;
	}

	//ttm_set_auth_cookie($user->ID, $credentials['remember'], $secure_cookie);
	ttm_set_auth_session($user->ID, $credentials['remember'], $secure_cookie);
	/**
	 * Fires after the user has successfully logged in.
	 *
	 * @since 1.5.0
	 *
	 * @param string  $user_login Username.
	 * @param TTM_User $user       TTM_User object of the logged-in user.
	 */
	do_action( 'ttm_login', $user->user_login, $user );
	return $user;
}

/**
 * Authenticate the user using the TTM auth cookie.
 *
 * @since 2.8.0
 *
 * @param TTM_User|TTM_Error|null $user     TTM_User or TTM_Error object from a previous callback. Default null.
 * @param string                $username Username. If not empty, cancels the cookie authentication.
 * @param string                $password Password. If not empty, cancels the cookie authentication.
 * @return TTM_User|TTM_Error TTM_User on success, TTM_Error on failure.
 */
function ttm_authenticate_cookie($user, $username, $password) {
	if ( $user instanceof TTM_User ) {
		return $user;
	}

	if ( empty($username) && empty($password) ) {
		$user_id = ttm_validate_auth_cookie();
		if ( $user_id )
			return new TTM_User($user_id);

		global $auth_secure_cookie;

		if ( $auth_secure_cookie )
			$auth_cookie = SECURE_AUTH_COOKIE;
		else
			$auth_cookie = AUTH_COOKIE;

		if ( !empty($_COOKIE[$auth_cookie]) )
			return new TTM_Error('expired_session', __('Please log in again.'));

		// If the cookie is not set, be silent.
	}

	return $user;
}
 
 /*
 * Insert an user into userdatabase
 *
 */
function ttm_insert_user($userdata) {
	global $db;
	if(!$db->insert($db->users, $userdata)) {
		return false;
	} else {
		return  $db->insert_id;
	}
}

function ttm_update_user($user_id, $userdata) {
	global $db;
	$where = array('ID' => $user_id);
	if(!$db->update($db->users, $userdata, $where)) {
		return false;
	}
	
	return true;
}

function ttm_delete_user($user_id) {
	global $db;
	$where = array('ID' => $user_id);
	if(!$db -> delete($db->users, $where)) {
		return false;
	}
	
	return true;
}

function ttm_approve_user($user_id) {
	global $db;
	$userdata = array('user_status' => 1);
	$where = array('ID' => $user_id);
	if(!$db->update($db->users, $userdata, $where)) {
		return false;
	}
	
	return true;
}

function ttm_decline_user($user_id) {
	global $db;
	$userdata = array('user_status' => -1);
	$where = array('ID' => $user_id);
	if(!$db->update($db->users, $userdata, $where)) {
		return false;
	}
	
	return true;
}

function ttm_suspend_user($user_id) {
	global $db;
	$userdata = array('user_status' => '2');
	$where = array('ID' => $user_id);
	if(!$db->update($db->users, $userdata, $where)) {
		return false;
	}
	return true;
}

if ( !function_exists('get_userdata') ) :
/**
 * Retrieve user info by user ID.
 *
 * @since 0.71
 *
 * @param int $user_id User ID
 * @return TTM_User|bool TTM_User object on success, false on failure.
 */
function get_userdata( $user_id ) {
	return get_user_by( 'id', $user_id );
}
endif;

if ( !function_exists('get_user_by') ) :
/**
 * Retrieve user info by a given field
 *
 * @since 2.8.0
 *
 * @param string $field The field to retrieve the user with. id | slug | email | login
 * @param int|string $value A value for $field. A user ID, slug, email address, or login name.
 * @return TTM_User|bool TTM_User object on success, false on failure.
 */
function get_user_by( $field, $value ) {
	
	global $db;

	if ( 'id' == $field ) {
		// Make sure the value is numeric to avoid casting objects, for example,
		// to int 1.
		if ( ! is_numeric( $value ) )
			return false;
		$value = intval( $value );
		if ( $value < 1 )
			return false;
	} else {
		$value = trim( $value );
	}

	if ( !$value )
		return false;
	
	switch ( $field ) {
		case 'id':
			$user_id = $value;
			$db_field = 'ID';
			break;
		case 'slug':
			$user_id = ttm_cache_get($value, 'userslugs');
			$db_field = 'user_nicename';
			break;
		case 'email':
			$user_id = ttm_cache_get($value, 'useremail');
			$db_field = 'user_email';
			break;
		case 'login':
			$value = sanitize_user( $value );
			$user_id = ttm_cache_get($value, 'userlogins');
			$db_field = 'user_login';
			break;
		default:
			return false;
	}

		if ( false !== $user_id ) {
			if ( $user = ttm_cache_get( $user_id, 'users' ) )
				return $user;
		}

		if ( !$user = $db->get_row( $db->prepare(
			"SELECT * FROM $db->users WHERE $db_field = %s", $value
		) ) )
			return false;

	return $user;
}
endif;

/**
 * Add meta data field to a user.
 *
 * Post meta data is called "Custom Fields" on the Administration Screens.
 *
 * @since 1.0.0
 * @uses add_metadata()
 * @link http://codex.iquincesoft.com/Function_Reference/add_user_meta
 *
 * @param int $user_id User ID.
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Metadata value.
 * @param bool $unique Optional, default is false. Whether the same key should not be added.
 * @return int|bool Meta ID on success, false on failure.
 */
function add_user_meta($user_id, $meta_key, $meta_value, $unique = false) {
	return add_metadata('user', $user_id, $meta_key, $meta_value, $unique);
}

/**
 * Remove metadata matching criteria from a user.
 *
 * You can match based on the key, or key and value. Removing based on key and
 * value, will keep from removing duplicate metadata with the same key. It also
 * allows removing all metadata matching key, if needed.
 *
 * @since 3.0.0
 * @uses delete_metadata()
 * @link http://codex.iquincesoft.com/Function_Reference/delete_user_meta
 *
 * @param int $user_id user ID
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Optional. Metadata value.
 * @return bool True on success, false on failure.
 */
function delete_user_meta($user_id, $meta_key, $meta_value = '') {
	return delete_metadata('user', $user_id, $meta_key, $meta_value);
}

/**
 * Retrieve user meta field for a user.
 *
 * @since 3.0.0
 * @uses get_metadata()
 * @link http://codex.iquincesoft.com/Function_Reference/get_user_meta
 *
 * @param int $user_id User ID.
 * @param string $key Optional. The meta key to retrieve. By default, returns data for all keys.
 * @param bool $single Whether to return a single value.
 * @return mixed Will be an array if $single is false. Will be value of meta data field if $single
 *  is true.
 */
function get_user_meta($user_id, $key = '', $single = false) {
	return get_metadata('user', $user_id, $key, $single);
}

/**
 * Update user meta field based on user ID.
 *
 * Use the $prev_value parameter to differentiate between meta fields with the
 * same key and user ID.
 *
 * If the meta field for the user does not exist, it will be added.
 *
 * @since 3.0.0
 * @uses update_metadata
 *
 * @param int $user_id User ID.
 * @param string $meta_key Metadata key.
 * @param mixed $meta_value Metadata value.
 * @param mixed $prev_value Optional. Previous value to check before removing.
 * @return int|bool Meta ID if the key didn't exist, true on successful update, false on failure.
 */
function update_user_meta($user_id, $meta_key, $meta_value, $prev_value = '') {
	return update_metadata('user', $user_id, $meta_key, $meta_value, $prev_value);
}

/**
 * Checks whether the given username exists.
 *
 * @since 2.0.0
 *
 * @param string $username Username.
 * @return null|int The user's ID on success, and null on failure.
 */
function username_exists( $username ) {
	if ( $user = get_user_by('login', $username ) ) {
		return $user->ID;
	} else {
		return null;
	}
}

/**
 * Checks whether the given email exists.
 *
 * @since 2.1.0
 *
 * @param string $email Email.
 * @return bool|int The user's ID on success, and false on failure.
 */
function email_exists( $email ) {
	if ( $user = get_user_by('email', $email) )
		return $user->ID;
	return false;
}

if ( !function_exists('ttm_generate_password') ) :
/**
 * Generates a random password drawn from the defined set of characters.
 *
 * @since 2.5.0
 *
 * @param int  $length              Optional. The length of password to generate. Default 12.
 * @param bool $special_chars       Optional. Whether to include standard special characters.
 *                                  Default true.
 * @param bool $extra_special_chars Optional. Whether to include other special characters.
 *                                  Used when generating secret keys and salts. Default false.
 * @return string The random password.
 */
function ttm_generate_password( $length = 12, $special_chars = true, $extra_special_chars = false ) {
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	if ( $special_chars )
		$chars .= '!@#$%^&*()';
	if ( $extra_special_chars )
		$chars .= '-_ []{}<>~`+=,.;:/?|';

	$password = '';
	for ( $i = 0; $i < $length; $i++ ) {
		$password .= substr($chars, rand(0, strlen($chars) - 1), 1);
	}

	/**
	 * Filter the randomly-generated password.
	 *
	 * @since 3.0.0
	 *
	 * @param string $password The generated password.
	 */
	return apply_filters( 'random_password', $password );
}
endif;


/**
 * Retrieves a user row based on password reset key and login
 *
 * A key is considered 'expired' if it exactly matches the value of the
 * user_activation_key field, rather than being matched after going through the
 * hashing process. This field is now hashed; old values are no longer accepted
 * but have a different TTM_Error code so good user feedback can be provided.
 *
 * @global db         $db      TTM database object for queries.
 * @global PasswordHash $ttm_hasher Portable PHP password hashing framework instance.
 *
 * @param string $key       Hash to validate sending user's password.
 * @param string $login     The user login.
 * @return TTM_User|TTM_Error TTM_User object on success, TTM_Error object for invalid or expired keys.
 */
function check_password_reset_key($key, $login) {
	global $db, $ttm_hasher;
	$key = preg_replace('/[^a-z0-9]/i', '', $key);
	if ( empty( $key ) || !is_string( $key ) )
		return new TTM_Error('invalid_key', ('Invalid key'));

	if ( empty($login) || !is_string($login) )
		return new TTM_Error('invalid_key', ('Invalid key'));
		
	
	$row = $db->get_row( $db->prepare( "SELECT ID, user_activation_key FROM $db->users WHERE user_login = %s", $login ) );
	
	if ( ! $row )
		return new TTM_Error('invalid_key', ('Invalid key'));

	if ( empty( $ttm_hasher ) ) {
		require_once ABSPATH . TTMINC . '/class-phpass.php';
		$ttm_hasher = new PasswordHash( 8, true );
	}

	if ( $ttm_hasher->CheckPassword( $key, $row->user_activation_key ) )
		return get_userdata( $row->ID );

	if ( $key === $row->user_activation_key ) {
		$return = new TTM_Error( 'expired_key', ( 'Invalid key' ) );
		$user_id = $row->ID;

		/**
		 * Filter the return value of check_password_reset_key() when an
		 * old-style key is used (plain-text key was stored in the database).
		 *
		 * @since 3.7.0
		 *
		 * @param TTM_Error $return  A TTM_Error object denoting an expired key.
		 *                          Return a TTM_User object to validate the key.
		 * @param int      $user_id The matched user ID.
		 */
		return apply_filters( 'password_reset_key_expired', $return, $user_id );
	}

	return new TTM_Error( 'invalid_key', ( 'Invalid key' ) );
}

/**
 * Handles resetting the user's password.
 *
 * @param object $user The user
 * @param string $new_pass New password for the user in plaintext
 */
function reset_password( $user, $new_pass ) {
	/**
	 * Fires before the user's password is reset.
	 *
	 * @since 1.5.0
	 *
	 * @param object $user     The user.
	 * @param string $new_pass New user password.
	 */
	do_action( 'password_reset', $user, $new_pass );

	ttm_set_password( $new_pass, $user->ID );
	//update_user_option( $user->ID, 'default_password_nag', false, true );

	//ttm_password_change_notification( $user );
}

if ( ! function_exists( 'hash_equals' ) ) :
/**
 * Compare two strings in constant time.
 *
 * This function was added in PHP 5.6.
 * It can leak the length of a string.
 *
 * @since 3.9.2
 *
 * @param string $a Expected string.
 * @param string $b Actual string.
 * @return bool Whether strings are equal.
 */
function hash_equals( $a, $b ) {
	$a_length = strlen( $a );
	if ( $a_length !== strlen( $b ) ) {
		return false;
	}
	$result = 0;

	// Do not attempt to "optimize" this.
	for ( $i = 0; $i < $a_length; $i++ ) {
		$result |= ord( $a[ $i ] ) ^ ord( $b[ $i ] );
	}

	return $result === 0;
}
endif;


/**
 * Set up global user vars.
 *
 * Used by ttm_set_current_user() for back compat. Might be deprecated in the future.
 *
 * @since 2.0.4
 * @global string $userdata User description.
 * @global string $user_login The user username for logging in
 * @global int $user_level The level of the user
 * @global int $user_ID The ID of the user
 * @global string $user_email The email address of the user
 * @global string $user_url The url in the user's profile
 * @global string $user_identity The display name of the user
 *
 * @param int $for_user_id Optional. User ID to set up global data.
 */
function setup_userdata($for_user_id = '') {
	global $user_login, $userdata, $user_level, $user_ID, $user_email, $user_url, $user_identity;

	if ( '' == $for_user_id )
		$for_user_id = get_current_user_id();
	$user = get_userdata( $for_user_id );

	if ( ! $user ) {
		$user_ID = 0;
		$user_level = 0;
		$userdata = null;
		$user_login = $user_email = $user_url = $user_identity = '';
		return;
	}

	$user_ID    = (int) $user->ID;
	$user_level = (int) $user->user_level;
	$userdata   = $user;
	$user_login = $user->user_login;
	$user_email = $user->user_email;
	$user_url   = $user->user_url;
	$user_identity = $user->display_name;
}


/**
 * Get the current user's ID
 *
 * @since MU
 *
 * @return int The current user's ID
 */
function get_current_user_id() {
	if ( ! function_exists( 'ttm_get_current_user' ) )
		return 0;
	$user = ttm_get_current_user();
	return ( isset( $user->ID ) ? (int) $user->ID : 0 );
}

/**
 * Retrieve the current session token from the logged_in cookie.
 *
 * @since 4.0.0
 *
 * @return string Token.
 */
function ttm_get_session_token() {
	$cookie = ttm_parse_auth_cookie( '', 'logged_in' );
	return ! empty( $cookie['token'] ) ? $cookie['token'] : '';
}

/**
 * Remove the current session token from the database.
 *
 * @since 4.0.0
 */
function ttm_destroy_current_session() {
	$token = ttm_get_session_token();
	if ( $token ) {
		$manager = TTM_Session_Tokens::get_instance( get_current_user_id() );
		$manager->destroy( $token );
	}
}

/**
 * TTM User Query class.
 *
 * @since 3.1.0
 *
 * @see TTM_User_Query::prepare_query() for information on accepted arguments.
 */
class TTM_User_Query {

	/**
	 * Query vars, after parsing
	 *
	 * @since 3.5.0
	 * @access public
	 * @var array
	 */
	public $query_vars = array();

	/**
	 * List of found user ids
	 *
	 * @since 3.1.0
	 * @access private
	 * @var array
	 */
	private $results;

	/**
	 * Total number of found users for the current query
	 *
	 * @since 3.1.0
	 * @access private
	 * @var int
	 */
	private $total_users = 0;

	/**
	 * Metadata query container.
	 *
	 * @since 4.2.0
	 * @access public
	 * @var object TTM_Meta_Query
	 */
	public $meta_query = false;

	private $compat_fields = array( 'results', 'total_users' );

	// SQL clauses
	public $query_fields;
	public $query_from;
	public $query_where;
	public $query_orderby;
	public $query_limit;

	/**
	 * PHP5 constructor.
	 *
	 * @since 3.1.0
	 *
	 * @param null|string|array $args Optional. The query variables.
	 */
	public function __construct( $query = null ) {
		if ( ! empty( $query ) ) {
			$this->prepare_query( $query );
			$this->query();
		}
	}

	/**
	 * Prepare the query variables.
	 *
	 * @since 3.1.0
	 * @since 4.2.0 Added 'meta_value_num' support for `$orderby` parameter. Added multi-dimensional array syntax
	 *              for `$orderby` parameter.
	 * @access public
	 *
	 * @param string|array $query {
	 *     Optional. Array or string of Query parameters.
	 *
	 *     @type int          $blog_id         The site ID. Default is the global blog id.
	 *     @type string       $role            Role name. Default empty.
	 *     @type string       $meta_key        User meta key. Default empty.
	 *     @type string       $meta_value      User meta value. Default empty.
	 *     @type string       $meta_compare    Comparison operator to test the `$meta_value`. Accepts '=', '!=',
	 *                                         '>', '>=', '<', '<=', 'LIKE', 'NOT LIKE', 'IN', 'NOT IN', 'BETWEEN',
	 *                                         'NOT BETWEEN', 'EXISTS', 'NOT EXISTS', 'REGEXP', 'NOT REGEXP',
	 *                                         or 'RLIKE'. Default '='.
	 *     @type array        $include         An array of user IDs to include. Default empty array.
	 *     @type array        $exclude         An array of user IDs to exclude. Default empty array.
	 *     @type string       $search          Search keyword. Searches for possible string matches on columns.
	 *                                         When `$search_columns` is left empty, it tries to determine which
	 *                                         column to search in based on search string. Default empty.
	 *     @type array        $search_columns  Array of column names to be searched. Accepts 'ID', 'login',
	 *                                         'nicename', 'email', 'url'. Default empty array.
	 *     @type string|array $orderby         Field(s) to sort the retrieved users by. May be a single value,
	 *                                         an array of values, or a multi-dimensional array with fields as keys
	 *                                         and orders ('ASC' or 'DESC') as values. Accepted values are'ID',
	 *                                         'display_name' (or 'name'), 'user_login' (or 'login'),
	 *                                         'user_nicename' (or 'nicename'), 'user_email' (or 'email'),
	 *                                         'user_url' (or 'url'), 'user_registered' (or 'registered'),
	 *                                         'post_count', 'meta_value', 'meta_value_num', the value of
	 *                                         `$meta_key`, or an array key of `$meta_query`. To use 'meta_value'
	 *                                         or 'meta_value_num', `$meta_key` must be also be defined.
	 *                                         Default 'user_login'.
	 *     @type string       $order           Designates ascending or descending order of users. Order values
	 *                                         passed as part of an `$orderby` array take precedence over this
	 *                                         parameter. Accepts 'ASC', 'DESC'. Default 'ASC'.
	 *     @type int          $offset          Number of users to offset in retrieved results. Can be used in
	 *                                         conjunction with pagination. Default 0.
	 *     @type int          $number          Number of users to limit the query for. Can be used in conjunction
	 *                                         with pagination. Value -1 (all) is not supported.
	 *                                         Default empty (all users).
	 *     @type bool         $count_total     Whether to count the total number of users found. If pagination is not
	 *                                         needed, setting this to false can improve performance. Default true.
	 *     @type string|array $fields          Which fields to return. Single or all fields (string), or array
	 *                                         of fields. Accepts 'ID', 'display_name', 'login', 'nicename', 'email',
	 *                                         'url', 'registered'. Use 'all' for all fields and 'all_with_meta' to
	 *                                         include meta fields. Default 'all'.
	 *     @type string       $who             Type of users to query. Accepts 'authors'. Default empty (all users).
	 * }
	 */
	public function prepare_query( $query = array() ) {
		global $db;

		if ( empty( $this->query_vars ) || ! empty( $query ) ) {
			$this->query_limit = null;
			$this->query_vars = ttm_parse_args( $query, array(
				'blog_id' => $GLOBALS['blog_id'],
				'role' => '',
				'meta_key' => '',
				'meta_value' => '',
				'meta_compare' => '',
				'include' => array(),
				'exclude' => array(),
				'search' => '',
				'search_columns' => array(),
				'orderby' => 'login',
				'order' => 'ASC',
				'offset' => '',
				'number' => '',
				'count_total' => true,
				'fields' => 'all',
				'who' => ''
			) );
		}

		/**
		 * Fires before the TTM_User_Query has been parsed.
		 *
		 * The passed TTM_User_Query object contains the query variables, not
		 * yet passed into SQL.
		 *
		 * @since 4.0.0
		 *
		 * @param TTM_User_Query $this The current TTM_User_Query instance,
		 *                            passed by reference.
		 */
		do_action( 'pre_get_users', $this );

		$qv =& $this->query_vars;

		if ( is_array( $qv['fields'] ) ) {
			$qv['fields'] = array_unique( $qv['fields'] );

			$this->query_fields = array();
			foreach ( $qv['fields'] as $field ) {
				$field = 'ID' === $field ? 'ID' : sanitize_key( $field );
				$this->query_fields[] = "$db->users.$field";
			}
			$this->query_fields = implode( ',', $this->query_fields );
		} elseif ( 'all' == $qv['fields'] ) {
			$this->query_fields = "$db->users.*";
		} else {
			$this->query_fields = "$db->users.ID";
		}

		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->query_fields = 'SQL_CALC_FOUND_ROWS ' . $this->query_fields;

		$this->query_from = "FROM $db->users";
		$this->query_where = "WHERE 1=1";

		// Parse and sanitize 'include', for use by 'orderby' as well as 'include' below.
		if ( ! empty( $qv['include'] ) ) {
			$include = ttm_parse_id_list( $qv['include'] );
		} else {
			$include = false;
		}

		$blog_id = 0;
		if ( isset( $qv['blog_id'] ) ) {
			$blog_id = absint( $qv['blog_id'] );
		}

		if ( isset( $qv['who'] ) && 'authors' == $qv['who'] && $blog_id ) {
			$qv['meta_key'] = $db->get_blog_prefix( $blog_id ) . 'user_level';
			$qv['meta_value'] = 0;
			$qv['meta_compare'] = '!=';
			$qv['blog_id'] = $blog_id = 0; // Prevent extra meta query
		}

		// Meta query.
		$this->meta_query = new TTM_Meta_Query();
		$this->meta_query->parse_query_vars( $qv );

		$role = '';
		if ( isset( $qv['role'] ) ) {
			$role = trim( $qv['role'] );
		}

		if ( $blog_id && ( $role || is_multisite() ) ) {
			$cap_meta_query = array();
			$cap_meta_query['key'] = $db->get_blog_prefix( $blog_id ) . 'capabilities';

			if ( $role ) {
				$cap_meta_query['value'] = '"' . $role . '"';
				$cap_meta_query['compare'] = 'like';
			}

			if ( empty( $this->meta_query->queries ) ) {
				$this->meta_query->queries = array( $cap_meta_query );
			} elseif ( ! in_array( $cap_meta_query, $this->meta_query->queries, true ) ) {
				// Append the cap query to the original queries and reparse the query.
				$this->meta_query->queries = array(
					'relation' => 'AND',
					array( $this->meta_query->queries, $cap_meta_query ),
				);
			}

			$this->meta_query->parse_query_vars( $this->meta_query->queries );
		}

		if ( ! empty( $this->meta_query->queries ) ) {
			$clauses = $this->meta_query->get_sql( 'user', $db->users, 'ID', $this );
			$this->query_from .= $clauses['join'];
			$this->query_where .= $clauses['where'];

			if ( 'OR' == $this->meta_query->relation ) {
				$this->query_fields = 'DISTINCT ' . $this->query_fields;
			}
		}

		// sorting
		$qv['order'] = isset( $qv['order'] ) ? strtoupper( $qv['order'] ) : '';
		$order = $this->parse_order( $qv['order'] );

		if ( empty( $qv['orderby'] ) ) {
			// Default order is by 'user_login'.
			$ordersby = array( 'user_login' => $order );
		} else if ( is_array( $qv['orderby'] ) ) {
			$ordersby = $qv['orderby'];
		} else {
			// 'orderby' values may be a comma- or space-separated list.
			$ordersby = preg_split( '/[,\s]+/', $qv['orderby'] );
		}

		$orderby_array = array();
		foreach ( $ordersby as $_key => $_value ) {
			if ( ! $_value ) {
				continue;
			}

			if ( is_int( $_key ) ) {
				// Integer key means this is a flat array of 'orderby' fields.
				$_orderby = $_value;
				$_order = $order;
			} else {
				// Non-integer key means this the key is the field and the value is ASC/DESC.
				$_orderby = $_key;
				$_order = $_value;
			}

			$parsed = $this->parse_orderby( $_orderby );

			if ( ! $parsed ) {
				continue;
			}

			$orderby_array[] = $parsed . ' ' . $this->parse_order( $_order );
		}

		// If no valid clauses were found, order by user_login.
		if ( empty( $orderby_array ) ) {
			$orderby_array[] = "user_login $order";
		}

		$this->query_orderby = 'ORDER BY ' . implode( ', ', $orderby_array );

		// limit
		if ( isset( $qv['number'] ) && $qv['number'] ) {
			if ( $qv['offset'] )
				$this->query_limit = $db->prepare("LIMIT %d, %d", $qv['offset'], $qv['number']);
			else
				$this->query_limit = $db->prepare("LIMIT %d", $qv['number']);
		}

		$search = '';
		if ( isset( $qv['search'] ) )
			$search = trim( $qv['search'] );

		if ( $search ) {
			$leading_wild = ( ltrim($search, '*') != $search );
			$trailing_wild = ( rtrim($search, '*') != $search );
			if ( $leading_wild && $trailing_wild )
				$wild = 'both';
			elseif ( $leading_wild )
				$wild = 'leading';
			elseif ( $trailing_wild )
				$wild = 'trailing';
			else
				$wild = false;
			if ( $wild )
				$search = trim($search, '*');

			$search_columns = array();
			if ( $qv['search_columns'] )
				$search_columns = array_intersect( $qv['search_columns'], array( 'ID', 'user_login', 'user_email', 'user_status', 'user_url', 'user_nicename' ) );
			if ( ! $search_columns ) {
				if ( false !== strpos( $search, '@') )
					$search_columns = array('user_email');
				elseif ( is_numeric($search) )
					$search_columns = array('user_login', 'ID');
				elseif ( preg_match('|^https?://|', $search) && ! ( is_multisite() && ttm_is_large_network( 'users' ) ) )
					$search_columns = array('user_url');
				else
					$search_columns = array('user_login', 'user_nicename');
			}

			/**
			 * Filter the columns to search in a TTM_User_Query search.
			 *
			 * The default columns depend on the search term, and include 'user_email',
			 * 'user_login', 'ID', 'user_url', and 'user_nicename'.
			 *
			 * @since 3.6.0
			 *
			 * @param array         $search_columns Array of column names to be searched.
			 * @param string        $search         Text being searched.
			 * @param TTM_User_Query $this           The current TTM_User_Query instance.
			 */
			$search_columns = apply_filters( 'user_search_columns', $search_columns, $search, $this );

			$this->query_where .= $this->get_search_sql( $search, $search_columns, $wild );
		}

		if ( ! empty( $include ) ) {
			// Sanitized earlier.
			$ids = implode( ',', $include );
			$this->query_where .= " AND $db->users.ID IN ($ids)";
		} elseif ( ! empty( $qv['exclude'] ) ) {
			$ids = implode( ',', ttm_parse_id_list( $qv['exclude'] ) );
			$this->query_where .= " AND $db->users.ID NOT IN ($ids)";
		}

		// Date queries are allowed for the user_registered field.
		if ( ! empty( $qv['date_query'] ) && is_array( $qv['date_query'] ) ) {
			$date_query = new TTM_Date_Query( $qv['date_query'], 'user_registered' );
			$this->query_where .= $date_query->get_sql();
		}

		/**
		 * Fires after the TTM_User_Query has been parsed, and before
		 * the query is executed.
		 *
		 * The passed TTM_User_Query object contains SQL parts formed
		 * from parsing the given query.
		 *
		 * @since 3.1.0
		 *
		 * @param TTM_User_Query $this The current TTM_User_Query instance,
		 *                            passed by reference.
		 */
		do_action_ref_array( 'pre_user_query', array( &$this ) );
	}

	/**
	 * Execute the query, with the current variables.
	 *
	 * @since 3.1.0
	 *
	 * @global db $db TTM database object for queries.
	 */
	public function query() {
		global $db;

		$qv =& $this->query_vars;

		$query = "SELECT $this->query_fields $this->query_from $this->query_where $this->query_orderby $this->query_limit";

		if ( is_array( $qv['fields'] ) || 'all' == $qv['fields'] ) {
			$this->results = $db->get_results( $query );
		} else {
			$this->results = $db->get_col( $query );
		}

		/**
		 * Filter SELECT FOUND_ROWS() query for the current TTM_User_Query instance.
		 *
		 * @since 3.2.0
		 *
		 * @global db $db TTM database abstraction object.
		 *
		 * @param string $sql The SELECT FOUND_ROWS() query for the current TTM_User_Query.
		 */
		if ( isset( $qv['count_total'] ) && $qv['count_total'] )
			$this->total_users = $db->get_var( apply_filters( 'found_users_query', 'SELECT FOUND_ROWS()' ) );

		if ( !$this->results )
			return;

		if ( 'all_with_meta' == $qv['fields'] ) {
			cache_users( $this->results );

			$r = array();
			foreach ( $this->results as $userid )
				$r[ $userid ] = new TTM_User( $userid, '', $qv['blog_id'] );

			$this->results = $r;
		} elseif ( 'all' == $qv['fields'] ) {
			foreach ( $this->results as $key => $user ) {
				$this->results[ $key ] = new TTM_User( $user, '', $qv['blog_id'] );
			}
		}
	}

	/**
	 * Retrieve query variable.
	 *
	 * @since 3.5.0
	 * @access public
	 *
	 * @param string $query_var Query variable key.
	 * @return mixed
	 */
	public function get( $query_var ) {
		if ( isset( $this->query_vars[$query_var] ) )
			return $this->query_vars[$query_var];

		return null;
	}

	/**
	 * Set query variable.
	 *
	 * @since 3.5.0
	 * @access public
	 *
	 * @param string $query_var Query variable key.
	 * @param mixed $value Query variable value.
	 */
	public function set( $query_var, $value ) {
		$this->query_vars[$query_var] = $value;
	}

	/**
	 * Used internally to generate an SQL string for searching across multiple columns
	 *
	 * @access protected
	 * @since 3.1.0
	 *
	 * @param string $string
	 * @param array $cols
	 * @param bool $wild Whether to allow wildcard searches. Default is false for Network Admin, true for
	 *  single site. Single site allows leading and trailing wildcards, Network Admin only trailing.
	 * @return string
	 */
	protected function get_search_sql( $string, $cols, $wild = false ) {
		global $db;

		$searches = array();
		$leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
		$trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
		$like = $leading_wild . $this->esc_like( $string ) . $trailing_wild;

		foreach ( $cols as $col ) {
			if ( 'ID' == $col ) {
				$searches[] = $db->prepare( "$col = %s", $string );
			} else {
				$searches[] = $db->prepare( "$col LIKE %s", $like );
			}
		}

		return ' AND (' . implode(' OR ', $searches) . ')';
	}
	
	public function esc_like( $text ) {
		return addcslashes( $text, '_%\\' );
	}
	
	/**
	 * Return the list of users.
	 *
	 * @since 3.1.0
	 * @access public
	 *
	 * @return array Array of results.
	 */
	public function get_results() {
		return $this->results;
	}

	/**
	 * Return the total number of users for the current query.
	 *
	 * @since 3.1.0
	 * @access public
	 *
	 * @return int Number of total users.
	 */
	public function get_total() {
		return $this->total_users;
	}

	/**
	 * Parse and sanitize 'orderby' keys passed to the user query.
	 *
	 * @since 4.2.0
	 * @access protected
	 *
	 * @global db $db TTM database abstraction object.
	 *
	 * @param string $orderby Alias for the field to order by.
	 * @return string|bool Value to used in the ORDER clause, if `$orderby` is valid. False otherwise.
	 */
	protected function parse_orderby( $orderby ) {
		global $db;

		$meta_query_clauses = $this->meta_query->get_clauses();

		$_orderby = '';
		if ( in_array( $orderby, array( 'login', 'nicename', 'email', 'url', 'registered' ) ) ) {
			$_orderby = 'user_' . $orderby;
		} elseif ( in_array( $orderby, array( 'user_login', 'user_nicename', 'user_email', 'user_url', 'user_registered' ) ) ) {
			$_orderby = $orderby;
		} elseif ( 'name' == $orderby || 'display_name' == $orderby ) {
			$_orderby = 'display_name';
		} elseif ( 'post_count' == $orderby ) {
			// todo: avoid the JOIN
			$where = get_posts_by_author_sql( 'post' );
			$this->query_from .= " LEFT OUTER JOIN (
				SELECT post_author, COUNT(*) as post_count
				FROM $db->posts
				$where
				GROUP BY post_author
			) p ON ({$db->users}.ID = p.post_author)
			";
			$_orderby = 'post_count';
		} elseif ( 'ID' == $orderby || 'id' == $orderby ) {
			$_orderby = 'ID';
		} elseif ( 'meta_value' == $orderby || $this->get( 'meta_key' ) == $orderby ) {
			$_orderby = "$db->usermeta.meta_value";
		} elseif ( 'meta_value_num' == $orderby ) {
			$_orderby = "$db->usermeta.meta_value+0";
		} elseif ( 'include' === $orderby && ! empty( $this->query_vars['include'] ) ) {
			$include = ttm_parse_id_list( $this->query_vars['include'] );
			$include_sql = implode( ',', $include );
			$_orderby = "FIELD( $db->users.ID, $include_sql )";
		} elseif ( isset( $meta_query_clauses[ $orderby ] ) ) {
			$meta_clause = $meta_query_clauses[ $orderby ];
			$_orderby = sprintf( "CAST(%s.meta_value AS %s)", esc_sql( $meta_clause['alias'] ), esc_sql( $meta_clause['cast'] ) );
		}

		return $_orderby;
	}

	/**
	 * Parse an 'order' query variable and cast it to ASC or DESC as necessary.
	 *
	 * @since 4.2.0
	 * @access protected
	 *
	 * @param string $order The 'order' query variable.
	 * @return string The sanitized 'order' query variable.
	 */
	protected function parse_order( $order ) {
		if ( ! is_string( $order ) || empty( $order ) ) {
			return 'DESC';
		}

		if ( 'ASC' === strtoupper( $order ) ) {
			return 'ASC';
		} else {
			return 'DESC';
		}
	}

	/**
	 * Make private properties readable for backwards compatibility.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param string $name Property to get.
	 * @return mixed Property.
	 */
	public function __get( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name;
		}
	}

	/**
	 * Make private properties settable for backwards compatibility.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param string $name  Property to check if set.
	 * @param mixed  $value Property value.
	 * @return mixed Newly-set property.
	 */
	public function __set( $name, $value ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return $this->$name = $value;
		}
	}

	/**
	 * Make private properties checkable for backwards compatibility.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param string $name Property to check if set.
	 * @return bool Whether the property is set.
	 */
	public function __isset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			return isset( $this->$name );
		}
	}

	/**
	 * Make private properties un-settable for backwards compatibility.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param string $name Property to unset.
	 */
	public function __unset( $name ) {
		if ( in_array( $name, $this->compat_fields ) ) {
			unset( $this->$name );
		}
	}

	/**
	 * Make private/protected methods readable for backwards compatibility.
	 *
	 * @since 4.0.0
	 * @access public
	 *
	 * @param callable $name      Method to call.
	 * @param array    $arguments Arguments to pass when calling.
	 * @return mixed|bool Return value of the callback, false otherwise.
	 */
	public function __call( $name, $arguments ) {
		if ( 'get_search_sql' === $name ) {
			return call_user_func_array( array( $this, $name ), $arguments );
		}
		return false;
	}
}

/**
 * Retrieve list of users matching criteria.
 *
 * @since 3.1.0
 *
 * @see TTM_User_Query
 *
 * @param array $args Optional. Arguments to retrieve users. See {@see TTM_User_Query::prepare_query()}
 *                    for more information on accepted arguments.
 * @return array List of users.
 */
function get_users( $args = array() ) {

	$args = ttm_parse_args( $args );
	$args['count_total'] = false;

	$user_search = new TTM_User_Query($args);

	return (array) $user_search->get_results();
}

function get_users_by_status($status) {
	
	global $db;
	//return $db->get_results("SELECT * FROM {$db->users} INNER JOIN {$db->usermeta} ON `ID`=`user_id` WHERE `user_status`=$status AND `meta_key`='role' AND `meta_value`='member' ORDER BY `user_nicename`;");
	return $db->get_results("SELECT * FROM {$db->users} WHERE `user_status`=$status ORDER BY `user_nicename`;");
}