<?php
/**
 * TTM Admins Page
 *
 * Handles authentication, registering, resetting passwords, forgot password,
 * and other admins handling.
 *
 * @package TTM
 */

/** Make sure that the TTM bootstrap has run before continuing. */
require( dirname(__FILE__) . '/ttm-load.php' );

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';
function admin_login_header($title = 'Log In', $message = '', $ttm_error = '' ) {

	?>
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="UTF-8">
			<title><?php echo $title; ?> | TimTraMac</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="ttm-admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="ttm-admin/bower_components/font-awesome/css/font-awesome.min.css">
			<!-- Ionicons -->
			<link rel="stylesheet" href="ttm-admin/bower_components/Ionicons/css/ionicons.min.css">
			<!-- Theme style -->
			<link rel="stylesheet" href="ttm-admin/dist/css/AdminTTM.min.css">
			<!-- Theme style -->
			<link rel="stylesheet" href="ttm-admin/dist/css/custom.css">
			<!-- iCheck -->
			<link rel="stylesheet" href="ttm-admin/plugins/iCheck/square/blue.css">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<!-- Google Font -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		</head>
		<body class="login-page">
		<div class="wrapper" style="background-color: rgba(0, 74, 113, 0.2)">
			<div class="login-box">
			<div class="login-logo">
				<!--<a href="<?php echo get_site_url(); ?>"><?php echo ttm_get_attachment_image(get_option('site_logo'), 'full', array('title' => 'Website Logo', 'alt' => 'Website Logo')); ?></a>-->
				<a href="#"><img src="<?php echo get_option('home'); ?>/dist/img/logo.jpg" /></a>
			</div>
			<!-- /.login-logo -->
			<div class='login'>
				<?php
					if(empty($ttm_error))
						$ttm_error = new TTM_Error();
					//unset( $login_header_url, $login_header_title );
					
					/**
					 * Filter the message to display above the login form.
					 *
					 * @since 2.1.0
					 *
					 * @param string $message Login message text.
					 */
					$message = apply_filters( 'login_message', $message );
					if ( !empty( $message ) )
						echo $message . "\n";
					
					// In case a plugin uses $error rather than the $errors object
					/* if ( !empty( $error ) ) {
						$error->add('error', $error);
						unset($error);
					} */
					
					if ( $ttm_error->get_error_code() ) {
						$errors = '';
						$messages = '';
						foreach ( $ttm_error->get_error_codes() as $code ) {
							$severity = $ttm_error->get_error_data($code);
							foreach ( $ttm_error->get_error_messages($code) as $error ) {
								if ( 'message' == $severity )
									$messages .= '	' . $error . "<br />\n";
								else
									$errors .= '	' . $error . "<br />\n";
							}
						}
						if ( ! empty( $errors ) ) {
							/**
							 * Filter the error messages displayed above the login form.
							 *
							 * @since 2.1.0
							 *
							 * @param string $errors Login error message.
							 */
							echo '<div id="login_error">' . apply_filters( 'login_errors', $errors ) . "</div>\n";
						}
						if ( ! empty( $messages ) ) {
							/**
							 * Filter instructional messages displayed above the login form.
							 *
							 * @since 2.5.0
							 *
							 * @param string $messages Login messages.
							 */
							echo '<p class="message">' . apply_filters( 'login_messages', $messages ) . "</p>\n";
						}
					}
					?>
			</div>
	<?php
}

function admin_login_footer() {
	?>
			</div><!-- /.login-box -->
			<!-- jQuery 2.1.3 -->
			<script src="ttm-admin/plugins/jQuery/jQuery-2.1.3.min.js"></script>
			<!-- Bootstrap 3.3.2 JS -->
			 <script src="ttm-admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
			<script src="ttm-admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			</div>
		</body>
	</html>
	<?php
}

/**
 * Handles sending password retrieval email to user.
 *
 * @global db         $db      TTM database abstraction object.
 * @global PasswordHash $ttm_hasher Portable PHP password hashing framework.
 *
 * @return bool|TTM_Error True: when finish. TTM_Error on error
 */
function retrive_password() {
	global $db, $ttm_hasher, $util;

	$errors = new TTM_Error();

	if ( empty( $_POST['usernameoremail'] ) ) {
		$errors->add('empty_username', ('<strong>ERROR</strong>: Enter a username or e-mail address.'));
	} elseif ( strpos( $_POST['usernameoremail'], '@' ) ) {
		$user_data = get_user_by( 'email', trim( $_POST['usernameoremail'] ) );
		if ( empty( $user_data ) )
			$errors->add('invalid_email', ('<strong>ERROR</strong>: There is no user registered with that email address.'));
	} else {
		$login = trim($_POST['usernameoremail']);
		$user_data = get_user_by('login', $login);
	}

	/**
	 * Fires before errors are returned from a password reset request.
	 *
	 * @since 2.1.0
	 */
	do_action( 'forgotusernameorpassword_post' );

	if ( $errors->get_error_code() )
		return $errors;

	if ( !$user_data ) {
		$errors->add('invalidcombo', ('<strong>ERROR</strong>: Invalid username or e-mail.'));
		return $errors;
	}

	// Redefining user_login ensures we return the right case in the email.
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;
	$user_nicename =  $user_data->user_nicename;

	/**
	 * Fires before a new password is retrieved.
	 *
	 * @since 1.5.0
	 * @deprecated 1.5.1 Misspelled. Use 'retrieve_password' hook instead.
	 *
	 * @param string $user_login The user login name.
	 */
	do_action( 'retreive_password', $user_login );

	/**
	 * Fires before a new password is retrieved.
	 *
	 * @since 1.5.1
	 *
	 * @param string $user_login The user login name.
	 */
	do_action( 'retrieve_password', $user_login );

	/**
	 * Filter whether to allow a password to be reset.
	 *
	 * @since 2.7.0
	 *
	 * @param bool true           Whether to allow the password to be reset. Default true.
	 * @param int  $user_data->ID The ID of the user attempting to reset a password.
	 */
	$allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

	if ( ! $allow ) {
		return new TTM_Error( 'no_password_reset', ('Password reset is not allowed for this user') );
	} elseif ( is_ttm_error( $allow ) ) {
		return $allow;
	}

	// Generate something random for a password reset key.
	$key = ttm_generate_password( 20, false );

	/**
	 * Fires when a password reset key is generated.
	 *
	 * @since 2.5.0
	 *
	 * @param string $user_login The username for the user.
	 * @param string $key        The generated password reset key.
	 */
	do_action( 'retrieve_password_key', $user_login, $key );

	// Now insert the key, hashed, into the DB.
	if ( empty( $ttm_hasher ) ) {
		require_once ABSPATH . TTMINC . '/class-phpass.php';
		$ttm_hasher = new PasswordHash( 8, true );
	}
	$hashed = $ttm_hasher->HashPassword( $key );
	
	$db->update( $db->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );
	$link = get_site_url()."/ttm-login.php?action=rp&key=$key&login=" . rawurlencode($user_login);
	$message = $util -> get_mail_template('forgot_password', array('[SITE_URL]' => get_site_url(), '[USER]' => $user_nicename, '[LINK]' => $link, '[NETWORK]' => NETWORK));

	if ( is_multisite() )
		$blogname = $GLOBALS['current_site']->site_name;
	else
		/*
		 * The blogname option is escaped with esc_html on the way into the database
		 * in sanitize_option we want to reverse this for the plain text arena of emails.
		 */
		$blogname = NETWORK; //ttm_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$title = sprintf(('Password Reset'), $blogname );

	/**
	 * Filter the subject of the password reset email.
	 *
	 * @since 2.8.0
	 *
	 * @param string $title Default email title.
	 */
	$title = apply_filters( 'retrieve_password_title', $title );

	/**
	 * Filter the message body of the password reset mail.
	 *
	 * @since 2.8.0
	 * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
	 *
	 * @param string  $message    Default mail message.
	 * @param string  $key        The activation key.
	 * @param string  $user_login The username for the user.
	 * @param TTM_User $user_data  TTM_User object.
	 */
	//$message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );
	
	
	$headers='MIME-Version: 1.0' . "\r\n";
	$headers .='Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= "From: info@airansoft.com" . "\r\n";
	
	if ( !mail($user_email,ttm_specialchars_decode( $title ),$message,$headers) )
		ttm_die( ('The e-mail could not be sent.') . "<br />\n" . ('Possible reason: your host may have disabled the mail() function.') );
	
	return true;
}

switch($action){
	case 'logout' :
		/* check_admin_referer('log-out');

		$user = ttm_get_current_user(); */

		ttm_logout();

		if ( ! empty( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = $requested_redirect_to = $_REQUEST['redirect_to'];
		} else {
			$redirect_to = 'ttm-login.php?loggedout=true';
			$requested_redirect_to = '';
		}

		/**
		 * Filter the log out redirect URL.
		 *
		 * @since 4.2.0
		 *
		 * @param string  $redirect_to           The redirect destination URL.
		 * @param string  $requested_redirect_to The requested redirect destination URL passed as a parameter.
		 * @param TTM_User $user                  The TTM_User object for the user that's logging out.
		 */
		$redirect_to = apply_filters( 'logout_redirect', $redirect_to, $requested_redirect_to, $user );
		ttm_safe_redirect( $redirect_to );
	exit();
	case 'forgotpassword': 
		if(!empty($_POST['usernameoremail'])) {
			$errors = retrive_password();
			print_R($errors);
			if ( !is_ttm_error($errors) ) {
				$redirect_to = !empty( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : 'ttm-login.php?checkemail=confirm';
				ttm_safe_redirect( $redirect_to );
				exit();
			}
		}
		
		if ( isset( $_GET['error'] ) ) {
			$errors = new TTM_Error();
			if ( 'invalidkey' == $_GET['error'] )
				$errors->add( 'invalidkey', ( 'Sorry, that key does not appear to be valid.' ) );
			elseif ( 'expiredkey' == $_GET['error'] )
				$errors->add( 'expiredkey', ( 'Sorry, that key has expired. Please try again.' ) );
		}
		
		/**
		 * Fires before the forgot username or password form.
		 *
		 * @since 1.5.1
		 */
		do_action( 'lost_password' );

		admin_login_header(('Forgot username or password'), '<p class="message">' . ('Please enter your username or email address. You will receive a link to create a new password via email.') . '</p>', $errors);	
	?>
	<div class="login-box-body">
		<!--<p class="login-box-msg">Please enter your username or email address. You will receive a link to create a new password via email.</p>-->
		<form name="login-form" action="" method="post" id="login-form" >
			<div class="form-group has-feedback">
				<input name="usernameoremail" type="text" class="form-control" placeholder="Username or Email"/>
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-6">                       
				</div>
				<!-- /.col -->
				<div class="col-xs-6">
					<input name='get_new_password' type="submit" value="Get New Password" class="btn bg-blue btn-block ">
				</div>
				<!-- /.col -->
			</div>
		</form>
		<a href="<?php echo get_site_url(); ?>/ttm-login.php">Log in</a>
	</div>
	<!-- /.login-box-body -->
	<?php
	admin_login_footer();
	break;
	
	case 'resetpass' :
	case 'rp' :
		list( $rp_path ) = explode( '?', ttm_unslash( $_SERVER['REQUEST_URI'] ) );
		$rp_cookie = 'ttm-resetpass-' . COOKIEHASH;
		if ( isset( $_GET['key'] ) ) {
			$value = sprintf( '%s:%s', ttm_unslash( $_GET['login'] ), ttm_unslash( $_GET['key'] ) );
			setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
			ttm_safe_redirect( remove_query_arg( array( 'key', 'login' ) ) );
			exit;
		}
		if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
			list( $rp_login, $rp_key ) = explode( ':', ttm_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
			$user = check_password_reset_key( $rp_key, $rp_login );
			if ( isset( $_POST['new_pswd'] ) && ! hash_equals( $rp_key, $_POST['rp_key'] ) ) {
				$user = false;
			}
		} else {

			$user = false;
		}

		if ( ! $user || is_ttm_error( $user ) ) {
			setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
			if ( $user && $user->get_error_code() === 'expired_key' )
				ttm_redirect( get_site_url().'/ttm-login.php?action=forgotusernameorpassword&error=expiredkey' );
			else
				ttm_redirect( get_site_url().'/ttm-login.php?action=forgotusernameorpassword&error=invalidkey' );
			exit;
		}

		$errors = new TTM_Error();

		if ( isset($_POST['new_pswd']) && $_POST['new_pswd'] != $_POST['cfm_new_pswd'] )
			$errors->add( 'password_reset_mismatch', ( 'The passwords do not match.' ) );

		/**
		 * Fires before the password reset procedure is validated.
		 *
		 * @since 3.5.0
		 *
		 * @param object           $errors TTM Error object.
		 * @param TTM_User|TTM_Error $user   TTM_User object if the login and reset key match. TTM_Error object otherwise.
		 */
		do_action( 'validate_password_reset', $errors, $user );

		if ( ( ! $errors->get_error_code() ) && isset( $_POST['new_pswd'] ) && !empty( $_POST['new_pswd'] ) ) {
			reset_password($user, $_POST['new_pswd']);
			setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
			admin_login_header( ( 'Password Reset' ), '<p class="message reset-pass">' . ( 'Your password has been reset.' ) . ' <a href="/ttm-login.php">' . ( 'Log in' ) . '</a></p>' );
			admin_login_footer();
			exit;
		}

		/* ttm_enqueue_script('utils');
		ttm_enqueue_script('user-profile'); */

		admin_login_header(('Reset Password'), '<p class="message reset-pass">' . ('Enter your new password below.') . '</p>', $errors );
	?>
	<div class="login-box-body">
		<p class="login-box-msg"></p>
		<form name="login-form" action="" method="post" id="login-form" >
			<div class="form-group">
				<input type="password" class="form-control" name="new_pswd" placeholder="New Password">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="form-group">
				<input type="password" class="form-control" name="cfm_new_pswd" placeholder="Confirm New Password">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-6">                       
				</div>
				<!-- /.col -->
				<div class="col-xs-6">
					<?php
						/**
						 * Fires following the 'Strength indicator' meter in the user password reset form.
						 *
						 * @since 3.9.0
						 *
						 * @param TTM_User $user User object of the user whose password is being reset.
						 */
						do_action( 'resetpass_form', $user );
						?>
					<input type="hidden" name="rp_key" value="<?php echo esc_attr( $rp_key ); ?>" />
					<input type="submit" name="ttm-submit" id="ttm-submit" class="btn btn-primary btn-block btn-flat" value="<?php echo 'Reset Password' ?>" />
				</div>
				<!-- /.col -->
			</div>
		</form>
	</div>
	<!-- /.login-box-body -->
	<?php
	
	admin_login_footer();
	break;
	case 'login' :
	default:
		$secure_cookie = '';
		// If the user wants ssl but the session is not ssl, force a secure cookie.
		if ( !empty($_POST['log']) && !force_ssl_admin() ) {
			$user_name = sanitize_user($_POST['log']);
			if ( $user = get_user_by('login', $user_name) ) {
				//if ( get_user_option('use_ssl', $user->ID) ) {
					$secure_cookie = false;
					force_ssl_admin(false);
				//}
			}
		}

		if ( isset( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = $_REQUEST['redirect_to'];
			// Redirect to https if user wants ssl
			if ( $secure_cookie && false !== strpos($redirect_to, 'ttm-admin') )
				$redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
		} else {
			$redirect_to = 'ttm-admin'; /* user_url(); */
		}

		$reauth = empty($_REQUEST['reauth']) ? false : true;

		$user = new TTM_Error;
		if($_POST['login']):
			if(empty($_POST['log'])) {
				$user->add('empty_username', 'Empty Username!' );
			} elseif(empty($_POST['pwd'])) {
				$user = new TTM_Error;
				$user->add('empty_password', 'Empty Password!' );
			} else {
				$user = ttm_signon( '', $secure_cookie );
			}
		endif;

		if ( empty( $_COOKIE[ LOGGED_IN_COOKIE ] ) ) {
			if ( headers_sent() ) {
				$user = new TTM_Error( 'test_cookie', sprintf( ( '<strong>ERROR</strong>: Cookies are blocked due to unexpected output. For help, please see <a href="%1$s">this documentation</a> or try the <a href="%2$s">support forums</a>.' ),
					( 'https://codex.ttm.org/Cookies' ), ( 'https://ttm.org/support/' ) ) );
			} elseif ( isset( $_POST['testcookie'] ) && empty( $_COOKIE[ TEST_COOKIE ] ) ) {
				// If cookies are disabled we can't log in even with a valid user+pass
				$user = new TTM_Error( 'test_cookie', sprintf( ( '<strong>ERROR</strong>: Cookies are blocked or not supported by your browser. You must <a href="%s">enable cookies</a> to use TTM.' ),
					( 'https://codex.ttm.org/Cookies' ) ) );
			}
		}

		$requested_redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '';
		/**
		 * Filter the login redirect URL.
		 *
		 * @since 3.0.0
		 *
		 * @param string           $redirect_to           The redirect destination URL.
		 * @param string           $requested_redirect_to The requested redirect destination URL passed as a parameter.
		 * @param TTM_User|TTM_Error $user                  TTM_User object if login was successful, TTM_Error object otherwise.
		 */
		$redirect_to = apply_filters( 'login_redirect', $redirect_to, $requested_redirect_to, $user );

		if ( !is_ttm_error($user) && !$reauth ) {
			if ( $interim_login ) {
				$message = '<p class="message">' . ('You have logged in successfully.') . '</p>';
				$interim_login = 'success';
				login_header( '', $message ); ?>
				</div>
				<?php
				/** This action is documented in ttm-login.php */
				do_action( 'login_footer' ); ?>
				<?php if ( $customize_login ) : ?>
					<script type="text/javascript">setTimeout( function(){ new ttm.customize.Messenger({ url: '<?php echo ttm_customize_url(); ?>', channel: 'login' }).send('login') }, 1000 );</script>
				<?php endif; ?>
				</body></html>
	<?php		exit;
			}

			/* if ( ( empty( $redirect_to ) || $redirect_to == 'ttm-admin/' || $redirect_to == 'ttm-admin'/* admin_url() */ //) ) { */
				// If the user doesn't belong to a blog, send them to user admin. If the user can't edit posts, send them to their profile.
				/* if ( is_multisite() && !get_active_blog_for_user($user->ID) && !is_super_admin( $user->ID ) )
					$redirect_to = user_admin_url();
				elseif ( is_multisite() && !$user->has_cap('read') )
					$redirect_to = get_dashboard_url( $user->ID );
				elseif ( !$user->has_cap('edit_posts') )
					$redirect_to = admin_url('profile.php');
			} */
			ttm_safe_redirect($redirect_to);
			exit();
		}

		$errors = $user;
		// Clear errors if loggedout is set.
		if ( !empty($_GET['loggedout']) || $reauth )
			$errors = new TTM_Error();

		if ( $interim_login ) {
			if ( ! $errors->get_error_code() )
				$errors->add('expired', ('Session expired. Please log in again. You will not move away from this page.'), 'message');
		} else {
			// Some parts of this script use the main login form to display a message
			if		( isset($_GET['loggedout']) && true == $_GET['loggedout'] )
				$errors->add('loggedout', ('You are now logged out.'), 'message');
			elseif	( isset($_GET['registration']) && 'disabled' == $_GET['registration'] )
				$errors->add('registerdisabled', ('User registration is currently not allowed.'));
			elseif	( isset($_GET['checkemail']) && 'confirm' == $_GET['checkemail'] )
				$errors->add('confirm', ('Check your e-mail for the confirmation link.'), 'message');
			elseif	( isset($_GET['checkemail']) && 'nettmass' == $_GET['checkemail'] )
				$errors->add('nettmass', ('Check your e-mail for your new password.'), 'message');
			elseif	( isset($_GET['checkemail']) && 'registered' == $_GET['checkemail'] )
				$errors->add('registered', ('Registration complete. Please check your e-mail.'), 'message');
			elseif ( strpos( $redirect_to, 'about.php?updated' ) )
				$errors->add('updated', ( '<strong>You have successfully updated TTM!</strong> Please log back in to see what&#8217;s new.' ), 'message' );
			elseif	( isset($_GET['invalid']) && true == $_GET['invalid'] )
				$errors->add('invalid', ('Unauthorised user to access the TTM (AMD) Admin Panel ')); 
			elseif	( isset($_GET['inactive']) && true == $_GET['inactive'] )
				$errors->add('inactive', ('You account is set to inactive. Please contact to Support Team')); 
				
		}

		/**
		 * Filter the login page errors.
		 *
		 * @since 3.6.0
		 *
		 * @param object $errors      TTM Error object.
		 * @param string $redirect_to Redirect destination URL.
		 */
		$errors = apply_filters( 'ttm_login_errors', $errors, $redirect_to );

		// Clear any stale cookies.
		if ( $reauth )
			ttm_clear_auth_cookie();

		admin_login_header(('Log In'), '', $errors);

		if ( isset($_POST['log']) )
			$user_login = ( 'incorrect_password' == $errors->get_error_code() || 'empty_password' == $errors->get_error_code() ) ? esc_attr(ttm_unslash($_POST['log'])) : '';
		$rememberme = ! empty( $_POST['rememberme'] );

		if ( ! empty( $errors->errors ) ) {
			$aria_describedby_error = ' aria-describedby="login_error"';
		} else {
			$aria_describedby_error = '';
		}
		
		?>
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>
			<form name="login-form" action="<?php $_SERVER['HTTP_SELF']; ?>" method="post" id="login-form" >
				<div class="form-group has-feedback">
					<input name="log" type="text" class="form-control" placeholder="Username"/>
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input name="pwd" type="password" class="form-control" placeholder="Password"/>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<!--<div class="checkbox icheck">
							<label>
							<input name="rememberme" type="checkbox" > Remember Me
							</label>
						</div>-->
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<input name='login' type="submit" value="Sign In" class="btn bg-blue btn-block ">
					</div>
					<!-- /.col -->
				</div>
			</form>
			<a href="<?php echo ttm_forgotpassword_url() ; ?>">I forgot my password</a>
		</div>
		<!-- /.login-box-body -->					
		<script type="text/javascript">
			function ttm_attempt_focus(){
			setTimeout( function(){ try{
			<?php if ( $user_login ) { ?>
			d = document.getElementById('user_pass');
			d.value = '';
			<?php } else { ?>
			d = document.getElementById('user_login');
			<?php if ( 'invalid_username' == $errors->get_error_code() ) { ?>
			if( d.value != '' )
			d.value = '';
			<?php
				}
				}?>
			d.focus();
			d.select();
			} catch(e){}
			}, 200);
			}
			
			<?php if ( !$error ) { ?>
			ttm_attempt_focus();
			<?php } ?>
			if(typeof ttmOnload=='function')ttmOnload();
			<?php if ( $interim_login ) { ?>
			(function(){
			try {
				var i, links = document.getElementsByTagName('a');
				for ( i in links ) {
					if ( links[i].href )
						links[i].target = '_blank';
				}
			} catch(e){}
			}());
			<?php } ?>
		</script>
	<?php
admin_login_footer();
break;
}
?>