<?php
/**
 * The base configurations of the TTM.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, TTM Language, and ABSPATH. 
 * You can get the MySQL settings from your web host.
 *
 * This file is used by the ttm-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "ttm-config.php" and fill in the values.
 *
 * @package TTM
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for TTM */
define('DB_NAME', 'airanu5b_ttm');

/** MySQL database username */
define('DB_USER', 'airanu5b_ttm');

/** MySQL database password */
define('DB_PASSWORD', 'ttm@2017');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('USE_EXT_MYSQL', false);

/**
 * Set common path
 */
define('SITE_URL', 'http://www.airansoft.com/dev/ttm/');
define('BASEPATH', dirname(__FILE__).'/');

/** Absolute path to the TTM directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

if ( !defined('IMAGEPATH') )
	define('IMAGEPATH', dirname(__FILE__) . '');
	
define( 'TTM_CONTENT_DIR', ABSPATH . 'ttm-content' );

/**
 * Set assets path
 */
define('THEME', 'TTM');
define('CLASSPATH', BASEPATH.'classes/');
define('CSSPATH', SITE_URL.'assets/css/');
define('JQUERYPATH', SITE_URL.'assets/jquery/');

/**#@-*/

/**
 * TTM Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ttm_';


/**
 * For developers: AMD debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use ttm_DEBUG
 * in their development environments.
 */
define('TTM_DEBUG', false);

define('TTM_DEBUG_LOG', false);

define('TTM_DEBUG_DISPLAY', false);

/** Sets up TTM vars and included files. */
require_once(ABSPATH . 'ttm-settings.php');

define( 'COOKIE_DOMAIN', 'TimTraMac' );

if ( ! defined('TTM_INSTALLING') ) {
	// Used to guarantee unique hash cookies
	$cookiehash = md5(get_site_url());
	/**
	 * Used to guarantee unique hash cookies
	 * @since 1.5
	 */
	define('COOKIEHASH', $cookiehash);
}

// Constants for expressing human-readable intervals
// in their respective number of seconds.
define( 'MINUTE_IN_SECONDS', 60 );
define( 'HOUR_IN_SECONDS',   60 * MINUTE_IN_SECONDS );
define( 'DAY_IN_SECONDS',    24 * HOUR_IN_SECONDS   );
define( 'WEEK_IN_SECONDS',    7 * DAY_IN_SECONDS    );
define( 'YEAR_IN_SECONDS',  365 * DAY_IN_SECONDS    );

?>