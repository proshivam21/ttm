<section class="content">

	<div class="row">

		<div class="col-xs-12">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-user-circle-o" aria-hidden="true"></i>

					<h3 class="box-title">Clients</h3>

					<a class="btn btn-info pull-right" href="?page=client-contacts&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New Client</a>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Client Name</th>

								<th>Client Email</th>

								<th>Client Phone No.</th>

								<th>Cost Center ID</th>

								<th>Comment</th>

								<th>Status</th>

								<th>Actions</th>

							</tr>

						</thead>

						<tbody>

							<?php 
								$client_contact_info = get_info_by_company_client_contact();
							
								$i=1;
								foreach($client_contact_info as $client_contact){
									
									echo '<tr class="per_company'.$client_contact->ID.'">

									<td>'.$i.'</td>

									<td>'.ucwords($client_contact -> client_first_name.' '.$client_contact -> client_last_name).'</td>

									<td>'.$client_contact ->client_email_address.'</td>
									
									<td>'.$client_contact ->client_phone_no.'</td>

									<td>'.$client_contact ->cost_center_id.'</td>

									<td>'.$client_contact ->comment.'</td>

									<td class="company-status">';
									
									$status = $client_contact -> status;
									if($status == 1) echo 'Active';									
									else echo 'Inactive';
									
									echo '</td>

									<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=client-contacts&action=edit&id='.$client_contact ->ID.'"><span class="glyphicon glyphicon-pencil"></span></a>
										
										<a title="Delete" onclick="delete_client_contact('.$client_contact ->ID.')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> ';
									
										if($status == 1){
											$action = 'deactivate_client_contact';
											$title = "Inactive Client Contact";
											$status_class = 'fa-undo';
										}
										else{ 
											$action = 'activate_client_contact';
											$title = "Active Client Contact";
											$status_class = 'fa-check';
										}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick="'.$action.'('.$client_contact ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
									
									echo '</td>

								</tr>';
								
								$i++;
								
								}  							
							?>

						</tbody>

					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>