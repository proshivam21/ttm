<?php 
	$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
	
	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> add_user();
		if($message['flag'] == 1){
			global $cc;
			$user_id = $message['user_id'];
			$message = $cc -> add_client_contact($user_id);
		}
	}
?>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" name="" id="client_contact">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user"><?php echo ' '.ucwords($_POST['first_name'].' '.$_POST['last_name']); ?></i></h3>

						<div class="pull-right">
							
							<a class="btn bg-red" href="?page=client-contacts">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info">Save</button>

						</div>

					</div>
					
					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo $_POST['first_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name</label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo $_POST['last_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="phone_number" class="col-sm-2 control-label">Phone No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="phone_number" type="tel" class="form-control" value="<?php echo $_POST['phone_number']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="email_address" class="col-sm-2 control-label">Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email" type="tel" class="form-control" value="<?php echo $_POST['email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="password" class="col-sm-2 control-label">Password<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="">

							</div>

						</div>
						
						<input type="hidden" name="role" value="6">

						<div class="form-group">

							<label for="company" class="col-sm-2 control-label">Client Company<sup>*</sup></label>

							<div class="col-sm-10">
								
								<select class="form-control select2" style="width: 100%;" name="client_company_id">
									<option value="">Select</option>
									<?php		
										$client_company_info = get_client_company_info(array('status' => 1) );
										foreach($client_company_info as $cc_info){
											$select = '';
											if($_POST['client_company_id'] == $cc_info->ID) $select = 'selected';
											echo '<option value="'.$cc_info->ID.'" '.$select.'>'.ucwords($cc_info->company_name).'</option>';
										}
									?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Cost Center ID<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="cost_center_id" type="text" class="form-control" value="<?php echo $_POST['cost_center_id']; ?>">

							</div>

						</div>

						
						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								<?php
									$status_info = array('1' => 'Yes', '0' => 'No');
									foreach($status_info as $k => $v){
										$select = '';
										if( (isset($_POST['status']) && $_POST['status'] == $k) || $k==1) $select = 'selected';
										echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';	
									}
								  ?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Type Here...."><?php echo $_POST['comment']; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>