<?php
	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> update_user();
	} 
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="super_admin_sub_user_signup">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user-secret"></i> <?php echo get_user_meta($id, 'first_name', 'true').' '.get_user_meta($id, 'last_name', 'true'); ?></h3>

						<div class="pull-right">

							<a class="btn bg-red" href="?page=system-admin-users">Discard</a>&nbsp;&nbsp;

							<!-- <button type="submit" class="btn btn-info" name="update">Update</button> -->
							
							<input type="submit" class="btn btn-info" name="update" value="Update">
						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo get_user_meta($id, 'first_name', true)?>">

							</div>

						</div>
						
						
						<input type="hidden" name="id" value="<?php echo $id; ?>">
						<input type="hidden" name="role" value="7">
						

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo get_user_meta($id, 'last_name', true)?>">

							</div>

						</div>

						
						
						<div class="form-group">

							<label for="user_email" class="col-sm-2 control-label">User Email<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email" type="email" class="form-control" value="<?php echo $user_info->user_email?>" disabled>

							</div>

						</div>

						<div class="form-group">

							<label for="user_pass" class="col-sm-2 control-label">User Password</label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="">

							</div>

						</div> 

						<div class="form-group">

							<label for="dob" class="col-sm-2 control-label">DOB<sup>*</sup></label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" class="form-control pull-right datepicker" value="<?php echo get_user_meta($id, 'dob', true); ?>" name="dob">

								</div>

							</div>

						</div>
						

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">
								
								<?php 
									$status_info = array('0' => 'No', '1' => 'Yes');
									$old_status = $user_info -> user_status;
									foreach($status_info as $k => $v){
										$select = '';
									
										if($old_status == $k){ $select = 'selected';
											echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';
										}
										else echo '<option value="'.$k.'">'.$v.'</option>';
									}
								?>
								</select>
							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->
</section>