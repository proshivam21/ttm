<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-user-secret" aria-hidden="true"></i>
					<h3 class="box-title">Admin Users</h3>
					<a class="btn btn-info pull-right" href="?page=system-admin-users&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New System Admin User</a>
				</div>
				
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>User Email</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						<?php 															
							
							global $db;		
							$i=1;
							$user_info = $db -> get_results("SELECT *FROM {$db -> users} where role_id = 7");		
							foreach($user_info as $info): 
						?>
								<tr class="per_user<?php echo $info -> ID; ?>">									
									<td><?php echo $i; ?></td>
									<td><?php echo ucfirst(get_user_meta($info -> ID, 'first_name', true)); ?></td>
									<td><?php echo ucfirst(get_user_meta($info -> ID, 'last_name', true)); ?></td>
									<td><?php echo $info -> user_email; ?></td>
									<td>										
										<?php	
											$status = $info -> user_status;
											if($status == 1) echo 'Active';									
											else echo 'Inactive';
										?>									
									</td>
									
									<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=system-admin-users&action=edit&id=<?php echo $info -> ID; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
										<a title="Delete" onclick="delete_user(<?php echo $info -> ID; ?>)" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
										<?php
											
											if($status == 1){
												$user_status = 'Active';
												$action = 'deactivate_user';
												$title = "Inactive User";
												$status_class = 'fa-undo';
											}
											else{ 
												$user_status = 'Inactive';
												$action = 'activate_user';
												$title = "Active User";
												$status_class = 'fa-check';
											}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick=" '.$action.'('.$info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
										?>
									</td>
								</tr>
							<?php 
								$i++;
								endforeach; 
							?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>