<?php 
	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> add_user();
	} 
?>

<section class="content">
	<?php 
		if($message['flag'] == 1){
			$_POST = '';		
		}
		
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="super_admin_sub_user_signup">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user-secret"></i> </h3>

						<div class="pull-right">
						
							<a class="btn bg-red" href="?page=system-admin-users">Discard</a>&nbsp;&nbsp;
							
							<button type="submit" class="btn btn-info" name="submit">Save</button>

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo $_POST['first_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo $_POST['last_name']; ?>">

							</div>

						</div>
						
						<input type="hidden" name="role" value="7">

						<div class="form-group">

							<label for="user_email" class="col-sm-2 control-label">User Email<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email" type="email" class="form-control" value="<?php echo $_POST['email']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="user_pass" class="col-sm-2 control-label">User Password<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="<?php echo $_POST['password']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="dob" class="col-sm-2 control-label">DOB<sup>*</sup></label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" class="form-control pull-right datepicker" name="dob" value="<?php echo $_POST['dob']; ?>">

								</div>

							</div>

						</div>

						

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <option value="1" selected="selected">Yes</option>

								  <option value="0">No</option>

								</select>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>