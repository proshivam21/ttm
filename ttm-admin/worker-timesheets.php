<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-xs-12 col-md-3">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Week</option>
				  <option>Oct 2 – Oct 8, 2017</option>
				  <option>Oct 16 – Oct 22, 2017</option>
				  <option>Oct 23 – Oct 29, 2017</option>
				  <option>Oct 30 – Nov 5, 2017</option>
				  <option>Nov 6 – Nov 12, 2018</option>
				</select>
			</div>
			<div class="col-xs-12 col-md-3">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Month</option>
				  <option>Jan</option>
				  <option>Feb</option>
				  <option>Mar</option>
				  <option>Apr</option>
				  <option>May</option>
				  <option>Jun</option>
				  <option>July</option>
				  <option>Aug</option>
				  <option>Sep</option>
				  <option>Oct</option>
				  <option>Nov</option>
				  <option>Dec</option>
				</select>
			</div>
			<div class="col-xs-12 col-md-3">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Year</option>
				  <option>2017</option>
				  <option>2018</option>
				  <option>2019</option>
				  <option>2020</option>
				  <option>2021</option>
				</select>
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-info">Select</button>
			</div>
			<div class="col-xs-1"></div>
		</div>
		<div class="col-xs-12">
			<!-- The time line -->
			<ul class="timeline">
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					2018
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th rowspan="3">Timesheet Name</th>
											<th rowspan="3">Status</th>
											<th colspan="6" class="text-center">Status</th>
										</tr>
										<tr>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Open</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Overdue</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Sent</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Rejected</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th colspan="7" class="text-center">
												<select class="selectpicker" data-width="fit">
													<option>Approved</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
										</tr>
										<tr>
											<th colspan="4"></th>
											<th>PM</th>
											<th>LM</th>
										</tr>
									</thead>
									<tbody>		
										
										<?php	
											global $db;
											$ts_info = $db -> get_results("SELECT *FROM {$db->timsheets} where company_id = {$current_user_company_id} order by year asc");
											
											foreach($ts_info as $info){													
										?>										
												<tr>
													<td><a href="?page=timesheet&ID=<?php echo $info ->ID; ?>"><?php echo $info -> timesheet_name; ?></a></td>
													<td>Open</td>
													<td><i class="fa fa-check-square-o"></i></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
										<?php 
											}
																					
										?>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				
			</ul>
		</div>
	</div>
	<!-- /.row -->
</section>