<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Bar chart -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-bar-chart-o"></i>
					<h3 class="box-title">Companies Account</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div id="companies-accounts" style="height: 300px;"></div>
				</div>
				<!-- /.box-body-->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
		<div class="col-md-12">
			<!-- Bar chart -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<i class="fa fa-bar-chart-o"></i>
					<h3 class="box-title">Companies User Account</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div id="companies-user-accounts" style="height: 300px;"></div>
				</div>
				<!-- /.box-body-->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->