<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-xs-12 col-md-5">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Project</option>
				  <option>TimTraMac</option>
				  <option>TimTraMac</option>
				  <option>TimTraMac</option>
				  <option>TimTraMac</option>
				  <option>TimTraMac</option>
				  <option>TimTraMac</option>
				</select>
			</div>
			<div class="col-xs-12 col-md-5">
				<select class="form-control select2" style="width: 100%;">
					<option selected="selected">Year</option>
					<option>2017</option>
					<option>2016</option>
					<option>2015</option>
					<option>2014</option>
					<option>2013</option>
				</select>
			</div>
			<div class="col-md-1">
				<button type="submit" class="btn btn-info">Select</button>
			</div>
			<div class="col-xs-1"></div>
		</div>
		<div class="col-xs-12">
			<!-- The time line -->
			<ul class="timeline">
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					TimTraMac
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">2017</h3>
								<div class="pull-right"><i class="fa fa-check-square-o"></i> Client Approval</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Period</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>PTS Ready</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>Client Name</th>
											<th>Send</th>
											<th>PTS Signed</th>
											<th>Send to Factoring</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Oct 2 – Oct 8, 2017</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
										</tr>
										<tr>
											<td>Oct 16 – Oct 22, 2017</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-remove"></i> No</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 23 – Oct 29, 2017</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 30 – Nov 5, 2017</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
										</tr>
										<tr>
											<td>Nov 6 – Nov 12, 2017</td>
											<td>No</td>
											<td>Shivam Agrawal</td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">2016</h3>
								<div class="pull-right"><i class="fa fa-check-square-o"></i> Client Approval</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Period</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>PTS Ready</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>Client Name</th>
											<th>Send</th>
											<th>PTS Signed</th>
											<th>Send to Factoring</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Oct 2 – Oct 8, 2016</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
										</tr>
										<tr>
											<td>Oct 16 – Oct 22, 2016</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-remove"></i> No</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 23 – Oct 29, 2016</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 30 – Nov 5, 2016</td>
											<td>Yes</td>
											<td>Shivam Agrawal</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
										</tr>
										<tr>
											<td>Nov 6 – Nov 12, 2016</td>
											<td>No</td>
											<td>Shivam Agrawal</td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					CRM Project
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">2017</h3>
								<div class="pull-right"><i class="fa fa-remove"></i> Client Approval</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Period</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>PTS Ready</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>Print</th>
											<th>Send</th>
											<th>PTS Signed</th>
											<th>Send to Factoring</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Oct 2 – Oct 8, 2017</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
										</tr>
										<tr>
											<td>Oct 16 – Oct 22, 2017</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><input type="checkbox" class="minimal-blue-" data-toggle="modal" data-target="#modal-store"></td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 23 – Oct 29, 2017</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 30 – Nov 5, 2017</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
										</tr>
										<tr>
											<td>Nov 6 – Nov 12, 2017</td>
											<td>No</td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">2016</h3>
								<div class="pull-right"><i class="fa fa-remove"></i> Client Approval</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Period</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>PTS Ready</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>Print</th>
											<th>Send</th>
											<th>PTS Signed</th>
											<th>Send to Factoring</th>
											
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Oct 2 – Oct 8, 2016</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
										</tr>
										<tr>
											<td>Oct 16 – Oct 22, 2016</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><input type="checkbox" class="minimal-blue-" data-toggle="modal" data-target="#modal-store"></td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 23 – Oct 29, 2016</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
										<tr>
											<td>Oct 30 – Nov 5, 2016</td>
											<td>Yes</td>
											<td><a class="btn bg-blue" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><i class="fa fa-check-square-o"></i> Sent</td>
											<td><i class="fa fa-check-square-o"></i> Yes</td>
											
											<td><a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
										</tr>
										<tr>
											<td>Nov 6 – Nov 12, 2016</td>
											<td>No</td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-print" aria-hidden="true"></i> Print</a></td>
											<td><a class="btn bg-blue disabled" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a></td>
											<td>-</td>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
			</ul>
		</div>
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="modal-store">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Upload Signed Document</h4>
	  </div>
	  <div class="modal-body">
		<div class="file-loading">
			<input class="file_uploader" name="signed_pts" type="file" multiple>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary">Store</button>
	  </div>
	</div>
	<!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->