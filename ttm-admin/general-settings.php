<?php
	$cur_user_id = ttm_get_session('id');
	$user_info   = get_userdata($cur_user_id);
	$company_id  = $user_info->company_id;
	
	if($_POST['save']) {
		extract($_POST);
		foreach ($_POST as $key => $value) {
			if ($key != 'save') {
				if (!empty($value))
					update_option($key, $value);
			}
		}
		$message = array(
			'title' => 'Success',
			'type' => 'success',
			'message' => 'Your settings has been saved successfully.'
		);
	}
	
	if($_POST['save1']){
		extract($_POST);
		
		$period_type = $period.$current_user_company_id;
		
		foreach ($_POST as $key => $value) {
			if ($key == "period{$current_user_company_id}") {
				
				if (!empty($value)) update_option($key, $value);
		
				$y = date('Y');													
				
				global $db;
				if($value == "weekly"){
					$count_weeks = date('W', mktime(0,0,0,12,28,$i) );						
					
					for($j=0; $j<$count_weeks; $j++){										
						$arr = getStartAndEndDate($j,$i);										
						
						$week_info = array(
										'company_id'		=>	$current_user_company_id,
										'timesheet_name'	=>	date('M d', strtotime($arr[0]) ).' - '.date('M d', strtotime($arr[1]) ).', '.date('Y', strtotime($arr[1])),
										'start_date'		=>	$arr[0],
										'end_date'			=>	$arr[1],
										'year'				=>	$y,
										'comments'			=>	'',
										'status'			=>	1,
									);
						$db->insert($db->timsheets, $week_info);
					}
				}										
				
				if($value == 'monthly'){
					
					for($j=1; $j<=12; $j++){	
						
						$timestamp  = strtotime(date('F', mktime(0,0,0,$j, 1, date('Y'))).' '.$y);
						$first = date('Y-m-01 00:00:00', $timestamp);
						$last  = date('Y-m-t 12:59:59', $timestamp); 
   
						$month_info = array(
											'company_id'		=>	$current_user_company_id,
											'timesheet_name'	=>	date('F', mktime(0,0,0,$j, 1, date('Y'))),
											'start_date'		=>	$first,
											'end_date'			=>	$last,
											'year'				=>	$y,
											'comments'			=>	'',
											'status'			=>	1,
										);
						$db->insert($db->timsheets, $month_info);
					}
				}	
			}
		}	
	}
	
	
	
	function firstDay($month = '', $year = '')
{
    if (empty($month)) {
      $month = date('m');
   }
   if (empty($year)) {
      $year = date('Y');
   }
   $result = strtotime("{$year}-{$month}-01");
   return date('Y-m-d', $result);
} 

?>

<section class="content">    	
	<?php		
		if (isset($message)) {			
			print_message($message);		
		}	
	?> 
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" name="" action="" method="post">														<div class="box-header with-border">						<h3 class="box-title"><i class="fa fa-cog" aria-hidden="true"></i> General Settings</h3>						<div class="pull-right">							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;							<input type="submit" class="btn btn-info" name="save" value="Save" >						</div>					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="overdue_measures" class="col-sm-2 control-label">Overdue Measures </label>
							<div class="col-sm-10">
								<input name="overdue_measures<?php echo $company_id; ?>" type="text" class="form-control" value="<?php echo get_option('overdue_measures'.$company_id); ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label for="factoring_dept_email" class="col-sm-2 control-label">Factoring Department Email</label>
							<div class="col-sm-10">
								<input name="factoring_dept_email<?php echo $company_id; ?>" type="email" class="form-control" value="<?php echo get_option('factoring_dept_email'.$company_id); ?>">
							</div>
						</div>
						
						<div class="form-group">
							<label for="storage_path" class="col-sm-2 control-label">Storage Path </label>
							<div class="col-sm-10">
								<input name="storage_path<?php echo $company_id; ?>" type="text" class="form-control" value="<?php echo get_option('storage_path'.$company_id); ?>">
							</div>
						</div>		
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
	
	
	
	
	
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" name="" action="" method="post">														
					<div class="box-header with-border">						
						<h3 class="box-title"></h3>		
					</div>
					
					<div class="box-body">
						<div class="form-group">
							<label for="country" class="col-sm-2 control-label">Period</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="period<?php echo $company_id; ?>">
									<option value="">Select</option>
									<?php
										$period_type = array('weekly', 'monthly');
										$old_period = get_option('period'.$company_id);
										foreach($period_type as $type){
											$select = '';
											if($type== $old_period) $select = 'selected';
											echo '<option value='.$type.' '.$select.'>'.ucfirst($type).'</option>';
										}
									?>
								</select>
							</div>
						</div>	
						
						
						<div class="form-group">
							<label for="country" class="col-sm-2 control-label"></label>
							<div class="col-sm-10">
								<input type="submit" class="btn btn-info" name="save1" value="Save" >
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	
	
</section>