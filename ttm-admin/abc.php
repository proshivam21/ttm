<?php 
	/* $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
	echo $page;
	
	
	if($page == 'add-new-system-user') include 'supadmin/registration.php';
	else echo 'hello';  */
	
	/* switch($page){
		case 'add-new-system-user':
			//include 'super-admin/registration.php';
			echo 'hi';
			break;
		default:
			echo 'hello';
		break;
	}   */
	
	//echo $_REQUEST['page'];
	
	if(isset($_POST) && !empty($_POST)){
		global $db;
		extract($_POST);
		
		$check = get_user_by('login', $email);
		if(!$check){
			$full_name = $first_name.' '.$last_name;
			/*
			 * Create user
			 */
			$userdata = array(
							'user_login' 		 =>  $email,
							'user_pass'  		 =>  ttm_hash_password($password),
							'user_email'  		 =>  $email,
							'role_id'			 =>  7,
							'user_status'		 =>  $status,
							'user_registered'    =>  date('Y-m-d h:i:s'),
							'display_name'       =>  ucwords($full_name),
							'user_nicename'		 =>  ucwords($full_name)
						);
			$user_id = ttm_insert_user( $userdata ) ;
			
			$meta = array(
							'first_name' => $first_name,
							'last_name'  => $last_name,
							'role' 		 => $role,
							'dob'  		 => $dob
					);
					
			foreach($meta as $key => $value){
				update_user_meta($user_id, $key, $value);
			} 
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Super admin user account has been added succeessfully.');
		}
		else $message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Email is already exist in our database. Please try another email.');
	} 
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user-secret"></i> Shivam Agrawal</h3>

						<div class="pull-right">

							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;

							<button type="submit" class="btn btn-info" name="submit">Save</button>

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name</label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name</label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="user_email" class="col-sm-2 control-label">User Email</label>

							<div class="col-sm-10">

								<input name="email" type="email" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="user_pass" class="col-sm-2 control-label">User Password</label>

							<div class="col-sm-10">

								<input name="user_pass" type="password" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="dob" class="col-sm-2 control-label">DOB</label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" class="form-control pull-right datepicker" value="01.7.1991">

								</div>

							</div>

						</div>

						

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <option value="1" selected="selected">Yes</option>

								  <option value="0">No</option>

								</select>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>

