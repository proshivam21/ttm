<?php
$page   = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if($page == 'projects') {
    switch ($action) {
        case 'edit':
            include 'projects/edit.php';
            break;
        case 'add':
            include 'projects/add.php';
            break;
        case '':
            include 'projects/view.php';
            break;
        default:
            include 'projects/view.php';
            break;
    }
}