<!DOCTYPE html>
<html>
	<head>
		<?php 
			$current_user_id = ttm_get_session('id');
			$current_user_company_id = get_cur_user_company_id($current_user_id);	
			$period_type = get_option('period'.$current_user_company_id);
			include 'head.php'; 		
		?>
	</head>
	
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<?php include 'topbar.php'; ?>
			<!-- Left side column. contains the logo and sidebar -->