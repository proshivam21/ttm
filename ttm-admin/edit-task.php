

<?php
	$ID = $_REQUEST['ID'];
	if($_POST['update_task']){		
		global $db;		
		extract($_POST);
		
		$task_w_ids = $db -> get_results("SELECT ID FROM {$db->company_project_task_workers} where task_id = $ID");
		foreach($task_w_ids as $val){
			$task_worker_ids[] = $val -> ID;
		}
		
		$id_for_deleted = array_diff($task_worker_ids,$task_worker_id);
		if(!empty($id_for_deleted)){
			foreach($id_for_deleted as $id_deleted){
				$db -> delete($db->company_project_task_workers, array('ID' => $id_deleted) );
			}
		}
		
		$task_info = array(						
						'task_name' 	=> $task_name,							
						'budget_total'  => $budget_total,							
						'start_date' 	=> date("Y-m-d", strtotime($start_date)),							
						'end_date' 		=> date("Y-m-d", strtotime($end_date)),						
					);				
		$where = array('ID' => $ID);
		
		$db->update($db->company_project_tasks, $task_info, $where);	
		
		$count_worker = count($worker_id);
		
		for($i=0; $i<$count_worker; $i++){
			
			if(!empty($task_worker_id[$i]))
			$db->update($db->company_project_task_workers, array('worker_id' => $worker_id[$i], 'budget_total' => $worker_budget_total[$i]), array('ID' => $task_worker_id[$i]) );
		
			else
			$db->insert($db->company_project_task_workers, array('task_id' => $ID, 'worker_id' => $worker_id[$i], 'budget_total' => $worker_budget_total[$i])); 
		}
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Task details has been updated successfully');
	}	
	
	
	$tasks_info = $db ->get_row("SELECT *FROM {$db->company_project_tasks} where ID = $ID ");
	
	$task_worker_info = $db -> get_results("SELECT *FROM {$db->company_project_task_workers} where task_id = $ID");
?>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

	<form class="form-horizontal" action="" method="post" id="task">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title"><?php echo $tasks_info -> task_name; ?></h4>
	  </div>
	  <div class="modal-body">
			<div class="box-body">
				<div class="form-group">
					<label for="task_name" class="col-sm-3 control-label">Task Name<sup>*</sup></label>
					<div class="col-sm-9">
						<input name="task_name" type="text" class="form-control" value="<?php echo $tasks_info -> task_name; ?>" id="task_name" >
					</div>
				</div>
				<div class="form-group">
					<label for="budget_total" class="col-sm-3 control-label"> Budget total<sup>*</sup></label>
					<div class="col-sm-9">
						<input name="budget_total" type="text" class="form-control" value="<?php echo $tasks_info -> budget_total; ?>" id="budget_total">
					</div>
				</div>
				<input type="hidden" name="task_id" value="" id="task_id">
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">Start Date<sup>*</sup></label>
					<div class="col-sm-9">
						<div class="input-group date">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input type="text" class="form-control pull-right datepicker" value="<?php echo date("d.m.Y", strtotime($tasks_info -> start_date)); ?>" name="start_date" id="start_date">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">End Date<sup>*</sup></label>
					<div class="col-sm-9">
						<div class="input-group date">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input type="text" class="form-control pull-right datepicker" value="<?php echo date("d.m.Y", strtotime($tasks_info -> end_date)); ?>" name="end_date" id="end_date">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">Workers</label>
					<div class="col-sm-9">
						<?php
							$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));		
							$workers = get_users_by_company_id($current_user_company_id, 8);
							
							$count_worker = count($workers);
							
							$count_val =  count($task_worker_info);
							$i=1;
							
							foreach($task_worker_info as $info){
								echo '<div class="row worker ap_worker'.$i.'">
										<div class="col-md-5">
											<select class="form-control select" style="width: 100%;" name="worker_id[]" id="worker_id">';
											
								echo '<option value="">Select Worker</option>';
								
								foreach($workers as $w_info){	
									$select = '';
									if($w_info -> ID == $info -> worker_id) $select = 'selected';
									echo '<option value="'.$w_info -> ID.'" '.$select.'>'.$w_info -> display_name.'</option>';
								}
								
								echo '</select>
										</div>
										<div class="col-sm-5">
											<input name="worker_budget_total[]" type="text" class="form-control" id="worker_budget_total" value="'.$info -> budget_total.'"><input type="hidden" name="task_worker_id[]" value="'.$info -> ID.'">
										</div>';
								
								echo '<div class="col-sm-2 as">
											<span class="glyphicon glyphicon-remove" onclick="wc_sub('.$i.')"></span>
										</div>';
								
								echo '</div>';
								$i++;
							} 
						?>
						
						<?php if($count_worker != $count_val){ 
						$i=1;
						?>
						<div class="workers"></div>
						<div class="row">
							<div class="col-md-12">
								<span class="btn btn-primary add-worker">Add Worker</span>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
		
	  </div>
	  <div class="modal-footer">
		<a href="?page=projects-details&ID=<?php echo $tasks_info->project_id; ?>" class="btn bg-red">Discard</a>&nbsp;&nbsp;
		<input type="submit" class="btn btn-primary" value="Update" name="update_task">
	  </div>	  	  
	</form>
			</div>

			<!-- /.box -->

		</div>

	</div>

	

	<!-- /.row -->
</section>