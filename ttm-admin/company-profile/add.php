<?php 
	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> add_user('company');
		
		if($message['flag'] == 1){ 
			global $company;
			$user_id = $message['user_id'];
			$message = $company -> add_company($user_id);
		}
	}
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				 <form class="form-horizontal" method="post" action="" id="company_sign_up" enctype="multipart/form-data">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-building-o"></i> </h3>

						<div class="pull-right">

							<a class="btn bg-red" href="#">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info" name="save">Save</button>

						</div>

					</div>
					
					<br><br><br>
					
					<div class="box-body">

						<div class="form-group">

							<label for="company_name" class="col-sm-2 control-label">Company Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_name" type="text" class="form-control" value="<?php echo $_POST['company_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="account_name" class="col-sm-2 control-label">Account Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="account_name" type="text" class="form-control" value="<?php echo $_POST['account_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="company_logo" class="col-sm-2 control-label">Company Logo</label>

							<div class="col-sm-10">

								<div class="file-loading">
									<input class="file_uploader" name="company_logo" type="file">
								</div>

							</div>

						</div>
						
						<input type="hidden" name="role" value="2">
						
						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">Contact First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo $_POST['first_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Contact Last Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo $_POST['last_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="contact_email_address" class="col-sm-2 control-label">Contact Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="contact_email_address" type="email" class="form-control" value="<?php echo $_POST['contact_email_address']; ?>">

							</div>

						</div>

						
						<div class="form-group">

							<label for="email" class="col-sm-2 control-label">User Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email" type="email" class="form-control" value="<?php echo $_POST['email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="password" class="col-sm-2 control-label">Password<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="<?php echo $_POST['password']; ?>">

							</div>

						</div>
						
						
						<div class="form-group">

							<label for="company_postal_address" class="col-sm-2 control-label">Company Postal Address<sup>*</sup></label>

							<div class="col-sm-10">

								<textarea name="company_postal_address" class="form-control" ><?php echo $_POST['company_postal_address']; ?></textarea>

							</div>

						</div>

						<div class="form-group">

							<label for="company_tax_no" class="col-sm-2 control-label">Company Tax No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_tax_no" type="text" class="form-control" value="<?php echo $_POST['company_tax_no']; ?>">

							</div>

						</div>
						
						
						<div class="form-group">

							<label for="billing_address" class="col-sm-2 control-label">Billing Address</label>

							<div class="col-sm-10">

								<input name="billing_address" type="text" class="form-control" value="<?php echo $_POST['billing_address']; ?>">

							</div>

						</div>
						
  						<div class="form-group">

							<label for="billing_email" class="col-sm-2 control-label">Billing Email</label>

							<div class="col-sm-10">

								<input name="billing_email" type="text" class="form-control" value="<?php echo $_POST['billing_email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <option value="1" selected="selected">Yes</option>

								  <option value="0">No</option>

								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here...."><?php echo $_POST['comment']; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>