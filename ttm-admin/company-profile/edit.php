<?php 
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	global $db;
	
	if($_POST['update'] && !empty($_POST['update'])){
		global $user;
		
		$user_id = get_users_by_company_id($id, 2)[0]->ID;
		
		$message = $user -> update_user($user_id);
		
		if($message['flag'] == 1){ 
			global $company;
			$message = $company -> update_company();
		}
	}

	if(!empty($id)){		
		$company_info = get_company_info_by_id($id);
	} 
		
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="company_sign_up" enctype="multipart/form-data">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-building-o"></i> <?php echo ucwords($company_info ->company_name); ?></h3>

						<div class="pull-right">

							<a class="btn bg-red" href="?page=companies-accounts-management">Discard</a>&nbsp;&nbsp;

							<input type="submit" class="btn btn-info" name="update" value="Save" >

						</div>

					</div>
				
					<div class="box-body">

						<div class="form-group">

							<label for="company_name" class="col-sm-2 control-label">Company Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_name" type="text" class="form-control" value="<?php echo $company_info ->company_name ; ?>" >

							</div>

						</div>

						<div class="form-group">

							<label for="account_name" class="col-sm-2 control-label">Account Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="account_name" type="text" class="form-control" value="<?php echo $company_info -> account_name; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="company_logo" class="col-sm-2 control-label">Company Logo</label>

							<div class="col-sm-10">

								<div class="file-loading">

									<?php echo ttm_get_attachment_image($company_info -> company_logo_id, 'thumbnail', array('class' => 'img-responsive img-thumbnail', 'alt' => 'Company Logo')); ?><br/><br/>
									
									<input class="file_uploader" name="company_logo" type="file">
									
								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">Contact First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo $company_info -> contact_first_name; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Contact Last Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo $company_info -> contact_last_name; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="contact_email_address" class="col-sm-2 control-label">Contact Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="contact_email_address" type="email" class="form-control" value="<?php echo $company_info -> contact_email_address; ?>" disabled>

							</div>

						</div>
						
						
						<input type="hidden" name="comp_id" value="<?php echo $id; ?>">
						
						<input type="hidden" name="role" value="2">
						
						
						<div class="form-group">

							<label for="user_email_address" class="col-sm-2 control-label">User Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input type="email" class="form-control" value="<?php echo $company_info -> contact_email_address; ?>" disabled>

							</div>

						</div>
						
						<div class="form-group">

							<label class="col-sm-2 control-label">User Password<sup>*</sup></label>

							<div class="col-sm-10">

								<input type="password" class="form-control" value="" name="user_pass">

							</div>

						</div>

						
						<div class="form-group">

							<label for="company_postal_address" class="col-sm-2 control-label">Company Postal Address<sup>*</sup></label>

							<div class="col-sm-10">

								<textarea name="company_postal_address" class="form-control"><?php echo $company_info -> company_postal_address; ?></textarea>

							</div>

						</div>

						<div class="form-group">

							<label for="company_tax_no" class="col-sm-2 control-label">Company Tax No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_tax_no" type="text" class="form-control" value="<?php echo $company_info -> company_tax_num; ?>">

							</div>

						</div>
						
						
						<div class="form-group">

							<label for="billing_address" class="col-sm-2 control-label">Billing Address</label>

							<div class="col-sm-10">

								<input name="billing_address" type="text" class="form-control" value="<?php echo get_custom_meta($db->companymeta, $id, 'billing_address');	 ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="billing_email" class="col-sm-2 control-label">Billing Email</label>

							<div class="col-sm-10">

								<input name="billing_email" type="text" class="form-control" value="<?php echo get_custom_meta($db->companymeta,  $id, 'billing_email');	 ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">
								
								<?php 
									$status_info = array('0' => 'No', '1' => 'Yes');
									$old_status = $company_info -> status;
									foreach($status_info as $k => $v){
										$select = '';
									
										if($old_status == $k){ $select = 'selected';
											echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';
										}
										else echo '<option value="'.$k.'">'.$v.'</option>';
									}
								?>
								</select>
							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here...."><?php echo $company_info -> comment; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>