<style>
	.comp_status_cls > a {
	  color: #000;
	}
	.comp_status_cls .active {
	  color: #00ACD6;
	}
</style>
<section class="content">

	<div class="row">

		<div class="col-xs-12">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-users" aria-hidden="true"></i>
					
					<h3 class="box-title">Companies Account</h3> - 
					<span class="comp_status_cls"> 
						<a href="?page=companies-accounts-management" class="<?php if(!isset($_GET['status']) ) echo 'active'; ?>">All</a> | <a href="?page=companies-accounts-management&status=1" class="<?php if($_GET['status'] == 1) echo 'active'; ?>" >Active </a> | <a href="?page=companies-accounts-management&status=0" class="<?php if($_GET['status'] == 0 && isset($_GET['status']) ) echo 'active'; ?>">Inactive</a> 
					</span>

					<a class="btn btn-info pull-right" href="?page=companies-accounts-management&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New Company Profile</a>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Company Name</th>

								<th>Status</th>

								<th>Contact Name</th>

								<th>Contact Email Address</th>

								<th>Billing Email Address</th>

								<th>Postal Address</th>

								<th>Actions</th>

							</tr>

						</thead>

						<tbody>

								<?php 															
									global $db;	
									$act_status = '';
									if(isset($_GET['status']) ) $act_status =  'where status = '.$_GET['status'];
									
									$company_info = $db -> get_results("SELECT *FROM {$db -> companies} $act_status");	
									
									$i=1;
									foreach($company_info as $info): 
								?>
							

								<tr class="per_company<?php echo $info -> ID; ?>">

									<td><?php echo $i; ?></td>

									<td><?php echo $info -> company_name; ?></td>

									<td>										
										<?php	
											$status = $info -> status;
											if($status == 1) $com_status = 'Active';									
											else $com_status =  'Inactive';	
										?>			
										<span class="company-status"><?php echo $com_status; ?></span>
									</td>

									<td><?php echo ucwords($info -> contact_first_name.' '.$info -> contact_last_name); ?></td>

									<td><?php echo $info -> contact_email_address; ?></td>

									<td><?php echo get_custom_meta($db->companymeta,  $info -> ID, 'billing_email');?></td>

									<td><?php echo $info -> company_postal_address; ?></td>

									<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=companies-accounts-management&action=edit&id=<?php echo $info -> ID; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
										<!-- <a title="Delete" onclick="delete_company(<?php echo $info -> ID; ?>)" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> -->
										<?php
											
											if($status == 1){
												$user_status = 'Active';
												$action = 'deactivate_company';
												$title = "Inactive Company";
												$status_class = 'fa-undo';
											}
											else{ 
												$user_status = 'Inactive';
												$action = 'activate_company';
												$title = "Active Company";
												$status_class = 'fa-check';
											}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick=" '.$action.'('.$info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
										$i++;
										?>
									</td>

								</tr>

							<?php endforeach; ?>

						</tbody>

					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>