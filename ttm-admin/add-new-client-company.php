<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-building-o"></i> ICO Independent Consultants</h3>
					<div class="pull-right">
						<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<label for="first_name" class="col-sm-2 control-label">Company Name</label>
							<div class="col-sm-10">
								<input name="company_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="first_name" class="col-sm-2 control-label">Company Logo</label>
							<div class="col-sm-10">
								<div class="file-loading">
									<input class="file_uploader" name="signed_pts" type="file" multiple>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="col-sm-2 control-label">Address</label>
							<div class="col-sm-10">
								<textarea name="address" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="first_name" class="col-sm-2 control-label">Client No.</label>
							<div class="col-sm-10">
								<input name="client_no" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="phone_number" class="col-sm-2 control-label">Phone No.</label>
							<div class="col-sm-10">
								<input name="phone_number" type="tel" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="email_address" class="col-sm-2 control-label">Email Address</label>
							<div class="col-sm-10">
								<input name="email_address" type="tel" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">Active</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">Yes</option>
								  <option>No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="10" class="form-control">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
		
	</div>
	<!-- /.row -->
</section>