<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-user"></i> Shivam Agrawal</h3>
					<div class="pull-right">
						<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<label for="first_name" class="col-sm-2 control-label">First Name</label>
							<div class="col-sm-10">
								<input name="first_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="col-sm-2 control-label">Last Name</label>
							<div class="col-sm-10">
								<input name="last_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="dob" class="col-sm-2 control-label">DOB</label>
							<div class="col-sm-10">
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right datepicker" value="01.7.1991">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="company" class="col-sm-2 control-label">Company</label>
							<div class="col-sm-10">
								<input name="company" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="country" class="col-sm-2 control-label">Country</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">Germany</option>
								  <option>USA</option>
								  <option>Austrilia</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="area" class="col-sm-2 control-label">Area</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">Berlin</option>
								  <option>Hamburg</option>
								  <option>Baden-Württemberg</option>
								</select>
							</div>
						</div>									
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">User Role</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" multiple="true">
								  <option selected="selected">Worker</option>
								  <option>Project Manager</option>
								  <option>Line Manager</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">Active</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">Yes</option>
								  <option>No</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="hr_id" class="col-sm-2 control-label">HR ID</label>
							<div class="col-sm-10">
								<input name="hr_id" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="line_manager" class="col-sm-2 control-label">Line Manager</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="true">Marc Rueckziegel</option>
								  <option>Shivam Agrawal</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="phone_number" class="col-sm-2 control-label">Phone No.</label>
							<div class="col-sm-10">
								<input name="phone_number" type="tel" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="email_address" class="col-sm-2 control-label">Email Address</label>
							<div class="col-sm-10">
								<input name="email_address" type="tel" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="10" class="form-control">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>