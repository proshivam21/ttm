<?php global $page; $breadcrumb = ucwords(str_replace('-', ' ', $page)); ?>
<section class="content-header">
	<h1>
		<?php echo $breadcrumb; ?>
		<small>Control Panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="#"><?php echo $breadcrumb; ?></a></li>
	</ol>
</section>