 <?php
$page   = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if ($page == 'client-contacts') {
    switch ($action) {
        case 'edit':
            include 'client-contacts/edit.php';
            break;
        case 'add':
            include 'client-contacts/add.php';
            break;
        case '':
            include 'client-contacts/view.php';
            break;
        default:
            include 'client-contacts/view.php';
            break;
    }
}
?> 