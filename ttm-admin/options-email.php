<?php

$template = isset($_REQUEST['template']) ? $_REQUEST['template'] : 'new_user_account';

if($_POST['save']) {
	$template = $_POST['templates'];
	if(!update_option($template, stripslashes($_POST['content']))) {
	
	}
	
	fp_redirect(get_option('home')."?page=options-email&update=true&template=$template");
}
?>

<!-- Right side column. Contains the navbar and content of the page -->

	<!-- Main content -->
	<section class="content">
	  <div class="row">
		<div class="col-xs-12">
		<?php
			if(isset($_REQUEST['update']) && 'true'==$_REQUEST['update']) {
				print_message(array('type' => 'success', 'title'=>'Success!', 'message'=>'Template Saved'));
			}
		?>
		  <div class="box">
			<div class="box-body">
				<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label class="control-label col-sm-2">Select HTML Template</label>
						<div class="control-label col-sm-10">
							<select class="form-control" name="templates" onChange="getTemplate(this.value);">
								<?php 
									$templates = array('new_user_account' => 'New Account Created', 'verify_email' => 'User Account Verify', 'send_mail_to_user' => 'Send Mail To User', 'send_interest' => 'Send Interest', 'recent_visitor' => 'Recent Visitor', 'profile_shortlist' => 'Shortlist', 'declined_interest' => 'Declined Interest',
									'accept_interest' => 'Accept Interest',
									'delete_interest' => 'Delete Interest',
									'delete_shortlist' => 'Delete Shortlist', 'new_ex_account' => 'New Executive Account Created',
									'plan_purchased' => 'Plan Purchased',
									'admin_plan_purchased' => 'Admin Plan purchased notify',
									'photo_approved' => 'Photo Approved',
									'admin_photo_approval_request' => 'Admin Photo Approval Request',
									);
									
									foreach($templates as $key => $value) {
										$selected = in_array($key, (array) $template)? 'selected="selected"' : '';
										echo '<option value='.$key.' '.$selected.'>'.$value.'</option>';
									}
								?>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2">HTML Template </label>
						<div class="col-sm-10">
							<textarea class="summernote" name="content" rows="10"><?php echo get_option($template); ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input class="btn bg-blue" type="submit" name="save" value="Save Template">
						</div>
					</div>
				</form>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- /.col -->
	  </div><!-- /.row -->
	</section><!-- /.content -->