<?php if($_POST) {	if(update_option('system_invoice_template', stripslashes($_POST['system_invoice_template']))) {		$mesg = array('type' => 'success', 'title'=>'Success!', 'message'=>'Template Saved');	}}?><section class="content">	<?php		if($mesg) {			print_message($mesg);		}	?>
	  <div class="row">
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title">Invoice Layout</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="summernote" name="system_invoice_template" rows="30"><?php echo get_option('system_invoice_template'); ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input class="btn bg-blue" type="submit" name="save" value="Save Template">
						</div>
					</div>
				</form>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- /.col -->
	  </div><!-- /.row -->
	</section><!-- /.content -->