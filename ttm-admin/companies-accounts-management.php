<?php
	$page   = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	if ($page == 'companies-accounts-management') {
		switch ($action) {
			case 'edit':
				include 'company-profile/edit.php';
				break;
			case 'add':
				include 'company-profile/add.php';
				break;
			case '':
				include 'company-profile/view.php';
				break;
			default:
				include 'company-profile/view.php';
				break;
		}
	}
?>
