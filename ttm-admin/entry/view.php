	<div class="col-xs-12">

			<div class="box">

				<div class="box-header">

					<h3 class="box-title"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $ts_info -> timesheet_name; ?> <a class="btn bg-blue" href="?page=timesheet&ID=<?php echo $ts_ID; ?>&action=add"><i class="fa fa-plus-square" aria-hidden="true"></i> Add New Entry</a></h3>

					<!--<div class="box-tools">

						<div class="input-group input-group-sm" style="width: 150px;">

							<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

							<div class="input-group-btn">

								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>

							</div>

						</div>

					</div>-->

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th rowspan="2">ID</th>

								<th rowspan="2">Date</th>

								<th rowspan="2">Start Time</th>

								<th rowspan="2">End Time</th>

								<th rowspan="2">Sum (hrs.)</th>

								<th rowspan="2">Project</th>

								<th rowspan="2">Task</th>

								<th rowspan="2" style="width: 250px;">Comment</th>

								<th colspan="2" style="text-align: center">History Status</th>

								<th rowspan="2">Actions</th>

								

							</tr>

							<tr>

								<th>Project Manager</th>

								<th>Line Manager</th>

							</tr>

						</thead>

						<tbody>

							<tr>

								<td>1</td>

								<td>30.10.2017</td>

								<td>09:00 AM</td>

								<td>05:00 AM</td>

								<td>08:00</td>

								<td>TimTraMac</td>

								<td>UI Designing</td>

								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>

								<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/></td>

								<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>

								<td>

									<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>

									<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>

								</td>

							</tr>

							<tr>

								<td>2</td>

								<td>31.10.2017</td>

								<td>09:00 AM</td>

								<td>05:00 AM</td>

								<td>08:00</td>

								<td>TimTraMac</td>

								<td>UI Designing</td>

								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>

								<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/></td>

								<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>

								<td>

									<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>

									<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>

								</td>

							</tr>

							<tr>

								<td>3</td>

								<td>01.11.2017</td>

								<td>09:00 AM</td>

								<td>05:00 AM</td>

								<td>08:00</td>

								<td>TimTraMac</td>

								<td>UI Designing</td>

								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>

								<td><i class="fa fa-remove"></i> <strong>PM:</strong> Michael<br/></td>

								<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>

								<td>

									<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>

									<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>

								</td>

							</tr>

							<tr>

								<td>4</td>

								<td>02.11.2017</td>

								<td>09:00 AM</td>

								<td>05:00 AM</td>

								<td>08:00</td>

								<td>TimTraMac</td>

								<td>UI Designing</td>

								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>

								<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/></td>

								<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>

								<td>

									<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>

									<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>

								</td>

							</tr>

							<tr>

								<td>5</td>

								<td>30.11.2017</td>

								<td>09:00 AM</td>

								<td>05:00 AM</td>

								<td>08:00</td>

								<td>TimTraMac</td>

								<td>UI Designing</td>

								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>

								<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Michael<br/></td>

								<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>

								<td>

									<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>

									<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>

								</td>

							</tr>

						</tbody>

					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>
