<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Oct 30 – Nov 5, 2017</h3>
					<div class="pull-right">
						<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Start Time</label>
							<div class="col-sm-10">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
									<input type="text" class="form-control timepicker">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">End Time</label>
							<div class="col-sm-10">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
									<input type="text" class="form-control timepicker">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Date</label>
							<div class="col-sm-10">
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right datepicker" value="01.11.2017">
								</div>
							</div>
						</div>
						<div class="form-group">							<label for="inputPassword3" class="col-sm-2 control-label">Hours Type</label>							<div class="col-sm-10">								<select class="form-control select2" style="width: 100%;" name="hours_type" id="hours_type">																	  <option value="">Select</option>								  								  <option value="pro_hours">Project Hours</option>								  <option value="int_hours">Internal Hours</option>								  <option value="vacations">Vacations</option>								  <option value="sick_leave">Sick Leave</option>								</select>							</div>						</div>																<div class="pro" style="display:none;">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Project</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">TimTraMac</option>
								  <option>TimTraMac</option>
								  <option>TimTraMac</option>
								  <option>TimTraMac</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Task</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;">
								  <option selected="selected">UI Design</option>
								  <option>HTML</option>
								  <option>CSS</option>
								  <option>Development</option>
								</select>
							</div>
						</div>					</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Rest Budget Available (hrs.)</label>
							<div class="col-sm-10">
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
									<input type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here....."></textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
		
	</div>
	<!-- /.row -->
</section>