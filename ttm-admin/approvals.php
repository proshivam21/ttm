<?php 
	if($role == 'pm') {
		include 'timesheet-approvals/pm-approvals.php';
	} elseif($role == 'lm') {
		include 'timesheet-approvals/lm-approvals.php';
	} elseif($role == 'client') {
		include 'timesheet-approvals/client-approvals.php';
	}
?>