<?php 
$userdata = get_userdata( $id ); 
if($_POST) {
	global $user;
	$message = $user -> update_user();
}

?>

<!-- Main content -->
<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<!--<h3 class="box-title"></h3>-->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="first_name" class="control-label col-sm-2">First Name</label>
							<div class="col-sm-10">
								<input name="first_name" type="text" class="form-control" id="first_name" placeholder="First Name" value="<?php echo get_user_meta($id, 'first_name', true); ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="control-label col-sm-2">Last Name</label>
							<div class="col-sm-10">
								<input name="last_name" type="text" class="form-control" id="last_name" placeholder="Last Name" value="<?php echo get_user_meta($id, 'last_name', true); ?>"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="control-label col-sm-2">Email/Username<sup>*</sup></label>
							<div class="col-sm-10">
								<input  type="email" class="form-control" id="email" placeholder="Email" value="<?php echo $userdata -> user_email; ?>" disabled />
								<div class="progress" id="emailProgressBar" style="margin: 5px 0 0 0; display: none;">
									<div class="progress-bar progress-bar-success progress-bar-striped active" style="width: 100%"></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="last_name" class="control-label col-sm-2">Password</label>
							<div class="col-sm-10">
								<input name="password" type="password" class="form-control" placeholder="Password"/>
							</div>
						</div>
						
						<input type = "hidden" name="status" value="1">
						<input type = "hidden" name="role" value="<?php echo $userdata ->role_id;?>">
						<input type = "hidden" name="id" value="<?php echo $id;?>">
						
						<div class="form-group">
							<label for="profile_photo" class="control-label col-sm-2">Profile Photo</label>
							<div class="col-sm-10">
								
								<?php echo ttm_get_attachment_image(get_user_meta($id, 'profile_photo', true), 'thumbnail', array('class' => 'img-responsive img-thumbnail', 'alt' => 'Profile Photo')); ?><br/><br/>
									
								<input class="file_uploader" name="profile_photo" type="file" multiple>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" class="btn bg-blue" value='Update Profile' />
							</div>
						</div>
					</form>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->