<?php 
	$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	if(!empty($id)){
		$user_info = get_userdata($id);
	} 
	

	if($page == 'system-admin-users'){
		switch($action) {
			case 'edit':
				include 'super-admin/edit_sup_admin_users.php';
				break;
				
			case 'add':
				include 'super-admin/add_sup_admin_users.php';
				break;
			
			case '':	
				include 'super-admin/view_sup_admin_users.php';
				break;
			
			default:
				include 'super-admin/view_sup_admin_users.php';
				break;
		} 
	}
?>