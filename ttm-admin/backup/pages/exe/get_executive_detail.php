<?php
	global $db;
	$exe_id = $_GET['exe_id'];
	$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
	$limit = get_option('posts_per_page');
	$offset = ($page - 1) * $limit;
	$get_code = get_user_meta($exe_id,'exe_code', true);
	$where = 'where execode="'.$get_code.'"';
	if(isset($_REQUEST['search'])){
		$where .= ' and txn_id like "%'.$search.'%"';
	}
	$query = "select * from {$db->transactions} ".$where."";
	$search = str_replace('CM', '', $_REQUEST['search']);
	
	$query2 = "select * from {$db->transactions} ".$where." LIMIT ".$limit." OFFSET ".$offset."";
	$results = $db->get_results($query);
	$results2 = $db->get_results($query2);
	$total_members = count($results);
	
	if(!empty(count($results)))
	{
?>
		
	<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>User ID</th> 
				<th>User Email</th>
				<th>Transaction ID</th>
				<th>Method</th>
				<th>Revenue</th>
				<th>Earning(USD)</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$i = 1;
			foreach(@$results2 as $result)
			{
		?>
				<tr>
					<td><?php echo get_user_prefix_id($result->user_id);?></td>
					<td><?php echo (get_userdata($result->user_id)->user_email) ?></td>
					<td><?php echo $result->txn_id; ?></td>
					<td>
						<?php 
							$mode = $result->mode; 
							echo ucwords(str_replace("_",' ',$mode));
						?>
					</td>
					<td><?php echo $result->actual_amount; ?></td>
					<td><?php echo get_discounted_amount($result->actual_amount,$result->exe_received_percent); ?></td>
					<td><?php echo get_f_date($result->date); ?></td>
				</tr>
		<?php
				$i++;
			}
		?>
		</tbody>
			
	</table>
	
<?php
	}
	else{
		echo '<p>No users were found!</p>';
	}
?>