<?php
	if(!empty(count($executives->results)))
	{
?>
	<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Executive ID</th>
				<th>Name</th>
				<th>User Name</th>
				<th>Total Members</th>
				<th>Revenue (USD)</th>
				<th>Earned Comission (USD)</th>
				<th>Amount Paid</th>
				<th>Amount Left</th>
				<th>Request Received</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php 
				foreach($executives->results as $executive)
				{
					global $db;
					$get_code = get_user_meta($executive->ID,'exe_code', true);
					
					$query1 = "select * from {$db->transactions} where execode='$get_code'";
					$results1 = $db->get_results($query1);
					$exe_comission = get_comission($executive->ID);
					$exe_paid = get_paid_amount($get_code);
					$exe_left = $exe_comission - $exe_paid;
					
					echo '<tr>
							<td>'.$get_code.'</td>
							<td>'.ucwords(get_user_name($executive->ID)).'</td>
							<td>'.$executive->user_login.'</td>
							
							<td>'.count($results1).'</td>
							<td>'.get_revenue($executive->ID).'</td>
							<td>'.get_comission($executive->ID).'</td>
							<td>'.get_paid_amount($get_code).'</td>
							<td>'.$exe_left.'</td>
							<td>'.count($results1).'</td>
							<td>'.get_user_status($executive->ID).'</td>
							<td><a class="btn bg-orange btn-xs" href="'.get_site_url().'/fp-admin/?page=executives&exe_id='.$executive->ID.'"><i class="fa fa-eye" aria-hidden="true" title="View Details"></i></a>&nbsp;
								<a class="make_pay" class="btn bg-orange btn-xs" href="" data-toggle="modal" data-target="#show_pay" exe_code="'.$get_code.'"  exe_left="'.$exe_left.'" exe_comission="'.get_comission($executive->ID).'" onclick="get_exe_code(this)"><i class="fa fa-money" title="Make Payment" aria-hidden="true"></i></a>
								
							</td>
							
						</tr>';;
				}
				
		
		?>
			</tfoot>
	</table>	
	<script>
		function get_exe_code(obj){
			var exe_code = $(obj).attr("exe_code");
			var exe_left = $(obj).attr("exe_left");
			var exe_comission = $(obj).attr("exe_comission");
			$("#exe_code").val(exe_code);
			$("#left").val(exe_left);
			$("#comission").val(exe_comission);
		}
	</script>
	
	<!-- Modal -->
	  <div class="modal fade" id="show_pay" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title text-center">Make Payment</h4>
			</div><br/>
			<div class="modal-body">
			<!--Modal body goes here-->
				<form action="" method="post">
					Make payment of $ &nbsp; 
					<input type="text" class="form-control" name="payment"><br/>
					<input type="hidden" id="exe_code" class="form-control" value="" name="code"><br/>
					<input type="hidden" id="left" class="form-control" value="" name="left"><br/>
					<input type="hidden" id="comission" class="form-control" value="" name="comission"><br/>
					</div>
					<div class="modal-footer">
						<div class="row">
							<div class="col-md-6">
								<input type="submit" class="btn btn-default form-control" value="Pay" name="submit"><br/>
							</div>
				</form>
					<div class="col-md-6">
						<button type="button" class="btn btn-default form-control" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		  </div>
		  
		</div>
	  </div>
	<!--Modal ends here -->
<?php
	}
	else{
		echo '<p>No executive found!</p>';
	}
?>

