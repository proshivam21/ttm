<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			CMS Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>CMS Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">News & Events</h3>&nbsp;
						<a href="<?php echo get_option('admin_url'); ?>/?page=news&action=add" class="btn bg-red" title="Add"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add news</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>News</th>
									<th>News Description</th>
									<th>News Image</th>
									<th>Last Edited By</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $news = get_pages('news'); foreach($news as $new): ?>
								<tr>
									<td><?php echo $new->id; ?></td>
									<td><?php echo $new->title; ?></td>
									<td><?php echo getExcerpt($new->description); ?></td>
									<td></td>
									<td><?php echo date('d M y h:i a', strtotime($new->last_updated)); ?></td>
									<td>
										<a href="<?php echo get_option('admin_url'); ?>/?page=news&action=edit&id=<?php echo $new->id; ?>" class="btn bg-orange btn-xs" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
										<a href="<?php echo get_option('admin_url'); ?>/?page=news&action=delete&id=<?php echo $new->id; ?>" class="btn bg-orange btn-xs" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>&nbsp;
									</td>
								</tr>
								<?php endforeach; ?>
								
								</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>