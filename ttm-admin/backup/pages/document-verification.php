<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Document Verification
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Document Verification</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Document Verification List</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>User Name</th>
									<th>Email Id</th>
									<th>Document</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Lokesh</td>
									<td>Lokesh1988</td>
									<td>Lokesh1988@gmail.com</td>
									<td><i class="fa fa-file-text" aria-hidden="true"></i></td>
									<td>Active</td>
									<td>
										<a href="#" class="btn bg-green btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;
										<a href="#" class="btn bg-red btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Rajesh</td>
									<td>Rajesh233</td>
									<td>rajesh1451@live.com</td>
									<td><i class="fa fa-file-text" aria-hidden="true"></i></td>
									<td>Active</td>
									<td>
										<a href="#" class="btn bg-green btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;
										<a href="#" class="btn bg-red btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Bruce</td>
									<td>bruce876</td>
									<td>bruce5543@gmail.com</td>
									<td><i class="fa fa-file-text" aria-hidden="true"></i></td>
									<td>Inactive</td>
									<td>
										<a href="#" class="btn bg-green btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>&nbsp;
										<a href="#" class="btn bg-red btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
									</td>
								</tr>
								</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>