<?php
	global $db;
	$query = "select * from {$db->transactions} where plan not like '%Free%' order by id desc ";
	$results = $db->get_results($query);
	//print_r($results);
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Plan Renewal Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Plan Renewal Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						
						<div class="row">
							<div class="col-sm-9">
								<h3 class="box-title">Plan Renewal</h3>
							</div>
							<div class="col-sm-3" align="right">
								<!--<a class="btn bg-orange" href="?action=export-members" title="Export CSV">
									<i class="fa fa-file-excel-o fa-lg"></i>
								</a>
								<a class="btn btn-danger delete-per-members" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>-->
								
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>Email Id</th>
									<th>Txn ID</th>
									<th>Member Type</th>
									<th>Pricing(USD</th>
									<th>Purchased On</th>
									<th>Expires On</th>
									<th>Duration</th>
									<th>Plan Status</th>
									<!--<th>Action</th>-->
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 1;
										foreach(@$results as $result){
									?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo ucwords(get_user_name($result->user_id)); ?></td>
											<td><?php echo (get_userdata($result->user_id)->user_email); ?></td>
											<td><?php echo $result->txn_id; ?></td>
											<td>
												<?php  
														$plan_name = unserialize($result->plan);
														echo $plan_name[name]; 
												?>
											</td>
											<td><?php echo $result->actual_amount; ?></td>
											<td><?php echo get_f_date($result->date);?></td>
											<td>
												<?php 
													$expiry_date = unserialize($result->plan);
													echo get_f_date($expiry_date[expiry_date]);
												?>
											</td>
											<td>
												<?php 
													$duration = unserialize($result->plan); 
													echo $duration[duration];
												?>
											</td>
											<td>
												<?php
													
													$plan_ex = is_plan_expired($result->txn_id);
													//echo gettype($plan_ex);
													if($plan_ex == false)
													{
														echo "Active";
													}
													if($plan_ex == true)
													{
														echo "Expired";
													}
												?>
											</td>
											<!--<td>
												<a onclick="delete_user(<?php echo $result->user_id; ?>)" class="btn bg-red btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
											</td>-->
										</tr>
									<?php
											$i++;
										}
									?>
								</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>