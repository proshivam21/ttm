<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3><?php echo total_members1(); ?></h3>
						<p>Total Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-users"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3><?php echo active_members2(); ?></h3>
						<p>Active Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-bolt"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=active" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-inactive">
					<div class="inner">
						<h3><?php echo inactive_members3(); ?></h3>
						<p>Inactive Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-lock"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=inactive" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3><?php echo free_members4(); ?></h3>
						<p>Free Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-flag"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=free" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- /.row -->
		<!-- second row -->
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-paid">
					<div class="inner">
						<h3><?php echo total_members1() - free_members4(); ?></h3>
						<p>Paid Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-key"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=paid" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-silver">
					<div class="inner">
						<h3><?php echo basic_members5(); ?></h3>
						<p>Basic Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-lightbulb-o"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=basic" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-diamond">
					<div class="inner">
						<h3><?php echo silver_members6(); ?></h3>
						<p>Silver Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-dot-circle-o"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=silver" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-diamond">
					<div class="inner">
						<h3><?php echo gold_members7(); ?></h3>
						<p>Gold Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-heart"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=gold" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-diamond">
					<div class="inner">
						<h3><?php echo diamond_members8(); ?></h3>
						<p>Diamond Members</p>
					</div>
					<div class="icon">
						<i class="fa fa-diamond"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=diamond" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-blue">
					<div class="inner">
						<h3><?php echo groom_count(); ?></h3>
						<p>Groom Profiles</p>
					</div>
					<div class="icon">
						<i class="fa fa-male"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=groom" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-lady">
					<div class="inner">
						<h3><?php echo bride_count(); ?></h3>
						<p>Bride Profiles</p>
					</div>
					<div class="icon">
						<i class="fa fa-female"></i>
					</div>
					<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=bride" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<!-- ./col -->
		</div>
		<!-- Main row -->
		
		<!-- /.row (main row) -->
	</section>
	<!-- /.content -->
</div>