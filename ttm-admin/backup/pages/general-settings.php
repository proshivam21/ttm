<?php 
	if($_POST['save']) {
		extract($_POST);
		$media_id = upload_media('site_logo');
		$_POST['site_logo'] = $media_id;
		foreach($_POST as $key => $value) {		
			if($key != 'save'){
				if(!empty($value)) update_option($key, $value);
			}
		}	
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your settings has been saved succeessfully.');
	}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Settings
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Settings</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">General Settings</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
								<div class="form-group">
									<label for="site_name" class="control-label col-sm-2">Website Name</label>
									<div class="control-label col-sm-10">
										<input name="website_name" class="form-control" value="<?php echo get_option('website_name'); ?>" type="text">
									</div>
								</div>
								<!--<div class="form-group">
									<label for="site_keywords" class="control-label col-sm-2">Website Keywords</label>
									<div class="control-label col-sm-10">
										<input name="site_keywords" class="form-control" value="<?php get_option('site_keywords'); ?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="site_description" class="control-label col-sm-2">Website Description</label>
									<div class="control-label col-sm-10">
										<textarea name="site_description" class="summernote"><?php get_option('site_description'); ?></textarea>
									</div>
								</div>-->
								<div class="form-group">
									<label for="profile_pefix" class="control-label col-sm-2">Profile Prefix</label>
									<div class="control-label col-sm-10">
										<input name="profile_prefix" class="form-control" value="<?php echo get_option('profile_prefix'); ?>" type="text">
									</div>
								</div>

								<div class="form-group">
									<label for="site_url" class="control-label col-sm-2">Site url</label>
									<div class="control-label col-sm-10">
										<input name="siteurl" class="form-control" value="<?php echo get_option('siteurl'); ?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="admin_url" class="control-label col-sm-2">Site Admin url</label>
									<div class="control-label col-sm-10">
										<input name="admin_url" class="form-control" value="<?php echo get_option('admin_url'); ?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="site_logo" class="control-label col-sm-2">Site logo</label>
									<div class="control-label col-sm-10">
										<?php echo fp_get_attachment_image(get_option('site_logo'), 'full', array('class' => 'img-responsive', 'alt' => 'Website Logo')); ?><br/><br/>
										<input name="site_logo" type="file" />
									</div>
								</div>
								<div class="form-group">
									<label for="contact" class="control-label col-sm-2">Contact</label>
									<div class="control-label col-sm-10">
										<input name="contact" class="form-control" value="<?php echo get_option('contact'); ?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<input class="btn bg-red" name="save" value="Save Changes" type="submit">
									</div>
								</div>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>