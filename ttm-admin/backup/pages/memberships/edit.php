<?php 
if($_POST){
	global $db;
	extract($_POST);
	
	$features_args = array('emails' => array('val' => $emails, 'type' => 'members', 'label' => 'Emails'), 'phone_no' => array('val' => $phone_no, 'type' => 'members', 'label' => 'Phone Numbers'), 'view_profile_cm_int' => array('val' => $view_profile_cm_int, 'type' => 'profiles', 'label' => 'View Profiles + Standers CM interests'), 'send_interest' => array('val' => $send_interest, 'type' => 'profiles', 'label' => 'Express customised interest'), 'initiate_chat' => array('val' => $initiate_chat, 'type' => 'profiles', 'label' => 'Initiate Chat'), 'view_family' => array('val' => $view_family, 'type' => 'profiles', 'label' => 'View Family Details'), 'view_horoscope' => array('val' => $view_horoscope, 'type' => 'profiles', 'label' => 'View Horoscope'), 'spotlight_profile_in_matches' => array('val' => $spotlight_profile_in_matches, 'type' => 'profiles', 'label' => 'Spotlight your profile in featured matches'), 'profile_highlights' => array('val' => $profile_highlights, 'type' => 'profiles', 'label' => 'Profile highlights'));
	
	$args = array('name' => $name, 'duration' => $duration, 'amount_in_usd' => $amount_in_usd, 'amount_in_inr' => $amount_in_inr, 'currency' => 'USD', 'features' => serialize($features_args));
	//print_r($args);
	if($db->update($db->plans, $args, array('id' => $_REQUEST['id']))){
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your plan has been updated succeessfully.');
	}
	else{
		$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Failed to update your plan. There was an error adding your plan to database. Please try again.');
	}
	
}
$plan_info = get_plan_info($_REQUEST['id']);
$features = unserialize($plan_info->features);
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Membership Plans
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Membership Plans</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Edit Plan</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
								<div class="form-group">
									<label for="name" class="control-label col-sm-2">Name</label>
									<div class="control-label col-sm-10">
										<input name="name" class="form-control" value="<?php echo $plan_info->name;?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="amount" class="control-label col-sm-2">Amount(USD)</label>
									<div class="control-label col-sm-10">
										<input name="amount_in_usd" class="form-control" value="<?php echo $plan_info->amount_in_usd;?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="amount" class="control-label col-sm-2">Amount(INR)</label>
									<div class="control-label col-sm-10">
										<input name="amount_in_inr" class="form-control" value="<?php echo $plan_info->amount_in_inr;?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Duration</label>
									<div class="control-label col-sm-10">
										<input name="duration" class="form-control" value="<?php echo $plan_info->duration;?>" type="text">
									</div>
								</div>
								<h4 class="plan_features"><strong>Features</strong></h4>
								<hr/>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Emails(members)</label>
									<div class="control-label col-sm-10">
										<input name="emails" class="form-control" value="<?php echo $features['emails']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Phone Numbers(members)</label>
									<div class="control-label col-sm-10">
										<input name="phone_no" class="form-control" value="<?php echo $features['phone_no']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Profiles + Standers CM interests</label>
									<div class="control-label col-sm-10">
										<input readonly name="view_profile_cm_int" class="form-control" value="<?php echo $features['view_profile_cm_int']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Express customised interest(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="send_interest" class="form-control" value="<?php echo $features['send_interest']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Initiate Chat(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="initiate_chat" class="form-control" value="<?php echo $features['initiate_chat']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Family Details(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="view_family" class="form-control" value="<?php echo $features['view_family']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Horoscope(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="view_horoscope" class="form-control" value="<?php echo $features['view_horoscope']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Spotlight your profile in featured matches</label>
									<div class="control-label col-sm-10">
										<input name="spotlight_profile_in_matches" class="form-control" value="<?php echo $features['spotlight_profile_in_matches']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Profile Highlights</label>
									<div class="control-label col-sm-10">
										<input name="profile_highlights" class="form-control" value="<?php echo $features['profile_highlights']['val'];?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<input class="btn bg-red" value="Update Plan" type="submit">
									</div>
								</div>
							</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>