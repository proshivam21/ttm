<?php 
if($_POST){
	global $db;
	extract($_POST);
	
	$features_args = array('emails' => array('val' => $emails, 'type' => 'members', 'label' => 'Emails'), 'phone_no' => array('val' => $phone_no, 'type' => 'members', 'label' => 'Phone Numbers'), 'view_profile_cm_int' => array('val' => $view_profile_cm_int, 'type' => '', 'label' => 'View Profiles + Standers CM interests'), 'send_interest' => array('val' => $send_interest, 'type' => 'profiles', 'label' => 'Express customised interest'), 'initiate_chat' => array('val' => $initiate_chat, 'type' => 'profiles', 'label' => 'Initiate Chat'), 'view_family' => array('val' => $view_family, 'type' => 'profiles', 'label' => 'View Family Details'), 'view_horoscope' => array('val' => $view_horoscope, 'type' => 'profiles', 'label' => 'View Horoscope'), 'spotlight_profile_in_matches' => array('val' => $spotlight_profile_in_matches, 'type' => '', 'label' => 'Spotlight your profile in featured matches'), 'profile_highlights' => array('val' => $profile_highlights, 'type' => '', 'label' => 'Profile highlights'));
	
	$args = array('name' => $name, 'duration' => $duration, 'amount' => $amount, 'currency' => 'USD', 'features' => serialize($features_args));
	
	if($db->insert($db->plans, $args)){
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your plan has been added succeessfully.');
	}
	else{
		$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Failed to add your plan. There was an error adding your plan to database. Please try again.');
	}
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Membership Plans
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Membership Plans</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Add Plan</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
								<div class="form-group">
									<label for="name" class="control-label col-sm-2">Name</label>
									<div class="control-label col-sm-10">
										<input name="name" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="amount" class="control-label col-sm-2">Amount(USD)</label>
									<div class="control-label col-sm-10">
										<input name="amount" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Duration</label>
									<div class="control-label col-sm-10">
										<input name="duration" class="form-control" value="" type="text">
									</div>
								</div>
								<h4 class="plan_features"><strong>Features</strong></h4>
								<hr/>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Emails(members)</label>
									<div class="control-label col-sm-10">
										<input name="emails" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Phone Numbers(members)</label>
									<div class="control-label col-sm-10">
										<input name="phone_no" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Profiles + Standers CM interests</label>
									<div class="control-label col-sm-10">
										<input readonly name="view_profile_cm_int" class="form-control" value="unlimited" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Express customised interest(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="send_interest" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Initiate Chat(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="initiate_chat" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Family Details(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="view_family" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">View Horoscope(profiles)</label>
									<div class="control-label col-sm-10">
										<input name="view_horoscope" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Spotlight your profile in featured matches</label>
									<div class="control-label col-sm-10">
										<input name="spotlight_profile_in_matches" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="duration" class="control-label col-sm-2">Profile Highlights</label>
									<div class="control-label col-sm-10">
										<input name="profile_highlights" class="form-control" value="" type="text">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<input class="btn bg-red" value="Add Plan" type="submit">
									</div>
								</div>
							</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>