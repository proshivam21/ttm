<?php $counter = 1; $membership_plans = get_user_plans(); ?>

<?php 
$plan_id = $_REQUEST['id'];
if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete'){
	global $db;
	if($db->delete($db->plans, array('id' => $plan_id))) {
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your plan has been deleted succeessfully.');
	} else {
		$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Failed to delete your plan. There was an error deleting your plan from database. Please try again.');
	}
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Membership Plans
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Membership Plans</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Plans List</h3>
						
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Member Type</th>
									<th>Amount (USD)</th>
									<th>Amount (INR)</th>
									<th>Duration</th>
									<th>Action</th>
								</tr>
							</thead>
								<tbody>
								<?php foreach($membership_plans as $plan): ?>
									<tr>
										<td><?php echo $counter ?></td>
										<td><?php echo $plan->name; ?></td>
										<td><?php echo $plan->amount_in_usd; ?></td>
										<td><?php echo $plan->amount_in_inr; ?></td>
										<td><?php echo $plan->duration; ?></td>
										<td>
											<a href="?page=memberships&action=edit&id=<?php echo $plan->id; ?>" class="btn bg-orange btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
											<a onClick="return confirm('Are you sure you want to delete this plan?')" href="?page=memberships&action=delete&id=<?php echo $plan->id; ?>" class="btn bg-red btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i></a>

										</td>
									</tr>
								<?php $counter++; endforeach; ?>
								</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>