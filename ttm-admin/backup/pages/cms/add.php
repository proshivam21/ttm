<?php 
/*
** Allow input type file only gor below parameters
*/
$allow_array = array('news', 'post', 'story');
$image_id = 0;
if($_POST) {
	extract($_POST);
	/*
	** upload image
	*/
	
	if(isset($_FILES['image'])){
		$image_id = upload_media('image');
	}
	$args = array('title' => $title, 'description' => $description, 'type' => $_GET['type'], 'image_id' => $image_id);
	global $db;
	if($db->insert($db->pages, $args)) {
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your '.$_GET['type'].' has been succeessfully inserted.');
	} else {
		$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Failed to insert your '.$_GET['type'].'. There was an error updating your '.$_GET['type'].'. Please try again.');
	}
}

$page = get_page($page_id); 
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			CMS Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>CMS Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Add <?php echo ucfirst($_GET['type']);?></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-12">
							<form action="" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label for="title">Title</label>
									<input name="title" type="text" class="form-control" placeholder="Enter title here" value="<?php echo $page->title; ?>">
								</div>
								<div class="form-group">
									<label for="content">Description</label>
									<textarea class="summernote" name="description">
										<?php echo $page->description; ?>
									</textarea>
								</div>
								<?php if(in_array($_GET['type'], $allow_array)): ?>
								<div class="form-group">
									<label for="content">Upload Image</label>
									<input type="file" name="image" class="form-control padd0"/>
								</div>
								<?php endif;?>
								<div class="form-group">
									<div class="col-md-offset-0">
										<input class="btn bg-red" name="save" value="Save Changes" type="submit">
									</div>
								</div>
							</form>
						</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>