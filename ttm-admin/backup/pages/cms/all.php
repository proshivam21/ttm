<?php 
/*
** Allow input type file only gor below parameters
*/
$allow_array = array('news', 'post', 'story');

	if($_GET['type'] == 'page') 
	{
		$title = "All Pages";
		$button_set = "New Page";
	}
	elseif($_GET['type'] == 'post'){
		$title = "Blog Posts";
		$button_set = "New Post";
	}
	elseif($_GET['type'] == 'story'){
		$title = "Stories";
		$button_set = "New Story";
	}
	else
	{
		$title = "News & Events";
		$button_set = "News & Event";
	}
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			CMS Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>CMS Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><?php echo $title; ?></h3>&nbsp;
						<a href="<?php echo get_option('admin_url').'/?page=cms&type='.$_GET["type"].'&action=add'; ?>" class="btn bg-red" title="Add"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Add&nbsp;<?php echo $button_set; ?></a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Title</th>
									<?php if(in_array($_GET['type'], $allow_array)): ?>
									<th>Image</th>
									<?php endif;?>
									<th>Description</th>
									<th>Last Edited By</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $pages = get_pages($_GET['type']); foreach($pages as $page): ?>
								<tr>
									<td><?php echo $page->id; ?></td>
									
									<td><?php echo $page->title; ?></td>
									<?php if(in_array($_GET['type'], $allow_array)): ?>
										<td><?php echo fp_get_attachment_image($page->image_id, 'small', array('class' => 'img-responsive img-thumbnail', 'alt' => 'Image')); ?></td>
									<?php endif;?>
									
									<td><?php echo getExcerpt(strip_tags($page->description)); ?></td>
									<td><?php echo date('d M y h:i a', strtotime($page->last_updated)); ?></td>
									<td>
										<a href="<?php echo get_option('admin_url'); ?>/?page=cms&type=<?php echo $_GET['type'];?>&action=edit&id=<?php echo $page->id; ?>" class="btn bg-orange btn-xs" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;
									</td>
								</tr>
								<?php endforeach; ?>
								
								</tfoot>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>