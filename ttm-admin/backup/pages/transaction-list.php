<style>
	.pagination {
	  float: right;
	}
	.search_back{
		float: right;
	}
	
	.er_msg {
	  background: #eee none repeat scroll 0 0;
	  border: 1px solid #c2c2c2;
	  border-radius: 10px;
	  color: red;
	  font-size: 24px;
	  margin-bottom: 20px;
	  margin-top: 20px;
	  padding: 11px 0;
	  text-align: center;
	}
</style>
<?php
	$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
	$limit = get_option('posts_per_page');
	$offset = ($page - 1) * $limit;
	global $db;
	
	$query = "select * from {$db->transactions} where plan not like '%Free%' order by id desc limit $limit offset $offset";
	$results = $db->get_results($query);
	
	$query2 = "select * from {$db->transactions} where plan not like '%Free%' order by id desc";
	$results2 = $db->get_results($query2);
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Transaction Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Transaction Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						
						<div class="row">
							<div class="col-sm-8">
								<?php
									$total_members = count($results2);
									
								?>
								<h3 class="box-title">Transaction List</h3>
							</div>
							<div class="col-sm-3" align="right">
								<!--<a class="btn bg-orange" href="?action=export-members" title="Export CSV">
									<i class="fa fa-file-excel-o fa-lg"></i>
								</a>
								<a class="btn btn-danger delete-per-members" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>-->
								
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>Email Id</th>
									<th>Txn ID</th>
									<th>Member Type</th>
									<th>Pricing(USD)</th>
									<th>Purchase Date</th>
									<th>Expires On</th>
									<th>Duration</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 1;
									foreach(@$results as $result)
									{
										
								?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo ucwords(get_user_name($result->user_id)); ?></td>
											<td><?php echo (get_userdata($result->user_id)->user_email); ?></td>
											<td>
												<?php 
													echo $result->txn_id;
												?>
											</td>
											<td><?php  
														$plan_name = unserialize($result->plan);
														echo $plan_name[name]; 
												?>
											</td>
											<td><?php echo $result->actual_amount; ?></td>
											<td>
												<?php
													echo get_f_date($result->date);
												?>
											</td>
											<td>
											<?php 
												$expiry_date = unserialize($result->plan);
												echo get_f_date($expiry_date[expiry_date]);
											?>
											</td>
											<td>
											<?php 
												$duration = unserialize($result->plan); 
												echo $duration[duration];
											?>
											</td>
											<td><?php echo get_user_status($result->user_id); ?></td>
										</tr>
									<?php
										$i++;
									}
									if(empty(count($results))){
									echo '<tr><td colspan=8>No results were found!</td></tr>';
									}
									?>
								</tfoot>
						</table>
						<?php echo backend_pagination($total_members, $limit, $page);?>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>