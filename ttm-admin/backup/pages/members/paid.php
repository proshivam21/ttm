<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Manage Members
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Manage Members</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Paid Members</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>User Name</th>
									<th>Password</th>
									<th>Email id</th>
									<th>Created On</th>
									<th>Status</th>
									<th>Type</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Lokesh</td>
									<td>Lokesh1988</td>
									<td>9845625</td>
									<td>Lokesh1988@gmail.com</td>
									<td>26/9/2016</td>
									<td>Active</td>
									<td>Paid - Platinum</td>
									<td>
										<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;
										<i class="fa fa-trash-o" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-key" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-toggle-on" aria-hidden="true"></i>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Rajesh</td>
									<td>Rajesh233</td>
									<td>32654</td>
									<td>rajesh1451@live.com</td>
									<td>30/8/2016</td>
									<td>Inactive</td>
									<td>Paid - Gold</td>
									<td>
										<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;
										<i class="fa fa-trash-o" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-key" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-toggle-on" aria-hidden="true"></i>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Wayne</td>
									<td>wayne</td>
									<td>102201</td>
									<td>wayne@yahoo.com</td>
									<td>10/09/2016</td>
									<td>Active</td>
									<td>Paid - Diamond</td>
									<td>
										<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;
										<i class="fa fa-trash-o" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-key" aria-hidden="true"></i>
										&nbsp;
										<i class="fa fa-toggle-on" aria-hidden="true"></i>
									</td>
								</tr>
								</tfoot>   
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>