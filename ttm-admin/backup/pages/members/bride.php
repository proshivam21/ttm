<style>
	.pagination {
	  float: right;
	}
	.search_back{
		float: right;
	}
	
	.er_msg {
	  background: #eee none repeat scroll 0 0;
	  border: 1px solid #c2c2c2;
	  border-radius: 10px;
	  color: red;
	  font-size: 24px;
	  margin-bottom: 20px;
	  margin-top: 20px;
	  padding: 11px 0;
	  text-align: center;
	}
</style>
<?php
	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	switch($action){
		case 'edit':
					include 'edit-members.php';
					break;
		default:
					break;
					
	}
?>
<?php
	$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
	$limit = get_option('posts_per_page');
	$offset = ($page - 1) * $limit;
	$search = $_REQUEST['search'];
	if($search && !empty($search) ){
		$search_keyword_info = array('search' => '*'.esc_attr( $search ).'*',
				'search_columns' => array( 'user_login', 'user_nicename', 'user_registered', 'user_email' ));
	}		
	else{ $search_keyword_info = array(); }
	$query_args[] = array(
							'key'     => 'role',
							'value'   => 'members',
							'compare' => '='
						);
		$query_args[] = array(
							'key'     => 'gender',
							'value'   => 'Female',
							'compare' => '='
						);
		$args = array(
					'number'  => $limit, 
					'offset'  => $offset,
					$search_keyword_info,
					'meta_query' => array(
						'relation' => 'AND',
							$query_args
					)
				 );
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Member Management	
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Member Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12"> 
				<div class="box">
					<div class="box-header">
						
						<div class="row">
							<div class="col-sm-8">
								<?php
							
									$users = new FP_User_Query($args);
									$total_members = $users->get_total();
									
								?>
								<h3 class="box-title">Basic Members</h3>
							</div>
							<div class="col-sm-4" align="right">
								<!--<a class="btn bg-orange" href="?action=export-members" title="Export CSV">
									<i class="fa fa-file-excel-o fa-lg"></i>
								</a>-->
							</div>
						</div>
						<div class="row">
							<div class="col-sm-9">
								
							</div>
							<div class="col-sm-3">
								<form name="" method="get" action="">
									<input value="members" type="hidden" name="page">
									<label>Search:</label>
									<input class="form-control" name="search" type="text" value="<?php echo $_REQUEST['search']; ?>">
									<input class="search_btn" value="Search" type="submit" style="display:none;">
									
								</form>
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>Email id</th>
									<th>Created On</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 1;
									foreach(@$users->results as $user)
									{
										$status = $user->user_status;
										if($status == 1)
										{
											$user_status = 'Active';
											$action = 'deactivate_user';
											$title = "Deactivate User";
											$status_class = 'fa-undo';
										}
										else{
											$user_status = 'Inactive';
											$action = 'activate_user';
											$title = "Activate User";
											$status_class = 'fa-check';
										}
										echo '
										<tr>
											<td>'.$i.'</td>
											<td>'.get_user_name($user->ID).'</td>
											<td>'.$user->user_login.'</td>
											<td>'.$user->user_registered.'</td>
											<td>'.$user_status.'</td>
											<td>
												<a href="'.get_site_url().'/fp-admin/?page=members&type=active&action=edit&id='.$user -> ID.'" class="btn bg-orange btn-xs"><i class="fa fa-pencil" aria-hidden="true" title="Edit"></i></a>&nbsp;
												<a onclick="delete_user('.$user->ID.')" class="btn bg-red btn-xs"><i class="fa fa-trash-o" aria-hidden="true" title="Delete"></i></a>
												&nbsp;
												<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick=" '.$action.'('.$user ->ID.')"><i class="fa '.$status_class.'"></i></a></span>
											</td>
										</tr>
									';
									$i++;
									}
									if(empty(count($users->results))){
									echo '<tr><td colspan=8>No results were found!</td></tr>';
								}
								?>
								</tfoot>
						</table>
						<?php echo backend_pagination($total_members, $limit, $page);?>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>