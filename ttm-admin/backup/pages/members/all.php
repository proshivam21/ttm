<?php
	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	switch($action){
		case 'edit':
					include 'edit-members.php';
					break;
		default:
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Manage Members
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Manage Members</li>
		</ol>
	</section>
	<!-- Main content -->
	
	<section class="content">
		 
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
				
					<div class="box-header">
						
						<div class="row">
							<div class="col-sm-8">
								<?php
									
									$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
									$limit = get_option('posts_per_page');
									$offset = ($page - 1) * $limit;
									
									$search = str_replace('CM', '', $_REQUEST['search']);
									if($search && !empty($search) ){
										$search_keyword_info = array('search' => '*'.esc_attr( $search ).'*',
												'search_columns' => array( 'ID', 'user_login', 'user_nicename', 'user_registered', 'user_email' ));
									}		
									else{ $search_keyword_info = array(); }
									
									$members_info = new fp_User_Query(array_merge(array('orderby' => 'user_registered', 'order' => 'DESC',  'meta_key' => 'role', 'meta_value' => 'members',  'number'  => $limit, 'offset'  => $offset), $search_keyword_info )); 
									
									//print_R($members_info);
									$total_members = $members_info->get_total();
								?>
								<h3 class="box-title">All Members</h3>
							</div>
							<div class="col-sm-4" align="right">
								<!--<a class="btn bg-orange" href="?action=export-members" title="Export CSV">
									<i class="fa fa-file-excel-o fa-lg"></i>
								</a>-->
								<!--<a class="btn btn-danger delete-per-members" title="Delete"><span class="glyphicon glyphicon-trash"></span></a> -->
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-9">
								
							</div>
							<div class="col-sm-3">
								<form name="" method="get" action="">
									<input value="members" type="hidden" name="page">
									<label>Search:</label>
									<input class="form-control" name="search" type="text" value="<?php echo $_REQUEST['search']; ?>">
									<input class="search_btn" value="Search" type="submit" style="display:none;">
									
								</form>
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						
							<?php
								if ( ! empty( $members_info->results ) ) {
									
									echo '<table id="" class="table table-bordered table-striped">
										<thead>
											<tr>
												
												<th>User ID</th> 
												<th>Name</th> 
												<th>User Name</th>
												<th>Email id</th>
												<th>Created On</th>
												<th>Status</th>
												<th>Paid / Free</th>
												<th>Plan Type</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>';
									
								
									$i=1;
									if(isset($_REQUEST['set']) && !empty($_REQUEST['set']) ){
										$i = $limit*($_REQUEST['set'] -1) +1;
									}
									
									foreach($members_info ->results as $info){
										echo '<tr class="per_user'.$info -> ID.'">';
										
										
										echo '<td>'.get_user_prefix_id($info -> ID).'</td>';
										echo '<td>'.get_user_name($info -> ID).'</td>';
										echo '<td>'.$info -> user_login.'</td>';
										
										echo '<td>'.$info -> user_email.'</td>';
										echo '<td>'.date('d M, Y h:i:s A', strtotime($info -> user_registered)).'</td>'; 
										
										$status = $info -> user_status;
										if($status == 1){
											$user_status = 'Active';
											$action = 'deactivate_user';
											$title = "Deactivate User";
											$status_class = 'fa-undo';
										}
										else{ 
											$user_status = 'Inactive';
											$action = 'activate_user';
											$title = "Activate User";
											$status_class = 'fa-check';
										}
										
										echo '<td><span class="user-status">'.$user_status.'</span></td>';
										
										echo '<td>'.ucfirst(get_user_meta($info -> ID,'user_type',true)).'</td>';
										
										echo '<td>'.ucfirst(get_user_meta($info->ID,'plan_type',true)).'</td>';
										
										echo '<td style="width: 110px;">
												<a href="'.get_site_url().'/fp-admin/?page=members&action=edit&id='.$info -> ID.'" class="btn bg-orange btn-xs"><i class="fa fa-pencil" title="Edit User"></i></a>&nbsp;';
										
										echo '<a onclick="delete_user('.$info->ID.')" class="btn bg-red btn-xs" title="Delete User"><i class="fa fa-trash"></i></a>&nbsp;';
										
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick=" '.$action.'('.$info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>
											</td>';
										echo '</tr>';
										
									}
							
							echo '</tbody>
								</table>';
							
							}
							else{
								echo '<div class = "er_msg">We are not finding result related to this keyword.</div>';
							}
							
							echo backend_pagination($total_members, $limit, $page); ?>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>

	<?php } 
	?>