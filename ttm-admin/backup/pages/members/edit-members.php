<?php
	$user_id = $_REQUEST['id'];
	
	if($_POST['profile_submit']) {
		$age = intval(date('Y', time() - strtotime($_POST['dob']))) - 1970;
		update_user_meta($user_id, 'age', $age);
		foreach($_POST as $key => $value) {
			if($key !='profile_submit') {
				update_user_meta($user_id, $key, $value);	
			}
		}
	}
?>
<style>
.accordion_head {
    background-color: #f1f1f2;
    color: #72727d;
    cursor: pointer;
    font-size: 14px;
    margin: 11px 0;
    padding: 7px 0 7px 10px;
}
.accordion_body {
    padding: 0 0 0 10px;
}

.plusminus {
    background-color: hsl(0, 0%, 100%);
    border: 1px solid hsl(0, 0%, 80%);
    float: left;
    line-height: 0.5;
    margin: 0 10px 0 0;
    padding: 4px 4px 7px;
}
.plusminus {
    float: left;
}


.btn-default {
    background-color: #fe5a60;
    border: medium none;
    color: #fff;
    font-size: 14px;
    width: 100%;
}
.btn:hover, .button:focus {

  color: #fe5a60;
}
.btn {
    -moz-user-select: none;
    background-image: none;
    border: 1px solid rgba(0, 0, 0, 0);
    border-radius: 3px;
    cursor: pointer;
    display: inline-block;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857;
    margin-bottom: 0;
    padding: 9px 12px 8px;
    text-align: center;
    vertical-align: middle;
    white-space: nowrap;
}

.form-group {
    margin-bottom: 15px;
}
.input-group-addon:last-child {
    border-left: 0 none;
}
.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:last-child > .btn, .input-group-btn:last-child > .btn-group > .btn, .input-group-btn:last-child > .dropdown-toggle, .input-group-btn:first-child > .btn:not(:first-child), .input-group-btn:first-child > .btn-group:not(:first-child) > .btn {
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}
.input-group-addon {
    background-color: #eeeeee;
    border: 1px solid #cccccc;
    border-radius: 4px;
    color: #555555;
    font-size: 14px;
    font-weight: 400;
    line-height: 1;
    padding: 6px 12px;
    text-align: center;
}
.input-group-addon, .input-group-btn {
    vertical-align: middle;
    white-space: nowrap;
    width: 1%;
}
.input-group-addon, .input-group-btn, .input-group .form-control {
    display: table-cell;
}
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: #eeeeee;
    cursor: not-allowed;
    opacity: 1;
}
.form-control {
    border-radius: 2px;
    box-shadow: none !important;
}

textarea {
    overflow: auto;
	 width: 100%;
    vertical-align: top;
}
</style>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Member Information
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Manage Members</li>
		</ol>
	</section>
	<!-- Main content -->
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<!--<div class="box-header">
						Detail Information
					<!-- /.box-body 
					</div>-->
                     <div class="box-body">
						 <form id="editprofile" name="editprofile" method="post" class="fv-form fv-form-bootstrap">
				<div class="accordion_container">
				
					<div class="accordion_head" id="personal-info">Personal Information <span class="plusminus">+</span></div> 
					
					<div style="display: none;" class="accordion_body">
					 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>First Name</label>
                              <input type="text" value="<?php echo get_user_meta($user_id, 'first_name', true);?>" name="first_name" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" value="<?php echo get_user_meta($user_id, 'last_name', true);?>" class="form-control" name="last_name">
                            </div>
                          </div>
                        </div>
						 <div class="row">
                            <div class="form-group">
								<div>
								<div class="col-sm-6 col-md-6 ">
								<div class="form-group">
									<label>DOB</label>
									<div class="select-arrowreg">
									<div class="input-group input-append date" id="dateRangePicker">
										<input id="dob" readonly type="text" value="<?php echo get_user_meta($user_id, 'dob', true);?>" class="form-control" name="dob" />
										<span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
								<?php //get_day();?>
								
								</div>
								</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									  <label>Age</label>
									  <input type="text" readonly value="<?php echo get_user_meta($user_id, 'age', true);?>" name="age" class="form-control">
									</div>
								  </div>
								<!--<div class="col-sm-6 col-md-4 form-group">
								<label>&nbsp;</label>
								<div class="select-arrowreg">
								<?php get_month();?>
								</div>
							</div>
								<div class="col-sm-6 col-md-4 form-group">
								<div class="form-group">
								<label>&nbsp;</label>
								<div class="select-arrowreg">
								<?php get_year();?>
								</div>
								</div>
								</div>-->
								</div>
							  </div>
                        </div>
		
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group select-arrowreg">
                              <label>Gender</label>
                              <div class="select-arrowreg">
                                <?php 
										$gender = get_user_meta($user_id, 'gender', true);
										get_gender('gender', $gender);
								?>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-6 col-md-6">
							  <div class="form-group">
                              <label>Marital Status</label>
                              <div class="select-arrowreg">
                               <?php 
									$marital_status = get_user_meta($user_id, 'marital_status', true);
									get_marital_status('marital_status', $marital_status);
								?>
                              </div>
                            </div>
							</div>
                        </div>
						
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group select-arrowreg">
                              <label>Have Children</label>
                              <div class="select-arrowreg">
                             
							   <?php 
									$get_children = get_user_meta($user_id, 'have_children', true);
									get_children('have_children', $get_children);
								?>
                              </div>
                            </div>
                          </div>
                          
						  
						    
                        <div class="col-sm-6 col-md-6">
							  <div class="form-group">
                              <label>Mother Tongue</label>
							  <div class="select-arrowreg">
							  <?php 
									$get_mother_tongues = get_user_meta($user_id, 'mother_tongue', true);
									get_mother_tongues('mother_tongue', $get_mother_tongues);
								?>
                             
						</div>
                           </div>
                          </div>
                        </div>
                        <div class="row" id="about">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <label>About Me</label><br>
                              <!--<textarea name="aboutme" class="form-control">Testttt</textarea>-->
							  <textarea rows="5" id="aboutme" value="" name="about_me"><?php echo get_user_meta($user_id, 'about_me', true);?></textarea>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
					</div><!-- accordion_body end here -->
				</div>
				
				<div class="accordion_head" id="physical-appearance">Physical Appearance <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                       
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Height</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_heights = get_user_meta($user_id, 'height', true);
									get_heights('height', $get_heights);
								?>
                              
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>Weight</label>
                              <input type="text" value="<?php echo get_user_meta($user_id, 'weight', true);?>" class="form-control" name="weight">
                            </div>
                          </div>
						  <div class="col-md-3">
                            <div class="form-group">
                              <label>Weight Type</label>
							   <?php 
									$get_weight_type = get_user_meta($user_id, 'weight_type', true);
									get_weight_type('weight_type', $get_weight_type);
								?>
                              
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Body Type</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_body_type = get_user_meta($user_id, 'body_type', true);
									get_body_type('body_type', $get_body_type);
								?>
                               
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Complexion</label>
                              <div class="select-arrowreg">
							   <?php 
									$get_skin_type = get_user_meta($user_id, 'skin_tone', true);
									get_skin_type('skin_tone', $get_skin_type);
								?>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Special Cases</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_special_cases = get_user_meta($user_id, 'special_cases', true);
									get_special_cases('special_cases', $get_special_cases);
								?>
                                
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Blood Group</label>
                              <div class="select-arrowreg">
							   <?php 
									$get_blood_group = get_user_meta($user_id, 'blood_group', true);
									get_blood_group('blood_group', $get_blood_group);
								?>
                              
                              </div>
                            </div>
                          </div>
                        </div>
						<div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Looking Creator</label>
                              <div class="select-arrowreg">
							  <?php 
									$looking_for = get_user_meta($user_id, 'looking_for', true);
									looking_for('looking_for', $looking_for)
								?>
                                
                              </div>
                           </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="religious-details">Religious Details <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                      
                        <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                              <label>Religion</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_religions = get_user_meta($user_id, 'religion', true);
									get_religions('religion', $get_religions);
								?>
							   
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Caste</label>
                              <div class="select-arrowreg" id="caste">
							   	<input value="<?php echo  get_user_meta($user_id, 'caste', true); ?>" name="caste" class="form-control" type="text">
                              </div>
                           </div>
                          </div></div>
						  <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Sub-Caste</label>
							  
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'subcaste', true); ?>" class="form-control" name="subcaste">
                   </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="gothram-star">Gothram & Star Details <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				  <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                       
                        <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                              <label>Gothram</label>
                              <div class="select-arrowreg">
							   <input type="text" value="<?php echo  get_user_meta($user_id, 'gothram', true); ?>" class="form-control" name="gothram">
                              </div>
                           </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Rasi</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_rasi = get_user_meta($user_id, 'rasi', true);
									get_rasi('rasi', $get_rasi);
								?>
                              </div>
                           </div>
                          </div>
						 </div>
						   <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Star</label>
							  <div class="select-arrowreg">
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'star', true); ?>" class="form-control" name="star">
                            </div>
						    </div>	
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="lifestyle">Lifestyle <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                       
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Smoking</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_smoke = get_user_meta($user_id, 'smoke', true);
									get_smoke('smoke', $get_smoke);
								?>
                               
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Drinking</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_drink = get_user_meta($user_id, 'drink', true);
									get_drink('drink', $get_drink);
								?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Diet</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_diet = get_user_meta($user_id, 'diet', true);
									get_diet('diet', $get_diet);
								?>
                              </div>
                           </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="education-career">Education & Career <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                       
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
							 
                              <label>Education</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_education_level = get_user_meta($user_id, 'education_level', true);
									get_education_level('education_level', $get_education_level);
								?>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
							 
                              <label>Specific Area</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_education_field = get_user_meta($user_id, 'education_field', true);
									get_education_field('education_field', $get_education_field);
								?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
							                              <label>Profession</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_working_as = get_user_meta($user_id, 'working_as', true);
									get_working_as('working_as', $get_working_as);
								?>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Annual Income</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_annual_income = get_user_meta($user_id, 'annual_income', true);
									get_annual_income('annual_income', $get_annual_income);
								?>
                              </div>
                           </div>
                          </div>
                        </div>
						<div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
							                              <label>Working With</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_working_with = get_user_meta($user_id, 'working_with', true);
									get_working_with('working_with', $get_working_with);
								?>
                              </div>
                            </div>
                          </div>
						  </div>
						
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="family-details">Family Details <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Fathers' Profession</label>
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'father_profession', true); ?>" name="father_profession" class="form-control">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Mothers' Profession</label>
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'mother_profession', true); ?>" class="form-control" name="mother_profession" data-fv-field="mother_pro">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>No of Brothers</label>
                                  <input type="text" value="<?php echo  get_user_meta($user_id, 'bro_mar', true); ?>" placeholder="Married" class="form-control" name="bro_mar">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label></label>
                                  <input type="text" value="<?php echo  get_user_meta($user_id, 'bro_unmar', true); ?>" placeholder="Unmarried" class="form-control" name="bro_unmar">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>No of Sisters</label>
                                  <input type="text" value="<?php echo  get_user_meta($user_id, 'sis_mar', true); ?>" placeholder="Married" class="form-control" name="sis_mar">
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label></label>
                                  <input type="text" value="<?php echo  get_user_meta($user_id, 'sis_unmar', true); ?>" placeholder="Unmarried" class="form-control" name="sis_unmar">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Family Values</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_family_values = get_user_meta($user_id, 'family_values', true);
									get_family_values('family_values', $get_family_values);
								?>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Family Status</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_affluence_level = get_user_meta($user_id, 'affluence_level', true);
									get_affluence_level('affluence_level', $get_affluence_level);
								?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="contact-details">Contact Details <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                        
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Fathers' Name</label>
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'father_name', true); ?>" class="form-control" name="father_name">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Mothers' Name</label>
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'mother_name', true); ?>" class="form-control" name="mother_name">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Address</label>
                              <input type="text" value="<?php echo  get_user_meta($user_id, 'address', true); ?>" class="form-control" name="address">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group select-arrowreg">
                              <label>Country</label>
							  <div id="country_ddl">
								  <?php 
										$country_id = get_user_meta($user_id, 'country_living_in', true);
										get_living_ins('country_living_in', $country_id);
									?>
								</div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group select-arrowreg">
                              <label>State</label>
								<div class="select-arrowreg" id="state_ddl"> 
								<?php 
											$state_id = get_user_meta($user_id, 'state_living_in', true);
											
											get_state_ddl($country_id, $state_id);
								?>
												</div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group select-arrowreg">
                              <label>City</label>
								<div class="select-arrowreg city_living_ddl" id="citynew"> 
								
								<?php 		
											$city_id = get_user_meta($user_id, 'city_living_in', true);
											get_city_ddl($country_id, $city_id, $state_id);
								?>
								</div>
                              	
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="row">
								 <div class="col-md-5">
								<label for="ex1">Area Code</label>
								<div class="form-group">
		<input type="text" value="<?php echo  get_user_meta($user_id, 'area_code', true); ?>" class="form-control" name="area_code">
								</div>
								</div>
							 
							  <div class="row">
							  <div class="col-md-6">
								<label for="ex2">Landline</label>
		<input type="text" value="<?php echo  get_user_meta($user_id, 'landline', true); ?>" class="form-control" name="landline">
								</div>
							  </div>
							  </div>
                            </div>
                          </div>
                          <div class="col-md-6">
						    <div class="form-group">
                              <div class="row">
								 <div class="col-md-5">
								<label for="ex1">Code</label>
								<div class="select-arrowreg">
								<input type="text" value="<?php echo  get_user_meta($user_id, 'mobile_country_code', true); ?>" class="form-control" name="mobile_country_code">
								<?php 
									//$get_country_codes = get_user_meta($user_id, 'mobile_country_code', true);
									//get_country_codes('mobile_country_code', $get_country_codes);
								?>
								</div>
								</div>
							 
							  <div class="row">
							  <div class="col-md-6">
								<label for="ex2">Mobile No</label>
								<input type="text" value="<?php echo  get_user_meta($user_id, 'mobile_no', true); ?>" class="form-control" name="mobile_no">
								</div>
							  </div>
							  </div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
				</div><!-- accordion_body end here -->
				
				<div class="accordion_head" id="partner-preference">Partner Preference <span class="plusminus">+</span></div> 
				<div style="display: none;" class="accordion_body">
				 <div class="row">
                    <div class="col-lg-12">
                      <div class="profile-subhead">
                       
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
            
              <div class="row ">
              <div class="col-xs-5 col-sm-5 col-md-6">
			    <label>Age From</label>
				<div class="select-arrowreg">
				<?php 
									$get_age = get_user_meta($user_id, 'pp_age_from', true);
									get_age('pp_age_from', $get_age);
								?>
				</div>
              </div>
			<div class="form-group">	
            
              <div class="col-xs-5 col-sm-5 col-md-6">
			    <label>Age To</label>
			  <div class="select-arrowreg ">
			  <?php 
									$get_age = get_user_meta($user_id, 'pp_age_to', true);
									get_age('pp_age_to', $get_age);
								?>
				</div>
              </div></div></div>
            </div>
            
                          </div>
         <div class="col-md-12">
           <div class="form-group">
              <div class="row">
              <div class="col-xs-5 col-sm-5 col-md-6 select-arrowreg">
              <label>Height From</label>
			  <?php 
									$pp_height_from = get_user_meta($user_id, 'pp_height_from', true);
									get_heights('pp_height_from', $pp_height_from);
								?>
              </div>
              <div class="col-xs-5 col-sm-5 col-md-6 select-arrowreg">
              <label for="maxAge">Height To</label>
			  <?php 
									$pp_height_to = get_user_meta($user_id, 'pp_height_to', true);
									get_heights('pp_height_to', $pp_height_to);
								?>
             </div>
			  </div>
            </div>
        </div>
            </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Gender</label>
							  <div class="select-arrowreg">
							  <?php 
									$get_gender = get_user_meta($user_id, 'pp_gender', true);
									get_gender('pp_gender', $get_gender);
								?>
								  </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Family Values</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_family_values = get_user_meta($user_id, 'pp_family_values', true);
									get_family_values('pp_family_values', $get_family_values);
								?>
                              </div>
                           </div>
                          </div>
                        </div>
                        
                      </div>
						<div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Marital Status</label>
							    <div class="select-arrowreg">
								<?php 
									$get_marital_status = get_user_meta($user_id, 'pp_marital_status', true);
									get_marital_status('pp_marital_status', $get_marital_status);
								?>
								</div>
							 </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Have Children</label>
                                <div class="select-arrowreg">
								<?php 
									$get_children = get_user_meta($user_id, 'pp_have_children', true);
									get_children('pp_have_children', $get_children);
								?>
								</div>
                              
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Smoking</label>
                                <div class="select-arrowreg">
								<?php 
									$get_smoke = get_user_meta($user_id, 'pp_smoke', true);
									get_smoke('pp_smoke', $get_smoke);
								?>
                             </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Drinking</label>
                                <div class="select-arrowreg">
								<?php 
									$get_drink = get_user_meta($user_id, 'pp_drink', true);
									get_drink('pp_drink', $get_drink);
								?>
								</div>
                           
                           </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Body Type</label>
                              <div class="select-arrowreg">
							  <?php 
									$get_body_type = get_user_meta($user_id, 'pp_body_type', true);
									get_body_type('pp_body_type', $get_body_type);
								?>
							  </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Complexion</label>
                               <div class="select-arrowreg">
							   <?php 
									$get_skin_type = get_user_meta($user_id, 'pp_skin_tone', true);
									get_skin_type('pp_skin_tone', $get_skin_type);
								?>
						     </div>
                              </div>
                            </div>
                         
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Diet</label>
                               <div class="select-arrowreg">
							   <?php 
									$get_diet = get_user_meta($user_id, 'pp_diet', true);
									get_diet('pp_diet', $get_diet);
								?>
								</div>
                           
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Special Cases</label>
                                <div class="select-arrowreg">
								<?php 
									$get_special_cases = get_user_meta($user_id, 'pp_special_cases', true);
									get_special_cases('pp_special_cases', $get_special_cases);
								?>
								</div>
                           
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
							   
                              <label>Education</label>
							    <div class="select-arrowreg"> 
								<?php 
									$get_education_level = get_user_meta($user_id, 'pp_education_level', true);
									get_education_level('pp_education_level', $get_education_level);
								?>
								</div>
                        
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
							                              <label>Profession</label>
                              <div class="select-arrowreg">
							 
								<?php 
									$pp_working_as = get_user_meta($user_id, 'pp_working_as', true);
									get_working_as('pp_working_as', $pp_working_as);
								?>
								</div>
                           
                            </div>
                          </div>
                        </div>
                      
                      </div>
                    </div>		
					
				</div><!-- accordion_body end here -->
				 
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-2"><center><input type="submit" value="Save Profile" class="btn btn-default pink2-btn  butt-on btn-block" name="profile_submit"></center></div>
						<div class="col-md-5"></div>
					</div>
                  
                  </form>
					<!-- /.box-body -->
					</div>						
				<!-- /.box -->
				</div>
			<!-- /.col -->
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
