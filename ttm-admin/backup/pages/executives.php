<?php 
	$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
	$limit = get_option('posts_per_page');
	$offset = ($page - 1) * $limit;
	$search = str_replace('CMEX', '', $_REQUEST['search']);
	if($search && !empty($search) ){
		$search_keyword_info = array('search' => '*'.esc_attr( $search ).'*',
				'search_columns' => array( 'ID', 'user_login', 'user_nicename', 'user_registered', 'user_email' ));
	}		
	else{ $search_keyword_info = array(); }
$query_args[] = array(
						'key'     => 'role',
						'value'   => 'executive',
						'compare' => '='
					);
$args = array(
				'number'  => $limit, 
				'offset'  => $offset,
				'meta_query' => array(
					'relation' => 'AND',
						$query_args
				)
			 );
$args = array_merge($args, $search_keyword_info);
$executives = new FP_User_Query( $args );
			
$total_members = $executives->get_total();
									
								
?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Manage Members
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<?php 
				if(isset($_GET['exe_id']))
				{
					$heading = 'Executive Program (Associated Users)';
				}
				else{
					$heading = 'Executive Program';
				}
			?>
			<li>Executive Program</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
						<div class="col-md-12">
						<?php if(isset($_POST['submit']))
				{
					global $db;
					$payment = $_POST['payment'];
					$exe_code = $_POST['code'];
					$exe_left = $_POST['left'];
					$exe_comission = $_POST['comission'];
					if(($payment <= $exe_left) && ($exe_left <= $exe_comission) )
					{
						$date = date("Y-m-d");
						$result = $db->insert($db->executive_payment, array("exe_code" => $exe_code,"amount_paid" => $payment, "date" => $date));
						if($result)
						{
							echo '
								<div class="alert alert-success alert-dismissable">
								  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								  <strong>Success!</strong> Record has been inserted successfully.
								</div>
							';
							//header("Refresh:0");
						}
					}
					else{
						echo '
							<script>
								alert("You have alloted extra money than executive comission");
							</script>
						';
						//header("Refresh:0");
					}
				}
				?>
				</div>
							<div class="col-sm-9">
							
								<h3 class="box-title"><?php echo $heading;?></h3>
							</div>
							<div class="col-sm-3">
								<form name="" method="get" action="">
									<input value="executives" type="hidden" name="page">
									<?php 
											if(isset($_REQUEST['exe_id'])){
												echo '<input value="'.$_REQUEST['exe_id'].'" type="hidden" name="exe_id">';
											}
									?>
									<label>Search:</label>
									<input class="form-control" name="search" type="text" value="<?php echo $_REQUEST['search']; ?>">
									<input class="search_btn" value="Search" type="submit" style="display:none;">
									
								</form>
							</div>
						</div>
						
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					<?php
						if(isset($_GET['exe_id']))
						{
							include('exe/get_executive_detail.php');
						}
						else{
							include('exe/get_executives.php');
						}
					?>
					<?php echo backend_pagination($total_members, $limit, $page);?>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>