<?php 
	$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
	
	switch($type) {
		case 'active':
			include 'members/active.php';
		break;
		case 'inactive':
			include 'members/inactive.php';
		break;
		case 'free':
			include 'members/free.php';
		break;
		case 'paid':
			include 'members/paid.php';
		break;
		case 'basic':
			include 'members/basic.php';
		break;
		case 'diamond':
			include 'members/diamond.php';
		break;
		case 'gold':
			include 'members/gold.php';
		break;
		case 'silver':
			include 'members/silver.php';
		break;
		case 'bride':
			include 'members/bride.php';
		break;
		case 'groom':
			include 'members/groom.php';
		break;
		default:
			include 'members/all.php';
		break;
		
	}
?>