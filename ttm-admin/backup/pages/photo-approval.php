<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Photo Approval Management
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url');?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Photo Approval Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Photo Approvals List</h3>
						<div class="row">
							<div class="col-sm-9">
								
							</div>
							<div class="col-sm-3">
								<form name="" method="get" action="">
									<input value="photo-approval" type="hidden" name="page">
									<label>Search:</label>
									<input class="form-control" name="search" type="text" value="<?php echo $_REQUEST['search']; ?>">
									<input class="search_btn" value="Search" type="submit" style="display:none;">
									
								</form>
							</div>
						</div>
					</div>
					<?php 
							/* $unapproved_images = get_unapproved_images(); */
							$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
							$limit = get_option('posts_per_page');
							$offset = ($page - 1) * $limit;
							$search = str_replace('CM', '', $_REQUEST['search']);
							
							if($search && !empty($search) ){
								$search_keyword_info = array('search' => '*'.esc_attr( $search ).'*',
										'search_columns' => array( 'ID', 'user_login', 'user_nicename', 'user_registered', 'user_email', 'display_name' ));
							}		
							else{ $search_keyword_info = array(); }
							
							$members_info = new fp_User_Query(array_merge(array('orderby' => 'user_registered', 'order' => 'DESC',  'meta_key' => 'role', 'meta_value' => 'members',  'number'  => $limit, 'offset'  => $offset), $search_keyword_info )); 
							
							$total_members = $members_info->get_total();
					?>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									
									<th>Email Id</th>
						
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							
									if ( ! empty( $members_info->results ) ) {
										foreach($members_info ->results as $user){
											$status = ($user->user_status == 0) ? 'Inactive' : 'Active' ;
											echo '<tr>
													<td>'.get_user_prefix_id($user->ID).'</td>
													<td>'.get_user_name($user->ID).'</td>
												
													<td>'.$user->user_email.'</td>
										
													<td>'.$status.'</td>
													<td>
														<a href="javascript:void(0);" onclick="approve_images('.$user->ID.')" class="btn bg-green btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>
													</td>
												</tr>';
										}
									}
									else{
										echo '<tr><td colspan="5">';
										echo '<div class = "er_msg">We are not finding result related to this keyword.</div>';
										echo '</tr>';
									}
									
								
									/* if($unapproved_images){
										foreach($unapproved_images as $user_id => $imgs){
											$status = (get_userdata($user_id)->user_status == 0) ? 'Inactive' : 'Active' ;
											echo '<tr>
													<td>'.get_user_prefix_id($user_id).'</td>
													<td>'.get_user_name($user_id).'</td>
												
													<td>'.get_userdata($user_id)->user_email.'</td>
										
													<td>'.$status.'</td>
													<td>
														<a href="javascript:void(0);" onclick="approve_images('.$user_id.')" class="btn bg-green btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>
													</td>
												</tr>';
										}
									} */
							?>
								
								</tbody>
						</table>
							<?php echo backend_pagination($total_members, $limit, $page);?>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- Modal -->
<div id="list_unapproved_imgs" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rajesh Waiting Photos</h4>
      </div>
      <div class="modal-body" id="pending_photos">
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>