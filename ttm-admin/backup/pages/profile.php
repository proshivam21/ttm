<?php 

$userdata = get_userdata( $id ); 
if($_POST) {
	if(update_user($id, $_POST)) {
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your profile has been succeessfully updated');
	} else {
		$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Failed to Update Profile details. There was an error updating your profile. Please try again');
	}
}

?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Profile Settings</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo get_option('admin_url'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><i class="fa fa-users"></i> Profile</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<?php 
			if(isset($message)){
				print_message($message);
			}
		?>
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<!--<h3 class="box-title"></h3>-->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
							<div class="form-group">
								<label for="first_name" class="control-label col-sm-2">First Name</label>
								<div class="col-sm-10">
									<input name="first_name" type="text" class="form-control" id="first_name" placeholder="First Name" value="<?php echo get_user_meta($id, 'first_name', true); ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label for="last_name" class="control-label col-sm-2">Last Name</label>
								<div class="col-sm-10">
									<input name="last_name" type="text" class="form-control" id="last_name" placeholder="Last Name" value="<?php echo get_user_meta($id, 'last_name', true); ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="control-label col-sm-2">Email<sup>*</sup></label>
								<div class="col-sm-10">
									<input name="email" type="email" class="form-control" id="email" placeholder="Email" value="<?php echo $userdata -> user_email; ?>"/>
									<!-- The progress bar is hidden initially -->
									<div class="progress" id="emailProgressBar" style="margin: 5px 0 0 0; display: none;">
										<div class="progress-bar progress-bar-success progress-bar-striped active" style="width: 100%"></div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="last_name" class="control-label col-sm-2">Username</label>
								<div class="col-sm-10">
									<input name="username" type="text" class="form-control" id="last_name" placeholder="Username" value="<?php echo $userdata -> user_login; ?>"/>
								</div>
							</div>
							<div class="form-group">
								<label for="password" class="control-label col-sm-2">Password<sup>*</sup></label>
								<div class="col-sm-10">
									<input name="password" type="password" class="form-control" id="password" placeholder="Password">
								</div>
							</div>
							<div class="form-group">
								<label for="repeat_password" class="control-label col-sm-2">Repeat Password<sup>*</sup></label>
								<div class="col-sm-10">
									<input name="repeat_password" type="password" class="form-control" id="repeat_password" placeholder="Repeat Password" />
								</div>
							</div>

							<div class="form-group">
								<label for="profile_photo" class="control-label col-sm-2">Profile Photo</label>
								<div class="col-sm-10">
									<?php echo fp_get_attachment_image(get_user_meta($id, 'profile_photo', true), 'thumbnail', array('class' => 'img-responsive img-thumbnail', 'alt' => 'Profile Photo')); ?><br/><br/>
									<input type="file" name="profile_photo" id="profile_photo" value="" />
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="submit" class="btn bg-red" value='Update Profile' />
								</div>
							</div>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->