<?php 

class user {
	function update_user($user_id, $data) {
		extract($data);
		$userdata = array(
						'user_login' 		=>		$username,
						'user_pass' 		=>		$password,
						'user_nicename' 	=>		$nicename,
						'user_email' 		=>		$email,
						'display_name' 		=>		$display_name
					);
		$media_id = upload_media('profile_photo');
		$usermeta = array(
						'first_name'		=>		$first_name,
						'last_name'			=>		$last_name,
						'profile_photo'		=>		$media_id
					);
		if(empty($userdata['user_pass'])) {
			unset($userdata['user_pass']);
		}
					
		fp_update_user($user_id, $userdata);
					
		foreach($usermeta as $key => $value) {
			if(!empty($value))
				fp_update_user($user_id, $key, $value);
		}
		
	}
}

?>