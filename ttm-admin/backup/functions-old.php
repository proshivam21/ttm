<?php 
function print_message($args){
	echo '<div class="alert alert-'.$args['type'].'">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			  <strong>'.$args['title'].'!</strong> '.$args['message'].'
			</div>';
}

function update_user($user_id, $data) {
	extract($data);
	$userdata = array(
					'user_login' 		=>		$username,
					'user_pass' 		=>		fp_hash_password($password),
					'user_nicename' 	=>		$first_name.' '.$last_name, 
					'user_email' 		=>		$email,
					'display_name' 		=>		$last_name.' '.$first_name
				);
	$media_id = upload_media('profile_photo');
	$usermeta = array(
					'first_name'		=>		$first_name,
					'last_name'			=>		$last_name,
					'profile_photo'		=>		$media_id
				);
	if(empty($userdata['user_pass'])) {
		unset($userdata['user_pass']);
	}

	if(fp_update_user($user_id, $userdata)) {
		foreach($usermeta as $key => $value) {
			if(!empty($value))
				update_user_meta($user_id, $key, $value);
		}
		return true;
		
	} else {
		return false;
	}
}

function cm_delete_user($id){
	global $db;
	$where = array('ID' => $id);
	if(!$db -> delete("{$db->users}", $where)){
		return false;
	}
	$db->get_results("DELETE FROM {$db->usermeta} WHERE user_id = '{$id}' ");
	return true;
}

function cm_activate_member($id) {
	global $db;
	$where = array('ID' => $id);
	if(!$db -> update("{$db->users}",array('user_status' => 1), $where)) {
		return false;
	}
	return true; 
}

function cm_deactivate_member($id) {
	global $db;
	$where = array('ID' => $id);
	if(!$db -> update("{$db->users}",array('user_status' => 0), $where)) {
		return false;
	}
	return true; 
}



function backend_pagination($total_records, $limit, $set){

	$qs = http_build_query($_GET);
	$qs = str_replace('?set='.$set, '', $qs);
	$qs = str_replace('&set='.$set, '', $qs);
	$qs = str_replace('set='.$set, '', $qs);
	$identifier = (empty($qs)) ? '?' : '&';
	$identifier2 = (empty($qs)) ? '' : '?' ;
	// How many adjacent sets should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/

	$total_sets = $total_records;
	
	/* Setup vars for query. */
	$targetset = get_site_url()."/fp-admin".$identifier2.$qs; 	//your file name  (the name of this file)

	if ($set == 0) $set = 1;					//if no set var is given, default to 1.
	$prev = $set - 1;							//previous set is set - 1
	$next = $set + 1;	

	$lastset = ceil($total_sets/$limit);		//lastset is = total sets / items per set, rounded up.
	$lpm1 = $lastset - 1;						//last set minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastset > 1)
	{	
		$pagination .= "<div class=\"pagination\"><ul class=\"pagination pagination_1\">";
		//previous button
		if ($set > 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set=$prev\">&laquo; Previous</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">&laquo; Previous</span></a></li>";	
		
		//sets	
		if ($lastset < 7 + ($adjacents * 2))	//not enough sets to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastset; $counter++)
			{
				if ($counter == $set)
					$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
				else
					$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
			}
		}
		elseif($lastset > 5 + ($adjacents * 2))	//enough sets to hide some
		{
			//close to beginning; only hide later sets
			if($set < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lastset\">$lastset</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastset - ($adjacents * 2) > $set && $set > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $set - $adjacents; $counter <= $set + $adjacents; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lastset\">$lastset</a></li>";		
			}
			//close to end; only hide early sets
			else
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $lastset - (2 + ($adjacents * 2)); $counter <= $lastset; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($set < $counter - 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set=$next\">Next &raquo;</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">Next &raquo;</span></a></li>";
		$pagination.= "</ul></div>\n";		
	}
		echo $pagination;
}