<?php

require_once(dirname(dirname(__FILE__)) . '/fp-load.php');
require_once('functions.php');
global $util;
$redirect = $util -> get_current_page_url();
$id = fp_get_session('id'); 
$user_type = fp_get_session('type');
$user_status = fp_get_session('status'); 
if(!is_user_logged_in()) {
	
	fp_redirect(get_option('siteurl').'/fp-login.php?redirect_to='.urlencode($redirect).'');
	
} elseif($user_status==0) {
	fp_redirect(get_option('siteurl').'/fp-login.php?inactive=true');
} else {
	$types = array('administrators');

	if(!in_array($user_type, $types)) {
		fp_redirect(get_option('siteurl').'/fp-login.php?invalid=true');
	}
}