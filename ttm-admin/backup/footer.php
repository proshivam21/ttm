<footer class="main-footer">
  <div class="pull-right hidden-xs"><b>Version</b> 1.0</div>
  <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://continentalmetrimony.com">Continental Metrimony</a>.</strong> All rights
  reserved.
</footer>

<!-- Control Sidebar -->
<?php include 'control-sidebar.php'; ?>
<!-- /.control-sidebar -->

<!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<?php include 'foot.php'; ?>

</body>
</html>



