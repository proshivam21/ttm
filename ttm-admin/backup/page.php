<?php 
$dir_path = ABSPATH . 'fp-admin';
$page_name = isset($_REQUEST['page']) ? $_REQUEST['page'] : 'dashboard';
$page_url = $dir_path.'/pages/'.$page_name.'.php';

if(file_exists($page_url)) {
	$page = $page_url;
} else {
	$page = $dir_path.'/pages/404.php';	
}
include_once $page;
?>