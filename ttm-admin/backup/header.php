<!DOCTYPE html>
<html>
	<head>
		<?php include 'head.php'; ?>
	</head>
	<body class="hold-transition skin-black sidebar-mini">
		<div class="wrapper">
		<!-- Header: style can be found in header.less -->
		<header class="main-header">
			<!-- Logo -->
			<a href="<?php echo get_option('admin_url'); ?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">CM</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><center><img src="http://myideaaz.com/cm/fp-content/themes/cm/assets/img/logo.png" class="img-responsive" width="43%" /></center></span>
			</a> 
			<!-- Header Navbar: style can be found in header.less -->
			<?php include 'header-navbar.php'; ?>
			<!-- / hader-navbar -->
		</header>
		<!-- / header -->