<?php

/*
==========================================================================
		All ajax function here(User, challenge management)
==========================================================================
*/

require_once (dirname(dirname(dirname(__FILE__))).'/fp-load.php');

include_once (dirname(dirname(__FILE__)).'/functions.php');

$actionPerformed = $_GET['action'];
extract($_POST);
if (isset($_POST['username'])) {
	$isAvailable = true;
	if($actionPerformed == 'add'){
		if ($result = username_exists($_POST['username'])) {
			$isAvailable = false;
		}
	}
	echo json_encode(array(
		'valid' => $isAvailable,
	));
}

if ($actionPerformed == 'getTemplate'):
	$template_code = $_GET['templateCode'];
	if (!$content = get_option("$template_code")) {
		echo false;
	}

	echo $content;
endif; 

if(($actionPerformed == 'delete_user')):
	$user_id = $_GET['user_id'];
	$response = delete_profile($user_id);
	if($response == 'success'){
		return true;
	}
	else{
		return false;
	}
	/* if (!cm_delete_user($user_id)) return false;
	return true; */
endif;

if(($actionPerformed == 'deactivateUser')):
	$user_id = $_GET['user_id'];
	if (!cm_deactivate_member($user_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateUser')):
	$user_id = $_GET['user_id'];
	if (!cm_activate_member($user_id)) return false;
	return true;
endif;
/*
** get unapproved images by user id
*/
if(($actionPerformed == 'approve_images')):
	
	/* $images = get_unapproved_images_by_uid($user_id); */
	$images = get_images_by_uid($user_id);
	
	if($images[0]):
	foreach(@$images as $img_id){
		$status = get_media_status($img_id);
		if($status == 0){
			$approve_button = '<img title="Approve" onclick="image_action('.$user_id.','.$img_id.',1);" style="width:30px;height:30px;" data-type="profile_pic" class="cross approve_btn_'.$img_id.'" src="'.get_site_url().'/fp-admin/images/checkit.png">';
			$reject_button = '<img title="Reject" onclick="image_action('.$user_id.','.$img_id.',2);" data-type="profile_pic" class="cross reject_btn_'.$img_id.'" src="'.get_site_url().'/fp-content/themes/cm/assets/images/cross.jpg">';
		}
		elseif($status == 1){
			$approve_button = '';
			$reject_button = '<img title="Reject" onclick="image_action('.$user_id.','.$img_id.',2);" data-type="profile_pic" class="cross reject_btn_'.$img_id.'" src="'.get_site_url().'/fp-content/themes/cm/assets/images/cross.jpg">';
		}
		elseif($status == 2){
			$approve_button = '<img title="Approve" onclick="image_action('.$user_id.','.$img_id.',1);" style="width:30px;height:30px;" data-type="profile_pic" class="cross approve_btn_'.$img_id.'" src="'.get_site_url().'/fp-admin/images/checkit.png">';
			$reject_button = '';
		}
		$popup_html .= '<div class="row row_'.$img_id.'">
			<div class="col-md-12 text-center">
				'.$approve_button.'
				<img class="img-thumbnail" src="'.fp_get_attachment_image_url($img_id, 'medium').'" alt="User Image" width="50%">
				'.$reject_button.'
				<br/>
			</div>
		</div>';
	}
	else:
	$popup_html = 'No photo uploaded yet!';
	endif;
	echo json_encode(array('display_name' => get_user_name($user_id), 'popup_html' => $popup_html));

endif;
/*
** if image approved/reject
*/
if(($actionPerformed == 'image_action')):
	
	global $util;
	update_media_status($img_id, $img_status);
	if((get_user_meta($user_id, 'profile_pic', true) == $img_id) && $img_status == 1){
		/*
		** send template in email if approved
		*/
		
		$args = array('[NAME]' => get_user_name($user_id));
			
		$message_temp = $util -> get_mail_template('photo_approved', $args);
		$subject = 'Continental Matrimony Photo has been approved!';
		fp_mail(get_userdata($user_id) -> user_email, $subject, $message_temp);
		
	}
endif;

?>