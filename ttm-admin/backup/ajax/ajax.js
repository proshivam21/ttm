/*
====================================================================================
					All backend js functions here(User, challenge management)
====================================================================================
*/

/************* Load template in options email *************/

var getTemplate = function (templateCode) {
	$.ajax({
		url: "ajax/ajax.php?action=getTemplate&templateCode="+templateCode,
		beforeSend:function() {
			$("#approveImg_"+templateCode).attr("src","images/wait.gif");
		},
		success: function(data) {

				$(".note-editable").fadeOut(500, function(){
					$('.note-editable').show();
					$('.note-editable').html(data);
					
				}); 
				$('.summernote').html(data);

		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError);
		}
	});
};
  
/*
** Delete user
*/
var delete_user = function (userID) {

	if (confirm("Are you sure you want to delete user permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_user&user_id="+userID,
			
			success: function(data) 
			{
				$(".per_user"+userID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};
/*
** deactivate user
*/
var deactivate_user = function (userID) {

	 if (confirm("Are you sure you want to deactivate user permanently?") ) {
		$.ajax({
			type: "GET",
			url: "ajax/ajax.php?action=deactivateUser&user_id="+userID,
			
			success: function(data) 
			{
				$(".per_user"+userID+" .act-main").replaceWith("<span class='act-main'><a title='Activate User' class='btn btn-xs bg-green act' onclick='activate_user("+userID+")'><i class='fa fa-check'></i></a></span>");
				
				$(".per_user"+userID+" .user-status").replaceWith("<span class='user-status'>Inactive</span>");
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false;
};

var activate_user = function (userID) {

	 if (confirm("Are you sure you want to activate user permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=activateUser&user_id="+userID,
			data: {user_id:userID, action:'activateUser'},
			
			success: function(data) 
			{
				$(".per_user"+userID+" .act-main").replaceWith("<span class='act-main'><a title = 'Deactivate User' class='btn btn-xs bg-green act' onclick='deactivate_user("+userID+")'><i class='fa fa-undo'></i></a></span>"); 
				$(".per_user"+userID+" .user-status").replaceWith("<span class='user-status'>Active</span>");
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false;
};

/*
** Approve images
*/


function approve_images(user_id){
	$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=approve_images",
			data: {user_id:user_id},
			
			success: function(data) 
			{
				//alert(data);
				var response = $.parseJSON(data);
				$("#list_unapproved_imgs .modal-title").html(response.display_name);
				$("#pending_photos").html(response.popup_html);
				$("#list_unapproved_imgs").modal();
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
}

/*
** image action for approval/reject
*/

function image_action(user_id, img_id, img_status){
	$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=image_action",
			data: {user_id: user_id, img_id: img_id, img_status: img_status},
			
			success: function(data) 
			{
				
				//var response = $.parseJSON(data);
				if(img_status === 1){
					$(".approve_btn_"+img_id).remove();
				}
				else if(img_status === 2){
					$(".reject_btn_"+img_id).remove();
				}
				
				
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
}

