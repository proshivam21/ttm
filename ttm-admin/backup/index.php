<?php
/**
 * Dashboard Administration Screen
 *
 * @package FP
 * @subpackage Administration
 */

/** Load FP Environment */
require_once( dirname( __FILE__ ) . '/admin.php' );

/** Load FP Header */
include( ABSPATH . 'fp-admin/header.php' );

/** Load Left side column. contains the logo and sidebar*/
include( ABSPATH . 'fp-admin/sidebar.php' );

/** Load FP Admin Pages */
include( ABSPATH . 'fp-admin/page.php' );

/** Load FP Admin Footer */
include( ABSPATH . 'fp-admin/footer.php' );

/** Load FP Admin Class Auloader Class */
include( ABSPATH . 'fp-admin/classes/autoloadClass.php' );