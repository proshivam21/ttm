<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?php 
					echo fp_get_attachment_image(get_user_meta($id, 'profile_photo', true), 'thumbnail', array('class' => 'img-circle img-responsive', 'alt' => 'Profile Picture')); 
				?>
			</div>
			<div class="pull-left info">
				<p>Admin</p>
				<a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<!--<form action="javascript:void(0)" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
				<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				</button>
				</span>
			</div>
		</form>-->
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="<?php if(!isset($_REQUEST['page'])){echo 'active';}?> treeview">
				<a href="<?php echo get_option('admin_url'); ?>">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
				</span>
				</a>
				<!--<ul class="treeview-menu">
					<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
					<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
					</ul>-->
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-users"></i>
				<span>Members Management</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members"><i class="fa fa-users" aria-hidden="true"></i>
						All Members</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=active"><i class="fa fa-bolt" aria-hidden="true"></i>
						Active Members</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=inactive"><i class="fa fa-lock" aria-hidden="true"></i>
						Inactive Members</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=free"><i class="fa fa-flag" aria-hidden="true"></i>
						Free Members</a>
					</li>
					<li class="treeview">
						<a href="<?php echo get_option('admin_url'); ?>/?page=members&type=paid"><i class="fa fa-key" aria-hidden="true"></i>
						Paid Members</a>
						<ul class="treeview-menu">
							<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=basic"><i class="fa fa-lightbulb-o" aria-hidden="true"></i>
								Basic Members</a>
							</li>
							<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=silver"><i class="fa fa-dot-circle-o" aria-hidden="true"></i>
								Silver Members</a>
							</li>
							<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=gold"><i class="fa fa-heart" aria-hidden="true"></i>
								Gold Members</a>
							</li>
							<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=diamond"><i class="fa fa-diamond" aria-hidden="true"></i>
								Diamond Members</a>
							</li>
						</ul>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=groom"><i class="fa fa-male" aria-hidden="true"></i>
						Groom Profiles</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=members&type=bride"><i class="fa fa-female" aria-hidden="true"></i>
						Bride Profiles</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-link"></i>
				<span>Executive Management</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=executives"><i class="fa fa-list" aria-hidden="true"></i>All Executives</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-wordpress"></i>
				<span>CMS Management</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=cms&type=page"><i class="fa fa-files-o" aria-hidden="true"></i>
						All Pages</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=cms&type=news"><i class="fa fa-bullhorn" aria-hidden="true"></i>News & Events</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=cms&type=post"><i class="fa fa-bullhorn" aria-hidden="true"></i>Blog Posts</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=cms&type=story"><i class="fa fa-bullhorn" aria-hidden="true"></i>Stories</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-credit-card"></i>
				<span>Membership Plans</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=memberships"><i class="fa fa-list" aria-hidden="true"></i>
						Plans List</a>
					</li>
					<!--<li><a href="<?php echo get_option('admin_url'); ?>/?page=memberships&action=add"><i class="fa fa-plus-square" aria-hidden="true"></i>
						Add Plan</a>
					</li>-->
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-picture-o"></i>
				<span>Photo Approval</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=photo-approval"><i class="fa fa-hourglass" aria-hidden="true"></i>
						Awaiting Photo Approval </a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-usd"></i>
				<span>Transaction Management</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=transaction-list"><i class="fa fa-circle-o"></i>Transaction List</a></li>
				</ul>
			</li>
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-history"></i>
				<span>Plan Renewal Management</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=renewal"><i class="fa fa-refresh" aria-hidden="true"></i>
						Renewal List</a>
					</li>
				</ul>
			</li>
			<!-- <li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-users"></i>
				<span>Manage Forum</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right">4</span>
				</span>
				</a>
				<ul class="treeview-menu">
				  <li><a href="javascript:void(0)"><i class="fa fa-circle-o"></i> Manage Forum</a></li>
				</ul>
				</li> -->
			<li class="treeview">
				<a href="javascript:void(0)">
				<i class="fa fa-cogs"></i>
				<span>System Settings</span>
				<span class="pull-right-container">
				<span class="label label-primary pull-right"></span>
				</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=general-settings"><i class="fa fa-wrench" aria-hidden="true"></i>
						General</a>
					</li>
					<li><a href="<?php echo get_option('admin_url'); ?>/?page=options-email"><i class="fa fa-wrench" aria-hidden="true"></i>
						Email Templates</a>
					</li>
				</ul>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>