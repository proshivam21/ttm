<?php 
	$ci = & get_instance();
?>
<style>
	.alert.alert-danger.alert-dismissable.text-left {
	  margin-top: 20px;
	}
</style>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>MillionDC | Log in</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<!-- Bootstrap 3.3.2 -->
		<link href="<?php echo $ci->admin_url; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- style.css -->
		<link href="<?php echo $ci->admin_url; ?>/bootstrap/css/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $ci->admin_url; ?>/css/style.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome Icons -->
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- Theme style -->
		<link href="<?php echo $ci->admin_url; ?>/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
		<!-- iCheck -->
		<link href="<?php echo $ci->admin_url; ?>/plugins/iCheck/square/red.css" rel="stylesheet" type="text/css" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="#"><img class="img-responsive" src="<?php echo $ci->admin_url; ?>/images/logo.png"></a>
			</div>
			<!-- /.login-logo -->
			<div class='login'>
			</div>
			
			
			<?php
			
			/* $action = $_REQUEST['action'];
		
			if(isset($action) && $action == 'forgotpassword'){ */
			?>
			<!--<div class="login-box-body">
				<p class="login-box-msg">Please enter password and confirm password. </p>
				<form name="login-form" action="" method="post" id="login-form" >
					<div class="form-group has-feedback">
						<input name="password" type="text" class="form-control" placeholder="Password"/>
						<input name="con_pass" type="text" class="form-control" placeholder="Confirm Password"/>
						
					</div>
					
					<div class="row">
						
						<!-- /.col -->
					<!--	<div class="col-xs-6">
							<input name='forget_pass' type="submit" value="Change Password" class="btn bg-red btn-block " style="margin-bottom: 10px;">
						</div>
						<!-- /.col -->
					<!-- </div>
				</form>
				</br> 
				<?php
					/* if(isset($success_mail)){
						echo $success_mail;
					} */
				?>
			</div> -->
			
			<?php
			/* }
			
			else { */
			?>
			
			<div class="login-box-body">
				<p class="login-box-msg">Sign in to start your session</p>
				<form name="login-form" action="<?php echo base_url();?>administrator/login" method="post" id="login-form" enctype="multipart/form-data">
					<div class="form-group has-feedback">
						<input name="user_email" type="text" class="form-control" placeholder="Username"/>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input name="password" type="password" class="form-control" placeholder="Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="row">
						<div class="col-xs-8">
							<div class="checkbox icheck">
								<label>
								<input name="rememberme" type="checkbox" > Remember Me
								</label>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-xs-4">
							<input name='login'  type="submit" value="Sign In" class="btn bg-red btn-block ">
						</div>
						<!-- /.col -->
					</div>
				</form>
				<a href="<?php echo base_url();?>administrator/forget_password">I forgot my password</a>
				<?php
					if(isset($err)){
						echo $err;
					}
				?>
			</div>
			
			<?php
			//}
			?>
			
			<!-- /.login-box-body -->					
			<script type="text/javascript">
				function isr_attempt_focus(){
				setTimeout( function(){ try{
							d = document.getElementById('user_login');
							d.focus();
				d.select();
				} catch(e){}
				}, 200);
				}
				
							isr_attempt_focus();
							if(typeof isrOnload=='function')isrOnload();
						
			</script>
		</div>
		<!-- /.login-box -->
		<!-- jQuery 2.1.3 -->
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="<?php echo $ci->admin_url; ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
		
		<script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
		<!-- Bootstrap 3.3.2 JS -->
		<script src="<?php echo $ci->admin_url; ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!-- iCheck -->
		<script src="<?php echo $ci->admin_url; ?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
		
		<script>
			$(function () {
			$('input').iCheck({
			 checkboxClass: 'icheckbox_square-red',
			 radioClass: 'iradio_square-red',
			 increaseArea: '20%' // optional
			});
			});
		</script>
		<!-- iCheck -->
	</body>
</html>
