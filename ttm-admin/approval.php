<?php 
	if($role == 'pm') {
		include 'timesheet-approvals/pm-approval-view.php';
	} else if($role == 'lm') {
		include 'timesheet-approvals/lm-approval-view.php';
	} else {
		include 'timesheet-approvals/client-approval-view.php';
	}
?>