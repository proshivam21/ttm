<?php
require_once(dirname(dirname(__FILE__)) . '/ttm-load.php');

require_once('functions.php');

global $util;

$redirect = $util -> get_current_page_url();

$id = ttm_get_session('id'); 

$user_type = ttm_get_session('type');

$user_status = ttm_get_session('status');

if(!is_user_logged_in()) {
	ttm_redirect(get_option('siteurl').'/ttm-login.php?redirect_to='.urlencode($redirect).'');	
} elseif($user_status==0) {
	ttm_redirect(get_option('siteurl').'/ttm-login.php?inactive=true');
} /* else {
	$types = array('administrators');

	if(!in_array($user_type, $types)) {
		ttm_redirect(get_option('siteurl').'/ttm-login.php?invalid=true');
	}
} */