<?php
$page   = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if ($page == 'client-companies') {
    switch ($action) {
        case 'edit':
            include 'client-companies/edit.php';
            break;
        case 'add':
            include 'client-companies/add.php';
            break;
        case '':
            include 'client-companies/view.php';
            break;
        default:
            include 'client-companies/view.php';
            break;
    }
}