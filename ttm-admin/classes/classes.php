<?php
	/** Load TTM User Class */
	
	include( ABSPATH . 'ttm-admin/classes/user.php' );
	$user = new user();
	
	
	/** Load TTM Client Contact */
	
	include( ABSPATH . 'ttm-admin/classes/clientContact.php' );
	$cc = new clientContact();
	
	
	/** Load TTM Project Class */
	
	include( ABSPATH . 'ttm-admin/classes/project.php' );
	$project = new project();
	
	/** Load TTM Company */
	
	include( ABSPATH . 'ttm-admin/classes/company.php' );
	$company = new company();
	
	/** Load TTM Bank Holidays */
	
	include( ABSPATH . 'ttm-admin/classes/holidays.php' );
	$holidays = new holidays();

?>