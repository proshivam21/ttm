<?php
class project{
	function add_project(){
		global $db;
		extract($_POST);
		/*
		 * Create project
		 */
		$db->insert($db->company_projects, $_POST);
		
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Project has been added sucessfully.');
		return $message;
	}
	
	function update_project($id = ''){
		global $db;
		extract($_POST);
		
		$project_info = array(
							'project_name' 			=> $project_name,
							'client_int_order_no' 	=> $client_int_order_no,
							'project_number' 		=> $project_number,
							'status' 				=> $status,
							'client_approval' 		=> $client_approval,
							'budget_total' 			=> $budget_total,
							'start_date' 			=> date("Y-m-d", strtotime($start_date)),
							'end_date' 				=> date("Y-m-d", strtotime($end_date)),
							'comment' 				=> $comment,
						);
						
		if(empty($budget_rest)){
			$budget_rest = $budget_total;
			$project_info = array_merge($project_info, array('budget_rest' => $budget_rest));
		}
		
		/*
		 * Update project
		 */
		if($db->update($db->company_projects, $project_info, array('ID' => $id) ) ) 
		return $message = array('title' => 'Success', 'type' => 'success', 'message'=>'Project has been updated sucessfully.');
	}
}


?>