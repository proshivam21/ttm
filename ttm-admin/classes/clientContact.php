<?php 

class clientContact extends user{
	
	function add_client_contact(){
		global $db, $util;
		extract($_POST);
	
		$full_name = $first_name.' '.$last_name;
		
		$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
		
		$role_name = ucwords(get_role_name_by_role_id($role) ->role_val);
		
		/*
		 * Create company client contact
		 */
		$company_client_contact = array(
									'client_first_name'		=>  $first_name,
									'client_last_name'		=>  $last_name,
									'client_phone_no' 		=>  $phone_number,
									
									'client_email_address' 	=>  $email,
									'client_company_id'		=>  $client_company_id,
									'company_id'			=>  $current_user_company_id,
									'cost_center_id'		=>  $cost_center_id,
									'status'				=>	$status,
									'comment'				=>	$comment,
									'created_at'    		=>  date('Y-m-d h:i:s')
								);
	
		$db->insert($db->company_client_contacts, $company_client_contact);
		
		/***
		Send Email To System Admin
		****/
		
		$message = $util -> get_mail_template('new_user_registered', array('[NAME]' => ucwords($full_name), '[USER_NAME]' => $email, '[PASSWORD]' => $password, '[ROLE]' => $role_name, '[ADMIN_PANEL_URL]' => get_site_url().'/ttm-admin') );

		// Sending email
		send_email($email, 'Your User Account Created on TTM', 'info@airansoft.com', $message);
		
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your client comntact has been added sucessfully.');	
	}
	
	
	function update_client_contact(){
		
	}
}

?>