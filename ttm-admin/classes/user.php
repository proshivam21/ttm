<?php 

class user{
	
	function add_user($type = ''){
		global $db, $util;
		extract($_POST);
		
		$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
		
		$check = get_user_by('login', $email);
		
		if(!$check){
			$full_name = $first_name.' '.$last_name;
			/*
			 * Create user
			 */
			$userdata = array(
							'user_login' 		 =>  $email,
							'user_pass'  		 =>  ttm_hash_password($password),
							'user_email'  		 =>  $email,
							'role_id'			 =>  $role,
							'user_status'		 =>  $status,
							'user_registered'    =>  date('Y-m-d h:i:s'),
							'display_name'       =>  ucwords($full_name),
							'user_nicename'		 =>  ucwords($full_name),
							'company_id'		 =>	 $current_user_company_id,
						);
			$user_id = ttm_insert_user( $userdata ) ;
			
			$meta = array(
							'first_name' 	=> 	$first_name,
							'last_name'  	=> 	$last_name,
							'dob'  		 	=>	$dob,
							'hr_id'		 	=> 	$hr_id,
							'phone_number' 	=> 	$phone_number,
							'comment'		=>	$comment,
							'country_id'	=>	$country_id,
							'area_id'		=>  $state_living_in,
							'lm_id'			=>  $lm_id,
					);
					
			foreach($meta as $key => $value){
				update_user_meta($user_id, $key, $value);
			} 
			
			$role_name = ucwords(get_role_name_by_role_id($role) ->role_val);
			
			if($type == 'company'){
				
			}
			else{
				$message = $util -> get_mail_template('new_user_registered', array('[NAME]' => ucwords($full_name), '[USER_NAME]' => $email, '[PASSWORD]' => $password, '[ROLE]' => $role_name, '[ADMIN_PANEL_URL]' => get_site_url().'/ttm-admin') );

				// Sending email
				send_email($email, 'Your User Account Created on TTM', 'info@airansoft.com', $message);
			}
			return array('title' => 'Success', 'type' => 'success', 'message'=>'Account has been created succeessfully.', 'flag' => 1, 'user_id' => $user_id);
		}
		else return array('title' => 'Error', 'type' => 'danger', 'message'=>'Email is already exist in our database. Please try another email.', 'flag' => 0);
	}
	
	function update_user($id = ''){
		global $db;
		extract($_POST);
		
		$full_name = $first_name.' '.$last_name;
		/*
		 * Update user
		 */
		
		$userdata = array(
						'role_id'			 =>  $role,
						'user_status'		 =>  $status,
						'display_name'       =>  ucwords($full_name),
						'user_nicename'		 =>  ucwords($full_name),
					);
		
		if(!empty($password) ) $userdata = array_merge($userdata, array('user_pass'  =>  ttm_hash_password($password)) );
		
		foreach($userdata as $key => $value) {
			global $db;
			$val = $db->update( $db->users, array($key => $value), array('ID' => $id) );
		}
		
		$meta = array(
						'first_name' 	=> 	$first_name,
						'last_name'  	=> 	$last_name,
						'dob'  		 	=>	$dob,
						'hr_id'		 	=> 	$hr_id,
						'phone_number' 	=> 	$phone_number,
						'comment'		=>	$comment,
						'country_id'	=>	$country_id,
						'area_id'		=>  $state_living_in,
						'lm_id'			=>  $lm_id,
				);
				
		if(isset($_FILES['profile_photo']) && !empty($_FILES['profile_photo']['name'])){
			$image_id = upload_media('profile_photo');
			$meta = array_merge($meta, array('profile_photo' => $image_id) );
		}
		
		foreach($meta as $key => $value){
			if(!empty($value))
			update_user_meta($id, $key, $value);
		}
		
		return array('title' => 'Success', 'type' => 'success', 'message'=>'Information has been updated sucessfully.', 'flag' => 1);
	}

}

?>