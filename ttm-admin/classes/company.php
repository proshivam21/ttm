<?php
	class company{
		function add_company($user_id = ''){
			global $db, $util;
			extract($_POST);
			$companydata = array(
								'company_name'		 	=>  $company_name,
								'account_name'		 	=>  $account_name,
								'contact_first_name' 	=>  $first_name,
								'contact_last_name'  	=>  $last_name,
								'contact_email_address' =>  $contact_email_address,
								'company_postal_address'=>  $company_postal_address,
								'company_tax_num'		=>	$company_tax_no,
								'status'				=>	$status,
								'comment'				=>	$comment,
								'created_at'    		=>  date('Y-m-d h:i:s')
							);
							
			if(isset($_FILES['company_logo']) && !empty($_FILES['company_logo']['name'])){
				$image_id = upload_media('company_logo');
				$companydata = array_merge($companydata, array('company_logo_id' => $image_id) );
			}
			
			$db->insert($db->companies, $companydata);
			
			$company_id = $db -> insert_id;
			
			$company_meta = array(
								'billing_email' 	=> $billing_email,
								'billing_address'  	=> $billing_address
							);
					
			foreach($company_meta as $key => $value){
					$db->insert( $db->companymeta, array(
						'meta_key' 		=> $key,
						'meta_value' 	=> $value,
						'company_id'	=> $company_id
						),
						array('%s', '%s', '%s') 
					);
			}	
			
			/***
			Send Email To Company Admin
			****/
		
			$admin_url  = get_site_url().'/ttm-admin';

			$message_to_system_admin = $util -> get_mail_template('add_company_account', array('[NAME]' => get_name_by_user_id($user_id), '[COMPANY_NAME]' => $company_name,'[USER_NAME]' => $contact_email_address, '[PASSWORD]' => $password, '[ADMIN_PANEL_URL]' => $admin_url) );

		
			// Sending email
			send_email($contact_email_address, 'Your System Admin User Account Created on TTM', 'info@airansoft.com', $message_to_system_admin);
			
			
			/***
			Send Email To System Admin
			****/
		
			$admin_url  = get_site_url().'/ttm-admin';

			$message = $util -> get_mail_template('new_company_registered', array('[NAME]' => get_name_by_user_id($user_id), '[COMPANY_NAME]' => $company_name,'[USER_NAME]' => $contact_email_address, '[PASSWORD]' => $password, '[ADMIN_PANEL_URL]' => $admin_url) );

			// Sending email
			send_email('proshivam21@gmail.com', 'Company Account Created on TTM', 'info@airansoft.com', $message);
			
			if(!empty($user_id)){
				global $db;
				$db->update( $db->users, array('company_id' => $company_id), array('ID' => $user_id) );
			}
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your Company has been added sucessfully.');
			return $message;
		}
		
		function update_company(){
			global $db;	
			extract($_POST);
			
			$id = $comp_id;
			
			$companydata = array(
								'company_name'		 	=>  $company_name,
								'account_name'		 	=>  $account_name,
								'contact_first_name' 	=>  $first_name,
								'contact_last_name'  	=>  $last_name,
								'company_postal_address'=>  $company_postal_address,
								'company_tax_num'		=>	$company_tax_no,
								'status'				=>	$status,
								'comment'				=>	$comment
							);
							
			if(isset($_FILES['company_logo']) && !empty($_FILES['company_logo']['name'])){
				$image_id = upload_media('company_logo');
				$companydata = array_merge($companydata, array('company_logo_id' => $image_id) );
			}
						
			$db->update($db->companies, $companydata, array('ID' => $id));
			
			$company_meta = array(
								'billing_email' 	=> $billing_email,
								'billing_address'  	=> $billing_address
							);
					
			foreach($company_meta as $key => $value){
				$count = check_key_exist($db->companymeta,  $id, $key);
				
				if($count == 0){
					$db->insert( $db->companymeta, array(
						'meta_key' 		=> $key,
						'meta_value' 	=> $value,
						'company_id'	=> $id
						),
						array('%s', '%s', '%s') 
					);
				}
				else{
					$db->update( $db->companymeta, 
						array(
							'meta_value' 	=> $value,
						),
						array('company_id' => $id, 'meta_key' => $key)
					);  
				}
				unset($count);
			}
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Company Information has been updated sucessfully.');
			return $message;
		}
	}
?>