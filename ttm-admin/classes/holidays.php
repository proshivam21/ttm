<?php
	class holidays{
		function add_holidays(){
			global $db;
			extract($_POST);

			/*
			 * Create bank holidays
			*/
			$company_id = get_cur_user_company_id(ttm_get_session('id'));
			
			$holidays = array(
							'company_id'		=>	$company_id,
							'holiday_name'		=>	$holiday_name,
							'holiday_date'		=>	date("Y-m-d", strtotime($holiday_date)),
							'holiday_free_time'	=>	$holiday_free_time,
							'comment'			=>	$comment,
						);
			
			if(!empty($areas)){
				$holidays = array_merge($holidays, array('holiday_areas_ids' => serialize($areas), 'holiday_country_id' => $country_id) );
			}
			
			$db->insert($db->bank_holidays, $holidays);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Bank holidays has been added sucessfully.');
			return $message;
		}
		
		function update_holidays(){
			global $db;
			extract($_POST);
			/*
			 * update bank holidays
			 */
			 
			$holidays = array(
							'holiday_name'		=>	$holiday_name,
							'holiday_date'		=>	date('y-m-d', strtotime($holiday_date)),
							'holiday_free_time'	=>	$holiday_free_time,
							'comment'			=>	$comment,
						);
			
			$where = array('ID' => $id);
			
			$db->update($db->bank_holidays, $holidays, $where);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Bank holidays has been updated sucessfully.');
			return $message;
		}
		
		function get_bank_holidays($arr = ''){
			global $db;
			$where = '';
			if(!empty($arr)){
				$where = 'where ';
				foreach($arr as $k => $v){
					$where .= "$k = $v AND ";
				}
				$where = rtrim($where,'AND ');
			}
			return $db -> get_results("select *from {$db -> bank_holidays} $where");		
		}
	}
?>