 <?php
	$start_date = $_REQUEST['start'];
	$end_date   = $_REQUEST['end'];
	
	$ts_ID = $_REQUEST['ID'];
	$period_type = 'period'.$current_user_company_id;
	
	$page       = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
	$action     = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
	
	
	global $db;
	$ts_info = $db -> get_row("SELECT *FROM {$db->timsheets} where ID = {$ts_ID}");
	
	if ($action != 'add' && $action != 'edit'){
?> 


<section class="content">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="box box-solid">
				<div class="box-header with-border">
					<i class="fa fa-list-alt" aria-hidden="true"></i>
					<h3 class="box-title">Overview</h3>
					<div class="pull-right">
						<a class="btn btn-info" href="timesheets"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
						<a class="btn bg-blue" href="#"><i class="fa fa-paper-plane" aria-hidden="true"></i> Send</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<div class="col-md-12 no-padding">
						<table class="table table-hover table-bordered table-striped">
							<tr>
								<th><?php echo $period_type; ?></th>
								<td> <?php echo $ts_info -> timesheet_name; ?></td>
							<tr>
								<th>Status</th>
								<td>Ready for Approval</td>
							</tr>
							<tr>
								<th>Year</th>
								<td><?php echo $ts_info -> year; ?></td>
							<tr>	
								<th>Total Hours Spent</th>
								<td>40:00</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<div class="col-xs-12 col-md-8">
			<div class="box box-solid">
				<div class="box-header with-border">
					<i class="fa fa-comment" aria-hidden="true"></i>
					<h3 class="box-title">Comments</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<div class="col-md-12 no-padding">
						<table class="table table-hover table-bordered table-striped">
							<tr>
								<th>Commented By</th>
								<th>Date</th>
								<th style="width: 65%;">Comment</th>
								
							</tr>
							<tr>
								<th>Project Manager: Marc</th>
								<td>01.11.2017</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</td>
							</tr>
							<tr>
								<th>Line Manager: Shivam</th>
								<td>03.11.2017</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	    <?php
		 }
			if ($page == 'timesheet') {
				switch ($action) {
					case 'edit':
						include 'entry/edit.php';
						break;
					case 'add':
						include 'entry/add.php';
						break;
					default:
						include 'entry/view.php';
						break;
				}
			}
		?> 
	</div>
	<!-- /.row -->
</section>