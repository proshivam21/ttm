<?php	
	if($_POST){
		global $db;
		extract($_POST);
		
		$id = ttm_get_session('id');
		$user = $db->get_row($db->prepare("SELECT * FROM $db->users WHERE ID='%s'", $id));
		
		$password_hashed =  $user -> user_pass;
		
		if(ttm_check_password($old_password, $password_hashed, $id)) {
			ttm_set_password( $new_password, $id);
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your password has been changed succeessfully.');
		}
		else{
			$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Old password does not match. Please enter correct password.');
		}
	}
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" id="change_password" method="post" action ="">										<div class="box-header with-border">						<h3 class="box-title"><i class="fa fa-cog" aria-hidden="true"></i> Change Password</h3>						<div class="pull-right">							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;							<button type="submit" class="btn btn-info" name="update_password">Update</button>						</div>					</div>					
					<div class="box-body">
						<div class="form-group">
							<label for="old_password" class="col-sm-2 control-label">Old Password<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="old_password" type="password" class="form-control" value="<?php echo $_POST['old_password']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="new_password" class="col-sm-2 control-label">New Password<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="new_password" type="password" class="form-control" id="new_password" value="<?php echo $_POST['new_password']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="confirm_password" class="col-sm-2 control-label">Confirm Password<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="confirm_password" type="password" class="form-control" value="<?php echo $_POST['confirm_password']; ?>">
							</div>
						</div>
						
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-cog" aria-hidden="true"></i> Set Default Language</h3>
					<div class="pull-right">
						<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
						<button type="submit" class="btn btn-info">Save</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal">
					<div class="box-body">
		
		<div class="form-group">
							<label for="old_password" class="col-sm-2 control-label">Language</label>
							<div class="col-sm-10">
								<select class="selectpicker" data-width="fit">
									<option data-content='<span class="flag-icon flag-icon-us"></span> English'>English</option>
									<option  data-content='<span class="flag-icon flag-icon-de"></span> German'>German</option>
								</select>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>