
<?php	$cur_user_id = ttm_get_session('id');	$projects_info = get_company_project_info(array('project_manager_id' => $cur_user_id));	//print_R($projects_info);?><section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-tasks" aria-hidden="true"></i>
					<h3 class="box-title">Projects Overview</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project Title</th>
								<th>Client Contact</th>
								<th>Client Company</th>
								<th>Budget Total</th>
								<th width="200">Budget Used</th>
								<th>Budget Rest</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Active</th>
								<th>Client Review Approval</th>
								<th width="100px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php 								$i = 1;								
							foreach($projects_info as $info){ 							
								$status = $info -> status;
								
								if($status == 1){							
									$action = 'deactivate_project';												
									$title = "Inactive Project";												
									$status_class = 'fa-undo';	
									$label = 'Yes';
								}											
								else{ 											
									$action = 'activate_project';												
									$title = "Active Project";												
									$status_class = 'fa-check';		
									$label = 'No';									
								}
								
								$client_approval_status = $info -> client_approval;
								
								if($client_approval_status == 1) $cp_label = 'Yes';
								else $cp_label = 'No';	

							?>
								<tr class="per_company<?php echo $info -> ID;?>">
									<td><?php echo $i; ?></td>
									<td><?php echo $info -> project_name; ?></td>
									<td><?php echo get_name_by_user_id($info -> client_id); ?></td>
									<td><?php echo ucwords(get_info_by_client_company($info -> client_company_id) -> company_name); ?></td>
									<td><?php echo $info -> budget_total; ?></td>
									<td>
										<div class="progress-group">
											<span class="progress-text">Budget Used</span>
											<?php
												$budget_used = $info -> budget_used;
												$per = round((($budget_used)/($info -> budget_total))*100, 2);
												if(empty($budget_used)){ 
													$budget_used = 0;
													$per = 0;
												}
												
												echo '<span class="progress-number"><b>'.$budget_used.'</b>/'.$info ->budget_total.'</span>';
												
												
												echo '<div class="progress lg">
													  <div class="progress-bar progress-bar-aqua" style="width: '.$per.'%">'.$per.'%</div>
													</div>';
											?>
											
										  </div>
									</td>
									<td><?php echo ($info -> budget_total) - $budget_used; ?></td>
									<td><?php echo date("d.m.Y", strtotime($info -> start_date)); ?></td>
									<td><?php echo date("d.m.Y", strtotime($info -> end_date)); ?></td>
									<td><?php echo $label; ?></td>
									<td><?php echo $cp_label; ?></td>
									
									<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=projects-details&ID=<?php echo $info -> ID; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
										<?php											
																																												echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick="'.$action.'('.$info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';										?>
									</td>
								</tr>							
							<?php 								$i++;								} 							?>
						</tbody>
					</table>   
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>