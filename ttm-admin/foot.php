<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
	$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!--<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>-->
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminTTM App --><script src="ajax/ajax.js"></script>
<script src="dist/js/adminttm.min.js"></script>
<!-- AdminTTM dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<!-- AdminTTM for demo purposes -->
<script src="dist/js/demo.js"></script>

<script src="dist/js/jquery.validate.min.js"></script>
<script src="dist/js/validation.js"></script>


<!-- include summernote js-->
<script src="summernote/js/summernote.min.js"></script>

		
<script type="text/javascript">
$(document).ready(function() {
	$('.summernote').summernote({
	  height: 300,                 // set editor height

	  minHeight: null,             // set minimum height of editor
	  maxHeight: null,             // set maximum height of editor

	  focus: true,                 // set focus to editable area after initializing summernote
	});

});
</script>

<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>

<!-- FLOT CHARTS -->
<script src="bower_components/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="bower_components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="bower_components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="bower_components/Flot/jquery.flot.categories.js"></script>

<?php //if($page == 'new-time-entry'): ?>
	<!-- Select2 -->
	<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
	<!-- bootstrap time picker -->
	<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<?php //endif; ?>
<!-- Bootstrap Language selector -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>

<!-- File Uploader JS-->
<script src="http://plugins.krajee.com/assets/21959b68/js/fileinput.min.js?ver=201711050906"></script>
<script src="http://plugins.krajee.com/assets/21959b68/themes/gly/theme.min.js?ver=201711050906"></script>

<!-- / File Uploader JS-->

<script type="text/javascript">
	//Date picker
    $(function(){
		$('.datepicker').datepicker({
			autoclose: true,
			format: 'dd.mm.yyyy'
		})
		
		//Timepicker
		$('.timepicker').timepicker({
			showInputs: false
		})
		
		//Initialize Select2 Elements
		$('.select2').select2()
		
		<!-- Bootstrap Language selector -->
		$('.selectpicker').selectpicker();
		
		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"].minimal-blue, input[type="radio"].minimal').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass   : 'iradio_minimal-blue'
		});
		
		$(".file_uploader").fileinput({
			theme: "gly",
			uploadUrl: "/file-upload-batch/2",
			hideThumbnailContent: true // hide image, pdf, text or other content in the thumbnail preview
		});
		
		/*
		 * DONUT CHART
		 * -----------
		 */

		var donutData = [
		  { label: '', data: 55, color: '#4A82BC' },
		  { label: '', data: 15, color: '#5C5F93' },
		  { label: '', data: 20, color: '#54B9D8' },
		  { label: '', data: 10, color: '#DB5B9B' }
		]
		$.plot('.donut-chart', donutData, {
		  series: {
			pie: {
			  show       : true,
			  radius     : 1,
			  innerRadius: 0.5,
			  label      : {
				show     : true,
				radius   : 2 / 3,
				formatter: labelFormatter,
				threshold: 0.1
			  }

			}
		  },
		  legend: {
			show: false
		  }
		})
		/*
		 * END DONUT CHART
		 */
		});
		
		 /*
		   * Custom Label formatter
		   * ----------------------
		   */
		  function labelFormatter(label, series) {
			return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
			  + label
			  + '<br>'
			  + Math.round(series.percent) + '%</div>'
		  }
		  
	/* COMPANIES ACCOUNTS BAR CHART
	 * ----------------------------
	 */
	var bar_data = {
		data: [
			['New Accounts', 35],
			['Deactivated Accounts', 20],
			['Active Accounts', 50],
			['Total Accounts', 105]
		],
		color: '#3c8dbc'
	}
	$.plot('#companies-accounts', [bar_data], {
		grid: {
			borderWidth: 1,
			borderColor: '#f3f3f3',
			tickColor: '#f3f3f3'
		},
		series: {
			bars: {
				show: true,
				barWidth: 0.5,
				align: 'center'
			}
		},
		xaxis: {
			mode: 'categories',
			tickLength: 0
		}
	})
	/* END BAR CHART */
	
	/* COMPANIES ACCOUNTS BAR CHART
	 * ----------------------------
	 */
	var bar_data = {
		data: [
			['Workers', 55],
			['Project Managers', 22],
			['Line Managers', 10],
			['Administrators', 5]
		],
		color: '#3c8dbc'
	}
	
	$.plot('#companies-user-accounts', [bar_data], {
		grid: {
			borderWidth: 1,
			borderColor: '#f3f3f3',
			tickColor: '#f3f3f3'
		},
		series: {
			bars: {
				show: true,
				barWidth: 0.5,
				align: 'center'
			}
		},
		xaxis: {
			mode: 'categories',
			tickLength: 0
		}
	})
	/* END BAR CHART */
</script>

<!-- Highlight the current page item -->

<script type="text/javascript" src="http://www.kevinleary.net/wp-samples/js/jquery.url.js"></script>

<?php
	$current_user_company_id1 = get_cur_user_company_id(ttm_get_session('id'));
	$workers1 = get_users_by_company_id($current_user_company_id1, 8);
	
	$rand = rand ( 10000 , 99999 );
	
	$class = 'worker';
	if($_REQUEST['page'] == 'projects-details') $class = '';
	
	
	$add_worker =  '<div class="row sp '.$class.' ap_worker'.$rand.'"><div class="col-md-5"><select class="form-control select" style="width: 100%;" name="worker_id[]"><option value="">Select Worker</option>';
				  
	foreach($workers1 as $w_info){
		$add_worker .= '<option value="'.$w_info -> ID.'">'.$w_info -> display_name.'</option>';
	}	
	
	$add_worker .= '</select></div><div class="col-sm-5"><input name="worker_budget_total[]" type="text" class="form-control" placeholder="Budget Total"></div><div class="col-sm-2 as"><span class="glyphicon glyphicon-remove delete"></span></div></div>';

	/* onclick="wc_sub('.$rand.')" */
?>
<script>
	$('.add-worker').click(function(){
		$('.workers').append('<?php echo $add_worker; ?>');
	});
	
	var wc_sub = function (rand) {
		$('.ap_worker'+rand).remove();
	};
	
	$(document).on('click', '.delete', function(e) {
	   e.preventDefault();
	   $(this).closest('.sp').remove();
	   return false;
	});


	$(function(){
		page = window.location.href;
			
		if(page == 'http://www.airansoft.com/dev/ttm/ttm-admin/') {
			page = 'http://www.airansoft.com/dev/ttm/ttm-admin/home';
		}
		$('.sidebar-menu li a, .sidebar-menu li ul li a').each(function(){
			var href = $(this).attr('href');
			href = 'http://www.airansoft.com/dev/ttm/ttm-admin/' + href;
			if ( (href == page) || (href == '') ) {
				$(this).closest('li').addClass('active');
				$(this).closest('ul').closest('li').addClass('active');
				$(this).closest('ul').css('display', 'block');
			}
		});
	});
</script>
<!-- / Highlight the current page item -->