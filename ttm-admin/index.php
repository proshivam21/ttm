<?php
/**
 * Dashboard Administration Screen
 *
 * @package TTM
 * @subpackage Administration
 */

/** Load TTM Environment */
require_once( dirname( __FILE__ ) . '/admin.php' );

/** Load TTM Header */
include( ABSPATH . 'ttm-admin/header.php' );

include( ABSPATH . 'ttm-admin/classes/classes.php' );

/** Load Left side column. contains the logo and sidebar*/
include( ABSPATH . 'ttm-admin/sidebar.php' );

/** Load TTM Admin Pages */
include( ABSPATH . 'ttm-admin/page.php' );

/** Load TTM Admin Footer */
include( ABSPATH . 'ttm-admin/footer.php' );