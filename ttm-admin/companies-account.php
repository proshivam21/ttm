<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-building-o" aria-hidden="true"></i>
					<h3 class="box-title">Companies</h3>
					<a class="btn btn-info pull-right" href="add-new-company-account"><i class="fa fa-plus" aria-hidden="true"></i> New Company Account</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Company Name</th>
								<th>Account Name</th>
								<th>Contact Person Name</th>
								<th>Company Postal Address</th>
								<th>Company Tax No.</th>
								<th>Email Address</th>
								<th>Active</th>
								<th width="100px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php for($i=1; $i<=10; $i++): ?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><a href="#">ICO Independent Consultants</a></td>
									<td>ico_independent_consultants</td>
									<td>Marc Rueckziegel</td>
									<td>Weinbergstraße 3, 64342 Seeheim-Jugenheim, Germany</td>
									<td>CLDU565</td>
									<td>info@independent-consultants.com</td>
									<td>Active</td>
									<td>
										<a title="Edit" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
										<a title="Delete" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>
										<a title="Suspend" class="btn btn-danger btn-xs"><i class="fa fa-power-off" aria-hidden="true"></i></a>
									</td>
								</tr>
							<?php endfor; ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>