<section class="content">
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="box box-solid">
				<div class="box-header with-border">
					<i class="fa fa-list-alt" aria-hidden="true"></i>
					<h3 class="box-title">Overview</h3>
					<div class="pull-right">
						<a class="btn btn-info" href="approvals"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<div class="col-md-12 no-padding">
						<table class="table table-hover table-bordered table-striped">
							<tr>
								<th>Worker</th>
								<td>Shivam Agrawal</td>
							</tr>
							<tr>
								<th>Period</th>
								<td>Oct 2 – Oct 8, 2017</td>
							</tr>
							<tr>	
								<th>Year</th>
								<td>2017</td>
							</tr>
						</table>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<div class="col-xs-12 col-md-8">
			<div class="box box-solid">
				<div class="box-header with-border">
					<i class="fa fa-comment" aria-hidden="true"></i>
					<h3 class="box-title">Comment</h3>
					<div class="pull-right">
						<a class="btn bg-danger" href="#"><i class="fa fa-remove" aria-hidden="true"></i> Reject</a>
						<a class="btn btn-info" href="#"><i class="fa fa-check-square-o" aria-hidden="true"></i> Approve</a>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal">
					<div class="box-body">
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="5" class="form-control">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
								</textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><i class="fa fa-calendar" aria-hidden="true"></i> Oct 30 – Nov 5, 2017
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Date</th>
								<th>Start Time</th>
								<th>End Time</th>
								<th>Sum (hrs.)</th>
								<th>Task</th>
								<th>Comment</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>30.10.2017</td>
								<td>09:00 AM</td>
								<td>03:00 AM</td>
								<td>06:00</td>
								<td>UI Designing</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
							</tr>
							<tr>
								<td>2</td>
								<td>31.10.2017</td>
								<td>09:00 AM</td>
								<td>03:00 AM</td>
								<td>06:00</td>
								<td>UI Designing</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
							</tr>
							<tr>
								<td>3</td>
								<td>01.11.2017</td>
								<td>09:00 AM</td>
								<td>03:00 AM</td>
								<td>06:00</td>
								<td>HTML Coding</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
							</tr>
							<tr>
								<td>4</td>
								<td>02.11.2017</td>
								<td>09:00 AM</td>
								<td>03:00 AM</td>
								<td>06:00</td>
								<td>HTML Coding</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
							</tr>
							<tr>
								<td>5</td>
								<td>30.11.2017</td>
								<td>09:00 AM</td>
								<td>03:00 AM</td>
								<td>06:00</td>
								<td>HTML Coding</td>
								<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			<div class="box">
				<div class="box-header">
					<h3 class="box-title"><i class="fa fa-history" aria-hidden="true"></i> Summary
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Period</th>
								<th>Project Hours</th>
								<th>Internal Hours</th>
								<th>Vacations</th>
								<th>Sick Leave</th>
								<th>Total Hours</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Oct 2 – Oct 8, 2017</td>
								<td>30:00 <span class="label label-success">30%</span></td>
								<td>10:00 <span class="label label-info">10%</span></td>
								<td>28:00 <span class="label label-success">28%</span></td>
								<td>32:00 <span class="label label-warning">32%</span></td>
								<td>100:00</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Jan 1 – Oct 8, 2017</td>
								<td>150:00 <span class="label label-success">30%</span></td>
								<td>50:00 <span class="label label-info">10%</span></td>
								<td>140:00 <span class="label label-success">28%</span></td>
								<td>160:00 <span class="label label-warning">32%</span></td>
								<td>500:00</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>