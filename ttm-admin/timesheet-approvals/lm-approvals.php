<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="col-md-1"></div>
			<div class="col-xs-12 col-md-5">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Period</option>
				  <option>Oct 2 – Oct 8, 2017</option>
				  <option>Oct 16 – Oct 22, 2017</option>
				  <option>Oct 23 – Oct 29, 2017</option>
				  <option>Oct 30 – Nov 5, 2017</option>
				  <option>Nov 6 – Nov 12, 2018</option>
				</select>
			</div>
			<div class="col-xs-12 col-md-5">
				<select class="form-control select2" style="width: 100%;">
				  <option selected="selected">Worker</option>
				  <option>Sam</option>
				  <option>Robert</option>
				  <option>Nick</option>
				  <option>Kevin</option>
				  <option>Shivam</option>
				</select>
			</div>
			<div class="col-xs-12 col-md-1">
				<button type="submit" class="btn btn-info">Select</button>
			</div>
		</div>
		<div class="col-xs-12">
			<!-- The time line -->
			<ul class="timeline">
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					Oct 2 – Oct 8, 2017
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<div class="timeline-item">
						<div class="box">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th rowspan="3">Worker</th>
											<th rowspan="3">Status</th>
											<th colspan="6" class="text-center">Status</th>
										</tr>
										<tr>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Open</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Overdue</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Ready for Review</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Rejected</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th colspan="7" class="text-center">
												<select class="selectpicker" data-width="fit">
													<option>Approved</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
										</tr>
										<tr>
											<th colspan="4"></th>
											<th>PM</th>
											<th>LM</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><a href="approval">Sam</a></td>
											<td>Approved</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc</td>
											<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam</td>
										</tr>
										<tr>
											<td><a href="approval">Robert</a></td>
											<td>Rejected</td>
											<td></td>
											<td></td>
											<td></td>
											<td>
												<i class="fa fa-remove"></i> <strong>PM:</strong> Marc<br/>
												<i class="fa fa-remove"></i> <strong>LM:</strong> Shivam
											</td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Nick</a></td>
											<td>Ready for Review</td>
											<td></td>
											<td></td>
											<td>
												<i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/>
												<i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam
											</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Kevin</a></td>
											<td>Overdue</td>
											<td></td>
											<td><i class="fa fa-check-square-o"></i></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Shivam</a></td>
											<td>Open</td>
											<td><i class="fa fa-check-square-o"></i></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">Oct 16 – Oct 22, 2017</span>
				</li>
				<!-- /.timeline-label -->
				
				<!-- timeline item -->
				<li>
					<div class="timeline-item">
						
						<div class="box">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th rowspan="3">Worker</th>
											<th rowspan="3">Status</th>
											<th colspan="6" class="text-center">Status</th>
										</tr>
										<tr>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Open</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Overdue</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Ready for Review</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th>
												<select class="selectpicker" data-width="fit">
													<option>Rejected</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
											<th colspan="7" class="text-center">
												<select class="selectpicker" data-width="fit">
													<option>Approved</option>
													<option data-content='<span class="fa fa-check-square-o"></span> Yes'>Yes</option>
													<option  data-content='<span class="fa fa-remove"></span> No'>No</option>
												</select>
											</th>
										</tr>
										<tr>
											<th colspan="4"></th>
											<th>PM</th>
											<th>LM</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><a href="approval">Sam</a></td>
											<td>Open</td>
											<td><i class="fa fa-check-square-o"></i></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Robert</a></td>
											<td>Overdue</td>
											<td></td>
											<td><i class="fa fa-check-square-o"></i></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Nick</a></td>
											<td>Ready for Review</td>
											<td></td>
											<td></td>
											<td>
												<i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/>
												<i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam
											</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Kevin</a></td>
											<td>Rejected</td>
											<td></td>
											<td></td>
											<td></td>
											<td>
												<i class="fa fa-remove"></i> <strong>PM:</strong> Marc<br/>
												<i class="fa fa-remove"></i> <strong>LM:</strong> Shivam
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td><a href="approval">Shivam</a></td>
											<td>Approved</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td><i class="fa fa-check-square-o"></i> <strong>PM:</strong> Marc<br/></td>
											<td><i class="fa fa-check-square-o"></i> <strong>LM:</strong> Shivam<br/></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
			</ul>
		</div>
	</div>
	<!-- /.row -->
</section>