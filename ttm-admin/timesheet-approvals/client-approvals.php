<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project</th>
								<th>Period</th>
								<th>Project Manager</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">TimTraMac</a></td>
								<td>Oct 2 – Oct 8, 2017</td>
								<td>Marc</td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">CRM Project</a></td>
								<td>Oct 16 – Oct 22, 2017</td>
								<td>Timo</td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">SMS Project</a></td>
								<td>Oct 23 – Oct 29, 2017</td>
								<td>Nick</td>
								
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">TTM Project</a></td>
								<td>Oct 30 – Nov 5, 2017</td>
								<td>Michael</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>