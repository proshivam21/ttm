var deactivate_user = function (userID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateUser&user_id="+userID,
		
		success: function(data) 
		{
			$(".per_user"+userID+" .act-main").replaceWith("<span class='act-main'><a title='Active User' class='btn btn-xs bg-green act' onclick='activate_user("+userID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_user"+userID+" .user-status").replaceWith("<span class='user-status'>Inactive</span>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

var activate_user = function (userID) {
	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateUser&user_id="+userID,
		data: {user_id:userID, action:'activateUser'},
		
		success: function(data) 
		{
			$(".per_user"+userID+" .act-main").replaceWith("<span class='act-main'><a title = 'Inactive User' class='btn btn-xs bg-green act' onclick='deactivate_user("+userID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_user"+userID+" .user-status").replaceWith("<span class='user-status'>Active</span>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete user
*/
var delete_user = function (userID) {

	if (confirm("Are you sure you want to delete this user permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_user&user_id="+userID,
			
			success: function(data) 
			{
				$(".per_user"+userID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};


var deactivate_company = function (companyID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateCompany&company_id="+companyID,
		
		success: function(data) 
		{
			$(".per_company"+companyID+" .act-main").replaceWith("<span class='act-main'><a title='Activate Company' class='btn btn-xs bg-green act' onclick='activate_company("+companyID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_company"+companyID+" .company-status").replaceWith("<span class='company-status'>Inactive</span>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
	
};


var activate_company = function (companyID) {

	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateCompany&company_id="+companyID,
		data: {company_id:companyID, action:'activatecompany'},
		
		success: function(data) 
		{
			$(".per_company"+companyID+" .act-main").replaceWith("<span class='act-main'><a title = 'Inactivate Company' class='btn btn-xs bg-green act' onclick='deactivate_company("+companyID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_company"+companyID+" .company-status").replaceWith("<span class='company-status'>Active</span>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete company
*/
var delete_company = function (companyID) {

	if (confirm("Are you sure you want to delete this company permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_company&company_id="+companyID,
			
			success: function(data) 
			{
				$(".per_company"+companyID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};


/*
** Delete company price
*/
var delete_price = function (priceID) {

	if (confirm("Are you sure you want to delete this price?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_price&price_id="+priceID,
			
			success: function(data) 
			{
				$(".per_company_price"+priceID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};




/************* Load template in options email *************/

var getTemplate = function (templateCode) {
	$.ajax({
		url: "ajax/ajax.php?action=getTemplate&templateCode="+templateCode,
		beforeSend:function() {
			$("#approveImg_"+templateCode).attr("src","images/wait.gif");
		},
		success: function(data) {

				$(".note-editable").fadeOut(500, function(){
					$('.note-editable').show();
					$('.note-editable').html(data);
					
				});
				$('.summernote').html(data);

		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError);
		}
	});
};

$( "#country_id" ).change(function() {
	$country_id = $('#country_id').val();
	get_state_ddl($country_id, state_name = 'state_living_in')
});


$( "#hours_type" ).change(function() {
	var val = $('#hours_type').val();
	if(val == 'pro_hours') $('.pro').show();
	else $('.pro').hide();
});


$( "#country_id1" ).change(function() {
	$country_id = $('#country_id1').val();
	get_state_dd2($country_id, state_name = 'areas[]')
});


$( "#project_id" ).change(function() {
	var project_id = $('#project_id').val();
	 $.ajax({
		url: "ajax/ajax.php?action=get_tasks_lists&project_id="+project_id,
		 beforeSend:function() {
		}, 
		success: function(data) {
			$("#task_ddl").html(data);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			//alert(thrownError);
		}
	});
});


function get_state_ddl(country_id, state_name = 'state_living_in'){

    $.ajax({
		url: "ajax/ajax.php?action=get_state_ddl&state_name="+state_name+"&country_id="+country_id,
		 beforeSend:function() {
		}, 
		success: function(data) {
			$("#state_ddl").html(data);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			//alert(thrownError);
		}
	}); 
}

function get_state_dd2(country_id, state_name = 'state_living_in'){

    $.ajax({
		url: "ajax/ajax.php?action=get_state_dd2&state_name="+state_name+"&country_id="+country_id,
		 beforeSend:function() {
		}, 
		success: function(data) {
			$("#state_dd2").html(data);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			//alert(thrownError);
		}
	}); 
}

var deactivate_client_company = function (clientCompID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateClientCompany&client_company_id="+clientCompID,
		
		success: function(data) 
		{
			$(".per_company"+clientCompID+" .act-main").replaceWith("<span class='act-main'><a title='Activate Company' class='btn btn-xs bg-green act' onclick='activate_client_company("+clientCompID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_company"+clientCompID+" .company-status").replaceWith("<td class='company-status'>Inactive</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
	
};

var activate_client_company = function (clientCompID) {

	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateClientCompany&client_company_id="+clientCompID,
		
		success: function(data) 
		{
			$(".per_company"+clientCompID+" .act-main").replaceWith("<span class='act-main'><a title = 'Inactivate Company' class='btn btn-xs bg-green act' onclick='deactivate_client_company("+clientCompID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_company"+clientCompID+" .company-status").replaceWith("<td class='company-status'>Active</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete client company
*/
var delete_client_company = function (clientCompID) {

	if (confirm("Are you sure you want to delete client company permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_client_company&client_company_id="+clientCompID,
			
			success: function(data) 
			{
				$(".per_company"+clientCompID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};






var deactivate_client_contact = function (clientConID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateClientContact&client_contact_id="+clientConID,
		
		success: function(data) 
		{
			$(".per_company"+clientConID+" .act-main").replaceWith("<span class='act-main'><a title='Activate Client Contact' class='btn btn-xs bg-green act' onclick='activate_client_contact("+clientConID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_company"+clientConID+" .company-status").replaceWith("<td class='company-status'>Inactive</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
	
};

var activate_client_contact = function (clientConID) {

	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateClientContact&client_contact_id="+clientConID,
		
		success: function(data) 
		{
			$(".per_company"+clientConID+" .act-main").replaceWith("<span class='act-main'><a title = 'Inactivate Client Contact' class='btn btn-xs bg-green act' onclick='deactivate_client_contact("+clientConID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_company"+clientConID+" .company-status").replaceWith("<td class='company-status'>Active</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete client contact
*/
var delete_client_contact= function (clientConID) {

	if (confirm("Are you sure you want to delete client contact permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_client_contact&client_contact_id="+clientConID,
			
			success: function(data) 
			{
				$(".per_company"+clientConID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};



var deactivate_project = function (projectID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateProject&project_id="+projectID,
		
		success: function(data) 
		{
			$(".per_company"+projectID+" .act-main").replaceWith("<span class='act-main'><a title='Activate Project' class='btn btn-xs bg-green act' onclick='activate_project("+projectID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_company"+projectID+" .company-status").replaceWith("<td class='company-status'>Inactive</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
	
};

var activate_project = function (projectID) {

	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateProject&project_id="+projectID,
		
		success: function(data) 
		{
			$(".per_company"+projectID+" .act-main").replaceWith("<span class='act-main'><a title = 'Inactivate Project' class='btn btn-xs bg-green act' onclick='deactivate_project("+projectID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_company"+projectID+" .company-status").replaceWith("<td class='company-status'>Active</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete project
*/
var delete_project = function (projectID) {

	if (confirm("Are you sure you want to delete this project permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_project&project_id="+projectID,
			
			success: function(data) 
			{
				$(".per_company"+projectID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};









var deactivate_holiday = function (holidayID) {

	$.ajax({
		type: "GET",
		url: "ajax/ajax.php?action=deactivateHoliday&holiday_id="+holidayID,
		
		success: function(data) 
		{
			$(".per_company"+holidayID+" .act-main").replaceWith("<span class='act-main'><a title='Activate Bank Holiday' class='btn btn-xs bg-green act' onclick='activate_holiday("+holidayID+")'><i class='fa fa-check'></i></a></span>");
			
			$(".per_company"+holidayID+" .company-status").replaceWith("<td class='company-status'>Inactive</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
	
};

var activate_holiday = function (holidayID) {

	$.ajax({
		type: "POST",
		url: "ajax/ajax.php?action=activateHoliday&holiday_id="+holidayID,
		
		success: function(data) 
		{
			$(".per_company"+holidayID+" .act-main").replaceWith("<span class='act-main'><a title = 'Activate Bank Holiday' class='btn btn-xs bg-green act' onclick='deactivate_holiday("+holidayID+")'><i class='fa fa-undo'></i></a></span>"); 
			$(".per_company"+holidayID+" .company-status").replaceWith("<td class='company-status'>Active</td>");
		},
		error: function(xhr, ajaxOptions, thrownError) {
		}
	});
};

/*
** Delete holiday
*/
var delete_holiday = function (holidayID) {

	if (confirm("Are you sure you want to delete this bank holiday permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_holiday&holiday_id="+holidayID,
			
			success: function(data) 
			{
				$(".per_company"+holidayID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};











/*
** Delete task
*/
var delete_task = function (taskID) {

	if (confirm("Are you sure you want to delete this task permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_task&task_id="+taskID,
			
			success: function(data) 
			{
				$(".per_task"+taskID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};


/*
** Delete country
*/
var delete_country = function (countryID) {

	if (confirm("Are you sure you want to delete this country permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_country&country_id="+countryID,
			
			success: function(data) 
			{
				$(".per_con"+countryID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};


/*
** Delete area
*/
var delete_area = function (areaID) {

	if (confirm("Are you sure you want to delete this area permanently?") ) {
		$.ajax({
			type: "POST",
			url: "ajax/ajax.php?action=delete_area&area_id="+areaID,
			
			success: function(data) 
			{
				$(".per_area"+areaID).fadeOut(2000, function(){ 
				}); 
			},
			error: function(xhr, ajaxOptions, thrownError) {
			}
		});
	}
	else return false; 
};



var data_country = function (conID) {
    var comp_name = $('.con'+conID).attr('data-country-name');
	var comp_code = $('.con'+conID).attr('data-country-code');
	
	$('#modal-country').modal('show'); 
	$('#com_name').val(comp_name);
	$('#com_code').val(comp_code);
	$('#con_id').val(conID);
}; 


var data_area = function (areaID) {
    var area_name = $('.area'+areaID).attr('data-area-name');
	var country_id = $('.area'+areaID).attr('data-country-id'); 
	$('#modal-area').modal('show'); 
	$('#area_name').val(area_name);
	$('#area_id').val(areaID);
	$('select[name^="con_id"] option[value="'+country_id+'"]').attr("selected","selected");
}; 

data_task = function (taskID) {
	var task_name = $('.task'+taskID).attr('data-task-name');
	var budget_total = $('.task'+taskID).attr('data-budget-total');
	var start_date = $('.task'+taskID).attr('data-start-date');
	var end_date = $('.task'+taskID).attr('data-end-date');	
	
	//var workers_id = $('.task'+taskID).attr('data-workers-id');
	
	
	$('#modal-edit-task').modal('show');
	$('#task_id').val(taskID);
	$('#task_name').val(task_name);
	$('#budget_total').val(budget_total);
	$('#start_date').val(start_date);
	$('#end_date').val(end_date);
	
};