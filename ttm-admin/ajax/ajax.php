<?php

/*
==========================================================================
		All ajax function here(User, challenge management)
==========================================================================
*/

require_once (dirname(dirname(dirname(__FILE__))).'/ttm-load.php');

include_once (dirname(dirname(__FILE__)).'/functions.php');

$actionPerformed = $_GET['action'];
extract($_POST);

if(($actionPerformed == 'delete_user')):
	$user_id = $_GET['user_id'];
	$response = delete_profile($user_id);
	if($response == 'success'){
		return true;
	}
	else{
		return false;
	}
	/* if (!ttm_delete_user($user_id)) return false;
	return true; */
endif;

if(($actionPerformed == 'deactivateUser')):
	$user_id = $_GET['user_id'];
	if (!ttm_deactivate_member($user_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateUser')):
	$user_id = $_GET['user_id'];
	if (!ttm_activate_member($user_id)) return false;
	return true;
endif;


if(($actionPerformed == 'deactivateCompany')):
	$company_id = $_GET['company_id'];
	if (!ttm_deactivate_company($company_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateCompany')):
	$company_id = $_GET['company_id'];
	if (!ttm_activate_company($company_id)) return false;
	return true;
endif;

if(($actionPerformed == 'delete_company')):
	$company_id = $_GET['company_id'];
	$response = delete_company($company_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;



if(($actionPerformed == 'deactivateClientCompany')):
	$client_company_id = $_GET['client_company_id'];
	if (!ttm_deactivate_client_company($client_company_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateClientCompany')):
	$client_company_id = $_GET['client_company_id'];
	if (!ttm_activate_client_company($client_company_id)) return false;
	return true;
endif;

if(($actionPerformed == 'delete_client_company')):
	$client_company_id = $_GET['client_company_id'];
	$response = delete_client_company($client_company_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;



if(($actionPerformed == 'deactivateClientContact')):
	$client_contact_id = $_GET['client_contact_id'];
	if (!ttm_deactivate_client_contact($client_contact_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateClientContact')):
	$client_contact_id = $_GET['client_contact_id'];
	if (!ttm_activate_client_contact($client_contact_id)) return false;
	return true;
endif;

if(($actionPerformed == 'delete_client_contact')):
	$client_contact_id = $_GET['client_contact_id'];
	$response = delete_client_contact($client_contact_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;


if(($actionPerformed == 'deactivateProject')):
	$project_id = $_GET['project_id'];
	if (!ttm_deactivate_project($project_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateProject')):
	$project_id = $_GET['project_id'];
	if (!ttm_activate_project($project_id)) return false;
	return true;
endif;

if(($actionPerformed == 'delete_project')):
	$project_id = $_GET['project_id'];
	$response = delete_project($project_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;



if(($actionPerformed == 'deactivateHoliday')):
	$holiday_id = $_GET['holiday_id'];
	if (!ttm_deactivate_holiday($holiday_id)) return false;
	return true;
endif;

if(($actionPerformed == 'activateHoliday')):
	$holiday_id = $_GET['holiday_id'];
	if (!ttm_activate_holiday($holiday_id)) return false;
	return true;
endif;

if(($actionPerformed == 'delete_holiday')):
	$holiday_id = $_GET['holiday_id'];
	$response = delete_holiday($holiday_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;




if(($actionPerformed == 'delete_task')):
	$task_id = $_GET['task_id'];
	$response = delete_task($task_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;



if(($actionPerformed == 'delete_price')):
	$price_id = $_GET['price_id'];
	$response = delete_price($price_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;



if ($actionPerformed == 'getTemplate'):
	$template_code = $_GET['templateCode'];
	if (!$content = get_option("$template_code")) {
		echo false;
	}
	echo $content;
endif; 

if(($actionPerformed == 'get_state_ddl')): 
	$country_id = $_GET['country_id'];
	$state_name = $_GET['state_name'];
	get_state_ddl($country_id, '', $state_name);
endif;

if(($actionPerformed == 'get_tasks_lists')): 
	$project_id = $_GET['project_id'];
	get_task_ddl($project_id);
endif;

if(($actionPerformed == 'get_state_dd2')): 
	$country_id = $_GET['country_id'];
	$state_name = $_GET['state_name'];
	get_state_dd2($country_id, '', $state_name);
endif;


if(($actionPerformed == 'delete_country')):
	$country_id = $_GET['country_id'];
	$response = delete_country($country_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;


if(($actionPerformed == 'delete_area')):
	$area_id = $_GET['area_id'];
	$response = delete_area($area_id);
	if($response == 1){
		return true;
	}
	else{
		return false;
	}
endif;


?>