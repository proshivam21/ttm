<?php global $user_type; $role = $user_type; ?>

<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?php 
					echo ttm_get_attachment_image(get_user_meta($id, 'profile_photo', true), 'thumbnail', array('class' => 'img-circle', 'alt' => 'Profile Photo', 'width' => '160', 'height' => '160')); 
				?>
			</div>
			<div class="pull-left info">
				<p><?php echo ucwords(get_user_meta($id, 'first_name', true).' '.get_user_meta($id, 'last_name', true) ); ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
				<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				</button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li>
				<a href="?page=home">
				<i class="fa fa-home"></i> <span>Home</span>
				</a>
			</li>
			<?php if($role != 'sysadmin' && $role != 'client' && $role != 'system_admin_user'): ?>
				<li>
					<a href="?page=timesheets">
					<i class="fa fa-clock-o "></i>
					<span>Time Tracking</span>
					</a>
				</li>
			<?php endif; 
			?>
			<?php if($role == 'sysadmin'): ?>
				<li>
					<a href="#">
					<i class="fa fa-desktop" aria-hidden="true"></i>
					<span>System Management</span>
					</a>
				</li>
				<li>
					<a href="?page=system-admin-users">
					<i class="fa fa-user-secret" aria-hidden="true"></i>
					<span>Admin User Management</span>
					</a>
				</li>
				<li>
					<a href="?page=billing">
					<i class="fa fa-credit-card" aria-hidden="true"></i>
					<span>Billing</span>
					</a>
				</li>
				<li class="treeview">
					<a href="?page=system-admin-general-settings">
					<i class="fa fa-wrench" aria-hidden="true"></i>
					<span>General Settings</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="?page=system-admin-general-settings"><i class="fa fa-wrench" aria-hidden="true"></i> <span>General Settings</span></a></li>
						<li><a href="?page=invoice-layout"><i class="fa fa-files-o" aria-hidden="true"></i> <span>Invoice Layout</span></a></li>
						<li><a href="?page=mail-templates"><i class="fa fa-envelope" aria-hidden="true"></i>
						Email Templates</a>
						<li><a href="?page=areas"><i class="fa fa-map-marker" aria-hidden="true"></i> <span>Area Management</span></a></li>
					</li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
					<i class="fa fa-user-circle" aria-hidden="true"></i>
					<span>Account</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="?page=companies-accounts-management"><i class="fa fa-user"></i> <span>Account Management</span></a></li>
						<li><a href="?page=statistics"><i class="fa fa-pie-chart"></i> <span>Statistics</span></a></li>
						<li><a href="?page=pricing"><i class="fa fa-money" aria-hidden="true"></i> <span>Pricing</span></a></li>
					</ul>
				</li>
			<?php endif; ?>
			
			
			<?php if($role == 'system_admin_user'): ?>
				<li>
					<a href="#">
					<i class="fa fa-desktop" aria-hidden="true"></i>
					<span>System Management</span>
					</a>
				</li>
				<!-- <li>
					<a href="?page=system-admin-users">
					<i class="fa fa-user-secret" aria-hidden="true"></i>
					<span>Admin User Management</span>
					</a>
				</li> -->
				
				<li>
					<a href="?page=billing">
					<i class="fa fa-credit-card" aria-hidden="true"></i>
					<span>Billing</span>
					</a>
				</li>
				<li class="treeview">
					<a href="?page=system-general-settings">
					<i class="fa fa-wrench" aria-hidden="true"></i>
					<span>General Settings</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="?page=system-admin-general-settings"><i class="fa fa-wrench" aria-hidden="true"></i> <span>General Settings</span></a></li>
						<li><a href="?page=invoice-layout"><i class="fa fa-files-o" aria-hidden="true"></i> <span>Invoice Layout</span></a></li>
						<li><a href="?page=mail-templates"><i class="fa fa-envelope" aria-hidden="true"></i>
						Email Templates</a>
						<li><a href="?page=areas"><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Area Management</span></a></li>
					</li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#">
					<i class="fa fa-user-circle" aria-hidden="true"></i>
					<span>Account</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="?page=companies-accounts-management"><i class="fa fa-user"></i> <span>Account Management</span></a></li>
						<li><a href="?page=statistics"><i class="fa fa-pie-chart"></i> <span>Statistics</span></a></li>
						<li><a href="?page=pricing"><i class="fa fa-money" aria-hidden="true"></i> <span>Pricing</span></a></li>
					</ul>
				</li>
			<?php endif; ?>
			
			
			
			<?php if($role == 'pm' || $role == 'lm' || $role == 'client'): ?>
				<li>
					<a href="?page=approvals">
					<i class="fa fa-check "></i>
					<span>Approvals</span>
					</a>
				</li>
			<?php endif; ?>
			<?php if($role == 'pm' || $role == 'admin'): ?>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-wrench" aria-hidden="true"></i> <span>Administrative Console</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<?php if($role == 'admin'): ?>
							<li><a href="?page=users"><i class="fa fa-users" aria-hidden="true"></i> <span>Users</span></a></li>
							<li><a href="?page=projects"><i class="fa fa-tasks" aria-hidden="true"></i> <span>Projects</span></a></li>
							<li><a href="?page=client-contacts"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <span>Client Contacts</span></a></li>
							<li><a href="?page=client-companies"><i class="fa fa-building-o" aria-hidden="true"></i> <span>Client Company</span></a></li>
							<li><a href="?page=bank-holidays"><i class="fa fa-university" aria-hidden="true"></i> <span>Bank Holidays</span></a></li>
							<li><a href="?page=timesheet-layout"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>Timesheet Layout</span></a></li>
							
							<li class="treeview">
								<a href="?page=general-setting">
								<i class="fa fa-wrench" aria-hidden="true"></i>
								<span>General Settings</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
								</a>
								<ul class="treeview-menu">
									<li><a href="?page=general-settings"><i class="fa fa-wrench" aria-hidden="true"></i> <span>General Settings</span></a></li>
									<li><a href="?page=mail-templates"><i class="fa fa-envelope" aria-hidden="true"></i>
									Email Templates</a>
									</li>
									<li><a href="?page=areas"><i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Area Management</span></a></li>
								</ul>
							</li>
				
							<li><a href="?page=account-data"><i class="fa fa-user" aria-hidden="true"></i> <span>Account Data</span></a></li>
						<?php elseif($role == 'pm'): ?>
							<li><a href="?page=projects-overview"><i class="fa fa-tasks" aria-hidden="true"></i> <span>Projects</span></a></li>
						<?php endif;?>
					</ul>
				</li>
			<?php endif; ?>
			
			<?php if($role == 'accountant'): ?>
				<li>
					<a href="timesheets"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Timesheets</span></a>
				</li>
			<?php endif; ?>
			<?php if($role != 'sysadmin' && $role != 'system_admin_user'): ?>
				<li>
					<a href="#">
					<i class="fa fa-pie-chart"></i>
					<span>Statistics</span>
					</a>
				</li>
				<li>
					<a href="#">
					<i class="fa fa-file-excel-o"></i>
					<span>Reports/Exports</span>
					</a>
				</li>
			<?php endif; ?>
			<li>
				<a href="?page=presets">
				<i class="fa fa-cog"></i>
				<span>Presets</span>
				</a>
			</li>			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>