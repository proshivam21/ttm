<section class="content">
	  <div class="row">
		<div class="col-xs-12">
		  <div class="box">
			<div class="box-header">
			  <h3 class="box-title">Timesheet Layout</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="summernote" name="content" rows="30"><?php include 'invoice.html'; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input class="btn bg-blue" type="submit" name="save" value="Save Template">
						</div>
					</div>
				</form>
			</div><!-- /.box-body -->
		  </div><!-- /.box -->
		</div><!-- /.col -->
	  </div><!-- /.row -->
	</section><!-- /.content -->