<?php 
	if($role == 'worker') {
		include 'dashboard/worker-dashboard.php';
	} elseif($role == 'pm') {
		include 'dashboard/project-manager-dashboard.php';
	} elseif($role == 'lm') {
		include 'dashboard/line-manager-dashboard.php';
	} elseif($role == 'client') {
		include 'dashboard/client-dashboard.php';
	}  elseif($role == 'accountant') {
		include 'dashboard/accountant-dashboard.php';
	} elseif($role == 'sysadmin') {
		include 'dashboard/system-admin-dashboard.php';
	}
?>