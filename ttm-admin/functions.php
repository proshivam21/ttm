<?php
	/*
	** Activate user by ID
	*/
	function ttm_activate_member($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->users}",array('user_status' => 1), $where)) {
			return false;
		}
		return true; 
	}
	
	
	/*
	** Deactivate user by ID
	*/
	function ttm_deactivate_member($id) {
		global $db;
		$where = array('ID' => $id);
		
		if(!$db -> update("{$db->users}",array('user_status' => 0), $where)) {
			return false;
		}
		return true; 
	}
	
	
	
	
	/*
	** Activate company by ID
	*/
	function ttm_activate_company($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->companies}",array('status' => 1), $where)) {
			return false;
		}
		$user_id = $db -> get_row("SELECT ID as user_id FROM {$db -> users} where company_id = {$id} ") -> user_id;
		if(!empty($user_id)) ttm_activate_member($user_id);
		return true; 
	}
	
	
	/*
	** Deactivate company by ID
	*/
	function ttm_deactivate_company($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->companies}",array('status' => 0), $where)) {
			return false;
		}
		$user_id = $db -> get_row("SELECT ID as user_id FROM {$db -> users} where company_id = {$id} ") -> user_id;
		if(!empty($user_id)) ttm_deactivate_member($user_id);
		return true; 
	}
	
	/*
	** Delete Company
	*/
	function delete_company($id){
		global $db;
		
		$user_id = $db -> get_row("SELECT ID as user_id FROM {$db -> users} where company_id = {$id} ") -> user_id;
		
		if(!empty($user_id)) delete_profile($user_id);
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->companies}", $where)) {
			return false;
		}
		 
		$where1 = array('company_id' => $id);
		$db -> delete("{$db->companymeta}", $where1);
		
		return true; 
	}
	
	/*
	** Activate client company
	*/
	function ttm_activate_client_company($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_clients}",array('status' => 1), $where)) {
			return false;
		}
		return true; 
	}
	
	
	/*
	** Deactivate client company 
	*/
	function ttm_deactivate_client_company($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_clients}",array('status' => 0), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Delete Client Company
	*/
	function delete_client_company($id){
		global $db;
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->company_clients}", $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Activate client conatct
	*/
	function ttm_activate_client_contact($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_client_contacts}",array('status' => 1), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Deactivate client conatct 
	*/
	function ttm_deactivate_client_contact($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_client_contacts}",array('status' => 0), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Delete Client conatct
	*/
	function delete_client_contact($id){
		global $db;
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->company_client_contacts}", $where)) {
			return false;
		}
		return true; 
	}
	
	
	
	
	
	/*
	** Activate Project
	*/
	function ttm_activate_project($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_projects}",array('status' => 1), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Deactivate Project 
	*/
	function ttm_deactivate_project($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->company_projects}",array('status' => 0), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Delete Client conatct
	*/
	function delete_project($id){
		global $db;
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->company_projects}", $where)) {
			return false;
		}
		return true; 
	}
	
	
	
	
	
	/*
	** Activate Project
	*/
	function ttm_activate_holiday($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->bank_holidays}",array('status' => 1), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Deactivate Project 
	*/
	function ttm_deactivate_holiday($id) {
		global $db;
		$where = array('ID' => $id);
		if(!$db -> update("{$db->bank_holidays}",array('status' => 0), $where)) {
			return false;
		}
		return true; 
	}
	
	/*
	** Delete Bank Holiday
	*/
	function delete_holiday($id){
		global $db;
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->bank_holidays}", $where)) {
			return false;
		}
		return true; 
	}
	
	
	
	
	/*
	** Delete task
	*/
	function delete_task($id){
		global $db;
		
		$where = array('ID' => $id);
		
		if(!$db -> delete("{$db->company_project_tasks}", $where)) {
			return false;
		}
		return true; 
	}
	
	
	
	
	
	/*
	** Delete Company Price
	*/
	function delete_price($id){
		global $db;
		
		$where = array('ID' => $id);
			
		if(!$db -> delete("{$db->company_pricing}", $where)) {
			return false;
		}
		return true; 
	}
	
	
	/*
	** Delete Country
	*/
	function delete_country($id){
		global $db;
		
		$where = array('ID' => $id);
			
		if(!$db -> delete("{$db->countries}", $where)) {
			return false;
		}
		return true; 
	}

	
	/*
	** Delete Area
	*/
	function delete_area($id){
		global $db;
		
		$where = array('ID' => $id);
			
		if(!$db -> delete("{$db->country_areas}", $where)) {
			return false;
		}
		return true; 
	}
	
	/***
	*** Get Meta Info
	**/
	function get_custom_meta($table_name, $id, $key){
		global $db;
		if(empty($table_name) AND empty($id) AND empty($key) ) return false;
		else{
			$meta_value = $db -> get_row("SELECT meta_value FROM {$table_name} where company_id = '{$id}' AND meta_key = '{$key}' ") -> meta_value;
			return $meta_value;
		}
	}
	
	function check_key_exist($table_name, $id, $key){
		global $db;
		if(empty($table_name) AND empty($id) AND empty($key) ) return false;
		else{
			$count = $db -> get_row("SELECT count(ID) as count1 FROM {$table_name} where company_id = '{$id}' AND meta_key = '{$key}' ") -> count1 ;
			return $count;
		}
	}
	
	
/*
 * Update user
 */
function update_user($user_id, $data) {
	extract($data);
	$userdata = array(
					'user_login' 		=>		$username,
					'user_nicename' 	=>		$first_name.' '.$last_name,
					'user_email' 		=>		$email,
					'display_name' 		=>		$last_name.' '.$first_name
				);
	$media_id = upload_media('profile_photo');
	$usermeta = array(
					'first_name'		=>		$first_name,
					'last_name'			=>		$last_name,
					'profile_photo'		=>		$media_id
				);
	if(ttm_update_user($user_id, $userdata)) {
		foreach($usermeta as $key => $value) {
			if(!empty($value))
				update_user_meta($user_id, $key, $value);
		}
		return true;
		
	} else {
		return false;
	}
}


function send_email($to, $subject, $from, $message){
	// To send HTML mail, the Content-type header must be set

	$headers  = 'MIME-Version: 1.0' . "\r\n";

	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
	// Create email headers
	$headers .= 'From: '.$from."\r\n".

		'Reply-To: '.$from."\r\n" .

		'X-Mailer: PHP/' . phpversion();
	
	mail($to, $subject, $message, $headers);
}

function get_users_by_company_id($comp_id, $role){
	if(!empty($comp_id)){
		global $db;
		
		if(!empty($role)) $role_str = "AND role_id = $role";
		
		return $db -> get_results("select *from {$db -> users} where company_id = $comp_id $role_str");
	}
}

function get_role_name_by_role_id($role_id){
	if(!empty($role_id)){
		global $db;
		return $db -> get_row("select role_val from {$db -> user_roles} where ID = $role_id");
	}
}

function get_company_info_by_id($com_id){
	if(!empty($com_id)){
		global $db;
		return $db -> get_row("select *from {$db -> companies} where ID = $com_id");
	}
}

function get_client_company_info_by_id($client_com_id){
	if(!empty($client_com_id)){
		global $db;
		return $db -> get_row("select *from {$db -> company_clients} where ID = $client_com_id");
	}
}

function get_user_roles(){
	global $db;
	return $db -> get_results("select *from {$db -> user_roles} ");
}

function get_state_by_country_id($country_id){
	if(!empty($country_id)){
		global $db;
		return $db -> get_results("select *from {$db -> country_areas} where country_id = $country_id");
	}
}

function get_info_by_client_company($client_company_id){
	if(!empty($client_company_id)){
		global $db;
		return $db -> get_row("select *from {$db -> company_clients} where ID = $client_company_id");
	}
}

function get_client_company_info($arr = ''){
	global $db;
	$where = '';
	if(!empty($arr)){
		$where = 'where ';
		foreach($arr as $k => $v){
			$where .= "$k = $v AND ";
		}
		$where = rtrim($where,'AND ');
	}
	return $db -> get_results("select *from {$db -> company_clients} $where");
}

function get_info_by_company_client_contact($arr = ''){
	global $db;
	$where = '';
	if(!empty($arr)){
		$where = 'where ';
		foreach($arr as $k => $v){
			$where .= "$k = $v,";
		}
		$where = rtrim($where,',');
	}
	return $db -> get_results("select *from {$db -> company_client_contacts} $where");
}


function get_company_project_info($arr = ''){
	global $db;
	$where = '';
	if(!empty($arr)){
		$where = 'where ';
		foreach($arr as $k => $v){
			$where .= "$k = $v,";
		}
		$where = rtrim($where,',');
	}
	return $db -> get_results("select *from {$db -> company_projects} $where"); 
}

function get_name_by_user_id($id){
	if(!empty($id))
	return ucwords(get_user_meta($id, 'first_name', true).' '.get_user_meta($id, 'last_name', true));
}

function get_cur_user_company_id($current_user_id){
	if(!empty($current_user_id))
	$current_user = get_userdata($current_user_id);
	return $current_user -> company_id;
}


/*
** Pagination
*/

function backend_pagination($total_records, $limit, $set){

	$qs = http_build_query($_GET);
	//print_R($qs);
	$qs = str_replace('?set='.$set, '', $qs);
	$qs = str_replace('&set='.$set, '', $qs);
	$qs = str_replace('set='.$set, '', $qs);
	$identifier = (empty($qs)) ? '?' : '&';
	$identifier2 = (empty($qs)) ? '' : '?' ;
	// How many adjacent sets should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/

	$total_sets = $total_records;
	
	/* Setup vars for query. */
	$targetset = get_site_url()."/ttm-admin".$identifier2.$qs; 	//your file name  (the name of this file)

	if ($set == 0) $set = 1;					//if no set var is given, default to 1.
	$prev = $set - 1;							//previous set is set - 1
	$next = $set + 1;	

	$lastset = ceil($total_sets/$limit);		//lastset is = total sets / items per set, rounded up.
	$lpm1 = $lastset - 1;						//last set minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastset > 1)
	{	
		$pagination .= "<div class=\"pagination\"><ul class=\"pagination pagination_1\">";
		//previous button
		if ($set > 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set=$prev\">&laquo; Previous</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">&laquo; Previous</span></a></li>";	
		
		//sets	
		if ($lastset < 7 + ($adjacents * 2))	//not enough sets to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastset; $counter++)
			{
				if ($counter == $set)
					$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
				else
					$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
			}
		}
		elseif($lastset > 5 + ($adjacents * 2))	//enough sets to hide some
		{
			//close to beginning; only hide later sets
			if($set < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lastset\">$lastset</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastset - ($adjacents * 2) > $set && $set > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $set - $adjacents; $counter <= $set + $adjacents; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=$lastset\">$lastset</a></li>";		
			}
			//close to end; only hide early sets
			else
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $lastset - (2 + ($adjacents * 2)); $counter <= $lastset; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set=$counter\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($set < $counter - 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set=$next\">Next &raquo;</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">Next &raquo;</span></a></li>";
		$pagination.= "</ul></div>\n";		
	}
		echo $pagination;
}


function pagination($total_records, $limit, $set){

	$qs = http_build_query($_GET);
	//print_R($qs);
	$qs = str_replace('?set_area='.$set, '', $qs);
	$qs = str_replace('&set_area='.$set, '', $qs);
	$qs = str_replace('set_area='.$set, '', $qs);
	$identifier = (empty($qs)) ? '?' : '&';
	$identifier2 = (empty($qs)) ? '' : '?' ;
	// How many adjacent sets should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/

	$total_sets = $total_records;
	
	/* Setup vars for query. */
	$targetset = get_site_url()."/ttm-admin".$identifier2.$qs; 	//your file name  (the name of this file)

	if ($set == 0) $set = 1;					//if no set var is given, default to 1.
	$prev = $set - 1;							//previous set is set - 1
	$next = $set + 1;	

	$lastset = ceil($total_sets/$limit);		//lastset is = total sets / items per set, rounded up.
	$lpm1 = $lastset - 1;						//last set minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastset > 1)
	{	
		$pagination .= "<div class=\"pagination\"><ul class=\"pagination pagination_1\">";
		//previous button
		if ($set > 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$prev\">&laquo; Previous</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">&laquo; Previous</span></a></li>";	
		
		//sets	
		if ($lastset < 7 + ($adjacents * 2))	//not enough sets to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastset; $counter++)
			{
				if ($counter == $set)
					$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
				else
					$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$counter\">$counter</a></li>";					
			}
		}
		elseif($lastset > 5 + ($adjacents * 2))	//enough sets to hide some
		{
			//close to beginning; only hide later sets
			if($set < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$lastset\">$lastset</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastset - ($adjacents * 2) > $set && $set > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $set - $adjacents; $counter <= $set + $adjacents; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$counter\">$counter</a></li>";					
				}
				$pagination.= "<li><a href=\"#\">...</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$lpm1\">$lpm1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$lastset\">$lastset</a></li>";		
			}
			//close to end; only hide early sets
			else
			{
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=1\">1</a></li>";
				$pagination.= "<li><a href=\"$targetset".$identifier."set_area=2\">2</a></li>";
				$pagination.= "<li><a href=\"#\">...</a></li>";
				for ($counter = $lastset - (2 + ($adjacents * 2)); $counter <= $lastset; $counter++)
				{
					if ($counter == $set)
						$pagination.= "<li class='active'><a href=\"#\" class=\"current\"><span>$counter</span></a></li>";
					else
						$pagination.= "<li><a href=\"$targetset".$identifier."set_area=$counter\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($set < $counter - 1) 
			$pagination.= "<li><a href=\"$targetset".$identifier."set=$next\">Next &raquo;</a></li>";
		else
			$pagination.= "<li><a href=\"#\"><span class=\"disabled\">Next &raquo;</span></a></li>";
		$pagination.= "</ul></div>\n";		
	}
		echo $pagination;
}

function get_areas_name($arr){
	foreach($arr as $val){
		$area_name = get_area_by_id($val);
		echo ' '.$area_name.',';
	}
}


function getStartAndEndDate($week, $year){
    $time = strtotime("1 January $year", time());
    $day = date('w', $time);
    $time += ((7*$week)+1-$day)*24*3600;
    $return[0] = date('Y-n-j', $time);
    $time += 6*24*3600;
    $return[1] = date('Y-n-j', $time);
    return $return;
}

function get_area_by_id($area_id){
	global $db;
	$area_info = $db -> get_row("SELECT area_name FROM {$db -> country_areas} where ID= $area_id");
	return $area_info -> area_name;
}

function get_worker_project_name(){
	global $db;
	$current_user_id = ttm_get_session('id');
	$info = $db -> get_results("SELECT distinct(project_id) FROM {$db->company_project_task_workers} a  JOIN {$db->company_project_tasks} b ON  a.task_id = b.ID where a.worker_id = {$current_user_id}");
	
	foreach($info as $val){
		$project_info[] = get_company_project_info(array('ID' => $val->project_id));
	}
	return $project_info;
}

?>