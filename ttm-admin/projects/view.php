<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-tasks" aria-hidden="true"></i>
					<h3 class="box-title">Projects</h3>
					<a class="btn btn-info pull-right" href="?page=projects&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New Project</a>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project Title</th>
								<th>Project Manager</th>
								<th>Client Contact</th>
								<th>Client Company</th>
								<th>Active</th>
								<th>Client Review Approval</th>
								<th>Comment</th>
								<th width="100px">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
								global $db;
								
								$all_comp_project_info = get_company_project_info();
								
								$i=1;
								foreach($all_comp_project_info as $project_info){
										
									echo '<tr class="per_company'.$project_info->ID.'">

									<td>'.$i.'</td>

									<td>'.ucfirst($project_info -> project_name).'</td>

									<td>'.get_name_by_user_id($project_info -> project_manager_id).'</td>

									<td>'.get_name_by_user_id($project_info -> client_id).'</td>

									<td>'.ucwords(get_client_company_info_by_id($project_info ->client_company_id) -> company_name).'</td>

									<td class="company-status">';
									
									$status = $project_info -> status;
									if($status == 1) echo 'Active';									
									else echo 'Inactive';
									
									echo '</td>';
									
									echo '<td>';
									$client_approval_status = $project_info ->client_approval;
									if($client_approval_status == 1) echo 'Yes';									
									else echo 'No';
									echo '</td>

									<td>'.$project_info ->comment.'</td>';
									
									echo '<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=projects&action=edit&id='.$project_info ->ID.'"><span class="glyphicon glyphicon-pencil"></span></a>
										
										<a title="Delete" onclick="delete_project('.$project_info ->ID.')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> ';
									
										if($status == 1){
											$action = 'deactivate_project';
											$title = "Inactive Project";
											$status_class = 'fa-undo';
										}
										else{ 
											$action = 'activate_project';
											$title = "Active Project";
											$status_class = 'fa-check';
										}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick="'.$action.'('.$project_info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
									
									echo '</td>

								</tr>';
								
								$i++;
								
								}
							?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>