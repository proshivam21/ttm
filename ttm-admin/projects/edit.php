<?php 
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	
	$current_user_id = ttm_get_session('id');
	$current_user_company_id = get_userdata($current_user_id)-> company_id;
	
	global $db;
	
	if(isset($_POST) && !empty($_POST)){
		global $project;
		$message = $project -> update_project($id);
	} 
	
	$project_info = get_company_project_info(array('ID' => $id))[0];
	
	$user_manager_info = get_users_by_company_id($current_user_company_id, 4);
?>



<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>

	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">
			
				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" action="" name="" id="" method="post">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-tasks"></i></h3>

						<div class="pull-right">

							<a class="btn bg-red" href="?page=projects">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info">Save</button>

						</div>

					</div>
					
					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">Project Title<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="project_name" type="text" class="form-control" value="<?php echo $project_info -> project_name; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="pm" class="col-sm-2 control-label">Project Manager<sup>*</sup></label>
							
							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="project_manager_id">

								    <option value="">Select</option>

									<?php
										$user_manager_info = get_users_by_company_id($current_user_company_id, 4);
										foreach($user_manager_info as $info){
											$select = '';
											if($info -> ID == $project_info -> project_manager_id) $select = 'selected';
											echo '<option value="'.$info -> ID.'" '.$select.'>'.$info ->display_name.'</option>';
										}
									?>

								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="pm" class="col-sm-2 control-label">Client Contact<sup>*</sup></label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="client_id">

								  <option value="">Select</option>

								<?php
									$user_client_info = get_users_by_company_id($current_user_company_id, 6);
									foreach($user_client_info as $info){
										$select = '';
										if($info -> ID == $project_info -> client_id) $select = 'selected';
										echo '<option value="'.$info -> ID.'" '.$select.'>'.$info ->display_name.'</option>';
									}
								?>

								</select>

							</div>

						</div>
						
						<div class="form-group">

							<label for="company" class="col-sm-2 control-label">Client Company<sup>*</sup></label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="client_company_id">

								  <option value="">Select</option>

									<?php
									$client_company = get_client_company_info(array('company_id' => $current_user_company_id, 'status' => 1) );
										foreach($client_company as $info){
											$select = '';
											if($info -> ID == $project_info -> client_company_id) $select = 'selected';
											echo '<option value="'.$info -> ID.'" '.$select.'>'.ucwords($info ->company_name).'</option>';
										}
									?>

								</select>
							</div>

						</div>

						

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								<?php
									$status_info = array('1' => 'Yes', '0' => 'No');
									foreach($status_info as $k => $v){
										$select = '';
										if( isset($_POST['status']) && ($_POST['status'] == $k) || ($k == 1) || ($info -> ID == $project_info -> status) ) $select = 'selected';
										
										echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';	
									}
								?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="client_review" class="col-sm-2 control-label">Client Review Approval</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="client_approval">

								<?php
									$client_approval_info = array('1' => 'Yes', '0' => 'No');
									foreach($client_approval_info as $k => $v){
										$select = '';
										if( isset($_POST['client_approval']) && ($_POST['client_approval'] == $k) || ($k == 1) || ($info -> ID == $project_info -> client_approval) ) $select = 'selected';
										echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';	
									}
								?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Type Here...."><?php echo $project_info->comment; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>