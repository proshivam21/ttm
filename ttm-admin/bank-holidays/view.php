<?php
	global $holidays;
	$holidays_info = $holidays -> get_bank_holidays(array('company_id' => $current_user_company_id));
	$i = 1;
?>

<section class="content">

	<div class="row">

		<div class="col-xs-12">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-university" aria-hidden="true"></i>

					<h3 class="box-title">Bank Holidays</h3>

					<a class="btn btn-info pull-right" href="?page=bank-holidays&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New Bank Holiday</a>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Holiday Name</th>

								<th>Date</th>

								<th>Area</th>

								<th>Free Time (%)</th>

								<th>Comment</th>
								
								<th style="width: 70px;">Actions</th>

							</tr>

						</thead>

						<tbody>

							<?php foreach($holidays_info as $info): ?>

								<tr class="per_company<?php echo $info -> ID; ?>">

									<td><?php echo $i; ?></td>

									<td><?php echo ucwords($info -> holiday_name); ?></td>

									<td><?php echo date('d M, Y', strtotime($info -> holiday_date) ); ?></td>

									<td>
									<?php 
										if(!empty($info -> holiday_areas_ids)){ 
											$arr = unserialize($info -> holiday_areas_ids);
											get_areas_name($arr);  
										}
									?></td>

									<td><?php echo $info -> holiday_free_time; ?></td>

									<td><?php echo $info -> comment; ?></td>

									<?php
									
									echo '<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=bank-holidays&action=edit&id='.$info ->ID.'"><span class="glyphicon glyphicon-pencil"></span></a>
										
										<a title="Delete" onclick="delete_holiday('.$info ->ID.')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> ';
									
									echo '</td>

								</tr>';

							
								$i++;
								endforeach;
							?>

						</tbody>

					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>