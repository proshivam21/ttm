<?php
	if($_POST){
		global $holidays;
		$message = $holidays -> add_holidays();
	}
	$countries = get_countries();
?>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="holiday">
				
				
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-university" aria-hidden="true"></i></h3>

						<div class="pull-right">

							<a class="btn bg-red" href="?page=bank-holidays">Discard</a>&nbsp;&nbsp;

							<button type="submit" class="btn btn-info"  name="save">Save</button>

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label class="col-sm-2 control-label">Holiday Name <sup>*</sup> </label>

							<div class="col-sm-10">

								<input name="holiday_name" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="date" class="col-sm-2 control-label">Date<sup>*</sup> </label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" name="holiday_date" class="form-control pull-right datepicker" value="">

								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="country" class="col-sm-2 control-label">Country</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="country_id" id= "country_id1">
									<option value="">Select</option>
									<?php 
										foreach($countries as $con_info){
											$select = '';
											if($con_info->ID == $_POST['country_id']) $select = 'selected';
											echo '<option value="'.$con_info->ID.'" '.$select.'>'.$con_info->country.'</option>';
										}
									?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="area" class="col-sm-2 control-label">Area</label>

							<div class="col-sm-10">
								<div id="state_dd2">
									<select class="form-control select2" style="width: 100%;" multiple="true">
									</select> 
								</div>
							</div>

						</div>

						<div class="form-group">

							<label class="col-sm-2 control-label">Free Time(%)</label>

							<div class="col-sm-10">

								<input name="holiday_free_time" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Write Here...."></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>