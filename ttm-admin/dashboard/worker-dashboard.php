<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>LM</h3>
					<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="62"> Marc Rueckziegel	
				</div>
				<div class="icon">
					<i class="fa fa-user"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>12</h3>
					<p>TIMESHEETS ALREADY</p>
					<p>APPROVED </p>
				</div>
				<div class="icon">
					<i class="fa fa-check"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>10</h3>
					<p>TIMESHEETS READY FOR</p>
					<p>ENTRIES</p>
				</div>
				<div class="icon">
					<i class="fa fa-paper-plane" aria-hidden="true"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>10</h3>
					<p>TIMESHEETS</p>
					<p>OVEDUE</p>
				</div>
				<div class="icon">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->
	
	<div class="row">
		<div class="col-xs-6">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-pie-chart" aria-hidden="true"></i>
					<h3 class="box-title">Chart</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<div class="col-xs-6">
						<select class="form-control select2" style="width: 100%;">
							<option selected="selected">2018</option>
							<option>2017</option>
							<option>2016</option>
							<option>2015</option>
						</select>
					</div>
					<div class="col-xs-6">
						<select class="form-control select2" style="width: 100%;">
							<option selected="selected">Jan</option>
							<option>Feb</option>
							<option>Mar</option>
							<option>Apr</option>
						</select>
					</div>
					<div class="col-xs-6">
						<div class="col-xs-2"></div>
						<div class="col-xs-8">
							<br/>
							<br/>
							<br/>
							<br/>
							<br/>
							<p><i class="fa fa-square" aria-hidden="true" style="color: #4A82BC"></i> <span>Projects</span></p>
							<p><i class="fa fa-square" aria-hidden="true" style="color: #5C5F93"></i> <span>Vacations</span></p>
							<p><i class="fa fa-square" aria-hidden="true" style="color: #54B9D8"></i> <span>Sick Leave</span></p>
							<p><i class="fa fa-square" aria-hidden="true" style="color: #DB5B9B"></i> <span>Others</span></p>
						</div>
						<div class="col-xs-2"></div>
					</div>
					<div class="col-xs-6">
						<div class="donut-chart" style="height: 300px;"></div>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<div class="col-xs-6">
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-users" aria-hidden="true"></i>
					<h3 class="box-title">Project Managers</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
					<div class="col-xs-6">
						<!-- small box -->
						<div class="small-box bg-gray">
							<div class="inner">
								<img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->

	<div class="row">
		<div class="col-xs-4">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
					<h3 class="box-title">Timesheets Overdue</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Timesheet Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>5</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>6</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		
		<div class="col-xs-8">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-ban" aria-hidden="true"></i>
					<h3 class="box-title">Timesheets Rejected</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Timesheet Name</th>
								<th>Project Manager</th>
								<th>Line Manager</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Marc Rueckziegel</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal</td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Marc Rueckziegel</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal</td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Marc Rueckziegel</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal</td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Marc Rueckziegel</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal</td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>5</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Marc Rueckziegel</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Shivam Agrawal</td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-list-alt" aria-hidden="true"></i>
					<h3 class="box-title">Projects Being Mapped as A Worker</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project Name</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Budget Total</th>
								<th>Budget Available</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">TimTraMac</a></td>
								<td>23.01.2016</td>
								<td>20.12.2017</td>
								<td>2000</td>
								<td>500</td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">CRM Project</a></td>
								<td>01.01.2017</td>
								<td>31.01.2018</td>
								<td>1800</td>
								<td>400</td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">SMS Project</a></td>
								<td>20.05.2016</td>
								<td>01.12.2016</td>
								<td>800</td>
								<td>50</td>
								
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">PTM Project</a></td>
								<td>25.01.2017</td>
								<td>01.12.2018</td>
								<td>2200</td>
								<td>450</td>
							</tr>
							<tr>
								<td>5</td>
								<td><a href="">TTM Project</a></td>
								<td>03.03.2016</td>
								<td>01.12.2018</td>
								<td>3500</td>
								<td>1500</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>