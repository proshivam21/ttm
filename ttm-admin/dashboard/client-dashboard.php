<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-xs-4 no-padding">
			<div class="col-xs-12">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>12</h3>
						<p>TIMESHEETS ALREADY</p>
						<p>APPROVED </p>
					</div>
					<div class="icon">
						<i class="fa fa-check"></i>
					</div>
					<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-xs-12">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>10</h3>
						<p>TIMESHEETS READY FOR</p>
						<p>APPROVAL</p>
					</div>
					<div class="icon">
						<i class="fa fa-paper-plane" aria-hidden="true"></i>
					</div>
					<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-xs-8">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
					<h3 class="box-title">Timesheets Ready For Approval</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project Name</th>
								<th>Timesheet Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>TimTraMac</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>2</td>
								<td>TimTraMac</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>3</td>
								<td>TimTraMac</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>4</td>
								<td>TimTraMac</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>5</td>
								<td>TimTraMac</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-list-alt" aria-hidden="true"></i>
					<h3 class="box-title">Projects Being Mapped as A Client</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project Name</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Budget Total</th>
								<th>Budget Available</th>
								<th>Budget Used</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">TimTraMac</a></td>
								<td>23.01.2016</td>
								<td>20.12.2017</td>
								<td>2000</td>
								<td>500</td>
								<td>90%</td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">CRM Project</a></td>
								<td>01.01.2017</td>
								<td>31.01.2018</td>
								<td>1800</td>
								<td>400</td>
								<td>80%</td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">SMS Project</a></td>
								<td>20.05.2016</td>
								<td>01.12.2016</td>
								<td>800</td>
								<td>50</td>
								<td>82%</td>
								
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">PTM Project</a></td>
								<td>25.01.2017</td>
								<td>01.12.2018</td>
								<td>2200</td>
								<td>450</td>
								<td>86%</td>
							</tr>
							<tr>
								<td>5</td>
								<td><a href="">TTM Project</a></td>
								<td>03.03.2016</td>
								<td>01.12.2018</td>
								<td>3500</td>
								<td>1500</td>
								<td>70%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>