<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3>15</h3>
					<p>PTS READY TO SEND</p>
				</div>
				<div class="icon">
					<i class="fa fa-clock-o"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>12</h3>
					<p>PTS ALREADY SEND</p>
				</div>
				<div class="icon">
					<i class="fa fa-paper-plane"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>10</h3>
					<p>PTS SIGNED</p>
				</div>
				<div class="icon">
					<i class="fa fa-check" aria-hidden="true"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h3>10</h3>
					<p>SNED TO FACTORING</p>
				</div>
				<div class="icon">
					<i class="ion ion-pie-graph"></i>
				</div>
				<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
			</div>
		</div>
		<!-- ./col -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Project</th>
								<th>PTS Ready</th>
								<th>Already Send</th>
								<th>Signed</th>
								<th>Send to Factoring</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><a href="">TimTraMac</a></td>
								<td>2</td>
								<td>1</td>
								<td>2</td>
								<td>2</td>
							</tr>
							<tr>
								<td>2</td>
								<td><a href="">CRM Project</a></td>
								<td>3</td>
								<td>2</td>
								<td>2</td>
								<td>2</td>
							</tr>
							<tr>
								<td>3</td>
								<td><a href="">SMS Project</a></td>
								<td>3</td>
								<td>3</td>
								<td>2</td>
								<td>2</td>
								
							</tr>
							<tr>
								<td>4</td>
								<td><a href="">PTM Project</a></td>
								<td>4</td>
								<td>3</td>
								<td>2</td>
								<td>2</td>
							</tr>
							<tr>
								<td>5</td>
								<td><a href="">TTM Project</a></td>
								<td>3</td>
								<td>3</td>
								<td>2</td>
								<td>2</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>