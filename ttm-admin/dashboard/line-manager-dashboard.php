<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-xs-4 no-padding">
			<div class="col-xs-12">
				<!-- small box -->
				<div class="small-box bg-green">
					<div class="inner">
						<h3>12</h3>
						<p>TIMESHEETS ALREADY APPROVED</p>
					</div>
					<div class="icon">
						<i class="fa fa-check"></i>
					</div>
					<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-xs-12">
				<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>10</h3>
						<p>TIMESHEETS READY FOR APPROVAL</p>
					</div>
					<div class="icon">
						<i class="fa fa-paper-plane" aria-hidden="true"></i>
					</div>
					<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
			<!-- ./col -->
			<div class="col-xs-12">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>10</h3>
						<p>TIMESHEETS OVEDUE</p>
					</div>
					<div class="icon">
						<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
					</div>
					<!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
				</div>
			</div>
		</div>
		<!-- ./col -->
		<div class="col-xs-8">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
					<h3 class="box-title">Timesheets Overdue</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Worker Name</th>
								<th>Timesheet Name</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Sam</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>2</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Sam</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>3</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Sam</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>4</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Sam</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
							<tr>
								<td>5</td>
								<td><img src="dist/img/user2-160x160.jpg" style="vertical-align:middle" class="img-circle" alt="User Image" width="30"> Sam</td>
								<td><a href="">Oct 30 - Nov 5, 2017</a></td>
								<td><a title="View" class="btn bg-green btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>