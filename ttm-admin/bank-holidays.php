 <?php
$page   = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if ($page == 'bank-holidays') {
    switch ($action) {
        case 'edit':
            include 'bank-holidays/edit.php';
            break;
        case 'add':
            include 'bank-holidays/add.php';
            break;
        default:
            include 'bank-holidays/view.php';
            break;
    }
}
?> 