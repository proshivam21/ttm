<?php 
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

	if($_POST['update'] && !empty($_POST['update'])){
		global $db;
		extract($_POST);
		
			$full_name = $contact_first_name.' '.$contact_last_name;
		
			/*
			 * Update company Info 
			 */
			global $db;			
			$companydata = array(
								'company_id'	=>  $company_id,
								'range'		 	=>  $range,
								'price' 		=>  $price,
								'currency'  	=>  $currency,
							);
			
			$db->update($db->company_pricing, $companydata, array('ID' => $id));
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Company price has been changed sucessfully.');
		
	}

	global $db;
	$comp_info = $db -> get_row("select a.ID, a.range as range1, a.price, a.currency, b.company_name, a.company_id from {$db->company_pricing} a inner join {$db->companies} b ON a.company_id = b.ID where a.ID = {$id}");
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" method="post" action="" id="company_price">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-money" aria-hidden="true"></i> Pricing</h3>
						<div class="pull-right">
							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
							<input type="submit" class="btn btn-info" name="update" value="Update" >
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="range" class="col-sm-2 control-label">Company<sup>*</sup></label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="company_id">
									<option value="">Select</option>
									<?php
										global $db;
										$company_info = $db -> get_results("SELECT *FROM {$db -> companies} where status = 1");		
										foreach($company_info as $info){
											$select = '';
											if($info -> ID == $comp_info -> company_id) $select = 'selected';
											echo '<option value="'.$info -> ID.'" '.$select.'>'.$info -> company_name.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="range" class="col-sm-2 control-label">Range<sup>*</sup></label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="range">
									<option value="">Select</option>
								<?php 
									for($i=1; $i<=10; $i++){
										$select = '';
										if($comp_info->range1 == '0-'.($i*20) ) $select = 'selected';
										echo '<option value="0-'.($i*20).'" '.$select.'>0-'.($i*20).'</option>'; 
									}
								?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="price" class="col-sm-2 control-label">Price<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="price" type="text" class="form-control" value="<?php echo $comp_info ->price; ?>">
							</div>
						</div>											
						
						<div class="form-group">							
							<label for="range" class="col-sm-2 control-label">Currency<sup>*</sup></label>							
							<div class="col-sm-10">								
								<select class="form-control select2" style="width: 100%;" name="currency">				
									<option value="">Select</option>
									<?php
										$cur = array('euro' => 'Euro', 'usd' => 'USD');
										foreach($cur as $k => $v){
											$select = '';
											if($k == $comp_info -> currency) $select = 'selected';
											echo '<option value="'.$k.'" '.$select.'>'.$v.'</option>';
										}
									?>									
								</select>							
							</div>						
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->

</section>