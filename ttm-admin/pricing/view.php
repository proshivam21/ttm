<?php
	if($_POST){
		global $db;
		extract($_POST);
		$price_ar = array(
						'company_id'	=>	$company_name,
						'range'			=>	$range,
						'price'			=>	$price,
						'currency'		=>	$currency,
					);

		if( !($db->insert('ttm_company_pricing', $price_ar)) ){
			$message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Some error occured. Please try again.');
		}
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Company price has been added sucessfully.');
	}
?>
<style>
	.ss {
	  text-transform: uppercase;
	}
</style>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" method="post" action="" id="company_price">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-money" aria-hidden="true"></i> Pricing</h3>
						<div class="pull-right">
							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info">Save</button>
						</div>
					</div>
					<div class="box-body">
						<div class="form-group">
							<label for="range" class="col-sm-2 control-label">Company<sup>*</sup></label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="company_name">
									<option value="">Select</option>
									<?php
										global $db;
										$company_info = $db -> get_results("SELECT *FROM {$db -> companies} where status = 1 ");		
										foreach($company_info as $info){
											echo '<option value="'.$info -> ID.'">'.$info -> company_name.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label for="range" class="col-sm-2 control-label">Range<sup>*</sup></label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="range">
									<option value="">Select</option>
								<?php 
									for($i=1; $i<=10; $i++){
										echo '<option value="0-'.($i*20).'">0-'.($i*20).'</option>'; 
									}
								?>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="price" class="col-sm-2 control-label">Price<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="price" type="text" class="form-control">
							</div>
						</div>											
						
						<div class="form-group">							
							<label for="range" class="col-sm-2 control-label">Currency<sup>*</sup></label>							
							<div class="col-sm-10">								
								<select class="form-control select2" style="width: 100%;" name="currency">				
									<option value="">Select</option>
									<option value="euro" selected>Euro</option>								  
									<option value="usd">USD</option>								
								</select>							
							</div>						
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
	
	
	
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<div class="box">
				<div class="box-header with-border">
					<i class="fa fa-money" aria-hidden="true"></i>
					<h3 class="box-title">Pricings</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>Client Company</th>
								<th>Range</th>
								<th>Price</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$company_price_info = $db -> get_results("select a.ID, a.range, a.price, a.currency, b.company_name from {$db->company_pricing} a inner join {$db->companies} b ON a.company_id = b.ID");
							$i=1;
							foreach($company_price_info as $price_info){
								echo '<tr class="per_company_price'.$price_info -> ID.'">
										<td>'.$i.'</td>
										<td>'.ucwords($price_info -> company_name).'</td>
										<td>'.$price_info -> range.'</td>
										<td class="ss">'.$price_info -> price.' '. ucwords($price_info -> currency). '</td>
										<td>
											<a title="Edit" href="?page=pricing&action=edit&id='.$price_info -> ID.'" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
											<a title="Delete" class="btn btn-danger btn-xs" onclick="delete_price('.$price_info -> ID.')"><span class="glyphicon glyphicon-trash"></span></a>
										</td>
									</tr>';
									$i++;
							}
						?>
							
							
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>