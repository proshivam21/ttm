<?php 	if($_POST['save']) {		extract($_POST);		foreach($_POST as $key => $value) {					if($key != 'save'){				if(!empty($value)) update_option($key, $value);			}		}			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your settings has been saved successfully.');	}?><section class="content">	<?php 		if(isset($message)){			print_message($message);		}	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">								<!-- form start -->				<form method="POST" action="" class="form-horizontal">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-cog" aria-hidden="true"></i> General Settings</h3>
						<input type="submit" class="btn btn-info pull-right" name="save" value="Save">
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="system_factoring_dept_email" class="col-sm-2 control-label">Factoring Department Email</label>
							<div class="col-sm-10">
								<input name="system_factoring_dept_email" type="email" value="<?php echo get_option('system_factoring_dept_email'); ?>" class="form-control">
							</div>
						</div>
						
						<div class="form-group">
							<label for="system_invoice_storage_path" class="col-sm-2 control-label">Storage Path </label>
							<div class="col-sm-10">
								<input name="system_invoice_storage_path" type="text" class="form-control" value="<?php echo get_option('system_invoice_storage_path'); ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="system_invoice_prefix" class="col-sm-2 control-label">Invoice Prefix</label>
							<div class="col-sm-10">
								<input name="system_invoice_prefix" type="text" class="form-control" value="<?php echo get_option('system_invoice_prefix'); ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="system_invoice_starting_num" class="col-sm-2 control-label">Invoice Starting Number</label>
							<div class="col-sm-10">
								<input name="system_invoice_starting_num" type="text" class="form-control" value="<?php echo get_option('system_invoice_starting_num'); ?>">
							</div>
						</div>												<div class="form-group">							<label for="site_url" class="control-label col-sm-2">Site url</label>							<div class="control-label col-sm-10">								<input name="siteurl" class="form-control" value="<?php echo get_option('siteurl'); ?>" type="text">							</div>						</div>						<div class="form-group">							<label for="admin_url" class="control-label col-sm-2">Site Admin url</label>							<div class="control-label col-sm-10">								<input name="admin_url" class="form-control" value="<?php echo get_option('home'); ?>" type="text">							</div>						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
	</div>
	<!-- /.row -->
</section>