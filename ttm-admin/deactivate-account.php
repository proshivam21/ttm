<section class="content">
	<div class="error-page">
		<h2 class="headline text-yellow" align="center"> <i class="fa fa-warning text-yellow"></i> Deactivate Account</h2>
		<div class="error-content">
			<p>
				Are you sure you want to deactivate your account?
			</p>
			<form class="search-form">
				<div class="input-group">
					<div class="input-group-btn">
						<button type="submit" name="submit" class="btn btn-warning btn-flat">Deactivate Account
						</button>
					</div>
				</div>
				<!-- /.input-group -->
			</form>
		</div>
		<!-- /.error-content -->
	</div>
	<!-- /.error-page -->
</section>