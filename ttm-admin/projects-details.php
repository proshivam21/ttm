<?php	
	$id = isset($_REQUEST['ID']) ? $_REQUEST['ID'] : '';	
	
	if($_POST['update']){	
		global $project;		
		$message = $project -> update_project($id);	
	}			

	if($_POST['add_task']){		
		global $db;		
		extract($_POST);
		
		$task_info = array(		
						'project_id' 	=> $id,							
						'task_name' 	=> $task_name,							
						'budget_total'  => $budget_total,							
						'start_date' 	=> date("Y-m-d", strtotime($start_date)),							
						'end_date' 		=> date("Y-m-d", strtotime($end_date)),						
					);				
		
		$db->insert($db->company_project_tasks, $task_info);	
		
		$task_id = $db -> insert_id;
	
		$count_worker = count($worker_id);
		for($i=0; $i<$count_worker; $i++){
			$db->insert($db->company_project_task_workers, array('task_id' => $task_id, 'worker_id' => $worker_id[$i], 'budget_total' => $worker_budget_total[$i]));
		}
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Task details has been added successfully');
		$page = $_SERVER['PHP_SELF'];
		$sec = "2";
		header("Refresh: $sec; url=?page=projects-details&ID=7");
	}	
	
	$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
	
	$projects_info = get_company_project_info(array('ID' => $id))[0];		
	$company_name = ucwords(get_info_by_client_company($projects_info -> client_company_id) -> company_name);	
?>
			
<section class="content">	<?php 		if(isset($message)){			print_message($message);		}	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">							<!-- /.box-header -->				<!-- form start -->				<form class="form-horizontal" action="" name="" method="post"  enctype="multipart/form-data" id="project1">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-tasks"></i> <?php echo $projects_info -> project_name; ?></h3>
						<div class="pull-right">
							<a href="?page=projects-overview" class="btn bg-red">Discard</a>&nbsp;&nbsp;
							<input type="submit" class="btn btn-info" value="Save" name="update" >
						</div>
					</div>
					
					<div class="box-body">
						<div class="form-group">
							<label for="project_name" class="col-sm-2 control-label">Project Title<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="project_name" type="text" class="form-control" value="<?php echo $projects_info -> project_name; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="pm" class="col-sm-2 control-label">Client Contact</label>
							<div class="col-sm-10">															<input type="text" class="form-control" value="<?php echo get_name_by_user_id($projects_info -> client_id); ?>" disabled>
														</div>
						</div>
						<div class="form-group">
							<label for="company" class="col-sm-2 control-label">Client Company</label>
							<div class="col-sm-10">
								<label class="control-label"><?php echo $company_name; ?></label>
							</div>
						</div>
						<div class="form-group">
							<label for="clien_int_order_no" class="col-sm-2 control-label">Client Internal Order no.<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="client_int_order_no" type="text" class="form-control" value="<?php echo $projects_info -> client_int_order_no; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="project_number" class="col-sm-2 control-label">Project Number<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="project_number" type="text" class="form-control" value="<?php echo $projects_info -> project_number; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">Active</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="status">								<?php									$status_info = array('1' => 'Yes', '0' => 'No');									foreach($status_info as $k => $v){										$select = '';										if($k == $projects_info -> status)  $select = 'selected';																				echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';										}								?>								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="client_approval" class="col-sm-2 control-label">Client Review Approval</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="client_approval">								<?php									$app_info = array('1' => 'Yes', '0' => 'No');									foreach($app_info as $k => $v){										$select = '';										if($k == $projects_info -> client_approval)  $select = 'selected';																				echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';										}								?>
								</select>
							</div>
						</div>						
						<?php
						
							$budget_used = $projects_info -> budget_used;
							if(empty($budget_used)){ 
								$budget_used = 0;
							}
							/* $budget_rest = ($projects_info -> budget_total) - $budget_used; */
							$budget_rest = $projects_info -> budget_rest;
						?>
						
						<div class="form-group">							<label class="col-sm-2 control-label">Budget Total</label>							<div class="col-sm-10">								<input name="budget_total" type="text" class="form-control" value="<?php echo $projects_info -> budget_total; ?>" >							</div>						</div>												<div class="form-group">							<label class="col-sm-2 control-label">Budget Used</label>							<div class="col-sm-10">								<input type="text" class="form-control" value="<?php echo $budget_used; ?>" disabled>							</div>						</div>												<div class="form-group">							<label class="col-sm-2 control-label">Budget Rest</label>							<div class="col-sm-10">								<input type="text" class="form-control" value="<?php echo $budget_rest; ?>" disabled>							</div>						</div>												<div class="form-group">							<label class="col-sm-2 control-label">Start Date</label>							<div class="col-sm-10">								<div class="input-group date">								  <div class="input-group-addon">									<i class="fa fa-calendar"></i>								  </div>								  <input type="text" class="form-control pull-right datepicker" value="<?php echo date("d.m.Y", strtotime($projects_info -> start_date)); ?>" name="start_date">								</div>							</div>						</div>												<div class="form-group">							<label class="col-sm-2 control-label">End Date</label>							<div class="col-sm-10">								<div class="input-group date">								  <div class="input-group-addon">									<i class="fa fa-calendar"></i>								  </div>								  <input type="text" class="form-control pull-right datepicker" value="<?php echo date("d.m.Y", strtotime($projects_info -> end_date)); ?>" name="end_date">								</div>							</div>						</div>						
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="10" class="form-control">								<?php echo $projects_info -> comment; ?>
								</textarea>
							</div>
						</div>																							</div>				</form>
				<div class="box-body">						<div class="form-group">
							<!-- The time line -->
							<label for="tasks" class="col-sm-2 control-label">Tasks</label>
							<div class="col-sm-10">
								<ul class="timeline">
									<!-- timeline time label -->
									<li class="time-label">
										<!--<span class="bg-red">Task Management</span>-->
										<i class="fa fa-bars bg-red"></i> <a data-toggle="modal" data-target="#modal-add-task" aria-hidden="true" style="margin-left: 58px;" class="btn btn-info" href="#"><i class="fa fa-plus2 fa-plus" aria-hidden="true"></i> New Task</a>
									</li>
									<!-- /.timeline-label -->
									<!-- timeline item -->
									<?php										
										$tasks_info = $db ->get_results("SELECT *FROM {$db->company_project_tasks} where project_id = {$id}");
										
										foreach($tasks_info as $info){	
										
										global $db;
										$task_worker_info = $db -> get_results("SELECT *FROM {$db->company_project_task_workers} where task_id = {$info->ID}");
										
										foreach($task_worker_info as $w_info){
											$worker_id[] = $w_info -> worker_id;
											$budget_total[] = $w_info -> budget_total;
										}
										
									?>
									<li class="per_task<?php echo $info ->ID; ?>">
										<i class="fa fa-tasks bg-blue"></i>
										<div class="timeline-item">
											<div class="box">
												<div class="box-header with-border">
													<h3 class="box-title"> <?php echo ucwords($info -> task_name); ?></h3>
													<div class="pull-right">
														<a href="?page=edit-task&ID=<?php echo $info -> ID; ?>" class="btn bg-orange btn-xs"><span class="glyphicon glyphicon-pencil" ></span></a> 
														
														<a title="Delete" onclick="delete_task(<?php echo $info ->ID; ?>)" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> 
													</div>
												</div>
												<!-- /.box-header -->
												<div class="box-body table-responsive no-padding">
													<table class="table table-hover table-bordered table-striped">
														<thead>												
															<tr>
																<th>Task Name</th>
																<th>Workers</td>
																<th>Budget total</th>
																<th>Budget used</th>
																<th>Budget rest</th>
																<th>Start</th>
																<th>End</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<th><?php echo $info->task_name; ?></th>
																<th></td>
																<th><?php echo $info->budget_total; ?></th>
																<td><b>
																<?php 
																	$budget_used = $info->budget_used; 
																	if(empty($budget_used)) $budget_used = 0;
																	echo $budget_used;
																?>
																</b></td>
																<td><b>
																<?php 
																	echo ($info->budget_total) - $budget_used;
																?>
																</b></td>
																<td><b><?php echo date("d.m.Y", strtotime($info -> start_date)); ?></b></td>
																<td><b><?php echo date("d.m.Y", strtotime($info->end_date)); ?></b></td>
															</tr>
															
															<?php
																
																foreach($task_worker_info as $w_info){
																	/* <span class="label label-info">Oct 2 – Oct 8, 2017</span> */
																	$bud_rest = $w_info->budget_rest;
																	$bud_used = $w_info->budget_used;
																	if(empty($bud_rest)) $bud_rest = $w_info-> budget_total;
																	if(empty($bud_used)) $bud_used = 0;
																	echo '<tr>
																		<td></td>
																		<td><b>'.get_name_by_user_id($w_info -> worker_id).'<b></td>
																		<td>'.$w_info->budget_total.'</td>
																		<td>'.$bud_used.' </td>
																		<td>'.$bud_rest.'</td>
																		<td></td>
																		<td></td>
																	</tr>';
																}
																
															?>
														</tbody>
													</table>
												</div>
												<!-- /.box-body -->
											</div>
											<!-- /.box -->
										</div>
									</li>
								<?php } ?>
									<!-- END timeline item -->
								</ul>
							</div>
						</div>
						<!-- /.box-body -->				</DIV>
					
				
			</div>
		</div>
	</div>
	<!-- /.row -->
</section>


<div class="modal fade" id="modal-add-task">
  <div class="modal-dialog">
	<div class="modal-content">	<form class="form-horizontal" action="" method="post" id="task">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Add New Task</h4>
	  </div>
	  <div class="modal-body">
			<div class="box-body">
				<div class="form-group">
					<label for="task_name" class="col-sm-3 control-label">Task Name<sup>*</sup></label>
					<div class="col-sm-9">
						<input name="task_name" type="text" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="budget_total" class="col-sm-3 control-label"> Budget total<sup>*</sup></label>
					<div class="col-sm-9">
						<input name="budget_total" type="text" class="form-control">
					</div>
				</div>
				
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">Start Date<sup>*</sup></label>
					<div class="col-sm-9">
						<div class="input-group date">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input type="text" class="form-control pull-right datepicker" value="" name="start_date">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">End Date<sup>*</sup></label>
					<div class="col-sm-9">
						<div class="input-group date">
						  <div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						  </div>
						  <input type="text" class="form-control pull-right datepicker" value="" name="end_date">
						</div>
					</div>
				</div>
				
				<?php
					$workers = get_users_by_company_id($current_user_company_id, 8);
				?>
				
				<div class="form-group">
					<label for="start_date" class="col-sm-3 control-label">Workers</label>
					<div class="col-sm-9">
					
						<?php	
							echo '<div class="row ap_worker1">
									<div class="col-md-5">
										<select class="form-control select" style="width: 100%;" name="worker_id[]">';
										
							foreach($workers as $w_info){			
								echo '<option value="'.$w_info -> ID.'">'.$w_info -> display_name.'</option>';
							}
							
							echo '</select>
									</div>
									<div class="col-sm-5">
										<input name="worker_budget_total[]" type="text" class="form-control" placeholder="Budget Total">
									</div>
									<div class="col-sm-2 as">
										<span class="glyphicon glyphicon-remove" onclick="wc_sub(1)"></span>
									</div>
								</div>';
						?>
						<div class="workers"></div>
						<div class="row sp">
							<div class="col-md-12">
								<span class="btn btn-primary add-worker">Add Worker</span>
							</div>
						</div>
						<div class="worker-0-"></div>
					</div>
					
				</div>
			</div>
			<!-- /.box-body -->
		
	  </div>
	  <div class="modal-footer">
		<a class="btn bg-red" href="?page=projects-details&ID=<?php echo $id; ?>">Discard</a>
		<input type="submit" class="btn btn-primary" value="ADD" name="add_task">
	  </div>	  	  </form>
	</div>
	<!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
