<?php
	if($_POST['add_country']){
		if(!empty($_POST)){
			global $db;
			extract($_POST);
			$add_country = array(
								'country'		=>	$country,
								'country_code'	=>	$country_code,
							);
							
			$db->insert($db->countries, $add_country);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Country has been added sucessfully.');
		}
	}
	
	if($_POST['update_country']){
		if(!empty($_POST)){
			global $db;
			extract($_POST);
			$update_country = array(
								'country'		=>	$country,
								'country_code'	=>	$country_code,
							);
			$where = array('ID' => $con_id);	
			
			$db->update($db->countries, $update_country, $where);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Country has been updated sucessfully.');
		}
	}
	
	if($_POST['add_area']){
		if(!empty($_POST)){
			global $db;
			extract($_POST);
			$add_country = array(
								'area_name'		=>	$area_name,
								'country_id'	=>	$con_id,
							);
							
			$db->insert($db->country_areas, $add_country);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Country area has been added sucessfully.');
		}
	}
	
	if($_POST['update_area']){
		if(!empty($_POST)){
			global $db;
			extract($_POST);
			$update_area = array(
								'area_name'		=>	$area_name,
								'country_id'	=>	$con_id,
							);
			$where = array('ID' => $area_id);	
			
			$db->update($db->country_areas, $update_area, $where);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Country area has been updated sucessfully.');
		}
	}
?>


<section class="content">
	
	<?php 		
		if(isset($message)){	
			print_message($message);		
		}	
	?>
	
	<div class="row">

		<div class="col-md-6">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="add_country">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-map-marker"></i> Country</h3>

						<div class="pull-right">

							<input type="submit" class="btn btn-info" value="Save" name="add_country">

						</div>

					</div>
					
					<div class="box-body sb">

						<div class="form-group">

							<label class="col-sm-3 control-label">Country Name<sup>*</sup></label>

							<div class="col-sm-9">

								<input name="country" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-3 control-label">Country Code<sup>*</sup></label>

							<div class="col-sm-9">

								<input name="country_code" type="text" class="form-control">

							</div>

						</div>					

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		<div class="col-md-6">

			<!-- Horizontal Form -->

			<div class="box box-info">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" action="" method="post" id="add_area">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-map-marker"></i> Area</h3>

						<div class="pull-right">

							<input type="submit" class="btn btn-info" value="Save" name="add_area">

						</div>

					</div>
					
					<div class="box-body sb">

						<div class="form-group">

							<label for="first_name" class="col-sm-3 control-label">Area Name<sup>*</sup></label>

							<div class="col-sm-9">

								<input name="area_name" type="text" class="form-control">

							</div>

						</div>

						<div class="form-group">

							<label for="company" class="col-sm-3 control-label">Country Name <sup>*</sup></label>

							<div class="col-sm-9">
								
								<select class="form-control select2" style="width: 100%;" name="con_id">
								<?php
									$countries_info = get_countries();
									foreach($countries_info as $con_info){
										$select = '';
										if($con_info -> country_code == 'DE') $select = 'selected';
										echo '<option value='.$con_info -> ID.' '.$select.'>'.$con_info -> country.'</option>';
									}
								?>

								</select>

							</div>

						</div>					

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		<div class="col-xs-6">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-map-marker" aria-hidden="true"></i>

					<h3 class="box-title">Countries</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Country Name</th>

								<th>Country Code</th>

								<th>Actions</th>

							</tr>

						</thead>

						<tbody>

							<?php 
								global $db;
								
								$page = empty( $_REQUEST['set'] ) ? 1 : $_REQUEST['set'];
								$limit = get_option('posts_per_page');
								$offset = ($page - 1) * $limit;
								
								$country_info = $db -> get_results("SELECT *FROM {$db->countries} limit $offset, $limit ");
								$i=1;
								
								$total_countries = $db->get_results("SELECT count(ID) as count1 FROM {$db->countries}")[0] -> count1;

								foreach($country_info as $info){ 
							?>
								<tr class="per_con<?php echo $info ->ID; ?>">

									<td>
										<?php 
											if(isset($_REQUEST['set'] )){
												echo $i+($offset);
											}
											else echo $i; 
										?>
									</td>

									<td><a href=""></a><?php echo $info -> country; ?></td>

									<td><?php echo $info -> country_code; ?></td>

									<td>
										  <a data-toggle="modal"  aria-hidden="true" class="btn bg-orange btn-xs con<?php echo $info -> ID; ?>" data-country-name = "<?php echo $info -> country; ?>"  data-country-code = "<?php echo $info -> country_code; ?>" onclick="data_country(<?php echo $info -> ID; ?>)" ><span class="glyphicon glyphicon-pencil" ></span></a>

										<a title="Delete" class="btn btn-danger btn-xs" onclick="delete_country(<?php echo $info ->ID; ?>)"><span class="glyphicon glyphicon-trash"></span></a>

									</td>

								</tr>

								<?php
									$i++;
								} 
								?>

						</tbody>

					</table>
					<?php echo backend_pagination($total_countries, $limit, $page); ?>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

		

		<div class="col-xs-6">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-map-marker" aria-hidden="true"></i>

					<h3 class="box-title">Areas</h3>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Area Name</th>

								<th>Country Name</th>

								<th>Actions</th>

							</tr>

						</thead>

						<tbody>

							<?php 
								global $db;
								
								$page = empty( $_REQUEST['set_area'] ) ? 1 : $_REQUEST['set_area'];
								$limit = get_option('posts_per_page');
								$offset = ($page - 1) * $limit;
								
								$areas_info = $db -> get_results("SELECT *FROM {$db->country_areas} limit $offset, $limit ");
								$i=1;
								
								$total_areas = $db->get_results("SELECT count(ID) as count1 FROM {$db->country_areas}")[0] -> count1;
								
								$count = 1;
								foreach($areas_info as $areas){
							?>

								<tr class="per_area<?php echo $areas -> ID; ?>">

									<td>
										<?php 
											if(isset($_REQUEST['set_area'] )){
												echo $count+($offset);
											}
											else echo $count; 
										?>
									</td>

									<td><a href=""></a><?php echo $areas -> area_name; ?></td>

									<td>
									<?php 
										echo $con_name = get_country_name($areas -> country_id);
									?>
									</td>

									<td>

										<a title="Edit" aria-hidden="true" class="btn bg-orange btn-xs area<?php echo $areas -> ID; ?>" data-area-name = "<?php echo $areas -> area_name; ?>" data-country-id = "<?php echo $areas -> country_id; ?>" onclick="data_area(<?php echo $areas -> ID; ?>)"><span class="glyphicon glyphicon-pencil"></span></a>

										<a title="Delete" class="btn btn-danger btn-xs" onclick="delete_area(<?php echo $areas ->ID; ?>)"><span class="glyphicon glyphicon-trash"></span></a>

									</td>

								</tr>

							<?php 
								$count++;
							} 
							?>

						</tbody>
						

					</table>
					
					
						<?php echo pagination($total_areas, $limit, $page); ?>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>

  
 <div class="modal fade" id="modal-country">
  <div class="modal-dialog">
	<div class="modal-content">	
	<form class="form-horizontal" action="" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Country</h4>
	  </div>
	  <div class="modal-body">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Country Name</label>
					<div class="col-sm-9">
						<input name="country" type="text" class="form-control" value="" id="com_name">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"> Country Code</label>
					<div class="col-sm-9">
						<input name="country_code" type="text" class="form-control" value="" id="com_code">
					</div>
				</div>
				<input type="hidden" name="con_id" value="" id="con_id">
			</div>
			<!-- /.box-body -->
	  </div>
	  <div class="modal-footer">
		<a class="btn bg-red" href="?page=areas">Discard</a>
		<input type="submit" class="btn btn-primary" value="Update" name="update_country">
	  </div>	  	  
	  </form>
	</div>
	<!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



 
 <div class="modal fade" id="modal-area">
  <div class="modal-dialog">
	<div class="modal-content">	
	<form class="form-horizontal" action="" method="post">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Area</h4>
	  </div>
	  <div class="modal-body">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Area Name</label>
					<div class="col-sm-9">
						<input name="area_name" type="text" class="form-control" value="" id="area_name">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"> Country Name</label>
					<div class="col-sm-9">
						<select class="form-control select" style="width: 100%;" name="con_id" id="con_id">
							<?php
								$countries_info = get_countries();
								foreach($countries_info as $con_info){
									$select = '';
									if($con_info -> country_code == 'DE') $select = 'selected';
									echo '<option value='.$con_info -> ID.' '.$select.'>'.$con_info -> country.'</option>';
								}
							?>
						</select>
					</div>
				</div>
				<input type="hidden" name="area_id" value="" id="area_id">
			</div>
			<!-- /.box-body -->
	  </div>
	  <div class="modal-footer">
		<a class="btn bg-red" href="?page=areas">Discard</a>
		<input type="submit" class="btn btn-primary" value="Update" name="update_area">
	  </div>	  	  
	  </form>
	</div>
	<!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->