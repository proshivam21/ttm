<?php 
	$current_user_id = ttm_get_session('id');
	$current_user_company_id = get_userdata($current_user_id)-> company_id;
	
	if(isset($_POST) && !empty($_POST)){
		global $db;
		extract($_POST);
		if(isset($_FILES['logo'])){
			$image_id = upload_media('logo');
		}
		
		/*
		 * Create client company
		 */
		$companydata = array(
							'company_name'		 	=>  $company_name,
							'client_no'		 		=>  $client_no,
							'phone_no' 				=>  $phone_no,
							'email_address' 		=>  $email_address,
							'company_id'			=>  $current_user_company_id,
							'company_logo_id'		=>  $image_id,
							'company_address'		=>  $company_address,
							'status'				=>	$status,
							'comment'				=>	$comment,
							'created_at'    		=>  date('Y-m-d h:i:s')
						);
		
		$db->insert($db->company_clients, $companydata);
		
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your client company has been added sucessfully.');
	} 
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

				<!-- /.box-header -->

				<!-- form start -->
				<form class="form-horizontal" method= "post" action="" id="client_company" enctype="multipart/form-data">
					
					<div class="box box-info">

					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-building-o"><?php echo ' '.ucwords($_POST['company_name']); ?></i></h3>

						<div class="pull-right">
						
							<a class="btn bg-red" href="?page=client-companies">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info">Save</button>

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label class="col-sm-2 control-label">Company Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_name" type="text" class="form-control" value="<?php echo $_POST['company_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label  class="col-sm-2 control-label">Company Logo</label>

							<div class="col-sm-10">

								<div class="file-loading">

									<input class="file_uploader" name="logo" type="file" multiple>

								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Address<sup>*</sup></label>

							<div class="col-sm-10">

								<textarea name="company_address" class="form-control"><?php echo $_POST['company_address']; ?></textarea>

							</div>

						</div>

						<div class="form-group">

							<label  class="col-sm-2 control-label">Client No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="client_no" type="text" class="form-control" value="<?php echo $_POST['client_no']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="phone_number" class="col-sm-2 control-label">Phone No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="phone_no" type="tel" class="form-control" value="<?php echo $_POST['phone_no']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="email_address" class="col-sm-2 control-label">Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email_address" type="tel" class="form-control" value="<?php echo $_POST['email_address']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								<?php
									$status_info = array('0' => 'No', '1' => 'Yes');
									foreach($status_info as $k => $v){
										$select = '';
										if($_POST['status'] == $k || $k==1) $select = 'selected';
										echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';	
									}
								  ?>
								</select>

							
							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here...."><?php echo $_POST['comment']; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>