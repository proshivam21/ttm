<section class="content">

	<div class="row">

		<div class="col-xs-12">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-user-circle-o" aria-hidden="true"></i>

					<h3 class="box-title">Clients</h3>

					<a class="btn btn-info pull-right" href="?page=client-companies&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New Company</a>

				</div>

				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Company Name</th>

								<th>Address</th>

								<th>Client No.</th>

								<th width="140px">Phone No.</th>

								<th>Email Address</th>

								<th >Comment</th>

								<th>Active</th>

								<th width="100px">Actions</th>

							</tr>

						</thead>

						<tbody>

							<?php 
								$current_user_id = ttm_get_session('id');
								$current_user_company_id = get_userdata($current_user_id)-> company_id;
								global $db;
								
								$all_client_comp_info = get_client_company_info();
								
								$i=1;
								foreach($all_client_comp_info as $comp_info){
										
									echo '<tr class="per_company'.$comp_info->ID.'">

									<td>'.$i.'</td>

									<td>'.ucfirst($comp_info -> company_name).'</td>

									<td>'.$comp_info -> company_address.'</td>

									<td>'.$comp_info -> client_no.'</td>

									<td>'.$comp_info ->phone_no.'</td>

									<td>'.$comp_info ->email_address.'</td>

									<td>'.$comp_info ->comment.'</td>

									<td class="company-status">';
									
									$status = $comp_info -> status;
									if($status == 1) echo 'Active';									
									else echo 'Inactive';
									
									echo '</td>

									<td>
										<a title="Edit" class="btn bg-orange btn-xs" href="?page=client-companies&action=edit&id='.$comp_info ->ID.'"><span class="glyphicon glyphicon-pencil"></span></a>
										
										<a title="Delete" onclick="delete_client_company('.$comp_info ->ID.')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> ';
									
										if($status == 1){
											$action = 'deactivate_client_company';
											$title = "Inactive Client Company";
											$status_class = 'fa-undo';
										}
										else{ 
											$action = 'activate_client_company';
											$title = "Active Client Company";
											$status_class = 'fa-check';
										}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick="'.$action.'('.$comp_info ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
									
									echo '</td>

								</tr>';
								
								$i++;
								
								}
							
							?>

						</tbody>

					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>