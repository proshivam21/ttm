<?php 
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	
	if(isset($_POST) && !empty($_POST)){
		global $db;
		extract($_POST);
		
		extract($_FILES);
		/*
		 * Update client company
		 */
		$companydata = array(
							'company_name'		 	=>  $company_name,
							'client_no'		 		=>  $client_no,
							'phone_no' 				=>  $phone_no,
							'email_address' 		=>  $email_address,
							
							'company_address'		=>  $company_address,
							'status'				=>	$status,
							'comment'				=>	$comment,
							'created_at'    		=>  date('Y-m-d h:i:s')
						);
		
		if(isset($_FILES['logo']) && !empty($_FILES['logo']['name'])){
			$image_id = upload_media('logo');
			$companydata = array_merge($companydata, array('company_logo_id' => $image_id) );
		}
		
		$db->update($db->company_clients, $companydata, array('ID' => $id));
		
		$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your client company has been updated sucessfully.');
	}
	
	if(!empty($id)){
		$client_company_info = get_info_by_client_company($id);
	} 
		
?>


<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method= "post" action="" id="client_company"  enctype="multipart/form-data">
					
					<div class="box box-info">

					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-building-o"><?php echo ' '.ucwords($client_company_info -> company_name); ?></i></h3>

						<div class="pull-right">
						
							<a class="btn bg-red" href="?page=client-companies">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info">Update</button>

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label  class="col-sm-2 control-label">Company Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_name" type="text" class="form-control" value="<?php echo $client_company_info -> company_name; ?>">

							</div>

						</div>

						<div class="form-group">

							<label  class="col-sm-2 control-label">Company Logo</label>

							<div class="col-sm-10">

								<div class="file-loading">

									<input class="file_uploader" name="logo" type="file" multiple>
									<?php
										$media_id = $client_company_info -> company_logo_id;
										echo ttm_get_attachment_image($media_id, 'thumbnail');
									
									?>

								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Address<sup>*</sup></label>

							<div class="col-sm-10">

								<textarea name="company_address" class="form-control"><?php echo $client_company_info->company_address; ?></textarea>

							</div>

						</div>

						<div class="form-group">

							<label  class="col-sm-2 control-label">Client No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="client_no" type="text" class="form-control" value="<?php echo $client_company_info -> client_no; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="phone_number" class="col-sm-2 control-label">Phone No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="phone_no" type="tel" class="form-control" value="<?php echo $client_company_info->phone_no; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="email_address" class="col-sm-2 control-label">Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email_address" type="tel" class="form-control" value="<?php echo $client_company_info -> email_address; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">
								  <?php
									$status_info = array('0' => 'No', '1' => 'Yes');
									$old_status = $client_company_info -> status;
									foreach($status_info as $k => $v){
										$select = '';
									
										if($old_status == $k){ $select = 'selected';
											echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';
										}
										else echo '<option value="'.$k.'">'.$v.'</option>';
									}
								  
								  ?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here...."><?php echo $client_company_info -> comment; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

		

	</div>

	<!-- /.row -->

</section>