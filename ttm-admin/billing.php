<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<!-- The time line -->
			<ul class="timeline">
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					2017
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">January</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Invoice Number</th>
											<th>Company Name</th>
											<th>Invoice Date</th>
											<th>Invoice Total</th>
											<th>Due Date</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>INV5O526</td>
											<td>ICO Independent Consultants</td>
											<td>25.01.2017</td>
											<td>5220,00 &euro;</td>
											<td>31.01.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>INV5O527</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2017</td>
											<td>6500,00 &euro;</td>
											<td>01.02.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>INV5O528</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2017</td>
											<td>2600,00 &euro;</td>
											<td>01.02.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>4</td>
											<td>INV5O529</td>
											<td>ICO Independent Consultants</td>
											<td>28.01.2017</td>
											<td>8900,00 &euro;</td>
											<td>05.02.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">Febuary </h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Invoice Number</th>
											<th>Company Name</th>
											<th>Invoice Date</th>
											<th>Invoice Total</th>
											<th>Due Date</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>INV5O526</td>
											<td>ICO Independent Consultants</td>
											<td>25.02.2017</td>
											<td>5220,00 &euro;</td>
											<td>28.02.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>INV5O527</td>
											<td>ICO Independent Consultants</td>
											<td>26.02.2017</td>
											<td>6500,00 &euro;</td>
											<td>01.03.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>INV5O528</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2017</td>
											<td>2600,00 &euro;</td>
											<td>01.03.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>4</td>
											<td>INV5O529</td>
											<td>ICO Independent Consultants</td>
											<td>28.01.2017</td>
											<td>8900,00 &euro;</td>
											<td>05.03.2017</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				<!-- timeline time label -->
				<li class="time-label">
					<span class="bg-red">
					2016
					</span>
				</li>
				<!-- /.timeline-label -->
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">January</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Invoice Number</th>
											<th>Company Name</th>
											<th>Invoice Date</th>
											<th>Invoice Total</th>
											<th>Due Date</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>INV5O526</td>
											<td>ICO Independent Consultants</td>
											<td>25.01.2016</td>
											<td>5220,00 &euro;</td>
											<td>31.01.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>INV5O527</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2016</td>
											<td>6500,00 &euro;</td>
											<td>01.02.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>INV5O528</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2016</td>
											<td>2600,00 &euro;</td>
											<td>01.02.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>4</td>
											<td>INV5O529</td>
											<td>ICO Independent Consultants</td>
											<td>28.01.2016</td>
											<td>8900,00 &euro;</td>
											<td>05.02.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
				
				<!-- timeline item -->
				<li>
					<i class="fa fa-clock-o bg-blue"></i>
					<div class="timeline-item">
						<div class="box">
							<div class="box-header with-border">
								<i class="fa fa-calendar" aria-hidden="true"></i>
								<h3 class="box-title">Febuary </h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Invoice Number</th>
											<th>Company Name</th>
											<th>Invoice Date</th>
											<th>Invoice Total</th>
											<th>Due Date</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>INV5O526</td>
											<td>ICO Independent Consultants</td>
											<td>25.02.2016</td>
											<td>5220,00 &euro;</td>
											<td>28.02.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>INV5O527</td>
											<td>ICO Independent Consultants</td>
											<td>26.02.2016</td>
											<td>6500,00 &euro;</td>
											<td>01.03.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>INV5O528</td>
											<td>ICO Independent Consultants</td>
											<td>26.01.2016</td>
											<td>2600,00 &euro;</td>
											<td>01.03.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
										<tr>
											<td>4</td>
											<td>INV5O529</td>
											<td>ICO Independent Consultants</td>
											<td>28.01.2016</td>
											<td>8900,00 &euro;</td>
											<td>05.03.2016</td>
											<td>Sent</td>
											<td><a class="btn bg-green btn-xs" href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</li>
				<!-- END timeline item -->
			</ul>
		</div>
	</div>
	<!-- /.row -->
</section>

<div class="modal fade" id="modal-store">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Upload Signed Document</h4>
	  </div>
	  <div class="modal-body">
		<div class="file-loading">
			<input class="file_uploader" name="signed_pts" type="file" multiple>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-primary">Store</button>
	  </div>
	</div>
	<!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->