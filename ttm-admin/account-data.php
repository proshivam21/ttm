 <?php
	global $db;
	$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
	
	if($_POST){
		global $user;
		$user_id = get_users_by_company_id($current_user_company_id, 2)[0]->ID;
		$message = $user -> update_user($user_id);
		
		if($message['flag'] == 1){ 
			global $company;
			$message = $company -> update_company();
		}
	}
	
	$com_info = get_company_info_by_id($current_user_company_id);
?> 

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title"><i class="fa fa-building-o" aria-hidden="true"></i> <?php echo ucfirst($com_info -> company_name); ?></h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" name="" method="post" id="company_sign_up">
					<div class="box-body">
						<div class="form-group">
							<label for="company_name" class="col-sm-2 control-label">Company Name<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="company_name" type="text" class="form-control" value="<?php echo $com_info -> company_name; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="contact_person_name" class="col-sm-2 control-label">Contact First Name<sup>*</sup></label>
							<div class="col-sm-10">
								<input name="first_name" type="text" class="form-control" value="<?php echo $com_info -> contact_first_name; ?>">
							</div>
						</div>		
						
						<input type="hidden" name="account_name" value="<?php echo $com_info -> account_name; ?>">
						<input type="hidden" name="company_postal_address" value="<?php echo $com_info -> company_postal_address; ?>">
						<input type="hidden" name="company_tax_no" value="<?php echo $com_info -> company_tax_num; ?>">
						<input type="hidden" name="status" value="<?php echo $com_info -> status; ?>">
						<input type="hidden" name="comment" value="<?php echo $com_info -> comment; ?>">
						<input type="hidden" name="comp_id" value="<?php echo $current_user_company_id; ?>">
						<input type="hidden" name="role" value="2">
						
						
						<div class="form-group">							<label for="contact_person_name" class="col-sm-2 control-label">Contact Last Name</label>							<div class="col-sm-10">								<input name="last_name" type="text" class="form-control" value="<?php echo $com_info -> contact_last_name; ?>">							</div>						</div>
						
						<div class="form-group">
							<label for="contact_person_name" class="col-sm-2 control-label">Billing Address</label>
							<div class="col-sm-10">
								<textarea name="billing_address" class="form-control"><?php echo get_custom_meta($db->companymeta, $current_user_company_id, 'billing_address'); ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="billing_email" class="col-sm-2 control-label">Billing Email</label>
							<div class="col-sm-10">
								<input name="billing_email" type="email" class="form-control" value="<?php echo get_custom_meta($db->companymeta, $current_user_company_id, 'billing_email'); ?>">
							</div>
						</div>																		<div class="form-group">							<div class="col-sm-offset-2 col-sm-10">								<input type="submit" class="btn btn-info" name="update" value="Update" />							</div>						</div>
	
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
		
	</div>
	<!-- /.row -->
</section>