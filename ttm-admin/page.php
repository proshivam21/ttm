<?php 

	/* @packages TimTraMac
	 * @subpackage Page Template
	 */
	$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 'home';
?>

<?php include 'sidebar.php'; ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<?php include 'breadcrumb.php'; ?>
	
	<?php if(file_exists("$page.php")) include "$page.php"; else include '404.php'; ?>
</div>