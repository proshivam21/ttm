
/****
** Validation For Super Admin User Sign UP
**/

$("#super_admin_sub_user_signup").validate({
	rules: {
		first_name: "required",
		last_name: "required",
		email: "required",
		password: "required",
		dob: "required",
	},
	messages: {
		first_name: "Enter Your First Name",
		last_name: "Enter Your Last Name",
		email: "Enter Your Email Address",
		password: "Enter Your Password",
		dob: "Enter Your Date Of Birth",
	}
})

/****
** Validation For Company Sign UP
**/

$("#company_sign_up").validate({
	rules: {
		company_name: "required",
		account_name: "required",
		contact_first_name: "required",
		contact_last_name: "required",
		contact_email_address: "required",
		user_email: "required",
		password: "required",
		company_postal_address: "required",
		company_tax_no: "required",
	},
	messages: {
		company_name: "Enter Your Company Name",
		account_name: "Enter Your Account Name",
		contact_first_name: "Enter Contact First Name",
		contact_last_name: "Enter Contact Last Name",
		account_name: "Enter Your Account Name",
		contact_email_address: "Enter Contact Email Address",
		user_email: "Enter User Email Address",
		password: "Enter Your Password",
		company_postal_address: "Enter Company Postal Address",
		company_tax_no: "Enter Company Tax Number"
	}
})

/****
** Validation For Change Password
**/

$("#change_password").validate({
	rules: {
		old_password: "required",
		new_password: "required",
		confirm_password: {
			equalTo: "#new_password",
		}
	},
	messages: {
		old_password: "Enter Your Password",
		new_password: "Enter Your New Password",
		confirm_password: "New Password And Confirm Password Are Not Match",
	}
})



/****
** Validation For Add Company Register Price 
**/

$("#company_price").validate({
	rules: {
		company_name: "required",
		range: "required",
		price: "required",
		currency: "required",
	},
	messages: {
		company_name: "Select Company Name",
		range: "Select Range",
		price: "Enter Price",
		currency: "Select Currency",
	}
})


/****
** Validation For Add User 
**/

$("#add_user_signup").validate({
	rules: {
		first_name: "required",
		country_id: "required",
		email: "required",
		dob: "required",
		area: "required",
		
		role: "required",
		hr_id: "required",
		phone_number: "required",
		
	},
	messages: {
		first_name: "Enter Your First Name",
		country_id: "Select Country",
		email: "Enter Your Email Address",
		dob: "Enter Your Date Of Birth",
		area: "Select Area",
		role: "Select Role",
		phone_number: "Enter Phone number",
		hr_id: "Enter HR ID",
	}
})

/****
** Validation For Client Company
**/

$("#client_company").validate({
	rules: {
		company_name: "required",
		company_address: "required",
		email_address: "required",
		client_no: "required",
		phone_no: "required",
	},
	messages: {
		company_name: "Enter Company Name",
		company_address: "Enter Company Address",
		email_address: "Enter Email Address",
		client_no: "Enter Client No",
		phone_no: "Enter Contact No",
	}
})

/****
** Validation For Client Contact
**/

$("#client_contact").validate({
	rules: {
		first_name: "required",
		client_company_id: "required",
		email_address: "required",
		cost_center_id: "required",
		phone_number: "required",
	},
	messages: {
		first_name: "Enter First Name",
		client_company_id: "Select Client Company Name",
		email_address: "Enter Email Address",
		cost_center_id: "Enter Cost Center ID",
		phone_number: "Enter Contact No",
	}
})







/****
** Validation For country
**/

$("#add_country").validate({
	rules: {
		country: "required",
		country_code: "required",
	},
	messages: {
		country: "Enter Country Name",
		country_code: "Enter Country Code",
	}
})




/****
** Validation For area
**/

$("#add_area").validate({
	rules: {
		area_name: "required",
		con_id: "required",
	},
	messages: {
		area_name: "Enter Area Name",
		con_id: "Select Country",
	}
})


/****
** Validation For project
**/

$("#project1").validate({
	rules: {
		project_name: "required",
		client_int_order_no: "required",
		project_number: "required",
	},
	messages: {
		project_name: "Enter Project Name",
		client_int_order_no: "Enter client internal order number",
		project_number: "Enter project number",
	}
})

$("#task").validate({
	rules: {
		task_name: "required",
		budget_total: "required",
		start_date: "required",
		end_date: "required"
	},
	messages: {
		task_name: "Enter task name",
		budget_total: "Enter budget total",
		start_date: "Select start date",
		end_date: "Select end date",
	}
})

$("#holiday").validate({
	rules: {
		holiday_name: "required",
		holiday_date: "required",
	},
	messages: {
		holiday_name: "Enter holiday name",
		holiday_date: "Select holiday date",
	}
})