<?php 	if(isset($_POST) && !empty($_POST)){		global $db;		extract($_POST);				$check = get_user_by('login', $contact_email_addres);		if(!$check){			$full_name = $contact_first_name.' '.$contact_last_name;						/*			 * Create company			 */			global $db;						$companydata = array(							'company_name'		 	=>  $company_name,							'account_name'		 	=>  $account_name,							'contact_first_name' 	=>  $contact_first_name,							'contact_last_name'  	=>  $contact_last_name,							'contact_email_address' =>  $contact_email_address,							'company_postal_address'=>  $company_postal_address,							'company_tax_no'		=>	$company_tax_no,							'status'				=>	$status,							'comment'				=>	$comment,							'created_at'    		=>  date('Y-m-d h:i:s')						);									$company_id = $db ->insert('ttm_companies', $companydata);									$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your Company has been added succeessfully.');						/*			 * Create user			 */			 			/* $userdata = array(							'user_login' 		 =>  $email,							'user_pass'  		 =>  ttm_hash_password($password),							'user_email'  		 =>  $email,							'role_id'			 =>  7,							'user_status'		 =>  $status,							'user_registered'    =>  date('Y-m-d h:i:s'),							'display_name'       =>  ucwords($full_name),							'user_nicename'		 =>  ucwords($full_name)						);			$user_id = ttm_insert_user( $userdata ) ;						$meta = array(							'first_name' => $first_name,							'last_name'  => $last_name,							'role' 		 => $role,							'dob'  		 => $dob					);								foreach($meta as $key => $value){				update_user_meta($user_id, $key, $value);			} 			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Super admin user account has been added succeessfully.'); */		}		else $message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Email is already exist in our database. Please try another email.');	} ?><section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- Horizontal Form -->
			<div class="box box-info">
				<!-- /.box-header -->
				<!-- form start -->
				<form class="form-horizontal" method="post" action="">										<div class="box-header with-border">						<h3 class="box-title"><i class="fa fa-building-o"></i> ICO Independent Consultants</h3>						<div class="pull-right">							<button type="submit" class="btn bg-red">Discard</button>&nbsp;&nbsp;							<button type="submit" class="btn btn-info" name="save">Save</button>						</div>					</div>				
					<div class="box-body">
						<div class="form-group">
							<label for="company_name" class="col-sm-2 control-label">Company Name</label>
							<div class="col-sm-10">
								<input name="company_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="account_name" class="col-sm-2 control-label">Account Name</label>
							<div class="col-sm-10">
								<input name="account_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="company_logo" class="col-sm-2 control-label">Company Logo</label>
							<div class="col-sm-10">
								<div class="file-loading">
									<input class="file_uploader" name="company_logo" type="file" multiple>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="contact_first_name" class="col-sm-2 control-label">Contact First Name</label>
							<div class="col-sm-10">
								<input name="contact_first_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="contact_last_name" class="col-sm-2 control-label">Contact Last Name</label>
							<div class="col-sm-10">
								<input name="contact_last_name" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="contact_email_addres" class="col-sm-2 control-label">Contact Email Address</label>
							<div class="col-sm-10">
								<input name="contact_email_addres" type="email" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="company_postal_address" class="col-sm-2 control-label">Company Postal Address</label>
							<div class="col-sm-10">
								<textarea name="company_postal_address" class="form-control"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="company_tax_no" class="col-sm-2 control-label">Company Tax No.</label>
							<div class="col-sm-10">
								<input name="company_tax_no" type="text" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label for="status" class="col-sm-2 control-label">Active</label>
							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" name="status">
								  <option selected="selected">Yes</option>
								  <option>No</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>
							<div class="col-sm-10">
								<textarea name="comment" rows="10" class="form-control">Please Comment Here....</textarea>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form>
			</div>
			<!-- /.box -->
		</div>
		
	</div>
	<!-- /.row -->
</section>