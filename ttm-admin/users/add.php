<?php
	$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
	$users_info_by_role = get_users_by_company_id($current_user_company_id, 3);
	
	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> add_user();
	}

	global $db;
	$countries = get_countries();
	
?>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				
				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="add_user_signup">
				
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user"><?php echo ucwords($_POST['first_name'].' '.$_POST['last_name']); ?></i> </h3>

						<div class="pull-right">

							<a class="btn bg-red" href="?page=users">Discard</a>&nbsp;&nbsp;
							
							<input type="submit" class="btn btn-info" name="submit" value="Save">

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name <sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo $_POST['first_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name</label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo $_POST['last_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="dob" class="col-sm-2 control-label">DOB<sup>*</sup></label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" class="form-control pull-right datepicker" name="dob" value="<?php echo $_POST['dob']; ?>">

								</div>

							</div>

						</div>


						<div class="form-group">

							<label for="country" class="col-sm-2 control-label">Country<sup>*</sup></label>

							<div class="col-sm-10">

								<select class="form-control select28" style="width: 100%;" name="country_id" id= "country_id">
									<option value="">Select</option>
									<?php 
										foreach($countries as $con_info){
											$select = '';
											if($con_info->ID == $_POST['country_id']) $select = 'selected';
											echo '<option value="'.$con_info->ID.'" '.$select.'>'.$con_info->country.'</option>';
										}
									?>

								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="area" class="col-sm-2 control-label">Area<sup>*</sup></label>

							<div class="col-sm-10" id="state_ddl">

								<select class="form-control select2" style="width: 100%;" name="area">
									<option value="">Select</option>
									<?php 
										$states_info = get_state_by_country_id($_POST['country_id']);

										foreach($states_info as $info){
											$selected = '';
											if($info->ID == $_POST['area']) $selected = 'selected';
											echo '<option value="'.$info->ID.'" '.$selected.'>'.$info->area_name.'</option>';
										}
									?>
								</select>

							</div>

						</div>									

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">User Role<sup>*</sup></label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;"  name="role">
								<?php
									$all_user_roles = get_user_roles();
									$not_needed_role = array(1,2,7);
									
									foreach($all_user_roles as $val){
										$select = '';
										if($val->ID == $_POST['role']) $select = 'selected';
										if(!in_array($val -> ID, $not_needed_role)){
											echo '<option value="'.$val -> ID.'" '.$select.'>'.$val -> role_val.'</option>';
										}
									}
								  ?>
								  
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <?php
									$status_info = array('0' => 'No', '1' => 'Yes');
									foreach($status_info as $k => $v){
										$select = '';
										if(isset($_POST['status']) == $k || $k==1) $select = 'selected';
										echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';	
									}
								  ?>

								</select>

							</div>

						</div>

						

						<div class="form-group">

							<label for="hr_id" class="col-sm-2 control-label">HR ID<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="hr_id" type="text" class="form-control" value="<?php echo $_POST['hr_id']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="line_manager" class="col-sm-2 control-label">Line Manager</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="lm_id">

									<option value="">Select</option>
									<?php
										foreach($users_info_by_role as $role_info){
											$select = '';
											if($role_info->ID == $_POST['lm_id']) $select = 'selected';
											echo '<option value="'.$role_info -> ID.'" '.$select.'>'.ucwords($role_info -> display_name).'</option>';
										}
									?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="phone_number" class="col-sm-2 control-label">Phone No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="phone_number" type="tel" class="form-control" value="<?php echo $_POST['phone_number']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="email_address" class="col-sm-2 control-label">Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="email" type="email" class="form-control" value="<?php echo $_POST['email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="password" class="col-sm-2 control-label">Password</label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="">

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Type Here...."><?php echo $_POST['comment']; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>