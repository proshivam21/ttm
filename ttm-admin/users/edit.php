<?php
	$current_user_company_id = get_cur_user_company_id(ttm_get_session('id'));
	$users_info_by_role = get_users_by_company_id($current_user_company_id, 3);

	if(isset($_POST) && !empty($_POST)){
		global $user;
		$message = $user -> update_user();
	}
	
	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
	
	if(!empty($id)){
		$user_info = get_userdata($id);
	} 

	global $db;
	$countries = get_countries();
?>

<section class="content">
	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
	<div class="row">

		<div class="col-md-12">

			<!-- Horizontal Form -->

			<div class="box box-info">

				
				<!-- /.box-header -->

				<!-- form start -->

				<form class="form-horizontal" method="post" action="" id="add_user_signup">
				
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-user"><?php echo ' '.ucwords(get_user_meta($id, 'first_name', true).' '.get_user_meta($id, 'last_name', true));?></i> </h3>

						<div class="pull-right">

							
							<a class="btn bg-red" href="?page=users">Discard</a>&nbsp;&nbsp;
							<input type="submit" class="btn btn-info" name="submit" value="Save">

						</div>

					</div>

					<div class="box-body">

						<div class="form-group">

							<label for="first_name" class="col-sm-2 control-label">First Name <sup>*</sup></label>

							<div class="col-sm-10">

								<input name="first_name" type="text" class="form-control" value="<?php echo get_user_meta($id, 'first_name', true)?>" >

							</div>

						</div>

						<div class="form-group">

							<label for="last_name" class="col-sm-2 control-label">Last Name</label>

							<div class="col-sm-10">

								<input name="last_name" type="text" class="form-control" value="<?php echo get_user_meta($id, 'last_name', true)?>" >

							</div>

						</div>

						<div class="form-group">

							<label for="dob" class="col-sm-2 control-label">DOB<sup>*</sup></label>

							<div class="col-sm-10">

								<div class="input-group date">

								  <div class="input-group-addon">

									<i class="fa fa-calendar"></i>

								  </div>

								  <input type="text" class="form-control pull-right datepicker" value="<?php echo get_user_meta($id, 'dob', true); ?>" name="dob">

								</div>

							</div>

						</div>
						
						<input type="hidden" name="id" value="<?php echo $id; ?>">

						<div class="form-group">

							<label for="country" class="col-sm-2 control-label">Country<sup>*</sup></label>

							<div class="col-sm-10">

								<select class="form-control select28" style="width: 100%;" name="country_id" id= "country_id">

								  <?php 
										$old_country_id = get_user_meta($id, 'country_id', true);
										foreach($countries as $con_info){
											$selected = '';
											if($con_info->ID == $old_country_id) $selected = 'selected';
											echo '<option value="'.$con_info->ID.'" '.$selected.'>'.$con_info->country.'</option>';
										}
								  ?>

								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="area" class="col-sm-2 control-label">Area</label>
							
							<div class="col-sm-10" id="state_ddl">

								<select class="form-control select28" style="width: 100%;" name="state_living_in" id= "">

								<?php 
									$states_info = get_state_by_country_id($old_country_id);

									foreach($states_info as $info){
										$selected = '';
										if($info->ID == get_user_meta($id, 'area_id', true)) $selected = 'selected';
										echo '<option value="'.$info->ID.'" '.$selected.'>'.$info->area_name.'</option>';
									}
								?>

								</select>
							</div>

						</div>									

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">User Role<sup>*</sup></label>

							<div class="col-sm-10">
								<select class="form-control select2" style="width: 100%;" multiple="true" name="role">
								  <?php
									$all_user_roles = get_user_roles();
									$not_needed_role = array(1,2,7);
									
									foreach($all_user_roles as $val){
										if(!in_array($val -> ID, $not_needed_role)){
											$select = '';
											if($val -> ID == $user_info -> role_id) $select = 'selected';
											echo '<option value="'.$val -> ID.'" '.$select.'>'.$val -> role_val.'</option>';
										}
									}
								  ?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <?php
									$status_info = array('0' => 'No', '1' => 'Yes');
									$old_status = $user_info -> user_status;
									foreach($status_info as $k => $v){
										$select = '';
									
										if($old_status == $k){ $select = 'selected';
											echo '<option value="'.$k.'"  '.$select.'>'.$v.'</option>';
										}
										else echo '<option value="'.$k.'">'.$v.'</option>';
									}
								  
								  ?>

								</select>

							</div>

						</div>

						

						<div class="form-group">

							<label for="hr_id" class="col-sm-2 control-label">HR ID<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="hr_id" type="text" class="form-control" value="<?php echo get_user_meta($id, 'hr_id', true)?>" >

							</div>

						</div>

						<div class="form-group">

							<label for="line_manager" class="col-sm-2 control-label">Line Manager</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="lm_id">
									<?php
										foreach($users_info_by_role as $info){
											$select = '';
											if($info -> ID == $user_info->lm_id) $select = 'selected';
											
											if($user_info->role_id != 3) echo '<option value="'.$info -> ID.'" '.$select.'>'.ucwords($info -> display_name).'</option>';
										}
									?>
								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="phone_number" class="col-sm-2 control-label">Phone No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="phone_number" type="tel" class="form-control" value="<?php echo get_user_meta($id, 'phone_number', true)?>" >

							</div>

						</div>

						<div class="form-group">

							<label for="email_address" class="col-sm-2 control-label">Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input class="form-control"  value="<?php echo $user_info->user_email?>" disabled> 

							</div>

						</div>
						
						<div class="form-group">

							<label for="password" class="col-sm-2 control-label">Password</label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="">

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control"><?php echo get_user_meta($id, 'comment', true)?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>