<section class="content">

	<div class="row">

		<div class="col-xs-12">

			<!-- The time line -->

			<div class="box">

				<div class="box-header with-border">

					<i class="fa fa-users" aria-hidden="true"></i>

					<h3 class="box-title">Users</h3>

					<a class="btn btn-info pull-right" href="?page=users&action=add"><i class="fa fa-plus" aria-hidden="true"></i> New User</a>

				</div>
				<!-- /.box-header -->

				<div class="box-body table-responsive no-padding">

					<table class="table table-hover table-bordered table-striped">

						<thead>

							<tr>

								<th>ID</th>

								<th>Name</th>

								<th>User Email</th>

								<th>Company</th>

								<th>Role</th>

								<th>Status</th>

								<th>Actions</th>

							</tr>

						</thead>

						<tbody>
	
							<?php
								global $db;
								$user_info = $db -> get_results("SELECT *FROM {$db -> users} where role_id NOT IN (1,2,7)");
								
								$i=1;
								
								foreach($user_info as $users){
									
									$name = ucwords(get_user_meta($users -> ID, 'first_name', true).' '.get_user_meta($users -> ID, 'last_name', true) );
									
									echo '<tr class="per_user'.$users ->ID.'">

										<td>'.$i.'</td>

										<td>'.$name.'</td>

										<td>'.$users -> user_email.'</td>

										<td>'.ucwords(get_company_info_by_id($users -> company_id) -> company_name).'</td>

										<td>'.get_role_name_by_role_id($users ->role_id) -> role_val.'</td>

										<td>';
									
									$status = $users -> user_status;
									if($status == 1) echo 'Active';									
									else echo 'Inactive';
									
									echo '</td>

										<td>
											<a title="Edit" class="btn bg-orange btn-xs" href="?page=users&action=edit&id='.$users ->ID.'"><span class="glyphicon glyphicon-pencil"></span></a>
											
											<a title="Delete" onclick="delete_user('.$users ->ID.')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> ';
										
										if($status == 1){
											$action = 'deactivate_user';
											$title = "Inactive User";
											$status_class = 'fa-undo';
										}
										else{ 
											$action = 'activate_user';
											$title = "Active User";
											$status_class = 'fa-check';
										}
											
										echo '<span class="act-main"><a class="btn btn-xs bg-green act" title="'.$title.'" onclick="'.$action.'('.$users ->ID.')"><i class="fa '.$status_class.'"></i></a></span>';
										
									echo	'</td>
									</tr>';
									$i++;
								}
							?>
							
						</tbody>
					</table>

				</div>

				<!-- /.box-body -->

			</div>

			<!-- /.box -->

		</div>

	</div>

	<!-- /.row -->

</section>