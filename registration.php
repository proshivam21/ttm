<?php 
	include_once 'ttm-load.php';
	include_once 'ttm-includes/functions.php';
	include_once 'ttm-admin/functions.php';
	
	if(isset($_POST) && !empty($_POST)){
		global $db;
		extract($_POST);
		
		$check = get_user_by('login', $user_email);
		
		if(!$check){
			$full_name = $contact_first_name.' '.$contact_last_name;
		
			/*
			 * Create company
			 */
			global $db;	

			/*Upload media*/
			/* $pics = array('company_logo');
			
			foreach($pics as $pic) {
				$pictures[] = upload_media($pic);
			} */
			
			$companydata = array(
								'company_name'		 	=>  $company_name,
								'account_name'		 	=>  $account_name,
								'contact_first_name' 	=>  $contact_first_name,
								'contact_last_name'  	=>  $contact_last_name,
								'contact_email_address' =>  $contact_email_address,
								'company_postal_address'=>  $company_postal_address,
								'company_tax_num'		=>	$company_tax_no,
								'status'				=>	$status,
								'comment'				=>	$comment,
								'created_at'    		=>  date('Y-m-d h:i:s')
							);
			
			$db->insert($db->companies, $companydata);
			
			$company_id = $db -> insert_id;
			
			$company_meta = array(
								'billing_email' 	=> $billing_email,
								'billing_address'  	=> $billing_address
							);
					
			foreach($company_meta as $key => $value){
					$db->insert( $db->companymeta, array(
						'meta_key' 		=> $key,
						'meta_value' 	=> $value,
						'company_id'	=> $company_id
						),
						array('%s', '%s', '%s') 
					);
			}
		
			/*
			 * Create user
			 */
			 
			$userdata = array(
							'user_login' 		 =>  $contact_email_address,
							'user_pass'  		 =>  ttm_hash_password($password),
							'user_email'  		 =>  $user_email,
							'role_id'			 =>  2,
							'user_status'		 =>  $status,
							'user_registered'    =>  date('Y-m-d h:i:s'),
							'display_name'       =>  ucwords($full_name),
							'user_nicename'		 =>  ucwords($full_name),
							'company_id'		 =>  $company_id
						);
			$user_id = ttm_insert_user( $userdata ) ;
			
			$meta = array(
							'first_name' => $contact_first_name,
							'last_name'  => $contact_last_name
					);
					
			foreach($meta as $key => $value){
				update_user_meta($user_id, $key, $value);
			}
			//echo $db->last_error ;
			
			
			/***
			Send Email To Company Admin
			****/
		
			$admin_url  = get_site_url().'/ttm-admin';

			$message = $util -> get_mail_template('add_company_account', array('[NAME]' => ucwords($full_name), '[COMPANY_NAME]' => $company_name,'[USER_NAME]' => $contact_email_address, '[PASSWORD]' => $password, '[ADMIN_PANEL_URL]' => $admin_url) );

		
			// Sending email
			send_email($contact_email_address, 'Your System Admin User Account Created on TTM', 'info@airansoft.com', $message);
			
			
			/***
			Send Email To System Admin
			****/
		
			$admin_url  = get_site_url().'/ttm-admin';

			$message_to_system_admin = $util -> get_mail_template('new_company_registered', array('[NAME]' => ucwords($full_name), '[COMPANY_NAME]' => $company_name,'[USER_NAME]' => $contact_email_address, '[PASSWORD]' => $password, '[ADMIN_PANEL_URL]' => $admin_url) );

			// Sending email
			send_email('proshivam21@gmail.com', 'Company Account Created on TTM', 'info@airansoft.com', $message_to_system_admin);
			
			$message = array('title' => 'Success', 'type' => 'success', 'message'=>'Your Company has been added sucessfully.');
		}
		else $message = array('title' => 'Error', 'type' => 'danger', 'message'=>'Email is already exist in our database. Please try another email.');
	} 
?>

<style>
	.error {
	  color: red;
	}
	sup {
	  color: red;
	  font-size: 14px !important;
	}
	.logo-lg {
	  background: #581845 none repeat scroll 0 0;
	  border: 1px solid;
	  color: #fff;
	  font-size: 30px;
	}
	.container span {
	  padding: 17px;
	}
	.container center {
	  margin-top: 24px;
	}
</style>




<!DOCTYPE html>
<html lang="en">
<head>
  <title>Companies Accounts Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
  <center><span class="logo-lg"><b>Tim</b>Tra<b>Mac</b></span></center>

	<?php 
		if(isset($message)){
			print_message($message);
		}
	?>
  <form class="form-horizontal" method="post" action="" id="company_sign_up">
					
					<div class="box-header with-border">

						<h3 class="box-title"><i class="fa fa-building-o"></i> </h3>

						<div class="pull-right">

							<a class="btn bg-red" href="#">Discard</a>&nbsp;&nbsp;
							<button type="submit" class="btn btn-info" name="save">Save</button>

						</div>

					</div>
					
					<br><br><br>
					
					<div class="box-body">

						<div class="form-group">

							<label for="company_name" class="col-sm-2 control-label">Company Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_name" type="text" class="form-control" value="<?php echo $_POST['company_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="account_name" class="col-sm-2 control-label">Account Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="account_name" type="text" class="form-control" value="<?php echo $_POST['account_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="company_logo" class="col-sm-2 control-label">Company Logo</label>

							<div class="col-sm-10">

								<div class="file-loading">

									<input class="file_uploader" name="company_logo" type="file" multiple>

								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="contact_first_name" class="col-sm-2 control-label">Contact First Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="contact_first_name" type="text" class="form-control" value="<?php echo $_POST['contact_first_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="contact_last_name" class="col-sm-2 control-label">Contact Last Name<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="contact_last_name" type="text" class="form-control" value="<?php echo $_POST['contact_last_name']; ?>">

							</div>

						</div>

						<div class="form-group">

							<label for="contact_email_address" class="col-sm-2 control-label">Contact Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="contact_email_address" type="email" class="form-control" value="<?php echo $_POST['contact_email_address']; ?>">

							</div>

						</div>

						
						<div class="form-group">

							<label for="user_email_address" class="col-sm-2 control-label">User Email Address<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="user_email" type="email" class="form-control" value="<?php echo $_POST['user_email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="password" class="col-sm-2 control-label">Password<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="password" type="password" class="form-control" value="<?php echo $_POST['password']; ?>">

							</div>

						</div>
						
						
						<div class="form-group">

							<label for="company_postal_address" class="col-sm-2 control-label">Company Postal Address<sup>*</sup></label>

							<div class="col-sm-10">

								<textarea name="company_postal_address" class="form-control" ><?php echo $_POST['company_postal_address']; ?></textarea>

							</div>

						</div>

						<div class="form-group">

							<label for="company_tax_no" class="col-sm-2 control-label">Company Tax No.<sup>*</sup></label>

							<div class="col-sm-10">

								<input name="company_tax_no" type="text" class="form-control" value="<?php echo $_POST['company_tax_no']; ?>">

							</div>

						</div>
						
						
						<div class="form-group">

							<label for="billing_address" class="col-sm-2 control-label">Billing Address</label>

							<div class="col-sm-10">

								<input name="billing_address" type="text" class="form-control" value="<?php echo $_POST['billing_address']; ?>">

							</div>

						</div>
						
  						<div class="form-group">

							<label for="billing_email" class="col-sm-2 control-label">Billing Email</label>

							<div class="col-sm-10">

								<input name="billing_email" type="text" class="form-control" value="<?php echo $_POST['billing_email']; ?>">

							</div>

						</div>
						
						<div class="form-group">

							<label for="status" class="col-sm-2 control-label">Active</label>

							<div class="col-sm-10">

								<select class="form-control select2" style="width: 100%;" name="status">

								  <option value="1" selected="selected">Yes</option>

								  <option value="0">No</option>

								</select>

							</div>

						</div>

						<div class="form-group">

							<label for="inputPassword3" class="col-sm-2 control-label">Comment</label>

							<div class="col-sm-10">

								<textarea name="comment" rows="10" class="form-control" placeholder="Please Comment Here...."><?php echo $_POST['comment']; ?></textarea>

							</div>

						</div>

					</div>

					<!-- /.box-body -->

				</form>

</div>

</body>
</html>

<script src="ttm-admin/dist/js/jquery.validate.min.js"></script>
<script src="ttm-admin/dist/js/validation.js"></script>