<?php
/**
 * Front to the TTM application. This file doesn't do anything, but loads
 * ttm-blog-header.php which does and tells TTM to load the theme.
 *
 * @package TTM
 */

/**
 * Tells TTM to load the TTM theme and output it.
 *
 * @var bool
 */
define('TTM_USE_THEMES', true);

/** Loads the TTM Environment and Template */
require( dirname( __FILE__ ) . '/ttm-blog-header.php' );
