<?php
/**
 * Loads the TTM environment and template.
 *
 * @package TTM
 */

if ( !isset($ttm_did_header) ) {

	$ttm_did_header = true;

	require_once( dirname(__FILE__) . '/ttm-load.php' );

	//ttm();

	require_once( ABSPATH . TTMINC . '/template-loader.php' );

}
