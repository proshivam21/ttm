<?php
/**
 * Used to set up and fix common variables and include
 * the TTM procedural and class library.
 *
 * Allows for some configuration in ttm-config.php (see default-constants.php)
 *
 * @internal This file must be parsable by PHP4.
 *
 * @package TTM
 */

/**
 * Stores the location of the TTM directory of functions, classes, and core content.
 *
 * @since 1.0.0
 */
define( 'TTMINC', 'ttm-includes' );

// Include files required for class initialization.
require_once( ABSPATH . TTMINC .'/classes/information.class.php');

require( ABSPATH . TTMINC . '/theme.php' );
require( ABSPATH . TTMINC . '/general-template.php' );
require( ABSPATH . TTMINC . '/user.php' );
require( ABSPATH . TTMINC . '/conference.php' );
require( ABSPATH . TTMINC . '/meta.php' );
require( ABSPATH . TTMINC . '/formatting.php' );
require( ABSPATH . TTMINC . '/cache.php' );

require( ABSPATH . TTMINC . '/pluggable.php' );
require( ABSPATH . TTMINC . '/kses.php' );
require( ABSPATH . TTMINC . '/error.class.php' );
require( ABSPATH . TTMINC . '/session.php' );
require( ABSPATH . TTMINC . '/capabilities.php' );

//Includes media file. It will handle the all media functions
require( ABSPATH . TTMINC . '/media.php' );
require( ABSPATH . TTMINC . '/htmlToPDF.php' );

// Include files required for initialization.
require( ABSPATH . TTMINC . '/load.php' );
//require( ABSPATH . TTMINC . '/default-constants.php' );

/*
 * These can't be directly globalized in version.php. When updating,
 * we're including version.php from another install and don't want
 * these values to be overridden if already set.
 */
global $ttm_version, $ttm_db_version, $tinymce_version, $required_php_version, $required_mysql_version;
require( ABSPATH . TTMINC . '/version.php' );

// Set initial default constants including TTM_MEMORY_LIMIT, TTM_MAX_MEMORY_LIMIT, TTM_DEBUG, TTM_CONTENT_DIR and TTM_CACHE.
/* ttm_initial_constants(); */

// Check for the required PHP version and for the MySQL extension or a database drop-in.
/* ttm_check_php_mysql_versions(); */

// Disable magic quotes at runtime. Magic quotes are added using db later in ttm-settings.php.
@ini_set( 'magic_quotes_runtime', 0 );
@ini_set( 'magic_quotes_sybase',  0 );

// TTM calculates offsets from UTC.
date_default_timezone_set( 'UTC' );

// Turn register_globals off.
/* ttm_unregister_GLOBALS(); */

// Standardize $_SERVER variables across setups.
ttm_fix_server_vars();

// Check if we have received a request due to missing favicon.ico
ttm_favicon_request();

// Check if we're in maintenance mode.
ttm_maintenance();

// Start loading timer.
timer_start();

// Check if we're in TTM_DEBUG mode.
TTM_DEBUG_mode();

// For an advanced caching plugin to use. Uses a static drop-in because you would only want one.
if ( TTM_CACHE )
	TTM_DEBUG ? include( TTM_CONTENT_DIR . '/advanced-cache.php' ) : @include( TTM_CONTENT_DIR . '/advanced-cache.php' );

// Define ttm_LANG_DIR if not set.
ttm_set_lang_dir();

// Load early TTM files.
//require( ABSPATH . TTMINC . '/compat.php' );
require( ABSPATH . TTMINC . '/functions.php' );
/* require( ABSPATH . TTMINC . '/class-ttm.php' );
require( ABSPATH . TTMINC . '/class-ttm-error.php' ); */
require( ABSPATH . TTMINC . '/plugin.php' );
/* require( ABSPATH . TTMINC . '/pomo/mo.php' ); */

// Include the db class and, if present, a db.php database drop-in.
require_ttm_db();

// Set the database table prefix and the format specifiers for database table columns.
$GLOBALS['table_prefix'] = $table_prefix;
ttm_set_db_vars();

// Start the TTM object cache, or an external object cache if the drop-in is present.
ttm_start_object_cache();

// Attach the default filters.
//require( ABSPATH . TTMINC . '/default-filters.php' );

// Initialize multisite if enabled.
if ( is_multisite() ) {
	require( ABSPATH . TTMINC . '/ms-blogs.php' );
	require( ABSPATH . TTMINC . '/ms-settings.php' );
} elseif ( ! defined( 'MULTISITE' ) ) {
	define( 'MULTISITE', false );
}

register_shutdown_function( 'shutdown_action_hook' );

// Stop most of TTM from being loaded if we just want the basics.
if ( SHORTINIT )
	return false;

// Load the L10n library.
require_once( ABSPATH . TTMINC . '/l10n.php' );

// Run the installer if TTM is not installed.
ttm_not_installed();

// Load most of TTM.
require( ABSPATH . TTMINC . '/class-ttm-walker.php' );
require( ABSPATH . TTMINC . '/class-ttm-ajax-response.php' );
/* require( ABSPATH . TTMINC . '/formatting.php' ); */
/* require( ABSPATH . TTMINC . '/capabilities.php' ); */
require( ABSPATH . TTMINC . '/query.php' );
require( ABSPATH . TTMINC . '/date.php' );
/* require( ABSPATH . TTMINC . '/theme.php' ); */
require( ABSPATH . TTMINC . '/class-ttm-theme.php' );
require( ABSPATH . TTMINC . '/template.php' );
/* require( ABSPATH . TTMINC . '/user.php' ); */
/* require( ABSPATH . TTMINC . '/meta.php' ); */
require( ABSPATH . TTMINC . '/general-template.php' );
require( ABSPATH . TTMINC . '/link-template.php' );
require( ABSPATH . TTMINC . '/author-template.php' );
require( ABSPATH . TTMINC . '/post.php' );
require( ABSPATH . TTMINC . '/post-template.php' );
require( ABSPATH . TTMINC . '/revision.php' );
require( ABSPATH . TTMINC . '/post-formats.php' );
require( ABSPATH . TTMINC . '/post-thumbnail-template.php' );
require( ABSPATH . TTMINC . '/category.php' );
require( ABSPATH . TTMINC . '/category-template.php' );
require( ABSPATH . TTMINC . '/comment.php' );
require( ABSPATH . TTMINC . '/comment-template.php' );
require( ABSPATH . TTMINC . '/rewrite.php' );
require( ABSPATH . TTMINC . '/feed.php' );
require( ABSPATH . TTMINC . '/bookmark.php' );
require( ABSPATH . TTMINC . '/bookmark-template.php' );
/* require( ABSPATH . TTMINC . '/kses.php' ); */
require( ABSPATH . TTMINC . '/cron.php' );
require( ABSPATH . TTMINC . '/deprecated.php' );
require( ABSPATH . TTMINC . '/script-loader.php' );
require( ABSPATH . TTMINC . '/taxonomy.php' );
require( ABSPATH . TTMINC . '/update.php' );
require( ABSPATH . TTMINC . '/canonical.php' );
require( ABSPATH . TTMINC . '/shortcodes.php' );
require( ABSPATH . TTMINC . '/class-ttm-embed.php' );
/* require( ABSPATH . TTMINC . '/media.php' ); */
require( ABSPATH . TTMINC . '/http.php' );
require( ABSPATH . TTMINC . '/class-http.php' );
require( ABSPATH . TTMINC . '/widgets.php' );
require( ABSPATH . TTMINC . '/nav-menu.php' );
require( ABSPATH . TTMINC . '/nav-menu-template.php' );
require( ABSPATH . TTMINC . '/admin-bar.php' );

// Load multisite-specific files.
/* if ( is_multisite() ) {
	require( ABSPATH . TTMINC . '/ms-functions.php' );
	require( ABSPATH . TTMINC . '/ms-default-filters.php' );
	require( ABSPATH . TTMINC . '/ms-deprecated.php' );
} */

// Define constants that rely on the API to obtain the default value.
// Define must-use plugin directory constants, which may be overridden in the sunrise.php drop-in.
ttm_plugin_directory_constants();

$GLOBALS['ttm_plugin_paths'] = array();

// Load must-use plugins.
foreach ( ttm_get_mu_plugins() as $mu_plugin ) {
	include_once( $mu_plugin );
}
unset( $mu_plugin );

// Load network activated plugins.
if ( is_multisite() ) {
	foreach( ttm_get_active_network_plugins() as $network_plugin ) {
		ttm_register_plugin_realpath( $network_plugin );
		include_once( $network_plugin );
	}
	unset( $network_plugin );
}

/**
 * Fires once all must-use and network-activated plugins have loaded.
 *
 * @since 2.8.0
 */
do_action( 'muplugins_loaded' );

if ( is_multisite() )
	ms_cookie_constants(  );

// Define constants after multisite is loaded. Cookie-related constants may be overridden in ms_network_cookies().
ttm_cookie_constants();

// Define and enforce our SSL constants
ttm_ssl_constants();

// Create common globals.
require( ABSPATH . TTMINC . '/vars.php' );

// Make taxonomies and posts available to plugins and themes.
// @plugin authors: warning: these get registered again on the init hook.
create_initial_taxonomies();
create_initial_post_types();

// Register the default theme directory root
register_theme_directory( get_theme_root() );

// Load active plugins.
foreach ( ttm_get_active_and_valid_plugins() as $plugin ) {
	ttm_register_plugin_realpath( $plugin );
	include_once( $plugin );
}
unset( $plugin );

// Load pluggable functions.
/* require( ABSPATH . TTMINC . '/pluggable.php' ); */
require( ABSPATH . TTMINC . '/pluggable-deprecated.php' );

// Set internal encoding.
ttm_set_internal_encoding();

// Run TTM_CACHE_postload() if object cache is enabled and the function exists.
if ( TTM_CACHE && function_exists( 'TTM_CACHE_postload' ) )
	TTM_CACHE_postload();

/**
 * Fires once activated plugins have loaded.
 *
 * Pluggable functions are also available at this point in the loading order.
 *
 * @since 1.5.0
 */
do_action( 'plugins_loaded' );

// Define constants which affect functionality if not already defined.
ttm_functionality_constants();

// Add magic quotes and set up $_REQUEST ( $_GET + $_POST )
ttm_magic_quotes();

/**
 * Fires when comment cookies are sanitized.
 *
 * @since 2.0.11
 */
do_action( 'sanitize_comment_cookies' );

/**
 * TTM Query object
 * @global object $ttm_the_query
 * @since 2.0.0
 */
$GLOBALS['ttm_the_query'] = new ttm_Query();

/**
 * Holds the reference to @see $ttm_the_query
 * Use this global for TTM queries
 * @global object $ttm_query
 * @since 1.5.0
 */
$GLOBALS['ttm_query'] = $GLOBALS['ttm_the_query'];

/**
 * Holds the TTM Rewrite object for creating pretty URLs
 * @global object $ttm_rewrite
 * @since 1.5.0
 */
$GLOBALS['ttm_rewrite'] = new ttm_Rewrite();

/**
 * TTM Object
 * @global object $ttm
 * @since 2.0.0
 */
$GLOBALS['ttm'] = new ttm();

/**
 * TTM Widget Factory Object
 * @global object $ttm_widget_factory
 * @since 2.8.0
 */
$GLOBALS['ttm_widget_factory'] = new ttm_Widget_Factory();

/**
 * TTM User Roles
 * @global object $ttm_roles
 * @since 2.0.0
 */
$GLOBALS['ttm_roles'] = new ttm_Roles();

/**
 * Fires before the theme is loaded.
 *
 * @since 2.6.0
 */
do_action( 'setup_theme' );

// Define the template related constants.
ttm_templating_constants(  );

// Load the default text localization domain.
load_default_textdomain();

$locale = get_locale();
$locale_file = ttm_LANG_DIR . "/$locale.php";
if ( ( 0 === validate_file( $locale ) ) && is_readable( $locale_file ) )
	require( $locale_file );
unset( $locale_file );

// Pull in locale data after loading text domain.
require_once( ABSPATH . TTMINC . '/locale.php' );

/**
 * TTM Locale object for loading locale domain date and various strings.
 * @global object $ttm_locale
 * @since 2.1.0
 */
$GLOBALS['ttm_locale'] = new ttm_Locale();

// Load the functions for the active theme, for both parent and child theme if applicable.
if ( ! defined( 'ttm_INSTALLING' ) || 'ttm-activate.php' === $pagenow ) {
	if ( TEMPLATEPATH !== STYLESHEETPATH && file_exists( STYLESHEETPATH . '/functions.php' ) )
		include( STYLESHEETPATH . '/functions.php' );
	if ( file_exists( TEMPLATEPATH . '/functions.php' ) )
		include( TEMPLATEPATH . '/functions.php' );
}

/**
 * Fires after the theme is loaded.
 *
 * @since 3.0.0
 */
do_action( 'after_setup_theme' );

// Set up current user.
$GLOBALS['ttm']->init();

/**
 * Fires after TTM has finished loading but before any headers are sent.
 *
 * Most of ttm is loaded at this stage, and the user is authenticated. ttm continues
 * to load on the init hook that follows (e.g. widgets), and many plugins instantiate
 * themselves on it for all sorts of reasons (e.g. they need a user, a taxonomy, etc.).
 *
 * If you wish to plug an action once ttm is loaded, use the ttm_loaded hook below.
 *
 * @since 1.5.0
 */
do_action( 'init' );

// Check site status
if ( is_multisite() ) {
	if ( true !== ( $file = ms_site_check() ) ) {
		require( $file );
		die();
	}
	unset($file);
}

/**
 * This hook is fired once ttm, all plugins, and the theme are fully loaded and instantiated.
 *
 * AJAX requests should use ttm-admin/admin-ajax.php. admin-ajax.php can handle requests for
 * users not logged in.
 *
 * @link http://codex.TTM.org/AJAX_in_Plugins
 *
 * @since 3.0.0
 */
do_action( 'ttm_loaded' );