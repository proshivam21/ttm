<?php
/**
 * Bootstrap file for setting the ABSPATH constant
 * and loading the ttm-config.php file. The ttm-config.php
 * file will then load the ttm-settings.php file, which
 * will then set up the TTM environment.
 *
 * If the ttm-config.php file is not found then an error
 * will be displayed asking the visitor to set up the
 * ttm-config.php file.
 *
 * Will also search for ttm-config.php in TTM' parent
 * directory to allow the TTM directory to remain
 * untouched.
 *
 * @internal This file must be parsable by PHP4.
 *
 * @package TTM
 */

/** Define ABSPATH as this file's directory */
define( 'ABSPATH', dirname(__FILE__) . '/' );

error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

/*
 * If ttm-config.php exists in the TTM root, or if it exists in the root and ttm-settings.php
 * doesn't, load ttm-config.php. The secondary check for ttm-settings.php has the added benefit
 * of avoiding cases where the current directory is a nested installation, e.g. / is TTM(a)
 * and /blog/ is TTM(b).
 *
 * If neither set of conditions is true, initiate loading the setup process.
 */
if ( file_exists( ABSPATH . 'ttm-config.php') ) {

	/** The config file resides in ABSPATH */
	require_once( ABSPATH . 'ttm-config.php' );

} elseif ( file_exists( dirname(ABSPATH) . '/ttm-config.php' ) && ! file_exists( dirname(ABSPATH) . '/ttm-settings.php' ) ) {

	/** The config file resides one level above ABSPATH but is not part of another install */
	require_once( dirname(ABSPATH) . '/ttm-config.php' );

} else {

	// A config file doesn't exist

	define( 'TTMINC', 'ttm-includes' );
	require_once( ABSPATH . TTMINC . '/load.php' );

	// Standardize $_SERVER variables across setups.
	ttm_fix_server_vars();

	require_once( ABSPATH . TTMINC . '/functions.php' );

	$path = ttm_guess_url() . '/ttm-admin/setup-config.php';

	/*
	 * We're going to redirect to setup-config.php. While this shouldn't result
	 * in an infinite loop, that's a silly thing to assume, don't you think? If
	 * we're traveling in circles, our last-ditch effort is "Need more help?"
	 */
	/* if ( false === strpos( $_SERVER['REQUEST_URI'], 'setup-config' ) ) {
		header( 'Location: ' . $path );
		exit;
	} */

	define( 'TTM_CONTENT_DIR', ABSPATH . 'ttm-content' );
	require_once( ABSPATH . TTMINC . '/version.php' );

	ttm_check_php_mysql_versions();
	ttm_load_translations_early();

	// Die with an error message
	$die  = __( "There doesn't seem to be a <code>ttm-config.php</code> file. I need this before we can get started." ) . '</p>';
	$die .= '<p>' . __( "Need more help? <a href='#'>We got it</a>." ) . '</p>';
	$die .= '<p>' . __( "You can create a <code>ttm-config.php</code> file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file." ) . '</p>';
	$die .= '<p><a href="' . $path . '" class="button button-large">' . __( "Create a Configuration File" ) . '</a>';

	ttm_die( $die, __( 'TTM &rsaquo; Error' ) );
}

require_ttm_db();
/* 
global $user1;
$user1 = new userClass(); */
global $util;
$util = new util();
/* 
global $project;
$project = new project(); */